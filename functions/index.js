const admin = require('firebase-admin');
const functions = require('firebase-functions');
const ratelimiter = require('firebase-functions-rate-limiter');

admin.initializeApp();

const realtimeDb = admin.database();

const perUserLimiter = ratelimiter.FirebaseFunctionsRateLimiter.withRealtimeDbBackend(
    {
        name: 'rate-limit-phone-check',
        maxCalls: 2,
        periodSeconds: 15,
    },
    realtimeDb
);
const globalLimiter = ratelimiter.FirebaseFunctionsRateLimiter.withRealtimeDbBackend(
    {
        name: 'rate-limit-phone-check',
        maxCalls: 10,
        periodSeconds: 15,
    },
    realtimeDb
);

exports.isPhoneNumberRegistered = functions.https.onCall(async (data, context) => {
  // assert required params
  if (!data.phoneNumber) {
    throw new functions.https.HttpsError(
        'invalid-argument',
        'Value for "phoneNumber" is required.'
      );
  }

  // rate limiter
  const globalLimitExceeded = await globalLimiter.isQuotaExceededOrRecordUsage('global');
  if (globalLimitExceeded) {
    throw new functions.https.HttpsError(
        'resource-exhausted',
        'Call quota exceeded. Try again later',
      );
  }
  return admin.auth().getUserByPhoneNumber(data.phoneNumber).then(userRecord => {
    return userRecord.uid;
  }).catch(error => {
    return error;
  });
})
