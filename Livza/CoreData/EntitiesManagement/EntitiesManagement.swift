//
//  EntitiesManagement.swift
//  Randoo
//
//  Created by HTS-Product on 12/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import Foundation
import CoreData

protocol storeDelegate {
    func gotStoredInfo(type:String,msg:Messages?)
}

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let context = appDelegate.persistentContainer.viewContext

class Entities {
    static let sharedInstance = Entities()
    
    var delegate : storeDelegate?

    //add recent chat home
    func addRecent(name:String,user_id:String,time:String,msg:String,user_image:String,msg_id:String,msg_type:String,chat_type:String)  {
        let entity = NSEntityDescription.entity(forEntityName: "Recents", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        newUser.setValue(name, forKey: "user_name")
        newUser.setValue(user_id, forKey: "user_id")
        newUser.setValue(time, forKey: "time")
        let cryptLib = CryptLib()
        let decryptedMsg = cryptLib.decryptCipherTextRandomIV(withCipherText: msg, key: ENCRYPT_KEY)
        newUser.setValue(msg_id, forKey: "msg_id")
        newUser.setValue(decryptedMsg, forKey: "msg")
        newUser.setValue(msg_type, forKey: "msg_type")
        newUser.setValue("\(UserModel.shared.userId)\(user_id)", forKey: "chat_id")
        newUser.setValue(user_image, forKey: "user_image")
        newUser.setValue(false, forKey: "isBlocked")
        newUser.setValue(false, forKey: "isRead")
        newUser.setValue(chat_type, forKey: "chat_type")
        do {
            try context.save()
        } catch {
            print("FAILED TO SAVE RECENT")
        }
    }
    
    //updated recent chat home
    func updateRecent(name:String,user_id:String,time:String,msg:String,user_image:String,msg_id:String,msg_type:String)  {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        request.predicate = NSPredicate(format: "chat_id = %@", "\(UserModel.shared.userId)\(user_id)")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            let recents =  result[0] as! Recents
            let cryptLib = CryptLib()
            let decryptedMsg = cryptLib.decryptCipherTextRandomIV(withCipherText: msg, key: ENCRYPT_KEY)
            recents.setValue(decryptedMsg, forKey: "msg")
            recents.setValue(msg_type, forKey: "msg_type")
            recents.setValue(time, forKey: "time")
            recents.setValue(user_image, forKey: "user_image")
            recents.setValue(name, forKey: "user_name")
            recents.setValue(msg_id, forKey: "msg_id")
            recents.setValue(false, forKey: "isRead")
        } catch {
            print("Failed")
        }
        do {
            try context.save()
        } catch {
            print("FAILED TO UPDATE RECENT")
        }
    }
    //updated read status
    func read(user_id:String)  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        request.predicate = NSPredicate(format: "chat_id = %@", "\(UserModel.shared.userId)\(user_id)")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.count != 0 {
                let recents =  result[0] as! Recents
                recents.setValue(true, forKey: "isRead")
                try context.save()
            }
        } catch {
            print("Failed")
        }
        
    }
    //get chat
    func info(user_id:String)-> Recents?  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        request.predicate = NSPredicate(format: "chat_id = %@", "\(UserModel.shared.userId)\(user_id)")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.count != 0 {
                let recents =  result[0] as! Recents
                return recents
            }else{
                return nil
            }
        } catch {
            print("Failed")
        }
        return nil
    }
    
    //updated block status
    func block(user_id:String,status:Bool)  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        request.predicate = NSPredicate(format: "chat_id = %@", "\(UserModel.shared.userId)\(user_id)")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.count != 0 {
                let recents =  result[0] as! Recents
                recents.setValue(status, forKey: "isBlocked")
                try context.save()
            }
        } catch {
            print("Failed")
        }
       
    }
    //add msg
    func addMsg(name:String,user_id:String,time:String,msg:String,user_image:String,msg_id:String,msg_type:String,status:String,receiver_id:String)  {
        let entity = NSEntityDescription.entity(forEntityName: "Messages", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        newUser.setValue(name, forKey: "user_name")
        newUser.setValue(user_id, forKey: "user_id")
        newUser.setValue(time, forKey: "time")
        newUser.setValue(msg_id, forKey: "msg_id")
        let cryptLib = CryptLib()
        let decryptedMsg = cryptLib.decryptCipherTextRandomIV(withCipherText: msg, key: ENCRYPT_KEY)
        newUser.setValue(decryptedMsg, forKey: "msg")
        newUser.setValue(msg_type, forKey: "msg_type")
        if user_id == UserModel.shared.userId {
            newUser.setValue("\(UserModel.shared.userId)\(receiver_id)", forKey: "chat_id")
        }else{
            newUser.setValue("\(UserModel.shared.userId)\(user_id)", forKey: "chat_id")
        }
        newUser.setValue(user_image, forKey: "user_image")
        newUser.setValue(receiver_id, forKey: "receiver_id")

        do {
            try context.save()
            self.getMsg(msg_id: msg_id)
        } catch {
            print("FAILED TO SAVE MESSAGE")
        }
    }
    
    //update online
    func updateOnline(user_id:String,status:Bool) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        request.predicate = NSPredicate(format: "user_id = %@", user_id)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.count != 0 {
                let recent =  result[0] as! Recents
                recent.setValue(status, forKey: "isOnline")
                try context.save()
            }
        } catch {
            print("Failed")
        }
    }
    //update online
    func updateTyping(user_id:String,status:Bool) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        request.predicate = NSPredicate(format: "user_id = %@", user_id)
        request.returnsObjectsAsFaults = false
        do {

            let result = try context.fetch(request)
            if result.count != 0 {
                let recent =  result[0] as! Recents
                recent.setValue(status, forKey: "isTyping")
                try context.save()
            }
        } catch {
            print("Failed")
        }
    }
    //stop all typing status initially
    func stopTyping()  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        let sort = NSSortDescriptor(key: #keyPath(Recents.time), ascending: false)
        request.sortDescriptors = [sort]
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.count != 0 {
            for chat in result{
                let recent = chat as! Recents
                recent.setValue(false, forKey: "isTyping")
            }
            }

        } catch {
            print("Failed")
        }
    }
    
    //update read msg
    func updateRead(msg_id:String) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Messages")
        request.predicate = NSPredicate(format: "msg_id = %@", msg_id)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.count != 0 {
                let msg =  result[0] as! Messages
                msg.setValue(true, forKey: "read_status")
                try context.save()
            }
        } catch {
            print("Failed")
        }
    }
    
    //updated block status
    func updateDownload(msg_id:String,imgData:Data) -> Messages? {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Messages")
        request.predicate = NSPredicate(format: "msg_id = %@", msg_id)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            let msg =  result[0] as! Messages
            msg.setValue(imgData, forKey: "attachment")
            msg.setValue(true, forKey: "isDownload")
            try context.save()
            return msg
        } catch {
            print("Failed")
        }
       return nil
    }
    
    //get msg
    func getMsg(msg_id:String)  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Messages")
        request.predicate = NSPredicate(format: "msg_id = %@", "\(msg_id)")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            self.delegate?.gotStoredInfo(type: "msg",msg:(result[0] as! Messages))
            print(result[0])
        } catch {
            print("Failed")
        }
    }
    //get msg
    func getAllMsg(chat_id:String,offset:Int)->NSMutableArray  {
        let msgList = NSMutableArray()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Messages")
        request.predicate = NSPredicate(format: "chat_id = %@", "\(chat_id)")
        request.fetchLimit = 20
        request.fetchOffset = offset
        let sort = NSSortDescriptor(key: #keyPath(Messages.time), ascending: false)
        request.sortDescriptors = [sort]
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            msgList.addObjects(from: result.reversed())
            return msgList
        } catch {
            print("Failed")
        }
        return msgList
    }
    
    
    //get all recent chats from list
    func getChats()->NSMutableArray  {
        let chats = NSMutableArray()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        let chat_sort = NSSortDescriptor(key: #keyPath(Recents.chat_type), ascending: true)
        
        let time_sort = NSSortDescriptor(key: #keyPath(Recents.time), ascending: false)
        request.sortDescriptors = [chat_sort,time_sort]
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            chats.addObjects(from: result)
            return chats
        } catch {
            print("Failed")
        }
        return chats
    }
    //get ids for online list
    func getIds()->NSMutableArray  {
        let userIDs = NSMutableArray()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        let sort = NSSortDescriptor(key: #keyPath(Recents.time), ascending: false)
        request.sortDescriptors = [sort]
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for recent in result{
                let model = recent as! Recents
                if model.user_id != APP_NAME{
                userIDs.add(model.user_id as Any)
                }
            }
            return userIDs
        } catch {
            print("Failed")
        }
        return userIDs
    }
    
    //check if user is exist
    func userExist(user_id:String)->Bool  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        request.predicate = NSPredicate(format: "user_id = %@", "\(user_id)")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if(result.count == 0){
                print("new user")
                return false
            }else{
                print("existing user")
                return true
            }
        } catch {
            
            print("Failed")
        }
        return false
    }
    
    
    //get all recent chats from list
    func getLastMsgID(user_id:String,type:String)->String  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Messages")
        let firstCondition = NSPredicate(format: "chat_id = %@", "\(UserModel.shared.userId)\(user_id)")
        var secondCondition = NSPredicate()
        if type == "instant" {
             secondCondition = NSPredicate(format: "user_id = %@", "\(UserModel.shared.userId)")
        }else{
             secondCondition = NSPredicate(format: "read_status = \(true)")
        }
        let joinPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [firstCondition, secondCondition])
        request.predicate = joinPredicate
        let sort = NSSortDescriptor(key: #keyPath(Messages.time), ascending: false)
        request.sortDescriptors = [sort]
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.count == 0{
                return ""
            }else{
            let msg = result[0] as! Messages
            print("last msg \(msg.msg!)")
            self.updateRead(msg_id: msg.msg_id!)
            return msg.msg_id!
            }
        } catch {
            print("Failed")
        }
        return ""
    }
    //get last msg
    func getLast(user_id:String)->Messages?  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Messages")
        request.predicate = NSPredicate(format: "chat_id = %@", "\(UserModel.shared.userId)\(user_id)")
        let sort = NSSortDescriptor(key: #keyPath(Messages.time), ascending: false)
        request.sortDescriptors = [sort]
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.count == 0{
                return nil
            }else{
                return (result[0] as! Messages)
            }
        } catch {
            print("Failed")
        }
        return nil
    }
    
    //delete all records
    func deleteAllRecords() {
        
        let recentFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        let recentRequest = NSBatchDeleteRequest(fetchRequest: recentFetch)
        let msgFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Messages")
        let msgRequest = NSBatchDeleteRequest(fetchRequest: msgFetch)
        do {
            try context.execute(recentRequest)
            try context.execute(msgRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    //clear chat
    func clear(chat_id:String)    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Messages")
        request.predicate = NSPredicate(format: "chat_id = %@", "\(chat_id)")
        let clearRequest = NSBatchDeleteRequest(fetchRequest: request)
        do
        {
            try context.execute(clearRequest)
            try context.save()
        }
        catch _ {
            print("Could not delete")
        }
    }
    //updated msg clear
    func emptyRecent(user_id:String)  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Recents")
        request.predicate = NSPredicate(format: "user_id = %@", user_id)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            let recent =  result[0] as! Recents
            recent.setValue("", forKey: "msg")
            try context.save()
        } catch {
            print("Failed")
        }
    }
            
}
