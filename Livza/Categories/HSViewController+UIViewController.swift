//
//  HSViewController+UIViewController.swift
//  HSTaxiUserApp
//
//  Created by APPLE on 15/03/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import Foundation

extension UIViewController{
  
    //show status alert based local json 
    func statusAlert(msg:String)  {
        CRNotifications.showNotification(textColor: .white, backgroundColor: PRIMARY_COLOR, image: UIImage.init(named: "brodcastInfo_icon"), title: APP_NAME, message: Utility.shared.appLanguage.value(forKey: msg) as! String, dismissDelay: 2)

//        BPStatusBarAlert()
//            .message(message: Utility.language.value(forKey: msg) as! String)
//            .show()
    }
    //from server
    func statusServer(alert:String)  {
//        BPStatusBarAlert()
//            .message(message: alert)
//            .show()
        CRNotifications.showNotification(textColor: .white, backgroundColor: PRIMARY_COLOR, image: UIImage.init(named: "brodcastInfo_icon"), title: APP_NAME, message: alert, dismissDelay: 2)

    }
    //show alert
    func showAlert(msg:String)  {
        let otherAlert = UIAlertController(title: nil, message: Utility.language.value(forKey: msg) as? String, preferredStyle: UIAlertController.Style.alert)
        let dismiss = UIAlertAction(title: Utility.language.value(forKey: "ok") as? String, style: UIAlertAction.Style.cancel, handler: nil)
        otherAlert.addAction(dismiss)
        present(otherAlert, animated: true, completion: nil)
    }
    
    func showAlert(error: Error)  {
        let otherAlert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let dismiss = UIAlertAction(title: Utility.language.value(forKey: "ok") as? String, style: UIAlertAction.Style.cancel, handler: nil)
        otherAlert.addAction(dismiss)
        present(otherAlert, animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String?) {
        let otherAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let dismiss = UIAlertAction(title: Utility.language.value(forKey: "ok") as? String, style: UIAlertAction.Style.cancel, handler: nil)
        otherAlert.addAction(dismiss)
        present(otherAlert, animated: true, completion: nil)
    }
    
    //MARK:location restriction alert
    func cameraPermissionAlert()  {
        let otherAlert = UIAlertController(title: nil, message: Utility.language.value(forKey: "camera_permission") as? String, preferredStyle: UIAlertController.Style.alert)
        let dismiss = UIAlertAction(title: Utility.language.value(forKey: "cancel") as? String, style: UIAlertAction.Style.cancel, handler: nil)
        otherAlert.addAction(dismiss)
        
        let setting = UIAlertAction(title: Utility.language.value(forKey: "setting") as? String, style: UIAlertAction.Style.default, handler:goSetting)
        otherAlert.addAction(setting)
        
        //ANIL
        if let delegate = UIApplication.shared.delegate as? AppDelegate
        { delegate.window?.rootViewController?.present(otherAlert, animated: true, completion: nil) }
        
//        UIApplication.shared.keyWindow?.rootViewController!.present(otherAlert, animated: true, completion: nil)
    }
    //MARK:location restriction alert
    func microPhonePermissionAlert()  {
        let otherAlert = UIAlertController(title: nil, message: Utility.language.value(forKey: "microphone_permission") as? String, preferredStyle: UIAlertController.Style.alert)
        let dismiss = UIAlertAction(title: Utility.language.value(forKey: "cancel") as? String, style: UIAlertAction.Style.cancel, handler: nil)
        otherAlert.addAction(dismiss)
        
        let setting = UIAlertAction(title: Utility.language.value(forKey: "setting") as? String, style: UIAlertAction.Style.default, handler:goSetting)
        otherAlert.addAction(setting)
        
        //ANIL
        if let delegate = UIApplication.shared.delegate as? AppDelegate
        { delegate.window?.rootViewController?.present(otherAlert, animated: true, completion: nil) }
        
//        UIApplication.shared.keyWindow?.rootViewController!.present(otherAlert, animated: true, completion: nil)
    }
    
    func goSetting(alert: UIAlertAction){
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    func centerPoint()->UIButton {
        let transitionBtn = UIButton()
        transitionBtn.frame = CGRect.init(x: FULL_WIDTH/2, y: FULL_HEIGHT/2, width: 0.1, height: 0.1);
        transitionBtn.backgroundColor = PRIMARY_COLOR
        self.view.addSubview(transitionBtn)
        return transitionBtn
    }
    //check if socket is chat socket or random socket
    func isChat(socket:String) -> Bool {
        let chatSocket = "\(WEB_SOCKET_CHAT_URL)\(UserModel.shared.userId)"
        if socket == chatSocket {
            return true
        }
        return false
    }
    
}
