//
//  HSView+UIView.swift
//  HSTaxiUserApp
//
//  Created by APPLE on 14/03/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import Foundation
import Lottie

extension UIView{
    //MARK: shadow effect
    func elevationEffect() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.layer.shadowRadius = 2;
        self.layer.shadowOpacity = 0.5;
    }
    //:MARK round corner radius
    func cornerViewRadius() {
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
    }
    //MARK: minimum corner radius
    func cornerViewMiniumRadius() {
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
    func crapView(radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    //MARK: remove corner radius
    func removeCornerRadius() {
        self.layer.cornerRadius = 0.0
        self.layer.masksToBounds = false
    }
    //card effect
    func applyCardEffect(){
        self.layer.masksToBounds = false
//        let shadowSize : CGFloat = 3.0
//        let shadowPath = UIBezierPath(rect: CGRect(x: 0,
//                                                   y: 0,
//                                                   width: self.frame.size.width + shadowSize,
//                                                   height: self.frame.size.height ))
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = CGFloat(2)
        self.layer.shadowOpacity = 0.24
//        self.layer.shadowPath = shadowPath.cgPath

    }
    
    //MARK:Specific corner radius
    func specificCornerRadius()  {
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [ .topRight , .topLeft], cornerRadii: CGSize(width: 15, height: 15)).cgPath
        self.layer.mask = rectShape
    }
    //MARK:Specific corner left
    func cornerLeft()  {
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [ .topRight , .bottomRight], cornerRadii: CGSize(width: 15, height: 15)).cgPath
        self.layer.mask = rectShape
    }
    
    //MARK: Set border
    func setViewBorder(color:UIColor) {
        self.layer.borderWidth = 2
        self.layer.borderColor = color.cgColor
    }
    func border(color:UIColor) {
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
    }
    
    func addLOT(lot_name:String,w:CGFloat,h:CGFloat)  {
        var lottieView = AnimationView()
        lottieView = AnimationView(name: lot_name)
        lottieView.frame = CGRect.init(x: 0, y: 0, width: w, height: h)
        lottieView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        lottieView.loopMode = .loop
        lottieView.contentMode = .scaleAspectFill
        self.clearsContextBeforeDrawing = true
        self.addSubview(lottieView)
        lottieView.play{ (finished) in
            // Do Something
        }
    }
    func addFitLOT(lot_name:String,w:CGFloat,h:CGFloat)  {
        var lottieView = AnimationView()
        lottieView = AnimationView(name: lot_name)
        lottieView.frame = CGRect.init(x: 0, y: 0, width: w, height: h)
        lottieView.loopMode = .loop
        lottieView.contentMode = .scaleAspectFit
        self.clearsContextBeforeDrawing = true
        self.addSubview(lottieView)
        lottieView.play{ (finished) in
            // Do Something
        }
    }
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
        // Using a function since `var image` might conflict with an existing variable
    func takeScreenshot() -> UIImage {
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
    func toast(alert:String)  {
        self.hideAllToasts()
//        self.clearToastQueue()
        self.makeToast(Utility.language.value(forKey: alert) as? String)
    }
    
    func rotate(angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat.pi
        let rotation = CGAffineTransform().rotated(by: radians)
        self.transform = rotation
    }
}


