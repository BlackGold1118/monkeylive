//
//  UIScrollViewExtensions.swift
//  Livza
//
//  Created by Serge Vysotsky on 16.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import UIKit

extension UIScrollView {
    func scrollToBottom(_ animated: Bool) {
        let origin = CGPoint(x: 0, y: contentSize.height)
        let lastVisibleRect = CGRect(origin: origin, size: CGSize(width: 1, height: 1))
        scrollRectToVisible(lastVisibleRect, animated: animated)
    }
}
