//
//  ReusableObject.swift
//  Livza
//
//  Created by Serge Vysotsky on 13.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import UIKit

protocol ReusableObject where Self: NSObjectProtocol {
    var objectReuseIdentifier: String { get }
    static var objectReuseIdentifier: String { get }
}

extension ReusableObject {
    var objectReuseIdentifier: String { Self.objectReuseIdentifier }
    static var objectReuseIdentifier: String {
        NSStringFromClass(self).components(separatedBy: .punctuationCharacters).last!
    }
}

extension ReusableObject where Self: UICollectionViewCell {
    static func dequeue(from collectionView: UICollectionView, for indexPath: IndexPath) -> Self {
        collectionView.dequeueReusableCell(withReuseIdentifier: objectReuseIdentifier, for: indexPath) as! Self
    }
}

enum Storyboards: String {
    case subscriptions = "Subscriptions"
}

extension ReusableObject where Self: UIViewController {
    static func instantiate(from storyboard: Storyboards) -> Self {
        UIStoryboard(name: storyboard.rawValue, bundle: .main).instantiateViewController(withIdentifier: objectReuseIdentifier) as! Self
    }
}
