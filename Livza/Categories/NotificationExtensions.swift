//
//  NotificationExtensions.swift
//  Livza
//
//  Created by Serge Vysotsky on 17.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let primeUpdated = Notification.Name("primeUpdated notification")
}
