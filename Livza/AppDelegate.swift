//
//  AppDelegate.swift
//  Randoo
//
//  Created by HTS-Product on 28/01/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import WebRTC
import CoreData
import FBSDKLoginKit
import IQKeyboardManagerSwift
import UserNotifications
import PushKit
import CallKit
import SwiftyJSON
import Firebase
import FirebaseDynamicLinks
import FirebaseUI
import GTMSessionFetcher
import FirebaseMessaging
import FirebaseRemoteConfig
import Amplitude
import GoogleMobileAds

var isSocketConnected = false
var launchURL :URL? = nil
var isWatchingLive = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,PKPushRegistryDelegate,SRWebSocketDelegate,CXProviderDelegate,MessagingDelegate, GADInterstitialDelegate {
    
    var window: UIWindow?
    let callNotificationCenter = UNUserNotificationCenter.current()
    var callTimer = Timer()
    var callController = CXCallController()
    var provider = CXProvider(configuration: CXProviderConfiguration(localizedName: APP_NAME))
    var callDict = NSDictionary()
    var callStatus : String!
    var baseUUId = UUID()
    var callKitPopup = false
    var callRequest = false
    var isLive = false
    var isCurrentChattingUser = String()
    var isNotify = true
    var isLaunched = false
    
    var interstitial: GADInterstitial!
    
    var remoteConfig: RemoteConfig!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        print("did finish")
        self.isLaunched = true
        self.initialSetup()
        
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        let requeest = GADRequest()
        interstitial.delegate = self
        interstitial.load(requeest)
        
        Amplitude.instance()?.initializeApiKey("848d8655314b3495d7c1c6fb66f55f2b")
        if !UserModel.shared.userId.isEmpty {
            print(UserModel.shared.userId)
            Apphud.startManually(apiKey: APPHUB_SDK_TOKEN, userID: UserModel.shared.userId, deviceID: UserModel.shared.userId, observerMode: false)
        }
        
        //config firebase
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        voipRegistry.desiredPushTypes = Set([PKPushType.voIP])
        voipRegistry.delegate = self;
        
        TextViewAround.executeWorkaround()
        
        // Use Firebase library to configure APIs.
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        GTMSessionFetcher.setLoggingEnabled(true)
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = IOS_BUNDLE_ID
        // Initialize the Google Mobile Ads SDK.
        //        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        //config facebook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        SubscriptionsViewController.requestProductsForCache()
        
        
        remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        
        remoteConfig.fetchAndActivate(completionHandler: nil)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if !isSocketConnected{
            //        isSocketConnected = false
            self.connectSockets()
        }
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        Utility.shared.setDefaultFilter()
        RTCShutdownInternalTracer()
        RTCCleanupSSL()
        self.saveContext()
        
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if Auth.auth().canHandle(url) {
            return true
        }
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            if dynamicLink.url != nil{
                let code = dynamicLink.url!.absoluteString.replacingOccurrences(of: "\(SITE_URL)/appstore?referal_code=", with: "")
                UserModel.shared.inviteCode = code
            }
            return true
        }else{
            
            let handled: Bool = ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
            // Add any custom logic here.
            return handled
        }
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            let code = dynamiclink!.url!.absoluteString.replacingOccurrences(of: "\(SITE_URL)/appstore?referal_code=", with: "")
            UserModel.shared.inviteCode = code
        }
        
        return handled
    }
    
    
    @available(iOS 9.0, *)
    private func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        return application(app, open: url,
                           sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                           annotation: "")
    }
    
    //intial setup
    func initialSetup()  {
        self.isCurrentChattingUser = ""
        UserModel.shared.alreadyInCall = false
        
        Utility.shared.configLanguage()
        Utility.shared.getDefaultDetails()
        //initial view
        
        self.setInitialViewController()
        //webrtc
        self.configWebRTC()
        //app permission
        self.appPermissions()
        //keyboard
        IQKeyboardManager.shared.enable = true
        
        if !UserModel.shared.userId.isEmpty {
            Utility.shared.checkBlocked()
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserModel.shared.FCM = fcmToken
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("FIRBASE PUSH KIT TOKEN \(deviceToken)")
        Messaging.messaging().apnsToken = deviceToken
        Auth.auth().setAPNSToken(deviceToken, type: .prod)
        
    }
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        print("pushRegistry didInvalidatePushTokenFor \(type)")
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        let deviceTokenString = pushCredentials.token.reduce("", {$0 + String(format: "%02X", $1)})
        print("PUSH KIT TOKEN \(deviceTokenString) AND \(pushCredentials.token)")
        UserModel.shared.VOIP = deviceTokenString
        Utility.shared.pushsignIn()
        
    }
    //MARK: VOIP PUSH NOTIFICATION ACTION
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        print("PUSHKIT NOTIFICATION \(payload.dictionaryPayload)")
        
        let msgDict:NSDictionary = payload.dictionaryPayload["notification_data"] as! NSDictionary
        let scope = msgDict.value(forKey: "scope") as! String
        if scope == "videocall" {
            if !UserModel().alreadyInCall {
                UserModel.shared.alreadyInCall = true
                callDict = msgDict
                let call_type = callDict.value(forKey: "call_type") as! String
                if call_type == "created"{
                    callKitPopup = true
                    baseUUId = UUID()
                    provider.setDelegate(self, queue: nil)
                    let update = CXCallUpdate()
                    update.remoteHandle = CXHandle(type: .generic, value: callDict.value(forKey: "user_name") as! String)
                    update.hasVideo = true
                    provider.reportNewIncomingCall(with: baseUUId, update: update, completion: { error in })
                }
            }
        }else if scope == "ended"{
            interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
            let requeest = GADRequest()
            interstitial.delegate = self
            interstitial.load(requeest)
            self.window?.rootViewController?.view.makeToast("aaaaaaa")
            let user_id =  msgDict.value(forKey: "user_id") as! String
            if UserModel().previousCallerId == user_id || UserModel().previousCallerId.isEmpty {
                isNotify = false
                print("END CALL")
                
                let endCallAction = CXEndCallAction(call:baseUUId)
                let transaction = CXTransaction(action: endCallAction)
                callController.request(transaction) { error in
                    if let error = error {
                        print("EndCallAction transaction request failed: \(error.localizedDescription).")
                        self.provider.reportCall(with: self.baseUUId, endedAt: Date(), reason: .remoteEnded)
                        return
                    }
                    print("EndCallAction transaction request successful")
                }
                callKitPopup = false
                
                UserModel.shared.alreadyInCall = false
                if IS_LIVZA {
                    if !isWatchingLive{
                        self.window?.rootViewController!.dismiss(animated: true, completion: nil)
                    }
                }else{
                    self.window?.rootViewController!.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK:FCM PUSH NOTIFICATION ACTION
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("FCM MESSAGE \(userInfo)")
        if Auth.auth().canHandleNotification(userInfo) {
            completionHandler(.noData)
            return
        }
        
        let msgDict = self.convertToDictionary(text: userInfo["notification_data"] as? String ?? "")
        guard let scope = msgDict?.value(forKey: "scope") as? String else { return }
        if scope == "admin"{
            Utility.shared.getAdminMsg()
            let body = msgDict!.value(forKey: "message") as! String
            if APP_NAME != self.isCurrentChattingUser {
                self.triggerPush(body:body, title:"\(APP_NAME) Team",type:scope,dict: msgDict!)
            }
        }else if scope == "txtchat"{
            let body = msgDict!.value(forKey: "message") as! String
            let title = msgDict!.value(forKey: "user_name") as! String
            let msg_type = msgDict!.value(forKey: "msg_type") as! String
            let cryptLib = CryptLib()
            var decryptedMsg = cryptLib.decryptCipherTextRandomIV(withCipherText: body, key: ENCRYPT_KEY)
            if msg_type == "image"{
                decryptedMsg = "Image"
            }
            if msg_type != "missed"{
                let user_id = msgDict!.value(forKey: "user_id") as! String
                if user_id != self.isCurrentChattingUser {
                    self.triggerPush(body:decryptedMsg! , title: title,type:scope, dict: msgDict!)
                }
            }
        }else if scope == "follow"{
            let body = msgDict!.value(forKey: "message") as! String
            self.triggerPush(body:body, title:"",type:scope,dict: msgDict!)
        }
        
        
        
        
        // This notification is not auth related, developer should handle it.
    }
    //trigger local push notification
    func triggerPush(body:String,title:String,type:String,dict:NSDictionary) {
        
        /*   let content = UNMutableNotificationContent()
         content.body = body
         if type != "follow"{
         content.title = title
         }
         content.userInfo = dict as! [AnyHashable : Any]
         content.sound = .default
         let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.1, repeats: false)
         let request = UNNotificationRequest.init(identifier: "call", content: content, trigger: trigger)
         UNUserNotificationCenter.current().delegate = self
         UNUserNotificationCenter.current().add(request) { (error) in
         }*/
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
                completionHandler(.alert)
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("user notification")
        UserDefaults.standard.setValue("true", forKey: "user_tapped_notification")
        let msgDict = self.convertToDictionary(text: response.notification.request.content.userInfo["notification_data"] as? String ?? "")
        
        let scope = msgDict?.value(forKey: "scope") as! String
        
        if !isLive{
            if scope == "txtchat"{
                let user_id = msgDict?.value(forKey: "user_id") as! String
                let user_name = msgDict?.value(forKey: "user_name") as! String
                let user_image = msgDict?.value(forKey: "user_image") as! String
                Utility.shared.setMsg(scope: scope, name: user_name, id: user_id, img: user_image)
            }
            else if scope == "admin"{
                Utility.shared.setMsg(scope: scope, name: "\(APP_NAME) \(Utility.language.value(forKey: "team") as! String)" , id: APP_NAME, img: "")
            }else if scope == "follow"{
                let user_id = msgDict?.value(forKey: "user_id") as! String
                Utility.shared.setFollow(scope: scope, id: user_id)
            }
        }
        
        if UIApplication.shared.applicationState == .active{
            self.redirectToView()
            
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.redirectToView()
            }
        }        
    }
    func redirectToView()  {
        //delay based on app state
        if isSocketConnected{
            self.setInitialViewController()
        }else{
            self.redirectToView()
        }
    }
    
    //check user logged in or out
    func setInitialViewController()  {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let viewController = self.checkUserProfileStatus()
        let root = UINavigationController(rootViewController: viewController)
        root.navigationBar.isTranslucent = false
        window?.rootViewController = root
    }
    func moveLoginPage()  {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let viewController = LoginPage()
        let root = UINavigationController(rootViewController: viewController)
        root.navigationBar.isTranslucent = false
        window?.rootViewController = root
    }
    func checkUserProfileStatus()->UIViewController {
        if UserModel.shared.profileComplete {
            return TabBarPage()
        }else if UserModel.shared.isFirstLogin {
            return TabBarPage()
        }else{
            return LoginPage()
        }
    }
    //initialise webrtc
    func configWebRTC()  {
        let fieldTrials: [AnyHashable : Any] = [:]
        RTCInitFieldTrialDictionary(fieldTrials as? [String : String])
        RTCInitializeSSL()
        RTCSetupInternalTracer()
        #if NDEBUG
        // In debug builds the default level is LS_INFO and in non-debug builds it is
        // disabled. Continue to log to console in non-debug builds, but only
        // warnings and errors.
        RTCSetMinDebugLogLevel(RTCLoggingSeverityWarning)
        #endif
    }
    
    func appPermissions()  {
        //camera
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
        }
        //micro
        AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
            
        })
        //Photo album
        PhotoAlbum.init()
    }
    
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        let requeest = GADRequest()
        interstitial.delegate = self
        interstitial.load(requeest)
    }
    
    
    //CALL KIT
    func connectToCall()  {
        if UIApplication.shared.applicationState == .active{
        }else{
            self.callRequest = true
            self.connectSockets()
        }
    }
    //open video call page
    func openCallPage() {
        if callRequest{
            let dict = NSMutableDictionary.init(dictionary: callDict)
            dict.setValue("video_call", forKey: "scope")
            Utility.shared.setCall(dict: dict)
            self.setInitialViewController()
            callRequest = false
        }
    }
    
    //MARK:call kit actions
    func providerDidReset(_ provider: CXProvider) {
    }
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction){
        
        let dict = NSMutableDictionary.init(dictionary: callDict)
        
        if isSocketConnected{
            let videoObj = VideoCallPage()
            
            videoObj.modalPresentationStyle = .fullScreen
            videoObj.user_name = dict.value(forKey: "user_name") as! String
            videoObj.user_img = dict.value(forKey: "user_image") as! String
            videoObj.sender = false
            videoObj.viewFrom = "callkit"
            videoObj.platform =  dict.value(forKey: "platform") as! String
            videoObj.room_id =  dict.value(forKey: "room_id") as! String
            videoObj.user_id =  dict.value(forKey: "user_id") as! String
            if IS_LIVZA {
                if !isWatchingLive{
                    self.window?.rootViewController!.present(videoObj, animated: true, completion: nil)
                }
            }else{
                self.window?.rootViewController!.present(videoObj, animated: true, completion: nil)
            }
        }else{
            self.connectToCall()
        }
        action.fulfill()
    }
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        print("CALL KIT CALL")
        callKitPopup = false
        UserModel.shared.alreadyInCall = false
        
        if isNotify{
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                HSChatSocket.sharedInstance.manageCall(receiverID: self.callDict.value(forKey: "user_id") as! String, room: self.callDict.value(forKey: "room_id") as! String, type: "ended")
                
                self.window?.rootViewController!.dismiss(animated: true, completion: nil)
            }
        }
        
        action.fulfill()
    }
    
    //end call kit
    func endCall(){
        
        if interstitial.isReady {
            print("END CALL")
            interstitial.present(fromRootViewController: window!.rootViewController!)
            self.window?.rootViewController?.view.makeToast("9999999")
        } else {
            print("not load.")
            self.window?.rootViewController?.view.makeToast("123456789")
        }
        
        isNotify = false
        print("END CALL")
        let endCallAction = CXEndCallAction(call:baseUUId)
        let transaction = CXTransaction(action: endCallAction)
        callController.request(transaction) { error in
            if let error = error {
                print("EndCallAction transaction request failed: \(error.localizedDescription).")
                self.provider.reportCall(with: self.baseUUId, endedAt: Date(), reason: .remoteEnded)
                return
            }
            print("EndCallAction transaction request successful")
        }
        callKitPopup = false
    }
    
    //MARK: SOCKET LISTENERS
    func connectSockets()  {
        if  !UserModel.shared.userId.isEmpty{
            HSWebSocket.sharedInstance.connect()
            HSChatSocket.sharedInstance.connect()
            HSChatSocket.chatSocket.delegate = self
            HSWebSocket.webSocket.delegate = self
        }
    }
    
    func webSocketDidOpen(_ webSocket: SRWebSocket!) {
        print("SOCKET CONNECTED \(webSocket.url.absoluteString)")
        isSocketConnected = true
        if self.isChat(socket: webSocket.url.absoluteString) {
            HSChatSocket.sharedInstance.makeAlive()
        }else{
            HSWebSocket.sharedInstance.update(status: ONLINE)
        }
        if callRequest{
            self.openCallPage()
        }
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didFailWithError error: Error!) {
        print("SOCKET NOT CONNECTED \(error.localizedDescription) URL \(webSocket.url.absoluteString)")
    }
    func webSocket(_ webSocket: SRWebSocket!, didReceiveMessage message: Any!) {
        print("GOT MSG \(String(describing: message))")
        let jsonString = message as! String
        let data = jsonString.data(using: .utf8)!
        do {
            let  msg = try JSON(data: data)
            let type = msg["type"].stringValue
            if type == "_callReceived" {
                let call_type = msg["call_type"].stringValue
                if call_type == "ended"{
                    let user_id = msg["user_id"].stringValue
                    if UserModel().previousCallerId == user_id || UserModel().previousCallerId.isEmpty{
                        appDelegate.endCall()
                        UserModel.shared.alreadyInCall = false
                        if IS_LIVZA {
                            if !isWatchingLive{
                                self.window?.rootViewController!.dismiss(animated: true, completion: nil)
                            }
                        }else{
                            self.window?.rootViewController!.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }else if type == "_callRejected" {
                HSChatSocket.sharedInstance.passDetails(type: type, dict:msg.dictionaryObject! as NSDictionary )
            }
            else if type == "_receiveChat" {
                Utility.shared.addToLocal(dict: msg.dictionaryObject! as NSDictionary)
            }else if type == "_offlineChat"{
                Utility.shared.addOfflineMsg(dict:  msg.dictionaryObject! as NSDictionary)
            }else if type == "_receiveReadStatus"{
                Utility.shared.updateReadStatus(dict:msg.dictionaryObject! as NSDictionary)
            }else if type == "_offlineReadStatus"{
                Utility.shared.addOfflineReadStatus(dict:msg.dictionaryObject! as NSDictionary)
            }else if type == "_onlineListStatus"{
                HSChatSocket.sharedInstance.passDetails(type: type, dict:msg.dictionaryObject! as NSDictionary)
            }else{
                HSChatSocket.sharedInstance.passDetails(type: type, dict:msg.dictionaryObject! as NSDictionary)
            }
            
        } catch  {
            
        }
    }
    //check if socket is chat socket or random socket
    func isChat(socket:String) -> Bool
    {
        if !UserModel.shared.userId.isEmpty
        {
            let chatSocket = "\(WEB_SOCKET_CHAT_URL)\(UserModel.shared.userId)"
            if socket == chatSocket
            { return true }
            else
            { return false }
        }
        else
        { return false }
    }
    
    func convertToDictionary(text: String) -> NSDictionary?
    {
        if let data = text.data(using: .utf8)
        {
            do
            { return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary }
            catch
            { print(error.localizedDescription) }
        }
        return nil
    }
    
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "db")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //                let nserror = error as NSError
                //                fatalError("Unresolved error \(nserror), \(nserror.userInfo)") //ANIL
            }
        }
    }
}

