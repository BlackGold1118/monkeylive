//
//  GradientView.swift
//  Livza
//
//  Created by Serge Vysotsky on 13.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: DesignableView {
    @IBInspectable var firstColor: UIColor?
    @IBInspectable var secondColor: UIColor?
    @IBInspectable var startPoint: CGPoint = .zero
    @IBInspectable var endPoint: CGPoint = .zero
    
    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }
    override class var layerClass: AnyClass { CAGradientLayer.self }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupGradient()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupGradient()
    }
    
    func setupGradient() {
        guard let first = firstColor, let second = secondColor else { return }
        gradientLayer.colors = [first.cgColor, second.cgColor]
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
    }
}
