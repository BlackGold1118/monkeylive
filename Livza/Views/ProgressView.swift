//
//  ProgressView.swift
//  Livza
//
//  Created by Serge Vysotsky on 14.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import UIKit

final class ProgressView: UIView {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .white)
        indicator.startAnimating()
        addSubview(indicator)
        return indicator
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        activityIndicator.center = center
    }
}

extension UIView {
    @discardableResult
    func showProgressView() -> ProgressView {
        let progressView = ProgressView(frame: bounds)
        addSubview(progressView)
        return progressView
    }
    
    @discardableResult
    func removeProgressView() -> ProgressView? {
        let progressView = subviews.first(where: { $0 is ProgressView }) as? ProgressView
        progressView?.removeFromSuperview()
        return progressView
    }
}
