//
//  GradientBorderView.swift
//  Livza
//
//  Created by Serge Vysotsky on 13.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import UIKit

@IBDesignable
final class GradientBorderView: GradientView {
    @IBInspectable var gradientBorderWidth: CGFloat = 0
    private let borderLayer = CAShapeLayer()
    
    override func setupGradient() {
        super.setupGradient()
        borderLayer.frame = bounds
        borderLayer.cornerRadius = gradientLayer.cornerRadius
        borderLayer.borderWidth = gradientBorderWidth
        gradientLayer.mask = borderLayer
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        borderLayer.frame = bounds
    }
}
