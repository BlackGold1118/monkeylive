//
//  WebPage.swift
//  Randoo
//
//  Created by HTS-Product on 14/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import WebKit

class WebPage: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet var webview: WKWebView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationView: UIView!
    
    @IBOutlet weak var loader: UIActivityIndicatorView!

    var resultDict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialSetup()
    }

    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.changeToRTL()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
            self.backBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.backBtn.frame = CGRect.init(x: FULL_WIDTH-50, y: 25, width: 40, height: 40)
        }
        else{
            self.backBtn.frame = CGRect.init(x: 10, y: 25, width: 40, height: 40)
        }
    }
    
    //MARK: Configure web view
    func initialSetup() {
        self.fd_prefersNavigationBarHidden = true
        self.webview.navigationDelegate = self
        loader.startAnimating()
        loader.color = PRIMARY_COLOR
        self.titleLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text: "")
        self.titleLbl.text = resultDict.value(forKey: "help_title") as? String
        loader.startAnimating()
        if resultDict.value(forKey: "help_descrip") != nil{
        let contentString:String = (resultDict.value(forKey: "help_descrip") as? String)!
        let htmlString:String = "<font face=\(APP_FONT_LIGHT) size='6'>\(contentString)"
        self.webview.loadHTMLString(htmlString, baseURL: nil)
        }
    }
    
   //MARK: WebKit navigation delegate
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loader.stopAnimating()
    }
 
}
