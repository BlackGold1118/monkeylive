//
//  ReportPage.swift
//  Randoo
//
//  Created by HTS-Product on 22/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import ContentSheet

class ReportPage: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var reportTableView: UITableView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var bgView: UIView!
    
    let reportArray = NSMutableArray()
    var partner_id = String()
    var getReport: ((_ id:String) ->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.bgView.backgroundColor = CIRCLE_BG_COLOR
        self.reportTableView.backgroundColor = .clear
        reportTableView.register(UINib(nibName: "ReportCell", bundle: nil), forCellReuseIdentifier: "ReportCell")
        self.reportArray.addObjects(from: UserModel.shared.getDefaults()?.value(forKey: "reports") as! [Any])
        print(self.reportArray.count)
        self.reportTableView.reloadData()
    }
    override func viewDidLayoutSubviews() {
        self.bgView.specificCornerRadius()
        
    }
    
    @objc override open func collapsedHeight(containedIn contentSheet: ContentSheet) -> CGFloat {
        return UIScreen.main.bounds.height*0.4
    }
    
    @objc override open func expandedHeight(containedIn contentSheet: ContentSheet) -> CGFloat {
        return UIScreen.main.bounds.height*0.8
    }
    
    override func scrollViewToObserve(containedIn contentSheet: ContentSheet) -> UIScrollView? {
        return reportTableView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reportArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
        let reportDict = reportArray.object(at: indexPath.row) as! NSDictionary
        cell.reportNameLbl?.text = "\(reportDict.value(forKey: "title") as! String)"
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = .clear
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.config(color: .white, font: mediumReg, align: .center, text: "report_user")
        headerView.addSubview(label)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let reportDict = reportArray.object(at: indexPath.row) as! NSDictionary
        self.reportService(title: "\(reportDict.value(forKey: "title") as! String)")
    }
    
    func reportService(title:String) {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(partner_id, forKey: "user_id")
        requestDict.setValue(UserModel.shared.userId, forKey: "report_by")
        requestDict.setValue(title, forKey: "report")
        Event.userReported(title)
        
        Obj.baseService(subURl: REPORT_USER_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                let reportid = dict?.value(forKey: "report_id") as! String
                self.getReport!(reportid)
            }
            self.dismiss(animated: true, completion: nil)
        })
    }
}
