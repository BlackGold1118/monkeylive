//
//  ReportCell.swift
//  Randoo
//
//  Created by HTS-Product on 22/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class ReportCell: UITableViewCell {

    @IBOutlet var reportIcon: UIImageView!
    @IBOutlet var reportNameLbl: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        reportNameLbl!.config(color: .white, font: medium, align: .left, text: "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
