//
//  SubscriptionPlan.swift
//  Livza
//
//  Created by Serge Vysotsky on 29.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import Foundation

enum SubscriptionPlan: Int, CaseIterable {
    case threeMonths
    case twelveMonths
    case month
    
    case monthNew
    case monthNewOffer
    
    var productId: String {
        Self.plansDict.first(where: { $0.value == self })!.key
    }
    
    var months: Int {
        switch self {
        case .threeMonths:
            return 3
        case .twelveMonths:
            return 12
        case .month, .monthNew, .monthNewOffer:
            return 1
        }
    }
    
    static let plansDict: [String: SubscriptionPlan] = [
        "com.monkeylivevip.3p.3months": .threeMonths,
        "com.monkeylivevip.3p.year60off": .twelveMonths,
        "com.monkeylivevip.3p.1month": .month,
        MONEKYLIVONEMONTH: .monthNew,
        MONEKYLIVONEMONTHOFFER: .monthNewOffer,
    ]
}
