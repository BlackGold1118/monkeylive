//
//  OneMonthSubscriptionViewController.swift
//  Livza
//
//  Created by Serge Vysotsky on 27.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import UIKit

class OneMonthSubscriptionViewController: SubscriptionsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedPlan = .monthNew
    }
}
