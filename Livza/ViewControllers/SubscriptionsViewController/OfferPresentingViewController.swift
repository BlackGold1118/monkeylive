//
//  OfferPresentingViewController.swift
//  Livza
//
//  Created by Serge Vysotsky on 26.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import UIKit
import StoreKit
import FirebaseRemoteConfig


class OfferPresentingViewController: UIViewController, ReusableObject {
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    var shouldAnimateAppear = true
    static var profilePage: ProfilePage! = nil
    static var recentPage: RecentPage! = nil
    static var editProfile: EditProfile! = nil
//    static var gemListPage: GemListPage! = nil
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        guard shouldAnimateAppear else { return }
        backgroundView.backgroundColor = .clear
        scrollView.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard shouldAnimateAppear else { return }
        defer { shouldAnimateAppear = false }
        
        scrollView.transform = CGAffineTransform(translationX: 0, y: view.bounds.height)
        scrollView.alpha = 1
        
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            self?.backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.82)
            self?.scrollView.transform = .identity
        })
    }
    
    @IBAction func dismissOffer() {
        dismissOffer(true) {
            if (OfferPresentingViewController.profilePage != nil) {
                OfferPresentingViewController.profilePage.configPrimeView()
                OfferPresentingViewController.profilePage = nil
            }
            if (OfferPresentingViewController.recentPage != nil) {
                OfferPresentingViewController.recentPage.configPrimeView()
                OfferPresentingViewController.recentPage = nil
            }
            if (OfferPresentingViewController.editProfile != nil) {
                OfferPresentingViewController.editProfile.configPrimeView()
                OfferPresentingViewController.editProfile = nil
            }
//            if (OfferPresentingViewController.gemListPage != nil) {
//                OfferPresentingViewController.gemListPage.navigationController?.popViewController(animated: true)
//                OfferPresentingViewController.gemListPage = nil
//            }
            NotificationCenter.default.post(name: .offerViewControllerDismissed(Self.self), object: nil)
        }
    }
    
    func dismissOffer(_ animated: Bool, completion: (() -> Void)?) {
        if animated {
            UIView.animate(withDuration: 0.4, animations: { [weak self] in
                self?.backgroundView.backgroundColor = .clear
                self?.scrollView.transform = CGAffineTransform(translationX: 0, y: self!.view.bounds.height)
            }, completion: { [weak self] _ in
                self?.dismiss(animated: false, completion: completion)
            })
        } else {
            dismiss(animated: false, completion: completion)
        }
    }
}

// MARK: - Notifications
extension Notification.Name {
    static func offerViewControllerDismissed<Offer: OfferPresentingViewController>(_ classType: Offer.Type) -> Self {
        .init("OfferPresentingViewController \(classType.objectReuseIdentifier)")
    }
    
    static let offerViewControllerWillAppear = Notification.Name("OfferViewControllerWillAppear")
    static let subscriptionPurchased = Notification.Name("subscriptionPurchased")
}

// MARK: - Caching
extension OfferPresentingViewController {
    private static let cacheController = CacheController()
    private class CacheController: NSObject, ApphudDelegate {
        var cachedProducts: [SKProduct] = [SKProduct]()
        var isRequesting = false
        var completions = [(() -> Void)?]()
        var idArr: [String]?
        
        func requestProducts(_ products: Set<String>, completion: (() -> Void)?) {
            guard cachedProducts.count == 0 else { completion?(); return }
            self.idArr = Array(products)
            Apphud.setDelegate(self)
            isRequesting = true
            completions.append(completion)
        }
        
        func apphudDidFetchStoreKitProducts(_ products: [SKProduct]) {
            for product in products {
                if (self.idArr?.contains(product.productIdentifier))! {
                    self.cachedProducts.append(product)
                }
            }
            for completion in self.completions where completion != nil {
                DispatchQueue.main.async(execute: completion!)
            }

            self.completions.removeAll()
            self.isRequesting = false
        }
    }
    
    static var cachedProducts: [SKProduct]? {
        cacheController.cachedProducts
    }
    
    static func requestProductsForCache(_ completion: (() -> Void)? = nil) {
        guard SKPaymentQueue.canMakePayments() else { return }
        cacheController.requestProducts(Set(SubscriptionPlan.plansDict.keys), completion: completion)
    }
    
    static func requestProductsAndShow(from controller: UIViewController?, completion: (() -> Void)? = nil) {
        let flowNavigationController = instantiateFromStoryboard()
        requestProductsForCache { [weak controller] in
            if ((controller as? ProfilePage) != nil) {
                profilePage = controller as! ProfilePage
            }
            if ((controller as? RecentPage) != nil) {
                recentPage = controller as! RecentPage
            }
            if ((controller as? EditProfile) != nil) {
                editProfile = controller as! EditProfile
            }
//            if ((controller as? GemListPage) != nil) {
//                gemListPage = controller as! GemListPage
//            }
            controller?.present(flowNavigationController, animated: false, completion: completion)
        }
    }
    
    static func instantiateFromStoryboard() -> UINavigationController {
        let offer = Self.instantiate(from: .subscriptions)
        let navigation = UINavigationController(rootViewController: offer)
        navigation.setNavigationBarHidden(true, animated: false)
        navigation.modalPresentationStyle = .overCurrentContext
        return navigation
    }
}

extension OfferPresentingViewController {
    static var RemoteConfiguredPlansViewController: OfferPresentingViewController.Type {
        let configs = RemoteConfig.remoteConfig()
        if configs.configValue(forKey: .enable_offer).boolValue, let offerCountdown = OptionalUserDefault<Int>(.offerCountdown).wrappedValue, offerCountdown > 0 {
            return OfferViewController.self
        } else {
            if configs.configValue(forKey: .should_use_one_month_subscription).boolValue {
                return OneMonthSubscriptionViewController.self
            } else {
                return SubscriptionsViewController.self
            }
        }
    }
}

