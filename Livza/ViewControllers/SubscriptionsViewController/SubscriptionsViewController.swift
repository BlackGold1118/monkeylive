//
//  SubscriptionsViewController.swift
//  Livza
//
//  Created by Serge Vysotsky on 13.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import UIKit
import StoreKit

class SubscriptionsViewController: OfferPresentingViewController {
    @IBOutlet private weak var priviledgesCollectionView: UICollectionView!
    @IBOutlet private weak var plansCollectionView: UICollectionView?
    @IBOutlet private weak var plansCollectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet private weak var pageControl: UIPageControl!
    @IBOutlet private weak var privacyTextView: UITextView!
    
    var selectedPlan = SubscriptionPlan.twelveMonths
    private var helpArray = NSMutableArray()
    private var selectedProduct: SKProduct?
    private var priviledges = [PrimePriviledge]()
    private var userDraggedLastTime: TimeInterval?
    private var isDragging = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        plansCollectionView?.selectItem(at: IndexPath(row: selectedPlan.rawValue, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        
        let serviceObj = BaseWebService()
        serviceObj.getDetails(subURl: HELP_API, onSuccess: { [weak self] response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == SUCCESS {
                self?.helpArray.addObjects(from: dict?.value(forKey: "help_list") as! [Any])
                self?.setupTermsTextView()
            }
        })
        
        // setup priviledges
        let defaultsDict = UserModel.shared.getDefaults()
        let primeDetails = defaultsDict?.value(forKey: "prime_details") as? NSDictionary
        let primeBenefits = primeDetails?.value(forKey: "prime_benefits") as? [[String: String]]
        guard let data = try? JSONSerialization.data(withJSONObject: primeBenefits as Any, options: []),
            let objects = try? JSONDecoder().decode([PrimePriviledge].self, from: data)
            else { return dismissOffer() }
        priviledges = objects
        pageControl?.numberOfPages = objects.count
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard shouldAnimateAppear else { return }
        super.viewDidAppear(animated)
        guard priviledgesCollectionView != nil else { return }
        let timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { [weak self] timer in
            guard let self = self else { return timer.invalidate() }
            
            
            if let lastDragTime = self.userDraggedLastTime, CFAbsoluteTimeGetCurrent() - lastDragTime < 3 {
                return
            }
            
            if self.isDragging {
                return
            }
            
            var nextOffset = self.priviledgesCollectionView.contentOffset.x + self.priviledgesCollectionView.frame.width
            if nextOffset + 1 > self.priviledgesCollectionView.contentSize.width {
                nextOffset = 0
            }
            
            self.pageControl.currentPage = Int(nextOffset / self.priviledgesCollectionView.frame.width)
            self.priviledgesCollectionView.setContentOffset(CGPoint(x: nextOffset, y: 0), animated: true)
        }
        
        RunLoop.main.add(timer, forMode: .common)
    }
    
    deinit {
        NotificationCenter.default.post(name: .primeUpdated, object: nil)
    }
}

// MARK: - @IBActions
private extension SubscriptionsViewController {
    @IBAction func continueWithPlan() {
        guard Utility.shared.isConnectedToNetwork() else { return }
        guard let products = Self.cachedProducts,
            let product = products.first(where: { $0.productIdentifier == selectedPlan.productId }) else { return }
        showProgress(true)
        self.purchaseProduct(product: product)
        selectedProduct = product
    }
    
    //Apphud payment
    func purchaseProduct(product: SKProduct) {
        Apphud.purchase(product) { result in
            if result.error != nil {
                if let error = result.transaction?.error as? SKError {
                    switch error.code {
                    case .paymentCancelled:
                        let alert = UIAlertController(title: "Purchase is canceled", message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true)
                    default:
                        break
                    }
                    self.showProgress(false)
                }
            } else {
                self.updatePrimeInfo(result.transaction!)
                self.selectedPlan.event.log()
            }
        }
    }
}


// MARK: - Private
private extension SubscriptionsViewController {
    func setupTermsTextView() {
        guard let attributedText = privacyTextView.attributedText else { return }
        let attributedString = NSMutableAttributedString(attributedString: attributedText)
        privacyTextView.linkTextAttributes = [
            .font: UIFont.systemFont(ofSize: 13),
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .foregroundColor: UIColor.white,
        ]
        
        let privacyRange = attributedString.mutableString.range(of: "Privacy Policy")
        attributedString.addAttribute(
            NSAttributedString.Key.link,
            value: "privacy",
            range: privacyRange
        )
        
        let tosRange = attributedString.mutableString.range(of: "Terms of Service")
        attributedString.addAttribute(
            NSAttributedString.Key.link,
            value: "tos",
            range: tosRange
        )
        
        privacyTextView.attributedText = attributedString
    }
}

// MARK: - SKPaymentTransactionObserver
extension SubscriptionsViewController {
    func showProgress(_ show: Bool) {
        buyButton.isEnabled = !show
        buyButton.isHidden = show
        if show {
            buyButton.superview?.showProgressView()
        } else {
            buyButton.superview?.removeProgressView()
        }
    }
    
    //update to service
    private func updatePrimeInfo(_ transaction: SKPaymentTransaction) {
        let transactionId = transaction.transactionIdentifier!
        let cachedProduct = Self.cachedProducts?.first(where: { $0.productIdentifier == transaction.payment.productIdentifier })
        guard let selectedProduct = selectedProduct ?? cachedProduct else { showProgress(false); return assertionFailure("This should never happen") }
        let updateOBj = BaseWebService()
        let request = NSMutableDictionary()
        request.setValue(transactionId, forKey: "transaction_id")
        request.setValue(UserModel.shared.userId, forKey: "user_id")
        request.setValue(selectedProduct.productIdentifier, forKey: "membership_id")
        request.setValue(selectedProduct.price.stringValue, forKey: "paid_amount")
        request.setValue(Utility.shared.getUTCTime(), forKey: "payment_time")
        //
        updateOBj.baseService(subURl: PRIME_PURCHASE_API, params:  request as? Parameters, onSuccess: { [weak self] response in
            self!.view.removeProgressView()
            let dict = response.result.value as? NSDictionary
            guard let status = dict?.value(forKey: "status") as? String, status == "true" else { return }
            UserModel.shared.isPremium = true
            UserModel.shared.alreadySubscribed = true
            UserModel.shared.updateProfile()
            let alert = AlertPopup()
            alert.viewType = "premium_alert"
            alert.modalPresentationStyle = .overCurrentContext
            alert.modalTransitionStyle = .crossDissolve
            alert.dismissed = { [weak self] in
                self?.dismiss(animated: true, completion: { [weak self] in
                    NotificationCenter.default.post(name: .subscriptionPurchased, object: transaction)
                    self?.dismissOffer()
                })
            }
            
            self!.present(alert, animated: true)
        })
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
extension SubscriptionsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView === priviledgesCollectionView {
            return priviledges.count
        } else if collectionView === plansCollectionView {
            return 5
        } else {
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView === priviledgesCollectionView {
            let priviledge = priviledges[indexPath.row]
            let cell = SubscriptionPriviledgeCell.dequeue(from: collectionView, for: indexPath)
            cell.priviledgeImageView.sd_setImage(
                with: URL(string: PRIME_IMAGE_URL + priviledge.image),
                placeholderImage: UIImage(named: "crown_icon")
            )
            
            cell.priviledgeTitle.text = priviledge.title
            cell.priviledgeDescription.text = priviledge.description
            return cell
        } else if collectionView === plansCollectionView {
            let cell = SubscriptionPlanCell.dequeue(from: collectionView, for: indexPath)
            configure(cell, plan: SubscriptionPlan(rawValue: indexPath.row)!)
            return cell
        } else {
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView === priviledgesCollectionView {
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        } else if collectionView === plansCollectionView {
            let width = view.frame.width - 44
            let itemSpacing = plansCollectionViewFlowLayout.minimumInteritemSpacing
            let itemWidth = (width - 3 * itemSpacing) / 3
            return CGSize(width: itemWidth, height: itemWidth * 136 / 105)
        } else {
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard collectionView === plansCollectionView else { return }
        collectionView.deselectItem(at: IndexPath(row: selectedPlan.rawValue, section: 0), animated: false)
        selectedPlan = SubscriptionPlan(rawValue: indexPath.row)!
        let cell = collectionView.cellForItem(at: indexPath) as! SubscriptionPlanCell
        makeCellSelected(cell, selected: true)
        
        for visible in collectionView.visibleCells where visible !== cell {
            makeCellSelected(visible as! SubscriptionPlanCell, selected: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard collectionView === plansCollectionView else { return }
        configure(cell as! SubscriptionPlanCell, plan: SubscriptionPlan(rawValue: indexPath.row)!)
    }
    
    private func makeCellSelected(_ cell: SubscriptionPlanCell, selected: Bool) {
        cell.planView.shadowOpacity = selected ? 0.5 : 0
        cell.borderView.isHidden = !selected
    }
    
    private func configure(_ cell: SubscriptionPlanCell, plan: SubscriptionPlan) {
        let product = Self.cachedProducts!.first(where: { $0.productIdentifier == plan.productId })!
        print(product.productIdentifier)
        let price = product.price
        let priceFormatter = NumberFormatter()
        priceFormatter.locale = product.priceLocale
        priceFormatter.numberStyle = .currencyAccounting
        priceFormatter.currencyDecimalSeparator = "."
        priceFormatter.currencyGroupingSeparator = ""
        priceFormatter.paddingCharacter = ""
        priceFormatter.maximumFractionDigits = product.price.doubleValue.remainder(dividingBy: 1) == 0 ? 0 : 2
        
        cell.priceLabel.text = priceFormatter.string(from: price)
        
        let isYear = plan == .twelveMonths
        cell.discountedPriceLabel.isHidden = !isYear
        cell.discountedPriceStrikethroughView.isHidden = !isYear
        cell.discountView.isHidden = !isYear
        if isYear {
            cell.discountedPriceLabel.text = priceFormatter.string(for: price.doubleValue / 0.4)
        }
        
        cell.durationLabel.text = plan.months.description
        if plan == .month {
            cell.monthsTitleLabel.text = "month"
        }
        
        makeCellSelected(cell, selected: selectedPlan == plan)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isDragging = true
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard scrollView === priviledgesCollectionView else { return }
        pageControl.currentPage = Int(targetContentOffset.pointee.x / scrollView.frame.width)
        userDraggedLastTime = CFAbsoluteTimeGetCurrent()
        isDragging = false
    }
}

// MARK: - UITextViewDelegate
extension SubscriptionsViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL.absoluteString == "tos" {
            let webObj =  WebPage()
            webObj.resultDict = self.helpArray.object(at: 2) as! NSDictionary
            navigationController?.pushViewController(webObj, animated: true)
        } else if URL.absoluteString == "privacy" {
            let webObj =  WebPage()
            webObj.resultDict = self.helpArray.object(at: 1) as! NSDictionary
            navigationController?.pushViewController(webObj, animated: true)
        }
        
        return false
    }
}

// MARK: - Cells
final class SubscriptionPlanCell: UICollectionViewCell, ReusableObject {
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var monthsTitleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var discountedPriceLabel: UILabel!
    @IBOutlet weak var discountedPriceStrikethroughView: UIView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var discountView: GradientView!
    @IBOutlet weak var borderView: GradientBorderView!
    @IBOutlet weak var planView: DesignableView!
}

final class SubscriptionPriviledgeCell: UICollectionViewCell, ReusableObject {
    @IBOutlet weak var priviledgeImageView: UIImageView!
    @IBOutlet weak var priviledgeTitle: UILabel!
    @IBOutlet weak var priviledgeDescription: UILabel!
}

private struct PrimePriviledge: Codable, Equatable {
    let id: String
    let description: String
    let image: String
    let title: String
}
