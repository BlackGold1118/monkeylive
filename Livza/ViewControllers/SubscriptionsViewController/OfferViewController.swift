//
//  OfferViewController.swift
//  Livza
//
//  Created by Serge Vysotsky on 27.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import UIKit
import Lottie

final class OfferViewController: OneMonthSubscriptionViewController {
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var animationView: Lottie.AnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedPlan = .monthNewOffer
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let animation = Lottie.Animation.named("confetti")
        animationView.animation = animation
        animationView.play()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.addParticleAnimation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: .offerViewControllerWillAppear, object: self)
    }
    
    func addParticleAnimation() {
        buyButton.layer.cornerRadius = buyButton.superview!.layer.cornerRadius
        buyButton.addParticlesAnimation(with: nil, effect: .spark)
        buyButton.startAnimatingParticles()
    }
}

