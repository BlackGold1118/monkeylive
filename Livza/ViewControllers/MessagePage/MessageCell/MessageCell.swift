//
//  MessageCell.swift
//  Randoo
//
//  Created by HTS-Product on 06/06/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    
    @IBOutlet var contactImg: UIImageView!
    @IBOutlet var contactNameLbl: UILabel!
    @IBOutlet var lastMsgLbl: UILabel!
    @IBOutlet var onlineIcon: UIImageView!
    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var typeIcon: UIImageView!
    @IBOutlet var separatorLbl: UILabel!
    @IBOutlet var profileBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contactNameLbl.config(color:TEXT_PRIMARY_COLOR, font: mediumReg, align: .left, text: "")
        self.lastMsgLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .left, text: "")
        self.timeLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .right, text: "")
        self.separatorLbl.backgroundColor = LINE_COLOR
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func config(model:Recents)  {
        if model.user_id != APP_NAME{
            self.contactImg.makeItRound()
        }
        
        self.contactNameLbl.text = model.user_name
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let msgTime = formatter.date(from: model.time!)
        
        //ANIL
        let calendar = NSCalendar.current
        if calendar.isDateInToday(msgTime ?? Date()) {
            self.timeLbl.text = "\(Utility.shared.timeStamp(time: msgTime ?? Date(), format: "hh:mm a"))"
        }else if calendar.isDateInYesterday(msgTime ?? Date()) {
            self.timeLbl.text = "\(Utility.shared.timeStamp(time: msgTime ?? Date(), format: "hh:mm a"))"
        }else{
            self.timeLbl.text = Utility.shared.timeStamp(time: msgTime ?? Date(), format: "dd/MM/yyyy")
        }        
        let imgURL = URL.init(string: PROFILE_IMAGE_URL+model.user_image!)
        if model.chat_type == "user_chat" {
            self.contactImg.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        }else{
            if IS_LIVZA{
                self.contactImg.image = #imageLiteral(resourceName: "live_app_icon")
            }else{
                self.contactImg.image = #imageLiteral(resourceName: "app_icon")
            }
        }
        if model.msg_type == "text" {
            self.typeIcon.isHidden = true
            self.lastMsgLbl.text = model.msg
            self.lastMsgLbl.frame = CGRect.init(x: self.contactNameLbl.frame.origin.x, y: self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height, width: 220, height: 21)
        }else if model.msg_type == "image"{
            self.typeIcon.isHidden = false
            self.typeIcon.image = #imageLiteral(resourceName: "gallery_icon")
            self.lastMsgLbl.text = "Image"
            self.lastMsgLbl.frame = CGRect.init(x: self.typeIcon.frame.origin.x+25, y: self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height, width: 220, height: 21)
        }else if model.msg_type == "missed"{
            self.typeIcon.isHidden = false
            self.typeIcon.image = #imageLiteral(resourceName: "missed_icon")
            self.lastMsgLbl.text = "Missed Call"
            self.lastMsgLbl.frame = CGRect.init(x: self.typeIcon.frame.origin.x+25, y: self.contactNameLbl.frame.origin.y+self.contactNameLbl.frame.size.height, width: 220, height: 21)
        }
        //online status
        if model.isOnline{
            self.onlineIcon.isHidden = false
        }else{
            self.onlineIcon.isHidden = true
        }
        
        //read status
        if model.isRead {
            self.contactNameLbl.textColor = TEXT_SECONDARY_COLOR
            self.lastMsgLbl.textColor = TEXT_SECONDARY_COLOR
        }else{
            self.contactNameLbl.textColor = TEXT_PRIMARY_COLOR
            self.lastMsgLbl.textColor = TEXT_PRIMARY_COLOR
        }
        
        
        //typing status
        if model.isTyping{
            self.lastMsgLbl.text = (Utility.language.value(forKey: "typing") as! String)
            self.lastMsgLbl.textColor = PRIMARY_COLOR
            if model.msg_type != "text"{
                self.typeIcon.isHidden = true
            }
        }else{
            //read status
            if model.isRead {
                self.lastMsgLbl.textColor = TEXT_SECONDARY_COLOR
            }else{
                self.lastMsgLbl.textColor = TEXT_PRIMARY_COLOR
            }
        }
    }
    
}
