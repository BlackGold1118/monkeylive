//
//  TextCell.swift
//  Randoo
//
//  Created by HTS-Product on 07/06/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class TextCell: UITableViewCell {
    
    @IBOutlet var msgView: UITextView!
    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var readIcon: UIImageView!
    @IBOutlet var userImgView: UIImageView!
    
    @IBOutlet var contanierView: UIView!
    
    var read_id = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.userImgView.makeItRound()
        self.readIcon.makeItRound()
        self.msgView.font = medium
        self.msgView.textColor = TEXT_PRIMARY_COLOR
        self.msgView.isUserInteractionEnabled = true
        self.msgView.isEditable = false
        self.msgView.isScrollEnabled = false
        self.timeLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .left, text: "")
        self.contanierView.crapView(radius: 10.0)
        self.msgView.textAlignment = .left
        
    }
    
    func changeToRTL() {
    
        if UserModel.shared.language == "Arabic" {
            self.timeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.msgView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else{
            self.timeLbl.transform = .identity
            self.msgView.transform = .identity
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func config(model:Messages)  {
        self.msgView.text = model.msg
        let imgURL = URL.init(string: PROFILE_IMAGE_URL+model.user_image!)
        self.userImgView.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        self.readIcon.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let msgTime = formatter.date(from: model.time!)
        self.timeLbl.text = Utility.shared.timeStamp(time: msgTime ?? Date(), format: "hh:mm a") //ANIL
        
        var fixedWidth =  self.msgView.intrinsicContentSize.width
        if fixedWidth > 240{
            fixedWidth = 240
        }
        self.msgView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize =  self.msgView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame =  self.msgView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        
        if model.user_id == UserModel.shared.userId{ //own msg design
            self.userImgView.isHidden = true
            if newFrame.size.width > 100{
                self.msgView.frame = CGRect.init(x: 7, y: 0, width: newFrame.size.width, height: newFrame.size.height)
            }else{
                self.msgView.frame = CGRect.init(x: 7, y: 0, width:100, height: newFrame.size.height)
                
            }
            self.timeLbl.frame = CGRect.init(x: self.msgView.frame.origin.x+4, y: self.msgView.frame.origin.y+self.msgView.frame.size.height-5, width: self.msgView.frame.width, height: 21)
            self.contanierView.frame = CGRect.init(x: FULL_WIDTH-(self.msgView.frame.size.width+30), y: 5, width: self.msgView.frame.size.width+20, height: self.timeLbl.frame.origin.y+self.timeLbl.frame.size.height+5)
            self.contanierView.backgroundColor = CHAT_SEND
            if model.msg_id == self.read_id{
                self.readIcon.isHidden = false
                self.readIcon.frame = CGRect.init(x: FULL_WIDTH-45, y: self.contanierView.frame.height-10, width: 26, height: 26)
                self.contentView.addSubview(self.readIcon)
            }else{
                self.readIcon.isHidden = true
            }
            
        }else{//other msg design
            
            
            self.readIcon.isHidden = true
            self.userImgView.frame = CGRect.init(x: 10, y: 0, width: 45, height: 45)
            if newFrame.size.width > 100{
                self.msgView.frame = CGRect.init(x: 7, y: -2, width: newFrame.size.width, height: newFrame.size.height)
            }else{
                self.msgView.frame = CGRect.init(x: 7, y: -2, width: 100, height: newFrame.size.height)
            }
            self.timeLbl.frame = CGRect.init(x: self.msgView.frame.origin.x+4, y: self.msgView.frame.origin.y+self.msgView.frame.size.height-5, width: 100, height: 21)
            let view_width = self.msgView.frame.size.width+20
            if model.user_id == APP_NAME{
                self.userImgView.isHidden = true
                self.contanierView.frame = CGRect.init(x: 10, y: 5, width: view_width, height: self.timeLbl.frame.origin.y+self.timeLbl.frame.size.height+5)
            }else{
                self.userImgView.isHidden = false
                self.contanierView.frame = CGRect.init(x: 60, y: 5, width: view_width, height: self.timeLbl.frame.origin.y+self.timeLbl.frame.size.height+5)
            }
            self.contanierView.backgroundColor = CHAT_RECEIVE
            
        }
        
        
        self.changeToRTL()
    }
    
}
