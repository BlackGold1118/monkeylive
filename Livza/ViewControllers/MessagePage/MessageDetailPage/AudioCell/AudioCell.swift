//
//  AudioCell.swift
//  Randoo
//
//  Created by HTS-Product on 28/09/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class AudioCell: UITableViewCell {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var playBtn: UIButton!
    @IBOutlet var progressSlider: UISlider!
    @IBOutlet var durationLbl: UILabel!
    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var readIcon: UIImageView!
    @IBOutlet var downloadBtn: UIButton!
    @IBOutlet var loader: UIActivityIndicatorView!
    @IBOutlet var userImgView: UIImageView!

    var read_id = String()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.timeLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .left, text: "")
        self.durationLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .right, text: "")
        self.containerView.crapView(radius: 10.0)
        self.durationLbl.text = "0.0"
        self.loader.color = PRIMARY_COLOR
        self.readIcon.makeItRound()
        self.userImgView.makeItRound()
        self.playBtn.setImage(UIImage.init(named: "Play_png"), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func changeToRTL() {
    
        if UserModel.shared.language == "Arabic" {
            self.timeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.userImgView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else{
            self.timeLbl.transform = .identity
            self.userImgView.transform = .identity
        }
    }
    
    func config(model:Messages)  {
        let userImgURL = URL.init(string: PROFILE_IMAGE_URL+model.user_image!)
        self.userImgView.sd_setImage(with: userImgURL, placeholderImage: PLACE_HOLDER_IMG)
        self.readIcon.sd_setImage(with: userImgURL, placeholderImage: PLACE_HOLDER_IMG)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let msgTime = formatter.date(from: model.time!)
        self.timeLbl.text = Utility.shared.timeStamp(time: msgTime!, format: "hh:mm a")
        if model.user_id == UserModel.shared.userId{//sender
            self.downloadBtn.isHidden = true
        
            self.containerView.backgroundColor = CHAT_SEND
            self.userImgView.isHidden = true
        self.containerView.frame = CGRect.init(x: FULL_WIDTH-250, y: 5, width: 240, height: 75)

            if model.msg_id == self.read_id{
                self.readIcon.isHidden = false
                self.readIcon.frame = CGRect.init(x: FULL_WIDTH-45, y: self.containerView.frame.height-10, width: 26, height: 26)
                self.contentView.addSubview(self.readIcon)
            }else{
                self.readIcon.isHidden = true
            }
        }else{//receiver
            self.readIcon.isHidden = true
            self.userImgView.isHidden = false

            self.userImgView.frame = CGRect.init(x: 10, y:40, width: 45, height: 45)
            self.containerView.frame = CGRect.init(x: 60, y: 5, width: 240, height: 75)
            self.containerView.backgroundColor = CHAT_RECEIVE
            if model.isDownload{
                self.downloadBtn.isHidden = true
            }else{
                self.downloadBtn.isHidden = false
            }
        }
        self.changeToRTL()
    }
}
