//
//  MessageDetailPage.swift
//  Randoo
//
//  Created by HTS-Product on 07/06/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import GrowingTextView
import IQKeyboardManagerSwift
import JTMaterialTransition
import MessageUI
//import GiphyUISDK
//import GiphyCoreSDK
import iRecordView
import AVFoundation

class MessageDetailPage: UIViewController,GrowingTextViewDelegate,UITextViewDelegate,storeDelegate,socketDelegate,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,RecordViewDelegate,AVAudioRecorderDelegate,AVAudioPlayerDelegate{
    
    var transition: JTMaterialTransition?
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var msgTableView: UITableView!
    @IBOutlet var usernameLbl: UILabel!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet var msgView: UIView!
    @IBOutlet var msgTxtView: GrowingTextView!
    @IBOutlet var lastSeenLbl: UILabel!
    @IBOutlet var videoCallBtn: UIButton!
    @IBOutlet var contactBtn: UIButton!
    @IBOutlet var moreBtn: UIButton!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var attachmentBtn: UIButton!
    @IBOutlet var chatBgImgView: UIImageView!
    @IBOutlet var gifbtn: UIButton!
    
    @IBOutlet var recordBtn: RecordButton!
    var audioPlayer = AVAudioPlayer()
    
    var newChanges: (() -> Void)?
    var contactID = String()
    var contactName = String()
    var contactImg = String()
    let imagePicker = UIImagePickerController()
    var msgArray = NSMutableArray()
    var tempArray = NSMutableArray()
    var arrangeMsgArray = NSMutableArray()
    var timeStamp = Date()
    var onlineTimer = Timer()
    var imageFrom = String()
    var isBlocked = Bool()
    var isBlockedMe = false
    
    var msg_read_id = String()
    var previous_read_id = String()
    @IBOutlet var slideView: RecordView!
    
    var msgSend = false
    var isMsgAvailable = true
    var isKeyboardEnable = false
    var audioRecorder: AVAudioRecorder!
    var recordingSession: AVAudioSession!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        GiphyUISDK.configure(apiKey: "wUJ7RuHbc0yBZ3etnoqI5ZucdCNZkCjv")
        // Do any additional setup after loading the view.
        self.initialSetup()
//        audioPlayer.delegate = self

    }
    override func viewDidLayoutSubviews() {
        if self.contactID == APP_NAME || self.isBlocked || self.isBlockedMe{
            self.usernameLbl.frame = CGRect.init(x: self.backBtn.frame.origin.x+self.backBtn.frame.size.width-1, y: self.backBtn.frame.origin.y, width: 230, height: self.backBtn.frame.size.height)
        }else{
            self.usernameLbl.frame = CGRect.init(x: self.backBtn.frame.origin.x+self.backBtn.frame.size.width-1, y: self.backBtn.frame.origin.y-13, width: 230, height: self.backBtn.frame.size.height)
        }
        self.navigationView.addSubview(self.usernameLbl)
        self.configRecordView()
    }
    func changeToRTL() {
        
        if UserModel.shared.language == "Arabic" {
            self.msgTableView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.navigationView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.usernameLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.lastSeenLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.videoCallBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.usernameLbl.textAlignment = .right
            self.lastSeenLbl.textAlignment = .right
            self.sendBtn.contentHorizontalAlignment = .right
            self.attachmentBtn.contentHorizontalAlignment = .right
            self.recordBtn.contentHorizontalAlignment = .right
            self.msgTxtView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.msgTxtView.textAlignment = .right
            self.msgView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.recordBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.msgView.frame = CGRect.init(x: 50, y: 4, width: 320, height: 50)
            self.attachmentBtn.frame = CGRect.init(x: 8, y: 10, width: 40, height: 40)
            self.recordBtn.frame = CGRect.init(x: 5, y: 10, width: 40, height: 40)
            
        }
        else{
            self.msgTableView.transform = .identity
            self.navigationView.transform = .identity
            self.usernameLbl.transform = .identity
            self.lastSeenLbl.transform = .identity
            self.usernameLbl.textAlignment = .left
            self.lastSeenLbl.textAlignment = .left
            self.videoCallBtn.transform = .identity
            self.sendBtn.contentHorizontalAlignment = .left
            self.attachmentBtn.contentHorizontalAlignment = .left
            self.msgTxtView.transform = .identity
            self.msgTxtView.textAlignment = .left
            self.msgView.transform = .identity
            self.recordBtn.transform = .identity
            self.recordBtn.contentHorizontalAlignment = .left
            self.msgView.frame = CGRect.init(x: 45, y: 4, width: 320, height: 50)
            self.attachmentBtn.frame = CGRect.init(x: 8, y: 10, width: 40, height: 40)
            self.recordBtn.frame = CGRect.init(x: FULL_WIDTH-50, y: 10, width: 40, height: 40)
        }
    }
    //config initial setup
    func initialSetup()  {
        appDelegate.isCurrentChattingUser = self.contactID
        self.imagePicker.delegate = self
        self.transition = JTMaterialTransition(animatedView: self.centerPoint())
        
        self.fd_prefersNavigationBarHidden = true
        self.usernameLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .left, text: "")
        self.lastSeenLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .left, text: "")
        self.usernameLbl.text = self.contactName
        self.configBottomView()
        Entities.sharedInstance.delegate = self
        HSChatSocket.sharedInstance.delegate = self
        msgTableView.register(UINib(nibName: "TextCell", bundle: nil), forCellReuseIdentifier: "TextCell")
        msgTableView.register(UINib(nibName: "ImageCell", bundle: nil), forCellReuseIdentifier: "ImageCell")
        msgTableView.register(UINib(nibName: "DateCell", bundle: nil), forCellReuseIdentifier: "DateCell")
        msgTableView.register(UINib(nibName: "AudioCell", bundle: nil), forCellReuseIdentifier: "AudioCell")
        
        msgArray = Entities.sharedInstance.getAllMsg(chat_id: "\(UserModel.shared.userId)\(self.contactID)",offset:0)
        self.tempArray = Entities.sharedInstance.getAllMsg(chat_id: "\(UserModel.shared.userId)\(self.contactID)",offset:0)
        if msgArray.count != 0 {
            self.msg_read_id = Entities.sharedInstance.getLastMsgID(user_id:self.contactID,type:"initial")
            print("msgid \(self.msg_read_id)")
            self.msgTableView.reloadData()
            self.scrollToBottom()
            
        }
        
        self.updateOnlineStatus()
        onlineTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.updateOnlineStatus), userInfo: nil, repeats: true)
        
        if self.isBlocked {
            self.lastSeenLbl.isHidden = true
        }else{
            //update read status
            HSChatSocket.sharedInstance.updateRead(receiver_id: self.contactID)
            Entities.sharedInstance.read(user_id: self.contactID)
        }
        self.contactBtn.config(color: TEXT_PRIMARY_COLOR, font: mediumReg, align: .center, title: "contact_us")
        if self.contactID == APP_NAME {
            self.contactBtn.isHidden = false
            self.bottomView.isHidden = true
            self.moreBtn.isHidden = true
            self.videoCallBtn.isHidden = true
            self.bottomView.isHidden = true
            self.lastSeenLbl.isHidden = true
            
        }else{
            self.contactBtn.isHidden = true
        }
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        self.msgTableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 30, right: 0)
        
        let sendIcon = #imageLiteral(resourceName: "send_icon").withRenderingMode(.alwaysTemplate)
        let galleryIcon = #imageLiteral(resourceName: "plus_icon").withRenderingMode(.alwaysTemplate)
        
        self.sendBtn.setImage(sendIcon, for: .normal)
        self.sendBtn.tintColor = PRIMARY_COLOR
        
        self.attachmentBtn.setImage(galleryIcon, for: .normal)
        self.attachmentBtn.tintColor = PRIMARY_COLOR
        
        if IS_LIVZA{
            self.chatBgImgView.image = #imageLiteral(resourceName: "live_chat_bg")
        }else{
            self.chatBgImgView.image = #imageLiteral(resourceName: "chat_bg")
        }
    }
    
    func configRecordView() {
           let recordimg =  #imageLiteral(resourceName: "mic_btn").withRenderingMode(.alwaysTemplate)
           recordBtn.setImage(recordimg, for: .normal)
           recordBtn.tintColor = PRIMARY_COLOR
           
           recordBtn.recordView = slideView
           slideView.delegate = self
           recordingSession = AVAudioSession.sharedInstance()
           do {
               try recordingSession.setCategory(.playAndRecord, mode: .default)
               try recordingSession.setActive(true)
               recordingSession.requestRecordPermission() { [unowned self] allowed in
               }
           } catch {
               // failed to record!
           }
       }
       
       
       
       func onStart() {
           //start recording
           self.slideView.isHidden = false
           self.msgView.isHidden = true
           self.attachmentBtn.isHidden = true
           print("onStart")
           startRecording()

       }
       
       func onCancel() {
           //when users swipes to delete the Record
           print("onCancel")
           audioRecorder.deleteRecording()
       }
       
       func onFinished(duration: CGFloat) {
           //user finished recording
           print("onFinished \(duration)")
           finishRecording(success: false)

           self.slideView.isHidden = true
           self.msgView.isHidden = false
           self.attachmentBtn.isHidden = false

       }
       
       //optional
       func onAnimationEnd() {
           self.slideView.isHidden = true
           self.msgView.isHidden = false
           self.attachmentBtn.isHidden = false
           //when Trash Animation is Finished
           print("onAnimationEnd")
       }
       
       
       func getDocumentsDirectory() -> URL {
           let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
           return paths[0]
       }
       func startRecording() {
           let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
           
           let settings = [
               AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
               AVSampleRateKey: 12000,
               AVNumberOfChannelsKey: 1,
               AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
           ]
           
           do {
               audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
               audioRecorder.delegate = self
               audioRecorder.record()
               
           } catch {
               finishRecording(success: false)
           }
       }
       
       func finishRecording(success: Bool) {
           audioRecorder.stop()
           audioRecorder = nil

       }
       func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
       
           do {
               let audioData = try Data(contentsOf: recorder.url)
               let uploadObj = BaseWebService()
               uploadObj.uploadChat(imgData: audioData,type:"audio", onSuccess: {response in
                   let dict = response.result.value as? NSDictionary
                   let status = dict?.value(forKey: "status") as! String
                   if status == "true"{
                       let imgName = dict?.value(forKey: "user_image") as! String
                       self.msgSend = true
                       HSChatSocket.sharedInstance.sendMsg(receiverID: self.contactID, msgType: "audio", msg: imgName,name:self.contactName,img:self.contactImg,attachment: audioData);
                   }
               })
               
           } catch {
               print("Unable to load data: \(error)")
           }

    
           
       }
    
    
    
    //request online
    @objc func updateOnlineStatus()  {
        HSChatSocket.sharedInstance.checkOnline(receiver_id: self.contactID)
    }
    @objc func appMovedToBackground() {
        self.msgTxtView.resignFirstResponder()
        self.view.bringSubviewToFront(self.bottomView)
    }
    
    func configBottomView()  {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        self.msgView.cornerViewRadius()
        self.msgView.border(color: LINE_COLOR)
        self.msgTxtView.delegate = self
        automaticallyAdjustsScrollViewInsets = false
        //        self.msgTxtView.maxLength = 250
        self.msgTxtView.placeholder = (Utility.shared.appLanguage.value(forKey: "type_msg") as! String)
        self.msgTxtView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        self.msgTxtView.font = medium
        self.msgTxtView.minHeight = 40.0
        self.msgTxtView.maxHeight = 150.0
        self.msgTxtView.backgroundColor = .clear
        self.sendBtn.isHidden = true
        
        //keyboard manager
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.msgTableView.estimatedRowHeight = 80.0
        self.msgTableView.rowHeight = UITableView.automaticDimension
    }
    override func viewWillAppear(_ animated: Bool) {
        print("called view will appear")
        self.changeToRTL()
    }
    override func viewWillDisappear(_ animated: Bool) {
        appDelegate.isCurrentChattingUser = ""
        self.onlineTimer.invalidate()
        self.msgTxtView.resignFirstResponder()
        //        Entities.sharedInstance.delegate = nil
        //        HSChatSocket.sharedInstance.delegate = nil
    }
    
    
    
    @IBAction func profileBtnTapped(_ sender: Any) {
        if self.contactID != APP_NAME {
            let profile = ProfilePage()
            profile.viewType = "fromMsg"
            profile.profileId = self.contactID
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.msgTxtView.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func moreBtnTapped(_ sender: Any) {
        self.msgTxtView.resignFirstResponder()
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        alert.addAction(UIAlertAction(title: Utility.shared.appLanguage.value(forKey: "cancel") as? String, style: UIAlertAction.Style.cancel, handler: nil))
        var blk_title = String()
        if isBlocked{
            blk_title = Utility.shared.appLanguage.value(forKey: "unblock") as! String
        }else{
            blk_title = Utility.shared.appLanguage.value(forKey: "block") as! String
        }
        
        alert.addAction(UIAlertAction(title: blk_title, style: UIAlertAction.Style.default, handler: { (action) in
            if self.isBlocked{
                HSChatSocket.sharedInstance.block(receiver_id: self.contactID, status: false)
                Entities.sharedInstance.block(user_id: self.contactID, status: false)
                self.isBlocked = false
                self.lastSeenLbl.isHidden = false
            }else{
                HSChatSocket.sharedInstance.block(receiver_id: self.contactID, status: true)
                self.isBlocked = true
                self.lastSeenLbl.isHidden = true
                Entities.sharedInstance.block(user_id: self.contactID, status: true)
            }
            self.newChanges!()
            self.viewDidLayoutSubviews()
            
        }))
        
        alert.addAction(UIAlertAction(title: Utility.language.value(forKey: "clear_chat") as? String, style: UIAlertAction.Style.default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
            
            let otherAlert = UIAlertController(title: nil, message: Utility.language.value(forKey: "clear_msg") as? String, preferredStyle: UIAlertController.Style.alert)
            let dismiss = UIAlertAction(title: Utility.language.value(forKey: "cancel") as? String, style: UIAlertAction.Style.cancel, handler: nil)
            otherAlert.addAction(dismiss)
            
            let okayBtn = UIAlertAction(title: Utility.language.value(forKey: "ok") as? String, style: UIAlertAction.Style.default, handler:self.clearMsg)
            otherAlert.addAction(okayBtn)
            self.present(otherAlert, animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    //logout
    func clearMsg(alert:UIAlertAction)  {
        Entities.sharedInstance.clear(chat_id: "\(UserModel.shared.userId)\(self.contactID)")
        Entities.sharedInstance.emptyRecent(user_id: self.contactID)
        self.msgArray.removeAllObjects()
        self.msgTableView.reloadData()
        self.newChanges!()
    }
    
    //video call action
    @IBAction func videoCallBtnTapped(_ sender: Any) {
        self.msgTxtView.resignFirstResponder()
        
        let defaultDict:NSDictionary = UserModel.shared.getDefaults()!
        let call_charge = defaultDict.value(forKey: "video_calls") as! Int
        
        if call_charge <= UserModel.shared.gemCount{
            if !self.isBlocked {
                DispatchQueue.main.async {
                    let videoObj = VideoCallPage()
                    videoObj.user_name = self.contactName
                    videoObj.user_img = self.contactImg
                    videoObj.sender = true
                    videoObj.user_id = self.contactID
                    videoObj.platform = "ios"
                    videoObj.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                    self.navigationController?.present(videoObj, animated: false, completion: nil)
                }
            }else{
                self.statusAlert(msg: "unblock_call")
            }
        }else{
            self.statusAlert(msg: "not_enough_gems")
        }
    }
    
    //pick image & camera
    @IBAction func attachmentBtnTapped(_ sender: Any) {
        self.msgTxtView.resignFirstResponder()
        if !self.isBlocked {
            self.choosePicture()
        }else{
            self.statusAlert(msg: "unblock_user")
        }
    }
    
    @IBAction func sendBtnTapped(_ sender: Any) {
        if !self.isBlocked {
            self.msgSend = true
            HSChatSocket.sharedInstance.sendMsg(receiverID: self.contactID, msgType: "text", msg: self.msgTxtView.text,name:self.contactName,img:self.contactImg,attachment:nil);
            self.msgTxtView.text = ""
            self.sendBtn.isHidden = true
            Event.DirectMessageSent.log()
        }else{
            self.statusAlert(msg: "unblock_user")
        }
    }
    
    @IBAction func contactBtn(_ sender: Any) {
        
        let defaultDict = UserModel.shared.getDefaults()
        let composeVC = MFMailComposeViewController()
        
        /*if (composeVC == nil)
        {
            let alert = UIAlertController(title: "Alert", message: "no email account found", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else
        {
            composeVC.mailComposeDelegate = self
            let msgContent = "\n\n\nNAME: iPhone\nMODEL:\(UIDevice.current.systemName)\nVERSION:\(UIDevice.current.systemVersion)"
            
            composeVC.setToRecipients([defaultDict?.value(forKey: "contact_email") as! String])
            composeVC.setSubject(Utility.language.value(forKey: "feedback") as! String)
            composeVC.setMessageBody(msgContent, isHTML: false)
            composeVC.modalPresentationStyle = .fullScreen
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }*/
        
        //ANIL
        if MFMailComposeViewController.canSendMail()
        {
            composeVC.mailComposeDelegate = self
            let msgContent = "\n\n\nNAME: iPhone\nMODEL:\(UIDevice.current.systemName)\nVERSION:\(UIDevice.current.systemVersion)"
            
            composeVC.setToRecipients([defaultDict?.value(forKey: "contact_email") as! String])
            composeVC.setSubject(Utility.language.value(forKey: "feedback") as! String)
            composeVC.setMessageBody(msgContent, isHTML: false)
            composeVC.modalPresentationStyle = .fullScreen
            self.present(composeVC, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "No configured email account found on this device.", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
        
        if result.rawValue == 0 || result.rawValue == 3{
            self.statusAlert(msg: "mail_cancelled")
        }else if result.rawValue == 1{
            self.statusAlert(msg: "mail_saved")
        }else if result.rawValue == 2{
            self.statusAlert(msg: "mail_sent")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.msgArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var customCell = UITableViewCell()
        let model = self.msgArray.object(at: indexPath.row) as! Messages
        if model.msg_type == "text" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath) as! TextCell
            cell.read_id = self.msg_read_id
            cell.config(model: model)
            if model.msg_id == self.msg_read_id{
                tableView.rowHeight = cell.contanierView.frame.size.height+18
            }else{
                tableView.rowHeight = cell.contanierView.frame.size.height+5
            }
            customCell = cell
        }else if model.msg_type == "image" || model.msg_type == "gif"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageCell
            cell.read_id = self.msg_read_id
            cell.config(model: model)
            cell.attachmentBtn.tag = indexPath.row
            cell.attachmentBtn.addTarget(self, action: #selector(self.viewImage(_:)), for: .touchUpInside)
            cell.downloadBtn.tag = indexPath.row
            cell.tag = indexPath.row+50000
            cell.downloadBtn.addTarget(self, action: #selector(self.download(_:)), for: .touchUpInside)
            if model.msg_id == self.msg_read_id{
                tableView.rowHeight = 203
            }else{
                tableView.rowHeight = 190
            }
            customCell = cell
        }else if model.msg_type == "audio"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AudioCell", for: indexPath) as! AudioCell
            cell.read_id = self.msg_read_id
            cell.config(model: model)
            cell.playBtn.tag = indexPath.row
            cell.playBtn.addTarget(self, action: #selector(self.playAudio(_:)), for: .touchUpInside)
            cell.downloadBtn.tag = indexPath.row
            cell.tag = indexPath.row+10000
            cell.downloadBtn.addTarget(self, action: #selector(self.downloadAudio(_:)), for: .touchUpInside)
            if model.msg_id == self.msg_read_id{
                tableView.rowHeight = 100
            }else{
                tableView.rowHeight = 85
            }
            customCell = cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DateCell", for: indexPath) as! DateCell
            cell.config(model: model)
            customCell = cell
            tableView.rowHeight = 45
            if UserModel.shared.language == "Arabic" {
                cell.infoLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            else
            {
                cell.infoLbl.transform = .identity
            }
            }
        
        return customCell
    }
    
    //scroll to bottom
    func scrollToBottom() {
        msgTableView.scrollToBottom(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = self.msgTableView.contentOffset
        if currentOffset.y < 50{
            if self.isMsgAvailable{
                var newArray = NSMutableArray()
                newArray = Entities.sharedInstance.getAllMsg(chat_id: "\(UserModel.shared.userId)\(self.contactID)", offset: self.msgArray.count)
                if newArray.count != 0{
                    self.msgArray.removeAllObjects()
                    arrangeMsgArray.removeAllObjects()
                    arrangeMsgArray.addObjects(from: newArray as! [Any])
                    arrangeMsgArray.addObjects(from: self.tempArray as! [Any])
                    self.msgArray.addObjects(from: arrangeMsgArray as! [Any])
                    self.tempArray.removeAllObjects()
                    self.tempArray.addObjects(from: arrangeMsgArray as! [Any])
                    self.msgTableView.reloadData()
                    let indexPath = IndexPath(row: newArray.count-1, section: 0)
                    self.msgTableView.scrollToRow(at: indexPath, at: .top, animated: false)
                }else{
                    self.isMsgAvailable = false
                }
            }
        }
    }
    
    @objc func playAudio(_ sender: UIButton){
        let model = self.msgArray.object(at: sender.tag) as! Messages
//        let audioURL = URL(string:CHAT_IMAGE_URL+model.msg!)
        let urlstr = CHAT_IMAGE_URL+model.msg!
        let escapedString = urlstr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let audioURL = URL(string:escapedString!)
        do {
                                       try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.voiceChat, options: .mixWithOthers)
                                       try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
                                       try AVAudioSession.sharedInstance().setActive(true)
                                   } catch {
                                       print("Speaker error : \(error)")
                                   }
//
        print("play audio \(String(describing: audioURL))");
        do {
            let imageData = try Data(contentsOf: audioURL!)
                                  audioPlayer = try AVAudioPlayer.init(data: imageData)
                              } catch {
                                  print("Unable to load data: \(error)")
                              }
                              audioPlayer.delegate = self
                              audioPlayer.prepareToPlay()
        audioPlayer.volume = 10
        audioPlayer.play()
        let cell = view.viewWithTag(sender.tag + 10000) as? AudioCell
        cell?.progressSlider.maximumValue = Float(audioPlayer.duration)
        cell?.progressSlider.value = 0.0;
    }
    
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("play done");

    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("error playing \(error?.localizedDescription)")
    }
    
    //download pic
    @objc func downloadAudio(_ sender: UIButton){
        let cell = view.viewWithTag(sender.tag + 10000) as? AudioCell
        cell?.loader.startAnimating()
        cell?.downloadBtn.isHidden = true
        let model = self.msgArray.object(at: sender.tag) as! Messages
        let audioURL = URL(string:CHAT_IMAGE_URL+model.msg!)
        let data = try? Data(contentsOf: audioURL!)
        let updatedModel = Entities.sharedInstance.updateDownload(msg_id: model.msg_id!, imgData: data!)
        self.msgArray.removeObject(at: sender.tag)
        self.msgArray.insert(updatedModel as Any, at: sender.tag)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.msgTableView.reloadData()
        }
    }
    
    //view pic
    @objc func viewImage(_ sender: UIButton){
        let model = self.msgArray.object(at: sender.tag) as! Messages
        let viewObj = ImageViewer()
        viewObj.modalPresentationStyle = .custom
        viewObj.transitioningDelegate = self.transition
        if model.attachment == nil {
            viewObj.imgData = nil
            viewObj.imgName = model.msg!
            viewObj.viewType = "3"
            viewObj.imageType = model.msg_type!
        }else{
            viewObj.imgData = model.attachment!
            viewObj.viewType = "3"
        }
        self.present(viewObj, animated: true, completion: nil)
    }
    
    //download pic
    @objc func download(_ sender: UIButton){
        let cell = view.viewWithTag(sender.tag + 50000) as? ImageCell
        cell?.loader.startAnimating()
        cell?.downloadBtn.isHidden = true
        let model = self.msgArray.object(at: sender.tag) as! Messages
        let imageURL = URL(string:CHAT_IMAGE_URL+model.msg!)
        let data = try? Data(contentsOf: imageURL!)
        let image = UIImage(data: data!)
        PhotoAlbum.sharedInstance.save(image: image!)
        let updatedModel = Entities.sharedInstance.updateDownload(msg_id: model.msg_id!, imgData: data!)
        self.msgArray.removeObject(at: sender.tag)
        self.msgArray.insert(updatedModel as Any, at: sender.tag)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.msgTableView.reloadData()
        }
    }
    
    
    //MARK: Keyboard hide/show
    @objc func keyboardWillShow(sender: NSNotification) {
        if !isKeyboardEnable {
            isKeyboardEnable = true
            let info = sender.userInfo!
            let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            self.msgTableView.frame.size.height -= keyboardFrame.height
            let topPadding = self.bottomView.frame.origin.y - keyboardFrame.height
            
            self.bottomView.frame = CGRect.init(x: 0, y: topPadding, width: FULL_WIDTH, height: self.bottomView.frame.size.height)
            self.view.addSubview(self.bottomView)
            if self.msgArray.count != 0 {
                self.scrollToBottom()
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        self.msgTableView.frame.size.height += keyboardFrame.height
        self.bottomView.frame.origin.y = FULL_HEIGHT -  self.bottomView.frame.size.height
        self.msgTableView.frame.size.height = self.bottomView.frame.origin.y - self.navigationView.frame.size.height
        isKeyboardEnable = false
        
    }
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        //        UIView.animate(withDuration: 0.2) {
        self.bottomView.frame.origin.y += self.bottomView.frame.size.height
        self.bottomView.frame.size.height = height+20
        self.msgTxtView.frame.size.height = height
        self.msgView.frame.size.height = height+10
        
        self.bottomView.frame.origin.y -= self.bottomView.frame.size.height
        self.msgView.frame.origin.y = 5
        self.msgTxtView.frame.origin.y = 5
        self.msgTableView.frame.size.height -= 60
        self.msgTableView.frame.size.height = self.bottomView.frame.origin.y - self.navigationView.frame.size.height
        self.scrollToBottom()
        //        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        if Utility.shared.checkEmpty(str: textView.text!+text) {
            self.sendBtn.isHidden = true
        }else{
            if self.sendBtn.isHidden{
                self.sendBtn.frame.origin.x -= 30
                UIView.animate(withDuration: 0.5, animations: {
                    self.sendBtn.isHidden = false
                    self.sendBtn.frame.origin.x += 30
                })
            }
        }
        
        if !self.isBlocked {
            HSChatSocket.sharedInstance.typing(status: "typing", receiver_id:self.contactID)
            let timeStamp = Date()
            self.timeStamp = timeStamp
            perform(#selector(self.endTyping(_:)), with: timeStamp, afterDelay: TimeInterval(1.5))
        }
        
        
        return true
    }
    @objc func endTyping(_ timeStamp: Date?) {
        if (timeStamp == self.timeStamp) {
            HSChatSocket.sharedInstance.typing(status: "untyping", receiver_id:self.contactID)
        }
        
    }
    
    //local storage information
    func gotStoredInfo(type: String, msg: Messages?) {
        if type == "msg" {
            if msg?.user_id == self.contactID || msg?.receiver_id == self.contactID{
                if !self.isBlocked{
                    HSChatSocket.sharedInstance.updateRead(receiver_id: self.contactID)
                    Entities.sharedInstance.read(user_id: self.contactID)
                }
                self.msgArray.add(msg as Any)
                self.msgTableView.reloadData()
                if self.msgSend{
                    self.scrollToBottom()
                    self.msgSend = false
                }
            }
        }
    }
    
    //instant information
    func gotSocketInfo(type: String, dict: NSDictionary?) {
        if type == "_profileStatus" {
            let status = dict?.value(forKey: "online_status")
            if status is Bool{
                let online_status = dict?.value(forKey: "online_status") as! Bool
                if online_status{
                    self.isBlockedMe = false
                    self.lastSeenLbl.text = (Utility.language.value(forKey: "online") as! String)
                }else{
                    self.lastSeenLbl.text = ""
                    self.isBlockedMe = true
                }
                self.viewDidLayoutSubviews()
                
            }else{
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                if let dateStr = status as? String, let localDate = formatter.date(from: dateStr) {
                    self.lastSeenLbl.text = Utility.shared.makeFormat(to: localDate)
                }
            }
            
        }else if type == "_listenTyping"{
            let typing_status = dict?.value(forKey: "typing_status") as? String
            let user_id = dict?.value(forKey: "user_id") as? String
            if user_id == self.contactID{
                if typing_status == "typing"{
                    self.lastSeenLbl.text = (Utility.language.value(forKey: "typing") as! String)
                }
            }
        }else if type == "_receiveReadStatus"{
            let user_id = dict?.value(forKey: "user_id") as! String
            if user_id == self.contactID{
                self.msg_read_id = Entities.sharedInstance.getLastMsgID(user_id:self.contactID,type:"instant")
                if previous_read_id != self.msg_read_id{
                    self.previous_read_id = self.msg_read_id
                    self.msgTableView.reloadData()
                }
            }
            
        }
    }
    
    
    
    
    //MARK: MEDIA PICKER
    //gallery popup
    func choosePicture()  {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: Utility.shared.appLanguage.value(forKey: "camera") as? String, style: .default) { (action) in
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                //already authorized
                self.moveToCamera()
            } else {
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                    if granted {
                        //access allowed
                        self.moveToCamera()
                    } else {
                        //access denied
                        DispatchQueue.main.async {
                            self.cameraPermissionAlert()
                        }
                    }
                })
            }
        }
        let gallery = UIAlertAction(title: Utility.language.value(forKey: "gallery") as? String, style: .default) { (action) in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.modalPresentationStyle = .fullScreen

            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: Utility.shared.appLanguage.value(forKey: "cancel") as? String, style: .cancel)
        alertController.addAction(camera)
        alertController.addAction(gallery)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //move to camera
    func moveToCamera()   {
        self.imageFrom = "camera"
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .camera
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            if Utility.shared.isConnectedToNetwork(){
                let imageData: Data = pickedImage.jpegData(compressionQuality: 0.0)!
                let uploadObj = BaseWebService()
                uploadObj.uploadChat(imgData: imageData,type:"image", onSuccess: {response in
                    let dict = response.result.value as? NSDictionary
                    let status = dict?.value(forKey: "status") as! String
                    if status == "true"{
                        let imgName = dict?.value(forKey: "user_image") as! String
                        self.msgSend = true
                        HSChatSocket.sharedInstance.sendMsg(receiverID: self.contactID, msgType: "image", msg: imgName,name:self.contactName,img:self.contactImg,attachment: imageData);
                        if self.imageFrom == "camera"{
                            PhotoAlbum.sharedInstance.save(image: pickedImage)
                        }
                    }
                })
            }
        }else{
            self.showAlert(msg: "choose_image")
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func Gifbtntab(_ sender: Any) {
//        let giphy = GiphyViewController()
        self.msgTxtView.resignFirstResponder()
        
        //giphy.theme = settingsViewController.theme
        //giphy.mediaTypeConfig = settingsViewController.mediaTypeConfig
//        GiphyViewController.trayHeightMultiplier = 0.7
        //giphy.layout = settingsViewController.layout
        //giphy.showConfirmationScreen = settingsViewController.confirmationScreen == .on
//        giphy.shouldLocalizeSearch = true
//        giphy.delegate = self as? GiphyDelegate
//        giphy.dimBackground = true
//        giphy.showCheckeredBackground = true
//        giphy.modalPresentationStyle = .overCurrentContext
//        present(giphy, animated: true, completion: nil)
    }
    func uploadGif(url:String) {
        print(url)
        _ = CryptLib()
        let encryptedMsg = url
        print(encryptedMsg)
        self.msgView.resignFirstResponder()
        do{
        let urlVal = URL(string: url)
        let imageData = try Data(contentsOf: urlVal!)
            HSChatSocket.sharedInstance.sendMsg(receiverID: self.contactID, msgType: "gif", msg: url,name:self.contactName,img:self.contactImg,attachment: imageData);

//        uploadObj.uploadChat(imgData: imageData,type:"gif", onSuccess: {response in
//            let dict = response.result.value as? NSDictionary
//            let status = dict?.value(forKey: "status") as! String
//            if status == "true"{
//                print("gifURL---> ",status)
//                let imgName = dict?.value(forKey: "user_image") as! String
//                self.msgSend = true
//            }
//        })
            self.msgTableView.reloadData()
            self.scrollToBottom()
     }
        catch{
            print("Unable to load gif: \(error)")
        }
    }
        
}
//extension MessageDetailPage: GiphyDelegate {
//    func didSelectMedia(giphyViewController: GiphyViewController, media: GPHMedia) {
//        giphyViewController.dismiss(animated: true, completion: { [weak self] in
//
//            let gifURL : String = media.url(rendition: .fixedWidth, fileType: .gif)!
//               if(gifURL != nil)
//               {
//                print(gifURL)
//                self?.uploadGif(url: gifURL)
////                self?.sendMessageasText(strText: gifURL, file_type: "gif",message_type: "media", chatid: self!.chat_id, senderid: self!.sender_id , userid:  Int(self!.USER_ID)!)
//            }
//               else if(media.id != nil)
//               {
//                self?.uploadGif(url: gifURL)
////                self?.sendMessageasText(strText: media.id, file_type: "gif",message_type: "media", chatid: self!.chat_id, senderid: self!.sender_id , userid:  Int(self!.USER_ID)!)
//            }
//
//         })
////        GPHCache.shared.clear(.memoryOnly)
//    }
//
////    func didDismiss(controller: GiphyViewController?) {
////        GPHCache.shared.clear(.memoryOnly)
////    }
//}


private extension UIScrollView {
    func scrollToBottom() {
        let origin = CGPoint(x: 0, y: contentSize.height)
        let lastVisibleRect = CGRect(origin: origin, size: .zero)
        scrollRectToVisible(lastVisibleRect, animated: true)
    }
}
