//
//  ImageCell.swift
//  Randoo
//
//  Created by HTS-Product on 09/06/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
//import GiphyUISDK
//import GiphyCoreSDK
//import SwiftyGif

class ImageCell: UITableViewCell {
    
    
    @IBOutlet weak var gifimageview: UIImageView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var attachmentBtn: UIButton!
    @IBOutlet var userImgView: UIImageView!
    @IBOutlet var downloadBtn: UIButton!
    @IBOutlet var loader: UIActivityIndicatorView!
    @IBOutlet var readIcon: UIImageView!
    
    var read_id = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.crapView(radius: 10.0)
        self.imgView.crapView(radius: 10.0)
        self.userImgView.makeItRound()
        self.timeLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .left, text: "")
        self.loader.color = PRIMARY_COLOR
        self.readIcon.makeItRound()
        
        self.gifimageview.isHidden = false
//        self.gifimageview.delegate = self
        self.userImgView.isHidden = false
        self.gifimageview.contentMode = .scaleAspectFill
        
    }
    
    func changeToRTL() {
    
        if UserModel.shared.language == "Arabic" {
            self.timeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.gifimageview.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.imgView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.userImgView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.attachmentBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else{
            self.timeLbl.transform = .identity
            self.gifimageview.transform = .identity
            self.imgView.transform = .identity
            self.userImgView.transform = .identity
            self.attachmentBtn.transform = .identity
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func config(model:Messages)  {
        let imgURL = URL.init(string: CHAT_IMAGE_URL+model.msg!)
        let userImgURL = URL.init(string: PROFILE_IMAGE_URL+model.user_image!)
        self.userImgView.sd_setImage(with: userImgURL, placeholderImage: PLACE_HOLDER_IMG)
        self.readIcon.sd_setImage(with: userImgURL, placeholderImage: PLACE_HOLDER_IMG)
        //let type:String = model.value(forKeyPath: "message_data.message_type") as! String
        
        let type = model.msg_type
//        if type == "gif"{
//            self.loadGifImage(url: model.msg!)
//        }
//        else{
            self.loadImage(url: "\(CHAT_IMAGE_URL+model.msg!)\(String(describing: model.attachment))")
//        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let time = model.time, let date = formatter.date(from: time) {
            self.timeLbl.text = Utility.shared.timeStamp(time: date, format: "hh:mm a")
        }
        if model.user_id == UserModel.shared.userId{//sender
            self.downloadBtn.isHidden = true
            if model.attachment != nil{
                let image = UIImage(data: model.attachment!)
                self.imgView.image = image
            }else{
                self.imgView.sd_setImage(with: imgURL, placeholderImage: nil)
            }
            self.userImgView.isHidden = true
            self.containerView.frame = CGRect.init(x: FULL_WIDTH-190, y: 5, width: 180, height: 180)
            self.containerView.backgroundColor = CHAT_SEND
            
            if model.msg_id == self.read_id{
                self.readIcon.isHidden = false
                self.readIcon.frame = CGRect.init(x: FULL_WIDTH-45, y: self.containerView.frame.height-10, width: 26, height: 26)
                self.contentView.addSubview(self.readIcon)
            }else{
                self.readIcon.isHidden = true
            }
            self.imgView.layer.minificationFilterBias = 0.0
        }else{//receiver
            self.readIcon.isHidden = true
            self.userImgView.isHidden = false
            self.userImgView.frame = CGRect.init(x: 10, y:40, width: 45, height: 45)
            self.containerView.frame = CGRect.init(x: 60, y: 5, width: 180, height: 180)
            self.containerView.backgroundColor = CHAT_RECEIVE
            if model.isDownload{
                let image = UIImage(data: model.attachment!)
                self.imgView.image = image
                self.imgView.layer.minificationFilterBias = 0.0
                self.downloadBtn.isHidden = true
                self.attachmentBtn.isHidden = false
            }else{
                self.imgView.sd_setImage(with: imgURL, placeholderImage: nil)
                self.downloadBtn.isHidden = false
                self.attachmentBtn.isHidden = true
                self.imgView.layer.minificationFilter = CALayerContentsFilter.trilinear
                self.imgView.layer.minificationFilterBias = 4.0
            }
            if type == "gif" {
                self.imgView.layer.minificationFilterBias = 0.0
                self.downloadBtn.isHidden = true
                self.attachmentBtn.isHidden = true
                self.loader.isHidden = true
            }
            else {
                self.loadImage(url: "\(CHAT_IMAGE_URL+model.msg!)\(String(describing: model.attachment))")
            }
        }
        self.changeToRTL()
    }
    
    func loadImage(url: String) {
        self.gifimageview.isHidden = true
        self.userImgView.isHidden = false
        self.containerView.backgroundColor = .white
//        self.gifimageview.clear()
        DispatchQueue.main.async {
            self.userImgView.sd_setImage(with: URL(string: url))
        }
    }
    
    func loadGifImage(url: String) {
        self.gifimageview.isHidden = false
        self.userImgView.isHidden = true
        self.containerView.backgroundColor = .clear
        if let imgUrl = URL(string: url) {
            self.gifimageview.isHidden = true
//            self.gifimageview.setGifFromURL(imgUrl)
            self.gifimageview.contentMode = .scaleAspectFit
        }
    }
    
    override func prepareForReuse() {
        self.gifimageview.image = nil
    }
    
}
//extension ImageCell: SwiftyGifDelegate {
//    func gifURLDidFinish(sender: UIImageView) {
//        DispatchQueue.main.async {
//            self.gifimageview.isHidden = false
//        }
//    }
//}
