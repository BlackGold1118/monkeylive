//
//  DateCell.swift
//  Randoo
//
//  Created by HTS-Product on 07/06/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class DateCell: UITableViewCell {
    
    @IBOutlet var infoLbl: UILabel!
    @IBOutlet var missedIcon: UIImageView!
    @IBOutlet var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.infoLbl.config(color: .white, font: lite, align: .center, text: "")
        containerView.crapView(radius: 10.0)
        self.infoLbl.crapView(radius: 10.0)
        
    }
    
    func changeToRTL() {
    
        if UserModel.shared.language == "Arabic" {
            self.infoLbl.transform = .identity
        }
        else{
            self.infoLbl.transform = .identity
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func config(model:Messages)  {
        
        self.changeToRTL()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let msgTime = formatter.date(from: model.time!)
        var width = CGFloat()
        if model.msg_type == "sticky" {
            self.missedIcon.isHidden = true
            self.infoLbl.text = msgTime.map { Utility.shared.timeStamp(time: $0, format: "MMMM dd yyyy") }
            width = self.infoLbl.intrinsicContentSize.width+40
            self.infoLbl.frame = CGRect.init(x:0 , y: 0, width: width, height: 35)
        }else if model.msg_type == "missed"{
            self.missedIcon.isHidden = false
            self.infoLbl.text = "\(Utility.language.value(forKey: "missed_call_at") as! String) \(Utility.shared.timeStamp(time: msgTime!, format: "hh:mm a"))"
            width = self.infoLbl.intrinsicContentSize.width+80
            self.missedIcon.frame = CGRect.init(x: 20, y: 7.5, width: 20, height: 20)
            self.infoLbl.frame = CGRect.init(x:40 , y: 0, width: width-40, height: 35)
        }
        containerView.frame = CGRect.init(x:(FULL_WIDTH-width)/2 , y: 5, width: width, height: 35)
        
    }
}
