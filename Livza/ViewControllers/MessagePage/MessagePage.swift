//
//  MessagePage.swift
//  Randoo
//
//  Created by HTS-Product on 08/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MessagePage: UIViewController,UITableViewDelegate,UITableViewDataSource,GADBannerViewDelegate,storeDelegate,socketDelegate {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var messageTableView: UITableView!
    var messageArray = NSMutableArray()
    var onlineTimer = Timer()
    var messageIDs = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.changeToRTL()
        self.initialSetup()
    }
    
    //will appear
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.fd_prefersNavigationBarHidden = true
        Entities.sharedInstance.delegate = self
        HSChatSocket.sharedInstance.delegate = self
        Entities.sharedInstance.stopTyping()
        self.refresh()
        if Utility.shared.isConnectedToNetwork(){
            onlineTimer.invalidate()
            onlineTimer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(self.updateOnlineList), userInfo: nil, repeats: true)
        }
        self.changeToRTL()
    }
    override func viewWillDisappear(_ animated: Bool) {
            self.changeToRTL()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func changeToRTL() {
        if UserModel.shared.language == "Arabic" {
                
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.titleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            
        }
        else{
            self.view.transform = .identity
            self.titleLbl.transform = .identity
        }
    }
    
    //config initial setup
    func initialSetup()  {
        self.titleLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text: "message")
        self.fd_prefersNavigationBarHidden = true
        messageTableView.register(UINib(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: "MessageCell")
        self.messageIDs = Entities.sharedInstance.getIds()
        self.updateOnlineList()
        
    }
    //request online list
    @objc func updateOnlineList()  {
        HSChatSocket().onlineList(ids: self.messageIDs)
        //        HSChatSocket.sharedInstance.onlineList(ids: self.messageIDs)
    }
    
    //MARK: tableview delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messageArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        let chats = self.messageArray.object(at: indexPath.row)
        cell.config(model: chats as! Recents)
        if indexPath.row != 0 {
            cell.profileBtn.tag = indexPath.row
            cell.profileBtn.addTarget(self, action: #selector(self.moreInfo(_:)), for: .touchUpInside)
        }
        
        if UserModel.shared.language == "Arabic"{
         
            cell.contactNameLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.contactNameLbl.textAlignment = .right
            cell.contactImg.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.lastMsgLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.lastMsgLbl.textAlignment = .right
            cell.onlineIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.timeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.timeLbl.textAlignment = .right
            cell.typeIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
            
        }
        else{
            
            cell.contactNameLbl?.transform = .identity
            cell.contactNameLbl?.textAlignment = .left
            cell.contactImg?.transform = .identity
            cell.lastMsgLbl?.transform = .identity
            cell.lastMsgLbl?.textAlignment = .left
            cell.onlineIcon?.transform = .identity
            cell.timeLbl?.transform = .identity
            cell.timeLbl?.textAlignment = .left
            cell.typeIcon?.transform = .identity
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let msgDetail = MessageDetailPage()
        let chats = self.messageArray.object(at: indexPath.row)
        let model = chats as! Recents
        msgDetail.contactID = model.user_id!
        msgDetail.contactName = model.user_name!
        msgDetail.contactImg = model.user_image!
        msgDetail.isBlocked = model.isBlocked
        msgDetail.newChanges = {
            self.messageArray.removeAllObjects()
            self.messageArray = Entities.sharedInstance.getChats()
            self.messageTableView.reloadData()
        }
        self.navigationController?.pushViewController(msgDetail, animated: true)
    }
    //move profile
    @objc func moreInfo(_ sender: UIButton){
        let profile = ProfilePage()
        let chats = self.messageArray.object(at:sender.tag) as! Recents
        profile.viewType = "fromMsg"
        profile.profileId = chats.user_id!
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    //local storage information
    func gotStoredInfo(type: String, msg: Messages?) {
        if type == "msg" {
            self.messageArray.removeAllObjects()
            self.messageArray = Entities.sharedInstance.getChats()
            self.messageTableView.reloadData()
        }
    }
    
    //instant information
    func gotSocketInfo(type: String, dict: NSDictionary?) {
        if type == "_onlineListStatus" {
            let listArray = dict?.value(forKey: "users_list") as! NSArray
            for user in listArray{
                let dict = user as! NSDictionary
                var status:Bool?
                status = dict.value(forKey: "online_status") as? Bool
                Entities.sharedInstance.updateOnline(user_id: dict.value(forKey: "receiver_id") as! String, status: status!)
            }
            self.refresh()
        }else if type == "_listenTyping"{
            let typing_status = dict?.value(forKey: "typing_status") as! String
            let user_id = dict?.value(forKey: "user_id") as! String
            if typing_status == "typing"{
                Entities.sharedInstance.updateTyping(user_id: user_id, status: true)
            }else{
                Entities.sharedInstance.updateTyping(user_id: user_id, status: false)
            }
            self.refresh()
        }
    }
    //load message from service
    func refresh()  {
        self.messageArray.removeAllObjects()
        self.messageArray = Entities.sharedInstance.getChats()
        self.messageTableView.reloadData()
    }
}


