//
//  InvitePage.swift
//  Randoo
//
//  Created by HTS-Product on 30/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import Lottie
import GoogleMobileAds
import Firebase
import FirebaseDynamicLinks

class InvitePage: UIViewController,GADBannerViewDelegate,GADRewardBasedVideoAdDelegate {
  
    
    @IBOutlet var timeLbl: UILabel!
    
    @IBOutlet var bannerView: GADBannerView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var desLbl: UILabel!
    @IBOutlet var animationView: UIView!
    @IBOutlet var referBtn: UIButton!
    @IBOutlet weak var Backbtn: UIButton!
    @IBOutlet weak var Copybtn: UIButton!
    @IBOutlet var watchBtn: UIButton!
    var request = false
    var invite_url:URL?
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initialSetup()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.checkWatchVideo()
        self.changeToRTL()

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.timer.invalidate()

    }
    override func viewDidLayoutSubviews() {
        self.animationView.frame = CGRect.init(x: 20, y: self.desLbl.frame.size.height+self.desLbl.frame.origin.y-20, width: FULL_WIDTH-40, height: FULL_WIDTH-40)
        self.addLot()
        self.referBtn.cornerRoundRadius()
        self.watchBtn.cornerRoundRadius()
    }
    
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
            self.Backbtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.Backbtn.frame = CGRect.init(x: FULL_WIDTH-50, y: 25, width: 40, height: 40)
            self.Copybtn.frame = CGRect.init(x: 10, y: 25, width: 40, height: 40)
        }
        else{
            self.Copybtn.frame = CGRect.init(x: FULL_WIDTH-50, y: 25, width: 40, height: 40)
            self.Backbtn.frame = CGRect.init(x: 10, y: 25, width: 40, height: 40)
        }
    }
    
    func initialSetup()  {
        
        self.fd_prefersNavigationBarHidden = true
        self.titleLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text: "earn_gems")
        self.desLbl.config(color: TEXT_PRIMARY_COLOR, font: medium, align: .center, text: "invite_descrption")
        self.timeLbl.config(color: .red, font: medium, align: .center, text: "")

        self.referBtn.border(color: LINE_COLOR)
        self.watchBtn.config(color: .white, font: medium, align: .center, title: "watch_video")
        self.referBtn.config(color: TEXT_SECONDARY_COLOR, font: medium, align: .center, title: "refer_friends")
        self.watchBtn.backgroundColor = PRIMARY_COLOR
        let defaultDict = UserModel.shared.getDefaults()
        let watchEnable = defaultDict?.value(forKey: "video_ads") as! String
        if watchEnable == "0"{ // managed from admin panel
            self.watchBtn.isHidden = true
        }
        self.configVideoAds()
        if Utility.shared.isAdsEnable() {
            self.configAds()
        }else{
            self.bannerView.isHidden = true
        }
        self.generateInviteLink()
    }
    //config watch video
    func checkWatchVideo()  {
        if !self.compareDate(){
            self.watchBtn.backgroundColor = .lightGray
            self.watchBtn.isUserInteractionEnabled = false
            self.updateTimer()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        }
    }
    //generate invite link by combine both ios & android infor
    func generateInviteLink()  {
        do{
            let jsonDecoder = JSONDecoder()
            let profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
            guard let link = URL(string: "\(SITE_URL)/\(profile.referal_link ?? "")") else { return }
            let dynamicLinksDomainURIPrefix = DYNAMIC_LINK
            let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
            linkBuilder?.iOSParameters?.customScheme = IOS_BUNDLE_ID
            linkBuilder?.iOSParameters?.appStoreID = APP_STORE_ID
            linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: IOS_BUNDLE_ID)
            linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: ANDROID_PACKAGE_NAME)
            linkBuilder!.shorten() { url, warnings, error in
                self.invite_url = url
            }
        }catch{
            
        }
    }
    
    func addLot() {
        var lottieView = AnimationView()
        lottieView = AnimationView(name: "friends")
        lottieView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH-40, height: FULL_WIDTH-40)
        lottieView.loopMode = .loop
        lottieView.contentMode = .scaleAspectFit
        self.animationView.clearsContextBeforeDrawing = true
        self.animationView.addSubview(lottieView)
        lottieView.play{ (finished) in
            // Do Something
        }
    }
    //config banner view
    func configAds()  {
        bannerView.adUnitID = Utility.shared.adUnitId
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    func configVideoAds()  {
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(),
                                                    withAdUnitID: Utility.shared.adsVideoId)
    }
    //banner view delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func watchBtnTapped(_ sender: Any) {
        if GADRewardBasedVideoAd.sharedInstance().isReady == true {
            if self.compareDate(){
                if !request{
                    GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
                    request = true
                }else{
                    self.statusAlert(msg: "already_request")
                }
            }
    }
    }
    //compare previous date
    func compareDate()->Bool {
        if UserModel().lastWatchedTime == nil{
            print("NO WATCHED YET")
            return true
        }else{
            let currentDate = Date()
            let order = UserModel().lastWatchedTime?.compare(currentDate)
        
            if order == ComparisonResult.orderedAscending || order == ComparisonResult.orderedSame{
                print("SAME OR ASCENDING")
                return true
            }else{
                print("WAIT SOME MORE TIME")
                return false
            }
        }
    }
        
    @IBAction func referBtnTapped(_ sender: Any) {
        let textToShare:String = "\(Utility.language.value(forKey: "not_tried") as! String) \(APP_NAME) \(Utility.language.value(forKey: "signup_and_get_free_gems") as! String))"
        if (invite_url != nil) {
            let objectsToShare = [textToShare, invite_url as Any] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                //New Excluded Activities Code
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                //
                activityVC.popoverPresentationController?.sourceView = self.view
                self.present(activityVC, animated: true, completion: nil)
            }
    }
    
    
    @IBAction func copyBtnTapped(_ sender: Any)
    {
        if let url = invite_url {
            UIPasteboard.general.url = url
            statusAlert(msg: "link_copied")
        }
    }
    
    
    //MARK: Reward video delegate
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        request = false
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(), withAdUnitID: Utility.shared.adsVideoId)
    }
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        
    }
    

    func rewardBasedVideoAdDidCompletePlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad has completed.")
        let defaultDict = UserModel.shared.getDefaults()
        let duration = defaultDict?.value(forKey: "schedule_video_ads") as! NSNumber
        let currentDate = Date()
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .minute, value: duration.intValue, to: currentDate)
        
        UserModel.shared.lastWatchedTime = date
        self.checkWatchVideo()
        request = false
        let Obj = BaseWebService()
            Obj.getDetails(subURl: "\(REWARD_VIDEO_API)/\(UserModel.shared.userId)", onSuccess: {response in
                let dict = response.result.value as? NSDictionary
                let status = dict?.value(forKey: "status") as! String
                if status == "true"{
                    UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
                    let total_gems = dict?.value(forKey: "total_gems") as! NSNumber
                    UserModel.shared.gemCount = total_gems.intValue
                    let alert = AlertPopup()
                    alert.viewType = "gems_updated"
                    alert.modalPresentationStyle = .overCurrentContext
                    alert.modalTransitionStyle = .crossDissolve
                    alert.dismissed = {
                        self.dismiss(animated: true, completion: nil)
                    }
                    self.present(alert, animated: true, completion: nil)
                }
            })
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        print("Reward based video ad failed to load.")
    }
    
    //Timer function
    @objc func updateTimer()  {
        let startDate = UserModel.shared.lastWatchedTime
        let endDate = Date()
        let calendar = Calendar.current
        let unitFlags = Set<Calendar.Component>([ .second])
        let datecomponenets = calendar.dateComponents(unitFlags, from:endDate ,to:startDate!)
        self.timerDidUpdateCounterValue(newValue: datecomponenets.second!)
    }
    func timerDidUpdateCounterValue(newValue: Int) {
        let (h,m,s) = self.secondsToHoursMinutesSeconds(seconds: newValue)
        let video_available = Utility.language.value(forKey: "video_available") as! String
        if h <= 0 && m <= 0 && s <= 0{
            self.watchBtn.backgroundColor = PRIMARY_COLOR
            self.watchBtn.isUserInteractionEnabled = true
            self.timer.invalidate()
            self.timeLbl.isHidden = true
        }else if m == 0{
            self.timeLbl.isHidden = false
            self.timeLbl.text = "\(video_available) \(s)s"
        }else if h == 0{
            self.timeLbl.isHidden = false
            self.timeLbl.text = "\(video_available) \(m)m \(s)s"
        }else{
            self.timeLbl.isHidden = false
            self.timeLbl.text = "\(video_available) \(h)h \(m)m \(s)s"
        }
    }
    
    //convert integer to date format
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
