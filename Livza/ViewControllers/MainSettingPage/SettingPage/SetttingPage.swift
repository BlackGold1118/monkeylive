//
//  SetttingPage.swift
//  Randoo
//
//  Created by HTS-Product on 30/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SetttingPage: UIViewController,GADBannerViewDelegate {

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var notificationLb: UILabel!
    @IBOutlet var chatLbl: UILabel!
    @IBOutlet var followLbl: UILabel!
    @IBOutlet var chatSwitch: UISwitch!
    @IBOutlet var profileLbl: UILabel!
    @IBOutlet var followSwitch: UISwitch!
    @IBOutlet var notifySeparatorLbl: UILabel!
    @IBOutlet var ageLbl: UILabel!
    @IBOutlet var contactLbl: UILabel!
    @IBOutlet var ageSwitch: UISwitch!
    @IBOutlet var contactSwitch: UISwitch!
    @IBOutlet var profileSeparatorLbl: UILabel!
    @IBOutlet var bannerView: GADBannerView!

    var profile:ProfileModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()

    }
    
    func changeToRTL(){
        
        if UserModel.shared.language == "Arabic" {
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.titleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.titleLbl.textAlignment = .center
            self.notificationLb.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.notificationLb.textAlignment = .right
            self.chatLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.chatLbl.textAlignment = .right
            self.followLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.followLbl.textAlignment = .right
            self.profileLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.profileLbl.textAlignment = .right
            self.ageLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.ageLbl.textAlignment = .right
            self.contactLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.contactLbl.textAlignment = .right
            self.ageSwitch.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.chatSwitch.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.followSwitch.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.contactSwitch.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else{
            self.view.transform = .identity
            self.titleLbl.transform = .identity
            self.titleLbl.textAlignment = .center
            self.notificationLb.transform = .identity
            self.notificationLb.textAlignment = .left
            self.chatLbl.transform = .identity
            self.chatLbl.textAlignment = .left
            self.followLbl.transform = .identity
            self.followLbl.textAlignment = .left
            self.profileLbl.transform = .identity
            self.profileLbl.textAlignment = .left
            self.ageLbl.transform = .identity
            self.ageLbl.textAlignment = .left
            self.contactLbl.transform = .identity
            self.contactLbl.textAlignment = .left
            self.ageSwitch.transform = .identity
            self.chatSwitch.transform = .identity
            self.followSwitch.transform = .identity
            self.contactSwitch.transform = .identity
        }
    }


    func initialSetup()  {
        self.fd_prefersNavigationBarHidden = true

        self.titleLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text: "general_setting")
        self.notificationLb.config(color: TEXT_SECONDARY_COLOR , font: medium, align: .left, text: "notification")
        self.profileLbl.config(color: TEXT_SECONDARY_COLOR , font: medium, align: .left, text: "profile")
        self.chatLbl.config(color: TEXT_PRIMARY_COLOR , font: medium, align: .left, text: "chat")
        self.followLbl.config(color: TEXT_PRIMARY_COLOR , font: medium, align: .left, text: "notify_follows")
        self.ageLbl.config(color: TEXT_PRIMARY_COLOR , font: medium, align: .left, text: "hide_age")
        self.contactLbl.config(color: TEXT_PRIMARY_COLOR , font: medium, align: .left, text: "no_one_contact")
        self.notifySeparatorLbl.backgroundColor = LINE_COLOR
        self.profileSeparatorLbl.backgroundColor = LINE_COLOR
        self.chatSwitch.onTintColor = PRIMARY_COLOR
        self.followSwitch.onTintColor = PRIMARY_COLOR
        self.contactSwitch.onTintColor = PRIMARY_COLOR
        self.ageSwitch.onTintColor = PRIMARY_COLOR

        
        if Utility.shared.isAdsEnable() {
            self.configAds()
        }else{
            self.bannerView.isHidden = true
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        self.setDetails()
        self.changeToRTL()
    }
    // set profile values
    func setDetails()  {
        do{
            let jsonDecoder = JSONDecoder()
             profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
            if profile.privacy_age! {
                self.ageSwitch.isOn = true
            }else{
                self.ageSwitch.isOn = false
            }
            if profile.privacy_contactme! {
                self.contactSwitch.isOn = true
            }else{
                self.contactSwitch.isOn = false
            }
            if profile.follow_notification! {
                self.followSwitch.isOn = true
            }else{
                self.followSwitch.isOn = false
            }
            if profile.chat_notification! {
                self.chatSwitch.isOn = true
            }else{
                self.chatSwitch.isOn = false
            }
        }catch{
            
        }
    }
   
    @IBAction func chatSwitchTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            self.update(setting: "chat_notification", value: chatSwitch.isOn)
        }
    }
    
    
    @IBAction func followSwtichTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            self.update(setting: "follow_notification", value: followSwitch.isOn)
        }
    }
    
    @IBAction func ageSwitchTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork() {
            if UserModel().isPremium {
                update(setting: "privacy_age", value: ageSwitch.isOn)
            }else{
                ageSwitch.isOn = false
                goPremium()
            }
        }
    }
    
    @IBAction func contactSwitchTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork() {
            if UserModel().isPremium {
                update(setting: "privacy_contactme", value: contactSwitch.isOn)
            }else{
                contactSwitch.isOn = false
                goPremium()
            }
        }
    }
    //go premium
    func goPremium(){
        OfferPresentingViewController.RemoteConfiguredPlansViewController.requestProductsAndShow(from: self)
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //update setting to details
    func update(setting:String,value:Bool)  {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue(UserModel.shared.userId, forKey: "profile_id")
        requestDict.setValue(value, forKey: setting)

        Obj.baseService(subURl: PROFILE_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.userData = response.data!
            }
        })
    }
    
    //config banner view
    func configAds()  {
        bannerView.adUnitID = Utility.shared.adUnitId
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    //banner view delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
