//
//  MainSettingPage.swift
//  Randoo
//
//  Created by HTS-Product on 02/06/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import GoogleMobileAds

enum MainSettings: String, CaseIterable {
    case general_setting
    case privacy
    case invite_frds
    case help
    case restore
    case language
    case logout
    case delete_account
    
    var icon: String {
        switch self {
        case .general_setting:
            return "general_icon"
        case .privacy:
            return "privacy_icon"
        case .invite_frds:
            return "invite_icon"
        case .help:
            return "help_icon"
        case .restore:
            return "restore_icon"
        case .language:
            return "language_icon"
        case .logout:
            return "logout_icon"
        case .delete_account:
            return "trash_icon"
        }
    }
    
    var settingsDict: NSDictionary {
        let settingDict = NSMutableDictionary()
        settingDict.setValue(rawValue, forKey: "setting_name");
        settingDict.setValue(icon, forKey: "icon_name")
        return settingDict
    }
}

class MainSettingPage: UIViewController,UITableViewDelegate,UITableViewDataSource,GADBannerViewDelegate,languageDelegate {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var settingTableView: UITableView!
    @IBOutlet var bannerView: GADBannerView!
    
    //    var settingArray = NSMutableArray()
    var termDict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        print("called view will appear")
        self.changeToRTL()
    }
    func changeToRTL() {
        
        if UserModel.shared.language == "Arabic" {
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.titleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.titleLbl.textAlignment = .center
        }
        else{
            self.view.transform = .identity
            self.titleLbl.transform = .identity
            self.titleLbl.textAlignment = .center
        }
    }
    func initialSetup()  {
        self.titleLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text: "setting")
        self.fd_prefersNavigationBarHidden = true
        settingTableView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        
        self.settingTableView.reloadData()
        self.getPrivacyService()
        if Utility.shared.isAdsEnable() {
            self.configAds()
        }else{
            self.bannerView.isHidden = true
        }
        
    }
    
    //config banner view
    func configAds()  {
        bannerView.adUnitID = Utility.shared.adUnitId
        bannerView.rootViewController = self
        let request = GADRequest()
        bannerView.load(request)
        bannerView.delegate = self
    }
    //banner view delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("BANNER ERROR \(error.localizedDescription)")
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    var settingButtons: [MainSettings] {
        var result = MainSettings.allCases
        if UserModel.shared.isPremium {
            result.removeAll(where: { $0 == .restore })
        }
        
        return result
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        settingButtons.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        cell.config(settingDict: settingButtons[indexPath.row].settingsDict)
        
        if UserModel.shared.language == "Arabic"{
            
            cell.settingNameLbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.settingNameLbl?.textAlignment = .right
            cell.settingIcon?.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.separatorLbl?.textAlignment = .right
        }
        else{
            cell.settingNameLbl.transform = .identity
            cell.settingNameLbl.textAlignment = .left
            cell.settingIcon.transform = .identity
            cell.separatorLbl.textAlignment = .left
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utility.shared.isConnectedToNetwork(){
            
            let setting = settingButtons[indexPath.row]
            switch setting {
            case .general_setting:
                navigationController?.pushViewController(SetttingPage(), animated: true)
            case .privacy:
                if Utility.shared.isConnectedToNetwork(){
                    let webObj = WebPage()
                    webObj.resultDict = termDict
                    navigationController?.pushViewController(webObj, animated: true)
                }
            case .invite_frds:
                navigationController?.pushViewController(InvitePage(), animated: true)
            case .help:
                navigationController?.pushViewController(HelpPage(), animated: true)
            case .restore:
                restoreSubscription()
            case .language:
                let langObj = LanguagePage()
                langObj.modalPresentationStyle = .overCurrentContext
                langObj.modalTransitionStyle = .crossDissolve
                langObj.delegate = self
                present(langObj, animated: true, completion: nil)
            case .logout:
                let otherAlert = UIAlertController(title: nil, message: Utility.language.value(forKey: "want_to_logout") as? String, preferredStyle: UIAlertController.Style.alert)
                let dismiss = UIAlertAction(title: Utility.language.value(forKey: "cancel") as? String, style: UIAlertAction.Style.cancel, handler: nil)
                otherAlert.addAction(dismiss)
                
                let okayBtn = UIAlertAction(title: Utility.language.value(forKey: "ok") as? String, style: UIAlertAction.Style.default, handler:logout)
                otherAlert.addAction(okayBtn)
                present(otherAlert, animated: true, completion: nil)
            case .delete_account:
                let alert = UIAlertController(title: "Are you sure?", message: "Do you really want to delete your account? This action cannot be undone.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive) { [weak self] _ in
                    self?.deleteAccount()
                })
                
                present(alert, animated: true)
            }
        }
    }
    
    //logout
    func logout(alert:UIAlertAction)  {
        if Utility.shared.isConnectedToNetwork() {
            Apphud.logout()
            UserModel.shared.logoutFromAll()
            Utility.shared.goToLoginPage()
        }
        
    }
    
    func restoreSubscription() {
        guard Utility.shared.isConnectedToNetwork() else { return }
        
        view.showProgressView().backgroundColor = UIColor.black.withAlphaComponent(0.3)
        BaseWebService().restore { [weak self] result in
            self?.view.removeProgressView()
            
            let noRestoreCompletion = { self?.showAlert(title: "No Restores", message: "There are no purchases to restore") }
            let restoredCompletion = { self?.showAlert(title: "Restored", message: "Your purchases have been restored successfully") }
            switch result {
            case let .success(json):
                if let premium = json["premium_member"].bool {
                    UserModel.shared.isPremium = premium
                    
                    if premium {
                        restoredCompletion()
                    } else {
                        noRestoreCompletion()
                    }
                } else {
                    restoredCompletion()
                }
                
                if let gems = json["available_gems"].int {
                    UserModel.shared.gemCount = gems
                }
                
                self?.settingTableView.reloadData()
            case .failure:
                noRestoreCompletion()
            }
        }
        
    }
    
    func deleteAccount() {
        guard Utility.shared.isConnectedToNetwork() else { return }
        let service = BaseWebService()
        
        view.showProgressView().backgroundColor = UIColor.black.withAlphaComponent(0.3)
        service.delete(subURl: DELETE_ACCOUNT_API, onFailure: { [weak self] _ in
            self?.view.removeProgressView()
            }, onSuccess: { [weak self] _ in
                Event.AccountDeleted.log()
                self?.view.removeProgressView()
                guard Utility.shared.isConnectedToNetwork() else { return }
                UserModel.shared.logoutFromAll()
                Utility.shared.goToLoginPage()
        })
    }
    
    //GET privacy & policy
    func getPrivacyService()  {
        let serviceObj = BaseWebService()
        serviceObj.getDetails(subURl: TERMS_API, onSuccess: {response in
            let dict = response.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == SUCCESS{
                self.termDict = dict?.value(forKey: "terms") as! NSDictionary
                
            }
            
        })
    }
    func selectedLanguage(language: String) {
        DispatchQueue.main.async {
            //setup language
            UserModel.shared.language = language
            Utility.shared.configLanguage()
            self.initialSetup()
            self.changeToRTL()
        }
    }
}
