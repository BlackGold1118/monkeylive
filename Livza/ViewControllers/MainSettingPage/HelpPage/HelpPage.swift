//
//  HelpPage.swift
//  Randoo
//
//  Created by HTS-Product on 02/06/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class HelpPage: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var helpTableView: UITableView!
    var helpArray = NSMutableArray()
    
    @IBOutlet var loader: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    func initialSetup()  {
        self.titleLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text: "help")
        self.fd_prefersNavigationBarHidden = true
        
        helpTableView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        self.loader.color = PRIMARY_COLOR
        self.loader.startAnimating()
        self.getHelpDetails()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    self.changeToRTL()
    }
    
    func changeToRTL() {
        if UserModel.shared.language == "Arabic"{
                
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.titleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            
        }
        else{
            self.view.transform = .identity
            self.titleLbl.transform = .identity
        }
    }

    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helpArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        let dict = helpArray.object(at: indexPath.row) as! NSDictionary
        cell.config(helpDict: dict)
        
        if UserModel.shared.language == "Arabic"{
        
            cell.settingNameLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.settingNameLbl.textAlignment = .right
            cell.separatorLbl.textAlignment = .right
            cell.settingIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
//            cell.settingIcon.contentHorizontalAlignment = .right
        }
        else{
            cell.settingNameLbl.transform = .identity
            cell.settingNameLbl.textAlignment = .left
            cell.separatorLbl.textAlignment = .left
            cell.settingIcon.transform = .identity
//            cell.settingIcon.contentHorizontalAlignment = .left
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utility.shared.isConnectedToNetwork(){

        let dict = helpArray.object(at: indexPath.row) as! NSDictionary
        let webObj = WebPage()
        webObj.resultDict = dict
        self.navigationController?.pushViewController(webObj, animated: true)
        }
    }
    
    
    
    //GET terms & conditions
    func getHelpDetails()  {
        let serviceObj = BaseWebService()
        serviceObj.getDetails(subURl: HELP_API, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == SUCCESS{
                self.helpArray.addObjects(from: dict?.value(forKey: "help_list") as! [Any])
                self.helpTableView.reloadData()
                self.loader.stopAnimating()
            }
            
        })
    }
}
