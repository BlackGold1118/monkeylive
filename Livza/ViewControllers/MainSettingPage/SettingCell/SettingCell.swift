//
//  SettingCell.swift
//  Randoo
//
//  Created by HTS-Product on 02/06/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {
    
    @IBOutlet var settingNameLbl: UILabel!
    @IBOutlet var settingIcon: UIImageView!
    @IBOutlet var separatorLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.separatorLbl.backgroundColor = LINE_COLOR

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func config(settingDict:NSDictionary)  {
        let title = settingDict.value(forKey: "setting_name") as! String
        let icon = settingDict.value(forKey: "icon_name") as! String

        self.settingNameLbl.config(color: TEXT_PRIMARY_COLOR, font: medium, align: .left, text: title)
        self.settingIcon.image = UIImage.init(named: icon)

    }
    
    func config(helpDict:NSDictionary)  {
        self.settingNameLbl.config(color: TEXT_PRIMARY_COLOR, font: medium, align: .left, text: "")
        self.settingNameLbl.text = helpDict.value(forKey: "help_title") as? String
        self.settingIcon.image = UIImage.init(named: "help_gray_icon")
        
    }
    
}
