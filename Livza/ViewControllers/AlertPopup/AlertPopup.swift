//
//  AlertPopup.swift
//  Randoo
//
//  Created by HTS-Product on 13/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class AlertPopup: UIViewController {

    @IBOutlet weak var alertDesLbl: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertIcon: UIImageView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var okayBtn: UIButton!
    var dismissed: (() -> Void)?

    var viewType = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        // Do any additional setup after loading the view.
    }

    override func viewDidLayoutSubviews() {
        self.alertIcon.frame = CGRect.init(x: (self.alertView.frame.width/2)-30, y:alertDesLbl.frame.origin.y + alertDesLbl.frame.size.height+15, width: 60, height:60)
    }
 
    func initialSetup()  {
        
        self.alertView.frame = CGRect.init(x: 40, y: (FULL_HEIGHT/2)-100, width: FULL_WIDTH-80, height: 200)
        self.alertView.cornerViewMiniumRadius()
        self.alertView.backgroundColor = .white
    
        self.alertDesLbl.numberOfLines = 0
        self.alertDesLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text: viewType)

        let height = Utility.shared.height(text:self.alertDesLbl.text! , font: averageReg!, width: self.alertView.frame.width-20)
        self.alertDesLbl.frame = CGRect.init(x: 10, y:20, width: self.alertView.frame.width-20, height: height)

        self.alertIcon.frame = CGRect.init(x: (self.alertView.frame.width/2)-30, y:alertDesLbl.frame.origin.y + alertDesLbl.frame.size.height+15, width: 60, height:60)
        self.cancelBtn.config(color: TEXT_SECONDARY_COLOR, font: smallReg, align: .center, title: "not_now")
        self.okayBtn.backgroundColor = PRIMARY_COLOR
       
        
        if viewType == "insufficient_funds" {
            self.alertIcon.image = UIImage.init(named: "gem_diamond")
            self.okayBtn.config(color: .white, font: smallReg, align: .center, title: "buy_gems")
            self.okayBtn.frame = CGRect.init(x: (self.alertView.frame.width/2)-70, y:self.alertIcon.frame.size.height+self.alertIcon.frame.origin.y+15, width: 140, height:35)
            self.cancelBtn.frame = CGRect.init(x: (self.alertView.frame.width/2)-70, y:self.okayBtn.frame.size.height+self.okayBtn.frame.origin.y+15, width: 140, height:30)
        }else if viewType == "no_user_found"{
            self.alertIcon.image = UIImage.init(named: "user_placeholder")
            self.alertIcon.makeItRound()
            self.okayBtn.config(color: .white, font: smallReg, align: .center, title: "go_back")
            self.cancelBtn.isHidden = true
            self.okayBtn.frame = CGRect.init(x: (self.alertView.frame.width/2)-70, y:self.alertIcon.frame.size.height+self.alertIcon.frame.origin.y+25, width: 140, height:35)
        }else if viewType == "gems_updated"{
            self.alertIcon.image = #imageLiteral(resourceName: "gem_diamond")
            self.alertIcon.makeItRound()
            self.okayBtn.config(color: .white, font: smallReg, align: .center, title: "okay")
            self.cancelBtn.isHidden = true
            self.okayBtn.frame = CGRect.init(x: (self.alertView.frame.width/2)-70, y:self.alertIcon.frame.size.height+self.alertIcon.frame.origin.y+25, width: 140, height:35)
        }else if viewType == "premium_alert"{
            self.alertIcon.image = #imageLiteral(resourceName: "crown_icon")
            self.okayBtn.config(color: .white, font: smallReg, align: .center, title: "okay")
            self.cancelBtn.isHidden = true
            self.okayBtn.frame = CGRect.init(x: (self.alertView.frame.width/2)-70, y:self.alertIcon.frame.size.height+self.alertIcon.frame.origin.y+25, width: 140, height:35)
        }else if viewType == "no_gift"{
            self.alertIcon.image = #imageLiteral(resourceName: "gift_icon")
            self.okayBtn.config(color: .white, font: smallReg, align: .center, title: "okay")
            self.cancelBtn.isHidden = true
            self.okayBtn.frame = CGRect.init(x: (self.alertView.frame.width/2)-70, y:self.alertIcon.frame.size.height+self.alertIcon.frame.origin.y+20, width: 140, height:35)
        }
        self.okayBtn.cornerRoundRadius()
        self.cancelBtn.cornerRoundRadius()
        var viewHeight = CGFloat()
        if self.cancelBtn.isHidden {
            viewHeight = self.okayBtn.frame.origin.y+55;
        }else{
            viewHeight = self.cancelBtn.frame.origin.y+50;
        }
        
        let ypos = viewHeight/2
        self.alertView.frame = CGRect.init(x: 40, y: (FULL_HEIGHT/2)-ypos, width: FULL_WIDTH-80, height: viewHeight)
    }

    @IBAction func okay(_ sender: Any) {
        self.dismissed!()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
