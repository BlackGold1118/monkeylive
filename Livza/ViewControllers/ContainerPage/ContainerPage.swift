//
//  ContainerPage.swift
//  Randoo
//
//  Created by HTS-Product on 02/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ContainerPage: UIViewController,UIPageViewControllerDelegate,GADBannerViewDelegate {
    
    var viewType = String()
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var InfoView: ShimmeringView!
    @IBOutlet weak var infoLbl: UILabel!
    var profileID = String()
    @IBOutlet weak var backBtn: UIButton!
    var changesDone: (() -> Void)?
    @IBOutlet var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configViews()
    }
    
    override func viewDidLayoutSubviews() {
        InfoView.contentView = self.infoLbl
        InfoView.isShimmering = true
        InfoView.shimmerAnimationOpacity = 0.1
        InfoView.shimmerSpeed = 120
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func configViews()  {
        
        var pageViewController = DLAutoSlidePageViewController()
        
        if viewType == "1"{
            self.view.backgroundColor = .white
            let  firstVC = LivePage()
            let secondVC = ProfilePage()
            secondVC.viewType = "livepage"
            secondVC.isLoadData = false
            
            firstVC.pageDismissed = {
                print("called container")
                self.changesDone!()
            }
            let pages = [firstVC, secondVC]
            pageViewController = DLAutoSlidePageViewController(pages: pages,
                                                               timeInterval: 0.0,
                                                               transitionStyle: .scroll,
                                                               interPageSpacing: 0.0,type:"live")
            self.backBtn.isHidden = true
            
            pageViewController.delegate = self
            self.addChild(pageViewController)
            self.view.addSubview(pageViewController.view)
        }else if viewType == "2"{
            UserModel.shared.scrollEnabled = true
            self.fd_interactivePopDisabled = false
            self.fd_prefersNavigationBarHidden = true
            let  firstVC = FollowersPage()
            let secondVC = FollowingPage()
            firstVC.profileID = self.profileID
            secondVC.profileID = self.profileID
            let pages = [firstVC, secondVC]
            pageViewController = DLAutoSlidePageViewController(pages: pages,
                                                               timeInterval: 0.0,
                                                               transitionStyle: .scroll,
                                                               interPageSpacing: 0.0,type:"first")
            self.setDetails(page: 0)
            pageViewController.pageChanged = { index in
                self.setDetails(page: index)
            }
            pageViewController.delegate = self
            self.addChild(pageViewController)
            self.view.addSubview(pageViewController.view)
            self.view.bringSubviewToFront(self.backBtn)
            if Utility.shared.isAdsEnable() {
                self.configAds()
            }else{
                self.bannerView.isHidden = true
            }
        }else if viewType == "3"{
            UserModel.shared.scrollEnabled = true
            self.fd_interactivePopDisabled = false
            self.fd_prefersNavigationBarHidden = true
            let  firstVC = FollowersPage()
            let secondVC = FollowingPage()
            firstVC.profileID = self.profileID
            secondVC.profileID = self.profileID
            let pages = [firstVC, secondVC]
            self.setDetails(page: 1)
            pageViewController = DLAutoSlidePageViewController(pages: pages,
                                                               timeInterval: 0.0,
                                                               transitionStyle: .scroll,
                                                               interPageSpacing: 0.0,type:"last")
            pageViewController.pageChanged = { index in
                self.setDetails(page: index)
            }
            pageViewController.delegate = self
            self.addChild(pageViewController)
            self.view.addSubview(pageViewController.view)
            self.view.bringSubviewToFront(self.backBtn)
            self.configAds()
        }
    }
    
    //config banner view
    func configAds()  {
        bannerView.adUnitID = Utility.shared.adUnitId
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    //banner view delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    func setDetails(page:Int)  {
        if page == 0 {
            self.infoLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .center, text: "swipe_right_followings")
            self.titleLbl.config(color: .white, font: averageReg, align: .center, text: "followers")
        }else{
            self.infoLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .center, text: "swipe_left_followers")
            self.titleLbl.config(color: .white, font: averageReg, align: .center, text: "followings")
        }
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
