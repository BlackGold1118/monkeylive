//
//  RegisterInfo.swift
//  Randoo
//
//  Created by HTS-Product on 28/01/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import AVKit
import Lottie
import JTMaterialTransition
import SwiftyJSON
import CoreTelephony
import StoreKit

class RegisterInfo: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVCaptureVideoDataOutputSampleBufferDelegate,UIViewControllerTransitioningDelegate,UITextFieldDelegate {
    
    var transition: JTMaterialTransition?
    var cameraView:UIImagePickerController?
    var cameraPreview:UIImagePickerController?
    let captureSession = AVCaptureSession()
    var previewLayer:CALayer!
    var captureDevice:AVCaptureDevice!
    var layoutEnable =  Bool()
    var gender = String()
    var age = Int()
    var dobDate = Date()
    let imagePicker = UIImagePickerController()
    var profileImg:UIImage?
    let network = NetworkManager.sharedInstance
    var country_name = String()
    var freeGem = Int()
    
    @IBOutlet weak var picUploadLoader: UIActivityIndicatorView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var basicInfoView: UIView!
    @IBOutlet weak var picUploadView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var freeGemBtn: UIButton!
    @IBOutlet weak var freeGemView: UIView!
    @IBOutlet weak var basicNextBtn: UIButton!
    @IBOutlet var basicInfoAnimation: UIView!
    @IBOutlet weak var basicHeaderLbl: UILabel!
    @IBOutlet weak var basicSubLbl: UILabel!
    @IBOutlet weak var basicNameView: UIView!
    @IBOutlet weak var basicGenderView: UIView!
    @IBOutlet weak var basicDOBView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var finishBtn: UIButton!
    @IBOutlet weak var picHeaderLbl: UILabel!
    @IBOutlet weak var picSubLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var freeGemHeaderLbl: UILabel!
    @IBOutlet weak var freegemDesLbl: UILabel!
    @IBOutlet weak var freegemAnimationView: UIView!
    @IBOutlet weak var picAnimationView: UIView!
    @IBOutlet var free_liveBg: UIImageView!
    @IBOutlet var basic_liveBg: UIImageView!
    @IBOutlet var profilepic_liveBg: UIImageView!
    @IBOutlet var uploadicon: UIImageView!
    @IBOutlet weak var freegembgAnimationView: UIView!
    let alert = BPStatusBarAlert()
    
    let primeTempArray = NSMutableArray()
    var productIDs: Array<String?> = []
    var selectedProduct = SKProduct()
    var completedInfo: ((_ gem_count:Int?) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialSetup()
        
    }
    
    override func viewDidLayoutSubviews() {
        
        if !UserModel.shared.profileComplete {
            if IS_LIVZA{
                self.free_liveBg.isHidden = false
                self.basic_liveBg.isHidden = false
                self.profilepic_liveBg.isHidden = false
            }else{
                self.free_liveBg.isHidden = true
                self.basic_liveBg.isHidden = true
                self.profilepic_liveBg.isHidden = true
                self.basicInfoAnimation.addLOT(lot_name: "signup_pop",w:FULL_WIDTH-40 ,h:self.basicInfoAnimation.frame.size.height)
                self.picAnimationView.addLOT(lot_name: "signup_pop",w:self.picAnimationView.frame.size.width ,h:self.picAnimationView.frame.size.height)
                self.freegembgAnimationView.addLOT(lot_name: "signup_pop",w:self.freegembgAnimationView.frame.size.width ,h:self.freegembgAnimationView.frame.size.height)

            }
        
            self.freegemAnimationView.addLOT(lot_name: "Greendiamond",w:self.freegemAnimationView.frame.size.width ,h:self.freegemAnimationView.frame.size.height)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDelegate.isLive = false

        
    }
    override func viewWillDisappear(_ animated: Bool) {
        print("will disappear")
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return countryCode
        }
    }
    //initial details
    func initialSetup()  {
        
        let infor = CTTelephonyNetworkInfo()
        let carrier = infor.subscriberCellularProvider
        let currentLocale = NSLocale.current
        if carrier?.isoCountryCode != nil, let isoCode = carrier?.isoCountryCode, let description = currentLocale.localizedString(forRegionCode: isoCode) {
            country_name = description
        } else {
            country_name = "global"
        }
        
        UserDefaults.standard.set(APP_RTC_URL, forKey: "web_rtc_web")
        Utility.shared.setDefaultFilter()
        self.loader.stopAnimating()
        self.picUploadLoader.stopAnimating()
        self.fd_prefersNavigationBarHidden = true
        self.fd_interactivePopDisabled = true
        self.imagePicker.delegate = self
        self.transition = JTMaterialTransition(animatedView: self.centerPoint())
        //        let entity  = Entities()
        self.transitioningDelegate = self
        //        Entities.sharedInstance.addNewChat()
        //        if !Entities.sharedInstance.checkIfUserExist(user_id: "5") {
        //
        //        }


        self.configBasicInfoView()
        
        //user initial view
        if UserModel.shared.profileComplete { // all basic details updated
            self.tabBarController?.tabBar.isHidden = false
            self.backgroundView.isHidden = true
            UserModel.shared.getTurnServers()
            UserModel.shared.updateProfile()
            Utility.shared.getAdminMsg()
            
        }else{ // still some details need to give
            self.basicInfoView.isHidden = true
            self.picUploadView.isHidden = true
            
            self.tabBarController?.tabBar.isHidden = true
            if !UserModel.shared.basicInfoEnabled {
                self.basicInfoView.isHidden = false
            }else if !UserModel.shared.profilePicEnabled {
                self.picUploadView.isHidden = false
            }
            
            self.freeGemView.isHidden = true
      
            if !UserModel.shared.basicInfoEnabled {
                if UserModel.shared.userLoginType == "facebook"{
                    self.nameTF.text = UserModel.shared.fbDetails.value(forKey: "name") as? String
                    if let urlStr = UserModel.shared.fbDetails.value(forKeyPath:"picture.data.url") as? String,
                        let url = URL(string: urlStr), let data = try? Data(contentsOf: url) {
                        profileImg = UIImage(data: data)
                    }
                }else if UserModel.shared.userLoginType == "apple"{
                    self.nameTF.text = UserModel.shared.appleName
                }
            }
        }
        

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive),
                                               name: UIApplication.didBecomeActiveNotification, // UIApplication.didBecomeActiveNotification for swift 4.2+
            object: nil)
        
        //network not available
        network.reachability.whenUnreachable = { reachability in
            self.alert.message(message: Utility.language.value(forKey: "network_error") as! String)
            self.alert.hideDisable = true
            self.alert.show()
            
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
        
        //available
        network.reachability.whenReachable = { reachability in
            self.alert.finishAnimating()
        }
        
        
    }
    

    
    
    //basic info view
    func configBasicInfoView()  {
        //basic
        self.basicHeaderLbl.config(color: .white, font: high, align: .center, text: "millions_people_waiting")
        self.basicSubLbl.config(color: .white, font: medium, align: .center, text: "worldwide")
        self.basicInfoView.bringSubviewToFront(self.basicHeaderLbl)
        self.basicSubLbl.bringSubviewToFront(self.basicSubLbl)
        
        self.basicInfoView.crapView(radius: 10.0)
        self.picUploadView.crapView(radius: 10.0)
        self.basicGenderView.cornerViewMiniumRadius()
        self.basicDOBView.cornerViewMiniumRadius()
        self.basicNameView.cornerViewMiniumRadius()
        self.basicGenderView.applyCardEffect()
        self.basicNameView.applyCardEffect()
        self.basicDOBView.applyCardEffect()
        
        self.nameLbl.config(color: TEXT_PRIMARY_COLOR, font: lite, align: .left, text: "name")
        self.genderLbl.config(color: TEXT_PRIMARY_COLOR, font: lite, align: .left, text: "gender")
        self.dobLbl.config(color: TEXT_PRIMARY_COLOR, font: lite, align: .left, text: "age")
        self.dobTF.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .right, place: "dob")
        let datePickerView = UIDatePicker()
        let calendar = Calendar.current
        datePickerView.datePickerMode = .date
        datePickerView.locale = .current
        datePickerView.timeZone = TimeZone.init(abbreviation:"GMT+0:00")
        dobTF.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(pickDOB(sender:)), for: .valueChanged)
        var dateComponents = DateComponents()
        dateComponents.year = -18
        //        calendar.date
        datePickerView.maximumDate = calendar.date(byAdding: dateComponents, to: Date())
        
        self.nameTF.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .right, place: "your_name")
        self.nameTF.delegate = self
        self.basicNextBtn.config(color: .white, font: medium, align: .center, title: "next")
        self.basicNextBtn.cornerViewMiniumRadius()
        self.basicNextBtn.backgroundColor = PRIMARY_COLOR
        
        //pic upload
        self.picHeaderLbl.config(color: .white, font: high, align: .center, text: "millions_people_waiting")
        self.picSubLbl.config(color: .white, font: medium, align: .center, text: "worldwide")
        self.userImgView.makeItRound()
        self.userImgView.backgroundColor = .white
        self.userImgView.layer.borderWidth = 0.7
        self.userImgView.layer.borderColor = LINE_COLOR.cgColor
        
        
        self.finishBtn.config(color: .white, font: medium, align: .center, title: "upload_skip")
        self.finishBtn.cornerViewMiniumRadius()
        self.finishBtn.backgroundColor = PRIMARY_COLOR
        
        //free gem view
        self.freeGemHeaderLbl.config(color: .white, font: high, align: .center, text: "free_gems")
        self.freegemDesLbl.config(color: .white, font: lite, align: .center, text: "free_gem_des")
        self.freeGemBtn.config(color: TEXT_PRIMARY_COLOR, font: lite, align: .center, title: "okay")
        self.freeGemBtn.cornerRoundRadius()
        self.freeGemBtn.backgroundColor = .white
        self.freeGemView.crapView(radius: 10.0)
        self.loader.color = PRIMARY_COLOR
    }
    
    
    
    
    
    
    //move to camera
    /*func moveToCamera()
    {
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .camera
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.present(self.imagePicker, animated: true, completion: nil)
    }*/
    
    func moveToCamera()
    {
        if UIImagePickerController.availableCaptureModes(for: .front) != nil
        {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            imagePicker.delegate = self
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker, animated: true, completion: nil)
        }
        else
        { print("Your device does not have camera") }
    }
    
    
  
    
    // MARK: Basic info screen action
    @IBAction func basicNextBtnTapped(_ sender: Any) {
        if (nameTF.isEmptyValue()){
            self.statusAlert(msg: "enter_name")
        }else if(gender.isEmpty){
            self.statusAlert(msg: "choose_gender")
        }else if(dobTF.isEmptyValue()){
            self.statusAlert(msg: "choose_date")
        }else{
            self.loader.startAnimating()
            self.signup()
        }
    }
    @IBAction func picImage(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: Utility.language.value(forKey: "camera") as? String, style: .default) { (action) in
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                //already authorized
                self.moveToCamera()
            } else {
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                    if granted {
                        //access allowed
                        self.moveToCamera()
                    } else {
                        //access denied
                        DispatchQueue.main.async {
                            self.cameraPermissionAlert()
                        }
                    }
                })
            }
        }
        let gallery = UIAlertAction(title: Utility.language.value(forKey: "gallery") as? String, style: .default) { (action) in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.modalPresentationStyle = .fullScreen

            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: Utility.language.value(forKey: "cancel") as? String, style: .cancel)
        alertController.addAction(camera)
        alertController.addAction(gallery)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func picUploadBtnTapped(_ sender: Any) {
        self.uploadProfilePic()
        Utility.shared.addAdminInitialMsg()
    }
    
    func uploadProfilePic()  {
        if profileImg != nil {
            self.picUploadLoader.startAnimating()
            let imageData: Data = self.profileImg!.jpegData(compressionQuality: 0.0)!
            let uploadObj = BaseWebService()
            uploadObj.uploadProfilePic(profileimage: imageData, onSuccess: {response in
                let dict = response.result.value as? NSDictionary
                guard let status = dict?.value(forKey: "status") as? String else { return }
                if status == "true"{
                    self.imageResponseAction()
                }else if status == "rejected", let msg = dict?.value(forKey: "message") as? String {
                    self.statusServer(alert: msg)
                }
            })
        }else{
            self.imageResponseAction()
        }
    }
    
    
    func imageResponseAction()  {
        self.loader.stopAnimating()
        UserModel.shared.profilePicEnabled = true
        UserModel.shared.profileComplete = true
        self.updateProfile()
        //check free gem available
        guard let defaultDict = UserModel.shared.getDefaults() else { return }
        self.freeGem = defaultDict.value(forKey: "freegems") as! Int
        
        if self.freeGem == 0{//hide free gem view
            self.freeGemBtnAction()
        }else{ // show free gem view
            self.freegemDesLbl.text = "\(Utility.language.value(forKey: "free_gem_des") as! String) \(self.freeGem) \(Utility.language.value(forKey: "gems_coins_des") as! String)"
            self.freeGemView.isHidden = false
        }
        
        if UserModel.shared.userLoginType == "facebook"{
            self.picUploadView.isHidden = true
            self.freeGemView.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
            UIView.transition(with: self.basicInfoView, duration: 0.5, options: .transitionFlipFromTop, animations: nil, completion: {_ in
                self.basicInfoView.isHidden = true
                self.freeGemView.transform = CGAffineTransform.identity
            })
        }else{
            self.freeGemView.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
            UIView.transition(with: self.picUploadView, duration: 0.5, options: .transitionFlipFromTop, animations: nil, completion: {_ in
                self.picUploadView.isHidden = true
                self.freeGemView.transform = CGAffineTransform.identity
            })
            self.picUploadLoader.startAnimating()
        }
    }
    
    @IBAction func freeGemBtnTapped(_ sender: Any) {
        self.freeGemBtnAction()
//        appDelegate.connectSockets()
        
    }
    //free gem action
    func freeGemBtnAction()  {
        self.backgroundView.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
        self.backgroundView.isHidden = true
        UserModel.shared.getTurnServers()
        self.updateProfile()
        self.completedInfo!(self.freeGem)

    }
    //pic dob
    @objc func pickDOB(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dobTF.text = dateFormatter.string(from: sender.date)
        dobDate = sender.date
        let calendar = Calendar(identifier: .gregorian)
        var components  = DateComponents()
        components = calendar.dateComponents([.year,.month,.day], from: sender.date, to: Date())
        self.age = components.year!
    }
    
    @IBAction func maleBtnTapped(_ sender: Any) {
        self.basicInfoView.toast(alert: "male")
        self.gender = "male"
        self.maleBtn.setImage(UIImage.init(named: "male_sel"), for: .normal)
        self.femaleBtn.setImage(UIImage.init(named: "female_unsel"), for: .normal)
    }
    
    @IBAction func femaleBtnTapped(_ sender: Any) {
        self.basicInfoView.toast(alert: "female")
        self.gender = "female"
        self.maleBtn.setImage(UIImage.init(named: "male_unsel"), for: .normal)
        self.femaleBtn.setImage(UIImage.init(named: "female_sel"), for: .normal)
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            self.userImgView.image = pickedImage
            self.uploadicon.isHidden = true
            self.profileImg = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: TextField delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn _: NSRange, replacementString string: String) -> Bool {
        
        if (textField.text?.count)! >= 20 {
            if string.isEmpty{
                return true
            }else{
                return false
            }
        }
        if !string.isEmpty{
            let set = CharacterSet(charactersIn: RISTRICTED_CHARACTERS)
            let inverted = set.inverted
            let filtered = string.components(separatedBy: inverted).joined(separator: "")
            return filtered != string
        }else{
            return true
        }
    }
    
    
    //call when application from background
    @objc func applicationDidBecomeActive() {
        captureSession.startRunning()
        AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
            if granted {
            } else {
                DispatchQueue.main.async {
                    
                    self.cameraPermissionAlert()
                }
            }
        })
        
        //check reachability
        let reachability = Reachability()
        switch reachability!.connection {
        case .wifi:
            alert.hideDisable = false
            alert.finishAnimating()
        case .cellular:
            alert.finishAnimating()
        case .none:
            alert.message(message: Utility.language.value(forKey: "network_error") as! String)
            alert.hideDisable = true
            alert.show()
        }
    }
    @objc func appMovedToBackground() {
        captureSession.stopRunning()
    }
    //MARK: sign up  services
    func signup()  {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userLoginType, forKey: "type")
        requestDict.setValue(UserModel.shared.userLoginId, forKey: "login_id")
        requestDict.setValue(nameTF.text, forKey: "name")
        requestDict.setValue(dobTF.text, forKey: "dob")
        requestDict.setValue(gender, forKey: "gender")
        requestDict.setValue(age, forKey: "age")
        requestDict.setValue(country_name, forKey: "location")
        if UserModel.shared.userLoginType == "facebook" || UserModel.shared.userLoginType == "apple"{
            requestDict.setValue(UserModel.shared.email, forKey: "mail_id")
        }
        
        Obj.baseService(subURl: SIGNUP_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserDefaults.standard.removeObject(forKey: "already_subscribed")
                UserModel.shared.accessToken = dict?.value(forKey: "auth_token") as! String
                UserModel.shared.userId = dict?.value(forKey: "user_id") as! String
                print(UserModel.shared.userId)
//                Apphud.start(apiKey: APPHUB_SDK_TOKEN, userID: UserModel.shared.userId)
                Apphud.startManually(apiKey: APPHUB_SDK_TOKEN, userID: UserModel.shared.userId, deviceID: UserModel.shared.userId, observerMode: false)
                self.updateProfile()
                UserModel.shared.basicInfoEnabled = true
                Utility.shared.pushsignIn()
                UserModel.shared.removeMail()
                if  !UserModel.shared.userId.isEmpty{
                    appDelegate.connectSockets()
                    Utility.shared.setDefaultFilter()
                }
                if !UserModel.shared.inviteCode.isEmpty {
                    Utility.shared.inviteCredits()
                }
                if UserModel.shared.userLoginType == "facebook"{
                    self.uploadProfilePic()
                }else{
                    self.loader.stopAnimating()
                    self.picUploadView.isHidden = false
                    self.picUploadView.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
                    self.freeGemView.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
                    UIView.transition(with: self.basicInfoView, duration: 0.5, options: .transitionFlipFromTop, animations: nil, completion: {_ in
                        self.basicInfoView.isHidden = true
                        self.picUploadView.transform = CGAffineTransform.identity
                    })
                }
            }
        })
    }
    
    //update profile details
    func updateProfile()  {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue(UserModel.shared.userId, forKey: "profile_id")
        Obj.baseService(subURl: PROFILE_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.userData = response.data!
                let premium_member = dict?.value(forKey: "premium_member") as! String
                UserModel.shared.isPremium = premium_member == "true"
                let available_gems = dict?.value(forKey: "available_gems") as! NSNumber
                UserModel.shared.gemCount = available_gems.intValue
//                self.gemCountLbl.text = available_gems.intValue.kmFormatted
            }
        })
    }
    
    //redirect notification
    func checkNotificationRedirect()  {
        if Utility().isNotificationRedirect {
            let msgDict = Utility.shared.getNotificationInfo()
            let scope = msgDict?.value(forKey: "scope") as! String
            if scope == "txtchat" || scope == "admin"{
                let user_id = msgDict?.value(forKey: "user_id") as! String
                let user_image = msgDict?.value(forKey: "user_image") as! String
                let user_name = msgDict?.value(forKey: "user_name") as! String
                let msgDetail = MessageDetailPage()
                msgDetail.contactID = user_id
                msgDetail.contactName = user_name
                msgDetail.contactImg = user_image
                msgDetail.isBlocked = false
                msgDetail.newChanges = {
                }
                self.navigationController?.pushViewController(msgDetail, animated: true)
            }else if scope == "follow"{
                let user_id = msgDict?.value(forKey: "user_id") as! String
                let profile = ProfilePage()
                profile.viewType = "fromMsg"
                profile.profileId = user_id
                self.navigationController?.pushViewController(profile, animated: true)
            }else if scope == "video_call"{
                
                DispatchQueue.main.async {
                    appDelegate.endCall()
                    UserModel.shared.alreadyInCall = true
                    let videoObj = VideoCallPage()
                    videoObj.user_name = msgDict!.value(forKey: "user_name") as! String
                    videoObj.user_img = msgDict!.value(forKey: "user_image") as! String
                    videoObj.sender = false
                    videoObj.viewFrom = "callkit"
                    videoObj.platform =  msgDict!.value(forKey: "platform") as! String
                    videoObj.modalPresentationStyle = .fullScreen
                    videoObj.user_id = msgDict!.value(forKey: "user_id") as! String
                    videoObj.room_id = msgDict!.value(forKey: "room_id") as! String
                    self.navigationController?.present(videoObj, animated: true, completion: nil)
                }
                
            }
          
            Utility().clearRedirectionInfo()
        }
    }
}



//MARK: GET PRIME INFO FROM STORE


