//
//  VideoPlayer.swift
//  Livza
//
//  Created by HTS-Product on 18/09/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class VideoPlayer: UIViewController ,VersaPlayerPlaybackDelegate,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var playerView: VersaPlayerView!
    @IBOutlet var playPauseBtn: UIButton!
    @IBOutlet var moreBtn: UIButton!
    @IBOutlet var videoControls: UIView!
    @IBOutlet var currentTimeLbl: UILabel!
    @IBOutlet var fullTimeLbl: UILabel!
    @IBOutlet var stremDetailsView: UIView!
    @IBOutlet weak var totalWatchCountLbl: UILabel!
    @IBOutlet weak var liveUsersLbl: UILabel!
    @IBOutlet weak var timeDurationTitleLbl: UILabel!
    @IBOutlet weak var likecountLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var totalLikesTitleLbl: UILabel!
    @IBOutlet var loader: UIActivityIndicatorView!
    @IBOutlet var commentsBtn: UIButton!
    @IBOutlet var reportIcon: UIImageView!
    @IBOutlet var commentsView: UIView!
    @IBOutlet var commentTableView: UITableView!
    @IBOutlet var commentTitleLbl: UILabel!
    @IBOutlet weak var closebtn: UIButton!
    
    var selected_index = Int()
    var deletedInfo: ((_ index:Int) -> Void)?
    var videoDict = NSDictionary()
    var player_item:VersaPlayerItem?
    var isCompletedPlaying = false
    var video_url = String()
    var reportArray = NSMutableArray()
    var commentArray = NSMutableArray()
    var isFollowReq = false
    var follow_status = String()
    
    @IBOutlet weak var followView: UIView!
    @IBOutlet weak var followImageView: UIImageView!
    @IBOutlet weak var followLbl: UILabel!
    //chat menu view outlets
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var streamDetailContainerView: UIView!
    @IBOutlet weak var broadCastDetailsLabel: UILabel!
    @IBOutlet weak var menu_reportLabel: UILabel!
    @IBOutlet weak var streamMenuNameLabel: UILabel!
    @IBOutlet weak var publisherNameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet var reportView: UIView!
    @IBOutlet var reportviewTitleLbl: UILabel!
    @IBOutlet var reportTableview: UITableView!
    
    @IBOutlet var reportDeleteBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    func initialSetup()  {
        self.getStreamInfo()
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
        //        controls.
        loader.color = .white
        loader.startAnimating()
        self.fd_interactivePopDisabled = false
        self.fd_prefersNavigationBarHidden = true
        self.playPauseBtn.cornerRoundRadius()
        self.moreBtn.cornerRoundRadius()
        self.commentsBtn.cornerRoundRadius()
        self.commentsBtn.backgroundColor = UIColor.init(white: 255.0/255.0, alpha: 0.3)
        self.playPauseBtn.backgroundColor = UIColor.init(white: 255.0/255.0, alpha: 0.3)
        self.moreBtn.backgroundColor = UIColor.init(white: 255.0/255.0, alpha: 0.3)
        self.currentTimeLbl.config(color: .white, font: medium, align: .right, text: "")
        self.fullTimeLbl.config(color: .white, font: medium, align: .left, text: "")
        self.fullTimeLbl.text = "00:00"
        self.currentTimeLbl.text = "00:00"
        self.configDetailsContainerView()
        
        //        let urlstr = self.videoDict.value(forKey: "playback_url") as! String
        let stream_name = self.videoDict.value(forKey: "name") as! String
        let defaultDict:NSDictionary = UserModel.shared.getDefaults()!
        let host_address = defaultDict.value(forKeyPath: "stream_connection_info.stream_host") as? String
        video_url =  "http://\(host_address!):1935/vod/mp4:\(stream_name).mp4/playlist.m3u8"
        print("URL \(video_url)")
        
        if let url = URL.init(string:video_url) {
            player_item = VersaPlayerItem(url: url)
            playerView.set(item: player_item)
            playerView.playbackDelegate = self
            playerView.renderingView.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        
    }
    override func viewDidLayoutSubviews() {
            self.changeToRTL()
    }
    
  
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
            self.videoControls.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.currentTimeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.fullTimeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.commentsBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.playPauseBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.menuView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.streamMenuNameLabel.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.streamMenuNameLabel.textAlignment = .right
            self.streamMenuNameLabel.textAlignment = .right
            self.publisherNameLabel.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.publisherNameLabel.textAlignment = .right
            self.followLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.followImageView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.closebtn.frame = CGRect.init(x: -40, y: 25, width: 50, height: 50)
        }
    else{
            self.videoControls.transform = .identity
            self.currentTimeLbl.transform = .identity
            self.fullTimeLbl.transform = .identity
            self.commentsBtn.transform = .identity
            self.playPauseBtn.transform = .identity
            self.menuView.transform = .identity
            self.streamMenuNameLabel.transform = .identity
            self.streamMenuNameLabel.textAlignment = .left
            self.publisherNameLabel.transform = .identity
            self.publisherNameLabel.textAlignment = .left
            self.followLbl.transform = .identity
            self.followImageView.transform = .identity
            self.closebtn.frame = CGRect.init(x: FULL_WIDTH-65, y: 25, width: 50, height: 50)
        }
    }
    
    //get details from server
    func getStreamInfo(){
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        let stream_name = self.videoDict.value(forKey: "name") as! String
        requestDict.setValue(stream_name, forKey: "name")
        Obj.baseService(subURl: STREAM_INFO_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                self.setDetails(dict: dict!)
            }
        })
        
    }
    func configDetailsContainerView()  {
        self.streamDetailContainerView.frame = CGRect.init(x: 0, y: FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)

        broadCastDetailsLabel.config(color: .white, font: average, align: .left, text: "broadcast_details")
        menu_reportLabel.config(color: .white, font: average, align: .left, text: "")

        publisherNameLabel.config(color: .white, font: averageReg, align: .left, text: "")
        streamMenuNameLabel.config(color: .white, font: high, align: .left, text: "")
        self.reportviewTitleLbl.config(color: .white, font: averageReg, align: .center, text: "report")
        reportTableview.register(UINib(nibName: "ReportCell", bundle: nil), forCellReuseIdentifier: "ReportCell")
        commentTableView.register(UINib(nibName: "GiftCell", bundle: nil), forCellReuseIdentifier: "GiftCell")
        commentTableView.register(UINib(nibName: "ChatCommentCell", bundle: nil), forCellReuseIdentifier: "ChatCommentCell")
        self.commentTitleLbl.config(color: .white, font: averageReg, align: .center, text: "comments")
        self.commentsView.frame = CGRect.init(x: 0, y: FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)

        self.reportArray.removeAllObjects()
        self.reportArray.addObjects(from: UserModel.shared.getDefaults()?.value(forKey: "reports") as! [Any])
        self.reportTableview.reloadData()
        self.reportView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
        
        //follow status
        followView.cornerViewRadius()
        followView.layer.borderWidth = 1.0
        followView.layer.borderColor = UIColor.white.cgColor
        followLbl.config(color: .white, font: medium, align: .center, text: "follow")
        
        let followTap = UITapGestureRecognizer(target: self, action: #selector(self.followBtnTapped(_:)))
        self.followView.addGestureRecognizer(followTap)
        userImageView.makeItRound()
        colorView.backgroundColor = UIColor().setRandomColor()
        colorView.cornerViewRadius()
        colorView.alpha = 0.5
        self.configStreamInfoView()
    }
    
    func configStreamInfoView()  {
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.streamDetailContainerView.addGestureRecognizer(swipeDown)
        
        let cmtSwipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        cmtSwipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.commentsView.addGestureRecognizer(cmtSwipeDown)
        
        self.totalWatchCountLbl.config(color: .white, font: high, align: .center, text: "")
        let watch_count =  self.videoDict.value(forKey: "watch_count") as! NSNumber
        self.totalWatchCountLbl.text = watch_count.stringValue
        
        self.liveUsersLbl.config(color: .white, font: medium, align: .center, text: "total_viewers")
        self.timeDurationTitleLbl.config(color: .white, font: medium, align: .left, text: "duration")
        self.totalLikesTitleLbl.config(color: .white, font: medium, align: .left, text: "total_likes")
        self.likecountLbl.config(color: .white, font: medium, align: .center, text: "")
        let likes =  self.videoDict.value(forKey: "likes") as! NSNumber
        self.likecountLbl.text = likes.stringValue
        self.durationLbl.config(color: .white, font: medium, align: .center, text: "")
        self.durationLbl.text = self.currentTimeLbl.text
    }
    
    //set stream details
    func setDetails(dict:NSDictionary) {
        self.totalWatchCountLbl.text = "\(dict.value(forKey: "watch_count") as! String)"
        follow_status = dict.value(forKey: "follow") as! String
        if follow_status == "true"{
            self.followerCircleView()
        }else{
            self.followerRectangleView()
        }
        let username = dict.value(forKey: "posted_by") as? String
        publisherNameLabel.text = username
        streamMenuNameLabel.text = dict.value(forKey: "title") as? String
        
        let postedUserID = self.videoDict.value(forKey: "publisher_id") as? String
        if (postedUserID == UserModel.shared.userId) {
            self.followView.isHidden = true
            self.reportIcon.image = UIImage.init(named: "delete_icon")
            self.menu_reportLabel.text = Utility.language.value(forKey: "delete_broadcast") as? String
            self.reportDeleteBtn.addTarget(self, action: #selector(self.deleteBtnTapped(_:)), for: .touchUpInside)
        }else{
            self.reportDeleteBtn.addTarget(self, action: #selector(self.reportBtnTapped(_:)), for: .touchUpInside)
            let report_status = dict.value(forKey: "reported") as! String
            if report_status == "true"{
                self.menu_reportLabel.text = Utility.language.value(forKey: "undo_report") as? String
            }else{
                self.menu_reportLabel.text = Utility.language.value(forKey: "report") as? String
            }
        }
        userImageView.sd_setImage(with: URL(string: (dict.value(forKey: "publisher_image") as? String)!), placeholderImage: #imageLiteral(resourceName: "user_placeholder"))
        self.commentArray.removeAllObjects()
        self.commentArray.addObjects(from: dict.value(forKey: "comments") as! [Any])
        self.commentTableView.reloadData()

    }
    
    @objc func followBtnTapped(_ sender: UITapGestureRecognizer){
        if isFollowReq{
            self.statusAlert(msg: "already_request")
        }else{
            self.isFollowReq = true
            var updateStatus = String()
            if self.follow_status == "false" {
                updateStatus = "Follow"
            }else{
                updateStatus = "Unfollow"
            }
            let updateOBj = BaseWebService()
            let request = NSMutableDictionary()
            request.setValue(self.videoDict.value(forKey: "publisher_id"), forKey: "user_id")
            request.setValue(UserModel.shared.userId, forKey: "follower_id")
            request.setValue(updateStatus, forKey: "type")
            updateOBj.baseService(subURl: FOLLOW_API, params:  request as? Parameters, onSuccess: {response in
                let dict = response.result.value as? NSDictionary
                let status = dict?.value(forKey: "status") as! String
                self.isFollowReq = false
                if status == "true"{
                    if updateStatus == "Follow"{
                        self.follow_status =  "true"
                        self.followerCircleView()
                    }else{
                        self.follow_status = "false"
                        self.followerRectangleView()
                    }
                }
            })
        }
    }
    
    @IBAction func streamInfoBtnTapped(_ sender: Any) {
        menuView.isHidden = true
        reportView.isHidden = true
        stremDetailsView.isHidden = false
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            self.streamDetailContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
        }, completion: nil)
    }
    
    @objc func reportBtnTapped(_ sender: Any) {
        if menu_reportLabel.text == Utility.language.value(forKey: "undo_report") as? String{
            self.reportService(title: "",type:"undoreport")
        }else{
            menuView.isHidden = true
            stremDetailsView.isHidden = true
            self.reportView.isHidden = false
            self.streamDetailContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.reportView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
        }
    }
    
    func reportService(title:String,type:String) {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        if type == "report"{
            requestDict.setValue(title, forKey: "report")
        }
        let stream_name = self.videoDict.value(forKey: "name") as! String
        requestDict.setValue(stream_name, forKey: "name")
        Obj.baseService(subURl: REPORT_STREAM_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                if type == "report"{
                    self.statusAlert(msg: "reported_successfully")
                    self.menu_reportLabel.text = Utility.language.value(forKey: "undo_report") as? String
                }else{
                    self.statusAlert(msg: "reported_undo_successfully")
                    self.menu_reportLabel.text = Utility.language.value(forKey: "report") as? String
                }
                self.streamDetailContainerView.frame = CGRect.init(x: 0, y: FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
            }
        })
    }
    @objc func appMovedToBackground() {
        print("bg state")
        //send offline
        if playerView.isPlaying {
            playerView.pause()
            self.playPauseBtn.setImage(#imageLiteral(resourceName: "play_icon"), for: .normal)
        }
    }
    
    //MARK: Follow button animation view
    func followerCircleView()  {
        self.followView.frame = CGRect(x:self.menuView.frame.size.width-40,y:self.userImageView.frame.origin.y+10,width:35,height:35)
        self.followImageView.frame = CGRect(x:7,y:7,width:20.5,height:20.5)
        self.followView.addSubview(self.followImageView)
        self.followImageView.image = #imageLiteral(resourceName: "FollowTic")
        self.followView.backgroundColor = .white
        self.followLbl.text = ""
    }
    
    func followerRectangleView()  {
        self.followView.frame = CGRect(x:self.menuView.frame.size.width-115,y:self.userImageView.frame.origin.y+10,width:100,height:35)
        self.followImageView.frame = CGRect(x:10,y:7,width:20.5,height:20.5)
        self.followView.addSubview(self.followImageView)
        self.followImageView.image = #imageLiteral(resourceName: "follow_icon")
        self.followView.backgroundColor = .clear
        self.followLbl.text = (Utility.language.value(forKey: "follow") as! String)
    }
    // swipe reponse gesture
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.down:
                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
                    self.videoControls.isHidden = false
                    // Add the transformation in this block
                    self.streamDetailContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
                    self.commentsView.frame = CGRect.init(x: 0, y: FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
                }, completion: nil)
            default:
                break
            }
        }
    }
    
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        self.playerView.pause()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func playPauseBtnTapped(_ sender: Any) {
        
        if !loader.isAnimating{
            
            if  isCompletedPlaying || !playerView.isPlaying{
                isCompletedPlaying = false
                playerView.play()
                self.playPauseBtn.setImage(#imageLiteral(resourceName: "pause_icon"), for: .normal)
            }else{
                playerView.pause()
                self.playPauseBtn.setImage(#imageLiteral(resourceName: "play_icon"), for: .normal)
            }
        }
    }
    
    @objc func deleteBtnTapped(_ sender: Any) {
        let alertVC = CPAlertVC(title:APP_NAME, message:Utility.language.value(forKey: "delete_alert") as! String)
        alertVC.animationType = .bounceUp
        alertVC.addAction(CPAlertAction(title: Utility.language.value(forKey: "yes") as! String, type: .normal, handler: {
            self.deleteVideo()
        }))
        alertVC.addAction(CPAlertAction(title: Utility.language.value(forKey: "no") as! String, type: .cancel, handler: {
        }))
        alertVC.show(into: self)
    }
    
    //delete video
    func deleteVideo()  {
        let stream_name = self.videoDict.value(forKey: "name") as! String
        let obj = BaseWebService()
        obj.delete(subURl: "\(DELETE_VIDEO_API)/\(UserModel.shared.userId)/\(stream_name)", onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                self.deletedInfo!(self.selected_index)
                self.dismiss(animated: true, completion: nil)
            }
        })
        
    }
    
    @IBAction func commentsBtnTapped(_ sender: Any) {
        if self.commentArray.count != 0{
        self.videoControls.isHidden = true
        self.streamDetailContainerView.frame = CGRect.init(x: 0, y: FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            self.commentsView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
        }, completion: nil)
        }else{
            self.statusAlert(msg: "no_comments")

        }
    }
    
    
    //menu btn tapped
    @IBAction func menuBtnTapped(_ sender: Any) {
//        if !loader.isAnimating{
            UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
                // Add the transformation in this block
                self.menuView.isHidden = false
                self.reportView.isHidden = true
                self.stremDetailsView.isHidden = true
                self.streamDetailContainerView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
            }, completion: nil)
//        }
    }
    
    //player delegate
    func timeDidChange(player: VersaPlayer, to time: CMTime) {
        
        self.update(toTime: player.endTime().seconds.rounded(), lbl: self.fullTimeLbl,type:"duration")
        self.update(toTime: time.seconds.rounded(), lbl: self.currentTimeLbl,type:"current")
        self.update(toTime: player.endTime().seconds.rounded(), lbl: self.durationLbl,type:"duration")
    }
    //did end play
    func playbackDidEnd(player: VersaPlayer) {
        isCompletedPlaying = true
        let time =  CMTime(seconds: 0, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        player.seek(to: time)
        player.pause()
        self.playPauseBtn.setImage(#imageLiteral(resourceName: "play_icon"), for: .normal)
    }
    
    func playbackDidFailed(with error: VersaPlayerPlaybackError) {
        print("play error \(error)")
        
    }
    func update(toTime: TimeInterval,lbl:UILabel,type:String) {
        var timeFormat: String = "HH:mm:ss"
        if toTime <= 3599{
            timeFormat = "mm:ss"
        }
        let date = Date(timeIntervalSince1970: toTime)
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        formatter.dateFormat = timeFormat
        if type == "current" {
            if formatter.string(from: date) == "00:01" || formatter.string(from: date) == "00:00:01"{
                self.loader.stopAnimating()
            }
        }
        lbl.text = formatter.string(from: date)
    }
    
    //#MARK: Table view delegate & data source
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int{
       if (tableView == commentTableView){
            return self.commentArray.count
        }else if(tableView == reportTableview){
            return self.reportArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var customCell = UITableViewCell()
        if (tableView == commentTableView){
                   
                   let responseDict:NSDictionary = commentArray.object(at: indexPath.row) as! NSDictionary
                   let type:String = responseDict.value(forKey: "type") as! String
                   if(type == "text"){
                       let cmtCell = tableView.dequeueReusableCell(withIdentifier: "ChatCommentCell", for: indexPath) as! ChatCommentCell
                       cmtCell.comment(dict: responseDict)
                    tableView.rowHeight = cmtCell.cmtContainerView.frame.size.height+5

                       customCell = cmtCell
                   }else if (type == "gift"){
                       let gift = tableView.dequeueReusableCell(withIdentifier: "GiftCell", for: indexPath) as! GiftCell
                       gift.config(dict: responseDict)
                     tableView.rowHeight = 65

                       customCell = gift
                   }
               

        }else if(tableView == reportTableview){
            let report = tableView.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
            let reportDict = reportArray.object(at: indexPath.row) as! NSDictionary
            report.reportNameLbl?.text = "\(reportDict.value(forKey: "title") as! String)"
            report.contentView.backgroundColor = .clear
            report.backgroundColor = .clear
            tableView.rowHeight = 50
            customCell = report
            
        }
        return customCell
    }
  /*  // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        if(tableView == commentTableView){
            let responseDict:NSDictionary = commentArray.object(at: indexPath.row) as! NSDictionary
            let responseType:String = responseDict.value(forKey: "type") as! String
            if(responseType == "text"){
                let msg:String = responseDict.value(forKey: "message") as! String
                let txtView = UITextView()
                txtView.font = lite
                txtView.text = msg
                var fixedWidth =  txtView.intrinsicContentSize.width
                if fixedWidth > 240{
                    fixedWidth = 240
                }
                txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                let newSize =  txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                var newFrame =  txtView.frame
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                return newFrame.size.height + 25
                
            }else if(responseType == "gift"){
                return 65
            }
        }else if(tableView == reportTableview){
            return 50
        }
        return 0
    } */
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath){
        if(tableView == reportTableview){
            let reportDict = reportArray.object(at: indexPath.row) as! NSDictionary
            self.reportService(title: "\(reportDict.value(forKey: "title") as! String)",type:"report")
        }
    }
}
