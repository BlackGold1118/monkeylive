//
//  LiveListPage.swift
//  Livza
//
//  Created by HTS-Product on 26/08/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//



import UIKit
import SystemConfiguration
import MediaPlayer
import AVKit
import Foundation
import JTMaterialTransition
import StoreKit

class LiveListPage: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,RangeSeekSliderDelegate{
   
    var homePageArray = NSMutableArray()
    let refreshControl = UIRefreshControl()
    var pullDownScroll = Bool()
    var isMenuAnimated = Bool()
    var minAge = String()
    var maxAge = String()
    var lastOffset = CGFloat()
    var lastCount = Int()
    var transition: JTMaterialTransition?
    var selectedGender = 2
    let primeTempArray = NSMutableArray()
    var productIDs: Array<String?> = []
    
    @IBOutlet var showLbl: UILabel!
    @IBOutlet weak var video_collectionView: UICollectionView!
    @IBOutlet var rangeValueLbl: UILabel!
    @IBOutlet var ageLbl: UILabel!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var rangeView: RangeSeekSlider!
    @IBOutlet weak var no_videoLbl: UILabel!
    @IBOutlet weak var noVideoView: UIView!
    @IBOutlet var filterView: UIView!
    @IBOutlet var loader: UIActivityIndicatorView!
    @IBOutlet var genderFilterSegment: TTSegmentedControl!
    @IBOutlet var filterLbl: UILabel!
    @IBOutlet var filterContainerView: UIView!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var locationBtn: UIButton!
    @IBOutlet var locationArrowBtn: UIButton!
    @IBOutlet weak var applogo: UIImageView!
    @IBOutlet weak var filterbtn: UIButton!
    @IBOutlet weak var rightbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        //configure collection view cell
        video_collectionView.register(UINib(nibName: "HomePageCell", bundle: nil), forCellWithReuseIdentifier: "HomePageCell")
        loader.startAnimating()
        Utility.shared.setDefaultFilter()
        self.initialSetup()
        self.getLiveVideos(offset:"0")
        self.filterContainerView.isHidden = true
        self.filterContainerView.frame = CGRect.init(x: 0, y: -FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
        self.changeToRTL()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialSetup()
        self.changeToRTL()
        if isSocketConnected{
            self.checkNotificationRedirect()
            }
        self.setUpNavigationBar()
        self.fd_prefersNavigationBarHidden = true
        self.fd_interactivePopDisabled = true
        
        if !UserModel.shared.profileComplete { // all basic details updated
            let obj = RegisterInfo()
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.completedInfo = { gem_count in
                self.loader.startAnimating()
                self.getLiveVideos(offset:"0")
                self.navigationController?.dismiss(animated:false, completion: nil)
            }
            self.navigationController?.present(obj, animated: false, completion: nil)
        }else{
            self.tabBarController?.tabBar.isHidden = false
            UserModel.shared.getTurnServers()
            UserModel.shared.updateProfile()
            Utility.shared.getAdminMsg()
            self.configFilterView()
        }
        self.configFilterView()
    }
    
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Setup navigation bar
    func setUpNavigationBar()  {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationView?.backgroundColor = BLACK_COLOR
        let rightBarButton = UIBarButtonItem(customView: navigationView)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    
    func changeToRTL(){
    
        if UserModel.shared.language == "Arabic"{
            self.view.transform = .identity
            self.filterView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.navigationView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.rightbtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.applogo.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.rangeValueLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.genderFilterSegment.transform = CGAffineTransform(scaleX: -1, y: 1)

        }
        else{
            self.view.transform = .identity
            self.filterView.transform = .identity
            self.navigationView.transform = .identity
            self.rightbtn.transform = .identity
            self.applogo.transform = .identity
            self.genderFilterSegment.transform = .identity
            self.rangeValueLbl.transform = .identity
        }
    }
    
    //MARK: Initial setup
    func configureView() {
     
        self.tabBarController?.tabBar.backgroundColor = BLACK_COLOR
        self.tabBarController?.tabBar.barTintColor = BLACK_COLOR
        //set collectionview layot
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .vertical
        video_collectionView.collectionViewLayout = collectionViewFlowLayout
        no_videoLbl.config(color: TEXT_SECONDARY_COLOR, font: medium, align: .center, text: "no_videos")
        //set activity loader
        pullDownRefresh()
        configureLoadMoreView()
        
 
    }
    func initialSetup()  {
        self.primeStoreInfo()
        minAge = "18"
        maxAge = "99"
        self.transition = JTMaterialTransition(animatedView: self.centerPoint())
        self.navigationView.backgroundColor = BLACK_COLOR
        self.isMenuAnimated = false
        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self.filterContainerView)
    }
    func configFilterView()  {
        self.genderFilterSegment.selectItemAt(index: self.selectedGender)
        self.filterLbl.config(color: .white, font: averageReg, align: .center, text: "filter")
        self.showLbl.config(color: .white, font: medium, align: .left, text: "show_me")
        self.genderFilterSegment.itemTitles = [Utility.language.value(forKey: "guys"),Utility.language.value(forKey: "girls"),Utility.language.value(forKey: "both")] as! [String]
        self.genderFilterSegment.backgroundColor = .clear
        self.genderFilterSegment.containerBackgroundColor = .clear
        self.genderFilterSegment.cornerRadius = 20
        self.genderFilterSegment.selectedTextFont = medium!
        self.genderFilterSegment.defaultTextFont = medium!
        self.genderFilterSegment.defaultTextColor = .white
        self.genderFilterSegment.selectedTextColor = .white
        self.genderFilterSegment.useGradient = false
        self.genderFilterSegment.useShadow = false
        self.genderFilterSegment.thumbColor = PRIMARY_COLOR
        self.genderFilterSegment.allowChangeThumbWidth = false
        self.genderFilterSegment.didSelectItemWith = { index, title in
            print(index)
            self.selectedGender = index
            if index == 0 {
                UserDefaults.standard.setValue("male", forKey: "gender")
            }else if index == 1{
                UserDefaults.standard.setValue("female", forKey: "gender")
            }else{
                UserDefaults.standard.setValue("both", forKey: "gender")
            }

        }
        self.locationBtn.config(color: PRIMARY_COLOR, font: lite, align: .right, title: "")
        self.locationLbl.config(color: .white, font: medium, align: .left, text: "location")
        let img = #imageLiteral(resourceName: "right_arrow").withRenderingMode(.alwaysTemplate)
        self.locationArrowBtn.setImage(img, for: .normal)
        self.locationArrowBtn.tintColor = .white
        let allLoc = Utility.language.value(forKey: "select_all") as! String
        let locationArray = NSMutableArray()
        if UserDefaults.standard.value(forKeyPath: "location") != nil{
        locationArray.addObjects(from: UserDefaults.standard.value(forKeyPath: "location") as! [Any])
        }
        if locationArray.contains(allLoc){
            self.locationBtn.config(color: PRIMARY_COLOR, font: lite, align: .right, title: "world_wide")
        }else{
            self.locationBtn.setTitle(locationArray.componentsJoined(by: ","), for: .normal)
        }
        
        self.ageLbl.config(color: .white, font: medium, align: .left, text: "age")
        self.rangeValueLbl.config(color: .white, font: medium, align: .right, text: "")
        self.rangeValueLbl.text = "\(minAge)-\(maxAge)"

        self.rangeView.minValue = 18
        self.rangeView.maxValue = 99
        self.rangeView.minDistance = 1
        self.rangeView.delegate = self
        self.rangeView.hideLabels = true
        self.rangeView.selectedMinValue = CGFloat((minAge as NSString).floatValue)
        self.rangeView.selectedMaxValue = CGFloat((maxAge as NSString).floatValue)
        print("max sel \(self.rangeView.selectedMaxValue)")
        self.rangeView.lineHeight = 2.0
        self.rangeView.handleColor = PRIMARY_COLOR
        self.rangeView.colorBetweenHandles = PRIMARY_COLOR
        self.rangeView.handleBorderColor = TEXT_SECONDARY_COLOR


    }
    
    @IBAction func tickBtnTapped(_ sender: Any) {
        UserDefaults.standard.setValue("1", forKey: "filter_applied")
        self.getLiveVideos(offset:"0")
        if !isMenuAnimated {
            isMenuAnimated = true
            self.fd_interactivePopDisabled = true
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseIn, animations: {
            self.filterContainerView.frame = CGRect.init(x: 0, y:-FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
        }, completion: { _ in
            self.filterContainerView.isHidden = true
            self.isMenuAnimated = false
        })
        }
    }
    
    @IBAction func locationBtnTapped(_ sender: Any) {
        self.filterContainerView.frame = CGRect.init(x: 0, y:-FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
        let locObj = LocationListPage()
        locObj.changesDone = {
            self.filterContainerView.isHidden = false
            self.filterContainerView.frame = CGRect.init(x: 0, y:0, width: FULL_WIDTH, height: FULL_HEIGHT)
        }
        self.navigationController!.pushViewController(locObj, animated: true)
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        
        Utility.shared.setDefaultFilter()
        self.getLiveVideos(offset:"0")
        if !isMenuAnimated {
            self.fd_interactivePopDisabled = true
            isMenuAnimated = true
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseIn, animations: {
            self.filterContainerView.frame = CGRect.init(x: 0, y:-FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
        }, completion: { _ in
            self.filterContainerView.isHidden = true
            self.isMenuAnimated = false
        })
        }
        minAge = "18"
        maxAge = "99"
        selectedGender = 2
        self.configFilterView()
        self.configFilterView()

    }
    
    @IBAction func filterBtnTapped(_ sender: Any) {
       
     if !isMenuAnimated {
            self.fd_interactivePopDisabled = false
            isMenuAnimated = true
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseIn, animations: {
            self.filterContainerView.isHidden = false
            self.filterContainerView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
            UIApplication.shared.keyWindow?.rootViewController?.view.bringSubviewToFront(self.filterContainerView)
        }, completion: { _ in
            self.isMenuAnimated = false
        })
        } 
    }
    
    @IBAction func tapDismiss(_ sender: Any) {
        print("Filter Dismissed")
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseIn, animations: {
                   self.filterContainerView.frame = CGRect.init(x: 0, y:-FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
               }, completion: { _ in
                   self.filterContainerView.isHidden = true
                   self.isMenuAnimated = false
               })
    }
    
 
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(velocity.y>0) {

            UIView.animate(withDuration: 0.8, delay: 0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                self.video_collectionView.frame = CGRect.init(x: 0, y:0 , width: FULL_WIDTH, height: FULL_HEIGHT)
                self.navigationView.isHidden = true
                self.tabBarController?.tabBar.isHidden = true
                print("Hide")
//                self.navigationView.frame.origin.y -= self.navigationView.frame.size.height
            }, completion: nil)
            
        } else {

            UIView.animate(withDuration: 0.8, delay: 0, options:UIView.AnimationOptions.transitionCrossDissolve, animations: {
                print("Unhide")
                self.navigationView.isHidden = false
                let collection_top = self.navigationView.frame.size.height
                self.video_collectionView.frame = CGRect.init(x: 0, y:collection_top-1 , width: FULL_WIDTH, height: FULL_HEIGHT-collection_top)
//                self.navigationView.frame.origin.y = 0
                self.tabBarController?.tabBar.isHidden = false
            }, completion: nil)
        }
    }
   
  
    
    
    //MARK: slider delegate
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        self.rangeValueLbl.text = "\(Int.init(minValue.rounded()))-\(Int.init(maxValue.rounded()))"
        UserDefaults.standard.setValue(Int.init(minValue.rounded()), forKey: "min_age")
        UserDefaults.standard.setValue(Int.init(maxValue.rounded()), forKey: "max_age")
    }
    
    //MARK: Pulldown refresh
    func pullDownRefresh()  {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        video_collectionView.tintColor = UIColor.white
        video_collectionView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.video_collectionView.removeInfiniteScroll()
            self?.getLiveVideos(offset:"0")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.video_collectionView.dg_stopLoading()
                self?.pullDownScroll = true
                self?.configureLoadMoreView()
            })
            }, loadingView: loadingView)
        video_collectionView.dg_setPullToRefreshFillColor(BLACK_COLOR)
        video_collectionView.dg_setPullToRefreshBackgroundColor(video_collectionView.backgroundColor!)
    }
        
    //MARK: Configure Load More
    func configureLoadMoreView()  {
        let frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        video_collectionView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: frame)
        video_collectionView.addInfiniteScroll { (collectionView) -> Void in
            self.video_collectionView.performBatchUpdates({ () -> Void in
                // update collection view
                let offset : Int = self.homePageArray.count
                let offsetString = String(offset)
                if(self.homePageArray.count == 0 || self.pullDownScroll){
                    self.getLiveVideos(offset:"0")
                    self.pullDownScroll = false
                }else{
                    if self.homePageArray.count%20 == 0{
                        self.getLiveVideos(offset:offsetString)
                    }
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                self.video_collectionView.finishInfiniteScroll()
            });
        }
    }
    
    //MARK: Collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.homePageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : HomePageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePageCell", for: indexPath) as! HomePageCell
        if (homePageArray.count > indexPath.row){
            let videoDetails:NSDictionary =  homePageArray.object(at: indexPath.row) as! NSDictionary
            cell.configureCellwithDetails(videoDict: videoDetails)
        }
        if UserModel.shared.language == "Arabic"{
//            cell.subscriberCountLabel.textAlignment = .right
            cell.userNameLabel.textAlignment = .right
//            cell.liveLabel.textAlignment = .center
            cell.videoDescribtionLabel.textAlignment = .right
//            cell.liveLabel.textAlignment = .right
        }
        else{
            cell.subscriberCountLabel.textAlignment = .left
            cell.userNameLabel.textAlignment = .left
//            cell.liveLabel.textAlignment = .center
            cell.videoDescribtionLabel.textAlignment = .left
            cell.liveLabel.textAlignment = .left
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewSize = video_collectionView.frame.size.width-30
        return CGSize(width: collectionViewSize/2, height: (collectionViewSize/2)-30)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let videoDetails:NSDictionary
        
            videoDetails =  homePageArray.object(at: indexPath.row) as! NSDictionary
        let videoType = (videoDetails.value(forKey: "type") as? String)!
        if (videoType == "recorded") {
//            let playback_ready = (videoDetails.value(forKey: "playback_ready") as? String)!
//            if playback_ready == "yes"{
            
            let videoObj = VideoPlayer()
            videoObj.modalPresentationStyle = .custom
            videoObj.transitioningDelegate = self.transition
            videoObj.videoDict = videoDetails
            videoObj.selected_index = indexPath.row
            videoObj.deletedInfo = { index in
                self.homePageArray.removeObject(at: index)
                self.video_collectionView.reloadData()
            }
            self.navigationController?.present(videoObj, animated: true, completion: nil)
//            }else{
//                self.showAlert(msg: "playback_not_ready_yet")
//            }
        }else{
            let subscriberObj = SubscriberLivePage()
            subscriberObj.modalPresentationStyle = .custom
            subscriberObj.transitioningDelegate = self.transition
            subscriberObj.streamDict = videoDetails
            subscriberObj.stream_name = videoDetails.value(forKey: "name") as! String
            self.navigationController?.present(subscriberObj, animated: true, completion: nil)
        }
    }
    
    // Layout: Set Edges
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }

    //MARK: Get Live List
    func getLiveVideos(offset:String){
            let Obj = BaseWebService()
            let requestDict = NSMutableDictionary.init()
            requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
            requestDict.setValue(UserModel.shared.userId, forKey: "profile_id")
            requestDict.setValue("", forKey: "search_key")
            requestDict.setValue("recent", forKey: "sort_by")
            requestDict.setValue(offset, forKey: "offset")
            requestDict.setValue("20", forKey: "limit")
            requestDict.setValue("live", forKey: "type")
       
        //filter params
        if UserDefaults.standard.value(forKeyPath: "filter_applied") == nil {//filter not applied
            requestDict.setValue("false", forKey: "filter_applied")
            requestDict.setValue("", forKey: "gender")
            requestDict.setValue("", forKey: "min_age")
            requestDict.setValue("", forKey: "max_age")
        }else{
        let  filterEnable = UserDefaults.standard.value(forKeyPath: "filter_applied") as! String
        let locationArray = NSMutableArray()
        let allLoc = Utility.language.value(forKey: "select_all") as! String
        locationArray.addObjects(from: UserDefaults.standard.value(forKeyPath: "location") as! [Any])
        if locationArray.contains(allLoc){
            locationArray.add("global")
        }
        
        if filterEnable == "1" { //filter applied
            requestDict.setValue("true", forKey: "filter_applied")
            requestDict.setValue(UserDefaults.standard.value(forKeyPath: "gender"), forKey: "gender")
            requestDict.setValue(UserDefaults.standard.value(forKeyPath: "min_age"), forKey: "min_age")
            requestDict.setValue(UserDefaults.standard.value(forKeyPath: "max_age"), forKey: "max_age")
        }else{ // not applied
            requestDict.setValue("false", forKey: "filter_applied")
            requestDict.setValue("", forKey: "gender")
            requestDict.setValue("", forKey: "min_age")
            requestDict.setValue("", forKey: "max_age")
        }
        
        requestDict.setValue(self.json(from: locationArray), forKey: "location")
        }

            Obj.baseService(subURl: LIVE_STREAM_API, params: requestDict as? Parameters, onSuccess: {response in
                let dict = response.result.value as? NSDictionary
                let status = dict?.value(forKey: "status") as! String
                self.loader.stopAnimating()
                self.video_collectionView.finishInfiniteScroll()

                if status == "true"{
                    if(offset == "0"){
                        self.lastCount = 0
                        self.homePageArray.removeAllObjects()
                    }else{
                        self.lastCount = self.homePageArray.count
                    }
                    self.homePageArray.addObjects(from: dict?.value(forKey: "result") as! [Any])
                    if(self.homePageArray.count != 0){
                        DispatchQueue.main.async {
                            if(offset == "0"){
                                self.checkAvailability() //load more
                            }else{
                                self.checkAvailability() //starting
                            }
                        }
                    }
                }else{
                    if(offset == "0"){
                            self.lastCount = 0
                            self.homePageArray.removeAllObjects()
                            self.video_collectionView.reloadData()
                    }
                    self.loader.stopAnimating()
                    self.checkAvailability()
                }
            })
    }
    
    //MARK: check if videos is empty
    func checkAvailability() {
        if (homePageArray.count == 0) {
            self.noVideoView.isHidden = false
            self.view.bringSubviewToFront(self.noVideoView)
            loader.stopAnimating()
        }else{
            self.video_collectionView.isHidden = false;
            self.noVideoView.isHidden = true
            self.video_collectionView.reloadData()
//            if self.homePageArray.count <= 20{ // first time animation
//                let trans = AnimationType.from(direction: .top, offset: 10)
//                video_collectionView?.performBatchUpdates({
//                    UIView.animate(views: self.video_collectionView!.orderedVisibleCells,
//                                   animations: [trans], completion: nil)
//                }, completion: nil)
//            }
        }
    }
 
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    //redirect notification
    func checkNotificationRedirect()  {
        if Utility().isNotificationRedirect {
            let msgDict = Utility.shared.getNotificationInfo()
            let scope = msgDict?.value(forKey: "scope") as! String
            if scope == "txtchat" || scope == "admin"{
                let user_id = msgDict?.value(forKey: "user_id") as! String
                let user_image = msgDict?.value(forKey: "user_image") as! String
                let user_name = msgDict?.value(forKey: "user_name") as! String
                let msgDetail = MessageDetailPage()
                msgDetail.contactID = user_id
                msgDetail.contactName = user_name
                msgDetail.contactImg = user_image
                msgDetail.isBlocked = false
                msgDetail.newChanges = {
                }
                self.navigationController?.pushViewController(msgDetail, animated: true)
            }else if scope == "follow"{
                let user_id = msgDict?.value(forKey: "user_id") as! String
                let profile = ProfilePage()
                profile.viewType = "fromMsg"
                profile.profileId = user_id
                self.navigationController?.pushViewController(profile, animated: true)
            }else if scope == "video_call"{
                
                DispatchQueue.main.async {
                    appDelegate.endCall()
                    UserModel.shared.alreadyInCall = true
                    let videoObj = VideoCallPage()

                    videoObj.user_name = msgDict!.value(forKey: "user_name") as! String
                    videoObj.user_img = msgDict!.value(forKey: "user_image") as! String
                    videoObj.sender = false
                    videoObj.platform =  msgDict!.value(forKey: "platform") as! String
                    videoObj.viewFrom = "callkit"
                    videoObj.modalPresentationStyle = .fullScreen
                    videoObj.user_id = msgDict!.value(forKey: "user_id") as! String
                    videoObj.room_id = msgDict!.value(forKey: "room_id") as! String
                    self.navigationController?.present(videoObj, animated: true, completion: nil)
                }
                
            }
           
            Utility().clearRedirectionInfo()
        }
    }
}


//MARK: GET PRIME INFO FROM STORE
extension LiveListPage: SKProductsRequestDelegate,SKPaymentTransactionObserver,SKRequestDelegate{
    
    func primeStoreInfo()  {
        let defaultDict = UserModel.shared.getDefaults()
        (defaultDict?.value(forKey: "membership_packages") as? [Any]).map(primeTempArray.addObjects)
        
        for prime in self.primeTempArray {
            let gemdict = prime as! NSDictionary
            let productid = gemdict.value(forKey: "subs_title")
            self.productIDs.append((productid as! String))
        }
        self.requestProductInfo()
        if !UserModel.shared.userId.isEmpty{
            if UserModel.shared.isPremium || UserModel.shared.alreadySubscribed { // already prime
                DispatchQueue.main.async {
                    Utility.shared.iapActiveStatus()
                }
            }
        }
    }
    
    //check in app product information by using productids
    func requestProductInfo() {
        SKPaymentQueue.default().add(self)
        if SKPaymentQueue.canMakePayments() {
            let productIdentifiers = NSSet(array: productIDs as [Any])
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as Set<NSObject> as! Set<String>)
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            print("Cannot perform In App Purchases.")
        }
    }
    
    //refresh delegate
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        for transaction in queue.transactions {
            var trans = SKPaymentTransaction()
            trans = transaction
            if transaction.transactionState == .purchased {
                // Pro Purchased
                print("check already done \(transaction.payment.productIdentifier)")
            }
            
        }
    }
    
    //store kit delegates
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if response.products.count != 0 {
            productIDs.removeAll()
            let product = response.products[0]
            let primeDict = NSMutableDictionary()
            primeDict.setValue(product.price.stringValue, forKey: "price")
            primeDict.setValue(product.priceLocale.currencySymbol!, forKey: "currency")
            if #available(iOS 11.2, *) {
                primeDict.setValue("\((NSNumber.init(value: (product.subscriptionPeriod?.numberOfUnits)!)).stringValue)\(unitName(unitRawValue: (product.subscriptionPeriod?.unit.rawValue)!))", forKey: "validity")
            } else {
                // Fallback on earlier versions
            }
            
            Utility.shared.primePackage = primeDict
            
        }
        else {
            print("There are no products.")
        }
    }
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            print("iap details \(transaction.transactionState), \(transaction.payment.productIdentifier)")
            if transaction.transactionState == .purchased {
                // Pro Purchased
                print("check  \(transaction.payment.productIdentifier)")
            }
            switch transaction.transactionState {
            case .purchasing: break
            default: queue.finishTransaction(transaction)
            }
        }
    }
    
    func unitName(unitRawValue:UInt) -> String {
        switch unitRawValue {
        case 0: return "D"
        case 1: return "W"
        case 2: return "M"
        case 3: return "Y"
        default: return ""
        }
}

}


