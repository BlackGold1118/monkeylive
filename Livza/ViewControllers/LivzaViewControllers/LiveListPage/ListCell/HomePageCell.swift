//
//  HomePageCell.swift
//  HSLiveStream
//
//  Created by APPLE on 23/01/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit
import Alamofire

class HomePageCell: UICollectionViewCell {

    @IBOutlet weak var liveLabel: UILabel!
    @IBOutlet weak var subscriberView: UIView!
    @IBOutlet weak var subscriberCountLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var videoDescribtionLabel: UILabel!
    @IBOutlet weak var video_ImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setUpView()
    }
    //MARK: Initial set up view
    func setUpView()  {
        self.contentView.cornerViewMiniumRadius()
        liveLabel.layer.cornerRadius = liveLabel.frame.size.height/2
        liveLabel.clipsToBounds = true
        liveLabel.backgroundColor = LIVE_COLOR
        subscriberView.cornerViewRadius()
        subscriberCountLabel.config(color: .white, font: low, align: .center, text: EMPTY_STRING)
        userNameLabel.config(color: .white, font: small, align: .left, text: EMPTY_STRING)
        videoDescribtionLabel.config(color: .white, font: low, align: .left, text: EMPTY_STRING)
       
        
    }
    
   
    //MARK: set values
    func configureCellwithDetails(videoDict:NSDictionary) {
        var videoType:String
        videoType = (videoDict.value(forKey: "type") as? String)!
        if (videoType == "recorded") {
            liveLabel.config(color: .white, font: small, align: .center, text: "")
            let duration = videoDict.value(forKey: "playback_duration") as? String
            if duration == "0:"{
                liveLabel.text = ""
            }else{
                liveLabel.text = duration
            }
            liveLabel.shadowColor = .lightGray
            liveLabel.shadowOffset = CGSize.init(width: 1, height: 1)
            liveLabel.backgroundColor = .clear
        }else{
            liveLabel.config(color: .white, font: low, align: .center, text: "live")
            liveLabel.backgroundColor = LIVE_COLOR
            liveLabel.shadowColor = .clear
            
        }
        userNameLabel.text = videoDict.value(forKey: "title") as? String
        let watch_count =  videoDict.value(forKey: "watch_count") as! NSNumber
        subscriberCountLabel.text = watch_count.stringValue
        videoDescribtionLabel.text = videoDict.value(forKey: "posted_by") as? String
   
        video_ImageView.sd_setImage(with: URL(string: (videoDict.value(forKey: "publisher_image") as? String)!), placeholderImage: #imageLiteral(resourceName: "user_placeholder"))
        video_ImageView.contentMode = .scaleAspectFill
        video_ImageView.clipsToBounds  = true   
        video_ImageView.layer.masksToBounds = true
    }
   
    
}
