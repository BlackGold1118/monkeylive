//
//  UserVideoListPage.swift
//  HSLiveStream
//
//  Created by APPLE on 20/02/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit
import JTMaterialTransition

class UserVideoListPage: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var videoArray = NSMutableArray()
    var profileID = String()
    var userName = String()
    var backImgURL = String()
    var transition: JTMaterialTransition?

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var backGroundImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var videoListTableView: UITableView!
    @IBOutlet weak var no_videoLbl: UILabel!
    @IBOutlet weak var noVideoView: UIView!
    
    @IBOutlet var loader: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpThisview()
        // Do any additional setup after loading the view.
        self.loadMoreBroadCastVideos()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("called view will appear")
        self.changeToRTL()
    }

    func changeToRTL() {
    
        if UserModel.shared.language == "Arabic" {
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.titleLabel.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.titleLabel.textAlignment = .center
//            self.userName.transform = CGAffineTransform(scaleX: -1, y: 1)
//            self.userName.textAlignment = .center
            
        }
        else
        {
            self.view.transform = .identity
            self.titleLabel.transform = .identity
            self.titleLabel.textAlignment = .center
//            self.userName.transform = .identity
//            self.userName.textAlignment = .center
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:initial setup
    func setUpThisview()   {
        self.transition = JTMaterialTransition(animatedView: self.centerPoint())

        self.navigationController?.isNavigationBarHidden = true
        self.fd_prefersNavigationBarHidden = true
        //add loader
        no_videoLbl.config(color: TEXT_SECONDARY_COLOR, font: medium, align: .center, text:"no_videos")
        self.noVideoView.isHidden = true

        backGroundImageView.sd_setImage(with: URL(string: backImgURL), placeholderImage: #imageLiteral(resourceName: "user_placeholder"))
        
        self.view.backgroundColor = TEXT_SECONDARY_COLOR
        videoListTableView.register(UINib(nibName: "RecentVideoCell", bundle: nil), forCellReuseIdentifier: "RecentVideoCell")
        videoListTableView.register(UINib(nibName: "RecordVideoCell", bundle: nil), forCellReuseIdentifier: "RecordVideoCell")

        if userName == EMPTY_STRING {
            titleLabel.config(color: .white, font: averageReg, align: .center, text: "recent_videos")
        }else{
            titleLabel.config(color: .white, font: averageReg, align: .center, text: "")
            titleLabel.text = "\(Utility.language.value(forKey: "recent_videos") as! String) \(Utility.language.value(forKey: "for") as! String) \(userName)"
        }
        
    }
 
    //MARK: check if videos is empty
    func checkAvailability() {
        if (videoArray.count == 0) {
            self.videoListTableView.isHidden = true;
            self.noVideoView.isHidden = false
        }else{
            self.videoListTableView.isHidden = false;
            self.noVideoView.isHidden = true
            self.videoListTableView.reloadData()
        }
    }
    
    
    //MARK:Load more broadcast
    func loadMoreBroadCastVideos()  {
        loader.startAnimating()
        let frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        videoListTableView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: frame)
        videoListTableView.beginInfiniteScroll(false)
        videoListTableView.addInfiniteScroll { (collectionView) -> Void in
            let offset : Int = self.videoArray.count
            let offsetString = String(offset)
            if(self.videoArray.count == 0){
                self.getLiveVideos(offset: "0")
            }else{
                self.getLiveVideos(offset: offsetString)
            }
            // finish infinite scroll animations
            self.videoListTableView.finishInfiniteScroll()
        }
    }
   

    
    //MARK: Get Live List
    func getLiveVideos(offset:String){
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue(self.profileID, forKey: "profile_id")
        requestDict.setValue("", forKey: "search_key")
        requestDict.setValue("recent", forKey: "sort_by")
        requestDict.setValue(offset, forKey: "offset")
        requestDict.setValue("20", forKey: "limit")
        requestDict.setValue("user", forKey: "type")
        //filter params
        requestDict.setValue("false", forKey: "filter_applied")
        requestDict.setValue("", forKey: "gender")
        requestDict.setValue("", forKey: "min_age")
        requestDict.setValue("", forKey: "max_age")
        
        Obj.baseService(subURl: LIVE_STREAM_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            self.loader.stopAnimating()
            if status == "true"{
                if(offset == "0"){
                    self.videoArray.removeAllObjects()
                }
                self.videoArray.addObjects(from: dict?.value(forKey: "result") as! [Any])
                self.view.isUserInteractionEnabled = true
                if(self.videoArray.count != 0){
                    DispatchQueue.main.async {
                        self.checkAvailability() //load more
                    }
                }
                
            }else{
                self.loader.stopAnimating()
                self.checkAvailability()
            }
        })
    }
    
    

    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int{
        return videoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var customCell = UITableViewCell()
        let videoDict:NSDictionary =  videoArray.object(at: indexPath.row) as! NSDictionary
        let type:String = (videoDict.value(forKey: "type") as? String)!
        if type == "recorded" {
            let recordCell = tableView.dequeueReusableCell(withIdentifier: "RecordVideoCell", for: indexPath) as! RecordVideoCell
            recordCell.configureCellWithDetails(videoDict: videoDict,viewType:EMPTY_STRING)
            customCell = recordCell
            if UserModel.shared.language == "Arabic" {
                recordCell.createdTimeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                recordCell.createdTimeLbl.textAlignment = .right
                recordCell.postedbyLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                recordCell.postedbyLbl.textAlignment = .right
                recordCell.videoDescriptionLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                recordCell.videoDescriptionLbl.textAlignment = .right
            }
            else
            {
                recordCell.createdTimeLbl.transform = .identity
                recordCell.createdTimeLbl.textAlignment = .left
                recordCell.postedbyLbl.transform = .identity
                recordCell.postedbyLbl.textAlignment = .left
                recordCell.videoDescriptionLbl.transform = .identity
                recordCell.videoDescriptionLbl.textAlignment = .left
            }
        }else{
        let liveCell = tableView.dequeueReusableCell(withIdentifier: "RecentVideoCell", for: indexPath) as! RecentVideoCell
            liveCell.configureCellWithDetails(videoDict: videoDict,viewType:EMPTY_STRING)
            customCell = liveCell
        }
        return customCell
    }
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 110
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath){
      let  videoDetails =  videoArray.object(at: indexPath.row) as! NSDictionary
        var videoType:String
        videoType = (videoDetails.value(forKey: "type") as? String)!
        if (videoType == "recorded") {
            let videoObj = VideoPlayer()
            videoObj.modalPresentationStyle = .custom
            videoObj.transitioningDelegate = self.transition
            videoObj.videoDict = videoDetails
            videoObj.selected_index = indexPath.row
            videoObj.deletedInfo = { index in
                print("deleted index \(index)")
                self.videoArray.removeObject(at: index)
                self.videoListTableView.reloadData()
            }
            self.navigationController?.present(videoObj, animated: true, completion: nil)
            
            
        }else{
            
            let subscriberObj = SubscriberLivePage()
            subscriberObj.modalPresentationStyle = .custom
            subscriberObj.transitioningDelegate = self.transition
            subscriberObj.streamDict = videoDetails
            subscriberObj.stream_name = videoDetails.value(forKey: "name") as! String
            self.navigationController?.present(subscriberObj, animated: true, completion: nil)
            
        }
    }

}
