//
//  SubscriberLivePage.swift
//  Livza
//
//  Created by HTS-Product on 05/09/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import WowzaGoCoderSDK
import IQKeyboardManagerSwift
import GrowingTextView
import SocketIO

class SubscriberLivePage: UIViewController,WOWZStatusCallback,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,GrowingTextViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var followView: UIView!
    @IBOutlet weak var followImageView: UIImageView!
    @IBOutlet weak var followLbl: UILabel!
    @IBOutlet weak var userlistCollectionView: UICollectionView!
    @IBOutlet weak var watchCountView: UIView!
    @IBOutlet weak var watchingCountLbel: UILabel!
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet var msgView: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var msgTxtView: GrowingTextView!
    @IBOutlet var giftBtn: UIButton!
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var chatUserContainerView: UIView!
    @IBOutlet weak var streamNameLabel: UILabel!
    @IBOutlet weak var preview_imgView: UIImageView!
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var CustomPreview: UIView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var postedByLabel: UILabel!
    @IBOutlet weak var postedUserImageView: UIImageView!
    
    @IBOutlet var postedImgOverlay: UIView!
    @IBOutlet weak var liveLabel: UILabel!
    @IBOutlet weak var subscriberCountView: UIView!
    //chat menu view outlets
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var hideLabel: UILabel!
    @IBOutlet weak var streamDetailContainerView: UIView!
    @IBOutlet weak var broadCastDetailsLabel: UILabel!
    @IBOutlet weak var menu_liveLabel: UILabel!
    @IBOutlet weak var menu_reportLabel: UILabel!
    @IBOutlet weak var menu_ViewCount: UILabel!
    @IBOutlet weak var menu_watchCountView: UIView!
    @IBOutlet weak var streamMenuNameLabel: UILabel!
    @IBOutlet weak var publisherNameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var menuUserListView: UIView!
    @IBOutlet weak var liveViewersLabel: UILabel!
    @IBOutlet weak var totalCountLbl: UILabel!
    @IBOutlet weak var totalViewersLabel: UILabel!
    @IBOutlet weak var userlistTableView: UITableView!
    @IBOutlet weak var settingsButton:UIButton!
    @IBOutlet weak var previewView:UIView!
    @IBOutlet weak var stickersView: UIView!
    @IBOutlet weak var stickerCollectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var giftSendView: UIView!
    @IBOutlet var giftTitleLbl: UILabel!
    @IBOutlet var giftSendBtn: UIButton!
    @IBOutlet var reportView: UIView!
    @IBOutlet var reportviewTitleLbl: UILabel!
    @IBOutlet var reportTableview: UITableView!
    
    @IBOutlet var loaderView: DotsLoader!
    @IBOutlet var loaderStatusLbl: UILabel!
    
    let chat_Hide_Seconds = 6.0
    var selectedGift = NSDictionary()
    var isKeyboardEnable = false
    var userListArray = NSMutableArray()
    var reportArray = NSMutableArray()
    var commentArray = NSMutableArray()
    var giftArray = NSMutableArray()
    var goCoderConfig:WowzaConfig!
    var profile:ProfileModel!
    lazy var player = WOWZPlayer()
    var streamDict = NSDictionary()
    var stream_name = String()
    var stream_des = String()
    var like_color = String()
    var receivedGoCoderEventCodes = Array<WOWZEvent>()
    var isFollowReq = false
    var follow_status = String()

    let SDKSampleSavedConfigKey = "SDKSampleSavedConfigKey"
    
    let manager = SocketManager(socketURL: URL(string: SOCKET_IO_URL)!, config: [.log(true), .compress])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        if let savedConfig:Data = UserDefaults.standard.object(forKey: SDKSampleSavedConfigKey) as? Data {
            if let wowzaConfig = NSKeyedUnarchiver.unarchiveObject(with: savedConfig) as? WowzaConfig {
                goCoderConfig = wowzaConfig
            }
            else {
                goCoderConfig = WowzaConfig()
            }
        }
        else {
            goCoderConfig = WowzaConfig()
        }
        //
        //        goCoderConfig.load(WOWZFrameSizePreset.preset640x480)
        //                self.goCoderConfig.videoWidth = 640
        //                self.goCoderConfig.videoHeight = 480
        
        self.loaderView.tintColor = .white
        self.loaderView.dotsCount = 4
        self.loaderView.dotsRadius = 5
        self.loaderView.startAnimating()
        self.loaderStatusLbl.config(color: .white, font: mediumReg, align: .center, text: "initialise")
        
        let config =  WowzaConfig()
        let defaultDict:NSDictionary = UserModel.shared.getDefaults()!
        
        config.hostAddress = defaultDict.value(forKeyPath: "stream_connection_info.stream_host") as? String
        config.applicationName = defaultDict.value(forKeyPath: "stream_connection_info.stream_application_name") as? String
        config.streamName = self.stream_name
        config.username = defaultDict.value(forKeyPath: "stream_connection_info.stream_username") as? String
        config.password = defaultDict.value(forKeyPath: "stream_connection_info.stream_password") as? String
        config.portNumber = defaultDict.value(forKeyPath: "stream_connection_info.stream_port") as! UInt
        goCoderConfig = config
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.watchStream()
        }
        // Log version and platform info
        print("WowzaGoCoderSDK version =\n major: \(WOWZVersionInfo.majorVersion())\n minor: \(WOWZVersionInfo.minorVersion())\n revision: \(WOWZVersionInfo.revision())\n build: \(WOWZVersionInfo.buildNumber())\n string: \(WOWZVersionInfo.string())\n verbose string: \(WOWZVersionInfo.verboseString())")
        
        print("Platform Info:\n\(WOWZPlatformInfo.string())")
        let sdk_license = defaultDict.value(forKeyPath: "stream_connection_info.ios_sdk_license") as? String
        if let goCoderLicensingError = WowzaGoCoder.registerLicenseKey(sdk_license!) {
            self.showAlert("GoCoder SDK Licensing Error", error: goCoderLicensingError as NSError)
        }
        
        do{
            let jsonDecoder = JSONDecoder()
            profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
        }catch{
            
        }
        //set like color
        like_color =  Utility.shared.likeHexColorCode()
        HeartTheme.selectedColor = like_color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let savedConfigData = NSKeyedArchiver.archivedData(withRootObject: goCoderConfig)
        UserDefaults.standard.set(savedConfigData, forKey: SDKSampleSavedConfigKey)
        self.initialSetup()
        self.changeToRTL()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        //        self.stopStream()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
            self.menu_reportLabel.textAlignment = .right
            self.hideLabel.textAlignment = .right
            self.broadCastDetailsLabel.textAlignment = .right
            self.reportView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else{
            self.menu_reportLabel.textAlignment = .left
            self.hideLabel.textAlignment = .left
            self.broadCastDetailsLabel.textAlignment = .left
            self.reportView.transform = .identity
            self.view.transform = .identity
        }
    }
    
    
    //initial setup
    func initialSetup()  {
        isWatchingLive = true
        
        if !UserModel.shared.userId.isEmpty {
            Utility.shared.checkBlocked()
        }
        UIApplication.shared.isIdleTimerDisabled = true
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        self.fd_prefersNavigationBarHidden = true
        self.fd_interactivePopDisabled = true
        self.configPreview()
    }
    
    //MARK: Register custom cells
    func registerCustomCells(){
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        userlistCollectionView.collectionViewLayout = collectionViewFlowLayout
        userListArray = NSMutableArray()
        
        chatTableView.register(UINib(nibName: "ChatCommentCell", bundle: nil), forCellReuseIdentifier: "ChatCommentCell")
        chatTableView.register(UINib(nibName: "UserJoinedCell", bundle: nil), forCellReuseIdentifier: "UserJoinedCell")
        userlistCollectionView.register(UINib(nibName: "UserListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserListCollectionViewCell")
        userlistTableView.register(UINib(nibName: "UserListTableCell", bundle: nil), forCellReuseIdentifier: "UserListTableCell")
        stickerCollectionView.register(UINib(nibName: "stickerCell", bundle: nil), forCellWithReuseIdentifier: "stickerCell")
        chatTableView.register(UINib(nibName: "GiftCell", bundle: nil), forCellReuseIdentifier: "GiftCell")
        reportTableview.register(UINib(nibName: "ReportCell", bundle: nil), forCellReuseIdentifier: "ReportCell")
    }
    
    //design details
    func configPreview()  {
        self.CustomPreview.isHidden = false
        CustomPreview.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
        watchCountView.backgroundColor = CIRCLE_BG_COLOR
        watchCountView.cornerLeft()
        postedByLabel.config(color: .white, font: medium, align: .left, text: "")
        streamNameLabel.config(color: .white, font: averageReg, align: .left, text: "")
        streamMenuNameLabel.config(color: .white, font: high, align: .left, text: "")
        
        postedUserImageView.sd_setImage(with: URL(string: (self.streamDict.value(forKey: "publisher_image") as? String)!), placeholderImage: #imageLiteral(resourceName: "user_placeholder"))
        postedUserImageView.makeItRound()
        postedImgOverlay.cornerViewRadius()
        postedImgOverlay.backgroundColor = UIColor().setRandomColor()
        postedImgOverlay.alpha = 0.5;
        
        let url = URL(string: (self.streamDict.value(forKey: "stream_thumbnail") as? String)!)
        if url == nil {
            preview_imgView.image = #imageLiteral(resourceName: "user_placeholder")
        }else{
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            if data == nil{ // if thumbnail is empty then load profile pic
                let profile_url = URL(string: (self.streamDict.value(forKey: "publisher_image") as? String)!)
                if profile_url != nil{
                    let profile_data = try? Data(contentsOf: profile_url!)
                    
                    if profile_data != nil{
                        preview_imgView.image = UIImage(data: profile_data!)
                    }else{
                        preview_imgView.image = #imageLiteral(resourceName: "user_placeholder")
                    }
                }else{
                    preview_imgView.image = #imageLiteral(resourceName: "user_placeholder")
                }
            }else{
                preview_imgView.image = UIImage(data: data!)
            }
        }
        
        self.preview_imgView.layer.minificationFilter = CALayerContentsFilter.trilinear
        self.preview_imgView.layer.minificationFilterBias = 5.0
        
        
        streamNameLabel.text = self.streamDict.value(forKey: "title") as? String
        postedByLabel.text = self.streamDict.value(forKey: "posted_by") as? String
        
        subscriberCountView.backgroundColor = .darkGray
        subscriberCountView.cornerViewRadius()
        liveLabel.backgroundColor = LIVE_COLOR
        liveLabel.config(color: .white, font: low, align: .center, text: "live")
        countLabel.config(color: .white, font: low, align: .center, text: "")
        
        countLabel.text = "0"
        self.configChatDetails()
        self.configStreamDetails()
        self.configGiftView()
        
    }
    
    //chat details comment,like & gift
    func configChatDetails(){        
        self.msgView.cornerViewRadius()
        self.msgView.border(color: .clear)
        self.msgTxtView.delegate = self
        automaticallyAdjustsScrollViewInsets = false
        self.msgTxtView.maxLength = 80
        self.msgTxtView.placeholder = (Utility.language.value(forKey: "say_something") as! String)
        self.msgTxtView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        self.msgTxtView.font = medium
        self.msgTxtView.minHeight = 40.0
        self.msgTxtView.maxHeight = 60.0
        self.msgTxtView.textColor = .white
        self.msgTxtView.backgroundColor = .clear
        self.msgView.backgroundColor = CIRCLE_BG_COLOR
        self.sendBtn.isHidden = true
        
        let sendIcon = #imageLiteral(resourceName: "send_icon").withRenderingMode(.alwaysTemplate)
        self.sendBtn.setImage(sendIcon, for: .normal)
        self.sendBtn.tintColor = .white
        
        //keyboard manager
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.chatTableView.frame = CGRect.init(x: 10, y: self.watchCountView.frame.origin.y-170, width: 270, height: 150)
        watchCountView.cornerLeft()
        
        //configure custom view cells
        registerCustomCells()
        //set collection view scroll horizontally
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        userlistCollectionView.collectionViewLayout = collectionViewFlowLayout
        //        self.configureStreamDetailMenuView()
        
        self.chatUserContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
        
        let swipeTop = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeTop.direction = UISwipeGestureRecognizer.Direction.up
        self.view.addGestureRecognizer(swipeTop)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    func configStreamDetails()  {
        
        hideLabel.config(color: .white, font: average, align: .left, text: "hide_chat")
        broadCastDetailsLabel.config(color: .white, font: average, align: .left, text: "broadcast_details")
        menu_reportLabel.config(color: .white, font: average, align: .left, text: "report")
        
        publisherNameLabel.config(color: .white, font: averageReg, align: .left, text: "")
        streamMenuNameLabel.config(color: .white, font: high, align: .left, text: "")
        
        
        self.reportviewTitleLbl.config(color: .white, font: averageReg, align: .center, text: "report")
        self.reportArray.removeAllObjects()
        self.reportArray.addObjects(from: UserModel.shared.getDefaults()?.value(forKey: "reports") as! [Any])
        self.reportTableview.reloadData()
        self.reportView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
        
        let username = streamDict.value(forKey: "posted_by") as? String
        publisherNameLabel.text = username
        streamMenuNameLabel.text = streamDict.value(forKey: "title") as? String
        
        
        menu_liveLabel.config(color: .white, font: lowReg, align: .center, text: "live")
        menu_liveLabel.backgroundColor = .darkGray
        menu_watchCountView.cornerViewRadius()
        menu_watchCountView.backgroundColor = PRIMARY_COLOR
        
        
        //Update live viewers count instantly
        menu_ViewCount.config(color: .white, font: lowReg, align: .center, text: "")
        watchingCountLbel.config(color: .white, font: mediumReg, align: .center, text: "")
        totalCountLbl.config(color: .white, font: high, align: .center, text: "")
        
        menu_ViewCount.text = "0"
        watchingCountLbel.text = "0"
        totalCountLbl.text = "0"
        
        
        //follow status
        followView.cornerViewRadius()
        followView.layer.borderWidth = 1.0
        followView.layer.borderColor = UIColor.white.cgColor
        followLbl.config(color: .white, font: medium, align: .center, text: "follow")
        
        let followTap = UITapGestureRecognizer(target: self, action: #selector(self.followBtnTapped(_:)))
        self.followView.addGestureRecognizer(followTap)
        
        streamDetailContainerView.isHidden = true
        self.chatUserContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
        
        userImageView.makeItRound()
        colorView.backgroundColor = UIColor().setRandomColor()
        colorView.cornerViewRadius()
        colorView.alpha = 0.5
        userImageView.sd_setImage(with: URL(string: (self.streamDict.value(forKey: "publisher_image") as? String)!), placeholderImage: #imageLiteral(resourceName: "user_placeholder"))
        
        // configure menu user list view
        
        totalViewersLabel.config(color: .white, font: averageReg, align: .center, text: "total_viewers")
        liveViewersLabel.config(color: .white, font: liteReg, align: .center, text: "live_viewers")
        
        menuUserListView.backgroundColor = .clear
    }
    
    
    //sticker view
    func configGiftView()  {
        
        self.view.bringSubviewToFront(self.stickersView)
        stickerCollectionView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height:self.stickersView.frame.size.height-30)
        stickersView.addSubview(stickerCollectionView)
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        stickerCollectionView.collectionViewLayout = collectionViewFlowLayout
        pageControl.hidesForSinglePage = true
        pageControl.currentPageIndicatorTintColor = PRIMARY_COLOR
        pageControl.pageIndicatorTintColor = .white
        self.stickersView.bringSubviewToFront(stickerCollectionView)
        self.view.bringSubviewToFront(self.stickersView)
        self.stickersView.isHidden = true
        self.stickersView.frame.origin.y = FULL_HEIGHT
        giftSendView.isHidden = true
        self.giftTitleLbl.config(color: .white, font: mediumReg, align: .left, text: "")
        if IS_LIVZA{
            self.giftSendBtn.config(color: PRIMARY_COLOR, font: mediumReg, align: .center, title: "send")
        }else{
            self.giftSendBtn.config(color: .yellow, font: mediumReg, align: .center, title: "send")
        }
        
        //load data
        self.giftArray.removeAllObjects()
        let defaultDict = UserModel.shared.getDefaults()
        self.giftArray.addObjects(from: defaultDict?.value(forKey: "gifts") as! [Any])
        let giftCount = self.giftArray.count/8
        if self.giftArray.count > 8{
            pageControl.numberOfPages = giftCount
        }else{
            pageControl.numberOfPages = 0
        }
        let Padding = FULL_WIDTH/2
        pageControl.frame = CGRect.init(x:Padding - (self.pageControl.frame.width/2), y: Padding+45, width: self.pageControl.frame.width, height:37)
        
        self.stickerCollectionView.reloadData()
    }
    
    //set stream details
    func setDetails(dict:NSDictionary) {
        self.menu_ViewCount.text = "\(dict.value(forKey: "watch_count") as! String)"
        self.watchingCountLbel.text = "\(dict.value(forKey: "watch_count") as! String)"
        self.totalCountLbl.text = "\(dict.value(forKey: "watch_count") as! String)"
        self.countLabel.text = "\(dict.value(forKey: "watch_count") as! String)"
        
        follow_status = dict.value(forKey: "follow") as! String
        if follow_status == "true"{
            self.followerCircleView()
        }else{
            self.followerRectangleView()
        }
        
        let report_status = dict.value(forKey: "reported") as! String
        if report_status == "true"{
            self.menu_reportLabel.text = Utility.language.value(forKey: "undo_report") as? String
        }else{
            self.menu_reportLabel.text = Utility.language.value(forKey: "report") as? String
        }
        self.userListArray.removeAllObjects()
        self.userListArray.addObjects(from:dict.value(forKey: "live_viewers") as! [Any])
        self.userlistTableView.reloadData()
        self.userlistCollectionView.reloadData()
    }
    
    //play live stream
    func watchStream()  {
        self.player.playerViewGravity = .resizeAspectFill
        if(self.player.currentPlayState() == WOWZState.idle){
            self.player.play(self.goCoderConfig, callback: self)
        }
    }
    // stop live stream
    func stopStream()  {
        UIApplication.shared.isIdleTimerDisabled = false
        self.unsubscribeStream()
        self.disConnectSocket()
        self.player.resetPlaybackErrorCount()
        self.player.stop()
        isWatchingLive = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func muteAudio()  {
        self.player.muted = !self.player.muted
    }
    
    func refreshCommentView()  {

      
            let indexPathArray = [IndexPath(row: self.commentArray.count - 1, section: 0)]
               self.chatTableView.beginUpdates()
               self.chatTableView.insertRows(at: indexPathArray, with: .automatic)
               self.chatTableView.endUpdates()
        
        let indexPath = IndexPath(row: self.commentArray.count-1, section: 0)
            self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                let indexPath = IndexPath(row: self.commentArray.count-1, section: 0)
                self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        
    }
    
    //MARK: update tableview cell from bottom
  
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    
    func updateTableContentInset() {
        
        let offset = chatTableView.contentSize.height - chatTableView.frame.size.height + chatTableView.contentInset.bottom

              UIView.animate(withDuration: 0.33, delay: 0, options: [.curveEaseOut, .allowUserInteraction], animations: {
                  self.chatTableView.setContentOffset(CGPoint(x: 0, y: offset), animated: true)
              }, completion: nil)
        
       /* let numRows = tableView(chatTableView, numberOfRowsInSection: 0)
        var contentInsetTop = self.chatTableView.bounds.size.height
        for i in 0..<numRows {
            contentInsetTop -= tableView(chatTableView, heightForRowAt: IndexPath(item: i, section: 0))
            if contentInsetTop <= 0 {
                contentInsetTop = 0
                break
            }
        }
        chatTableView.contentInset = UIEdgeInsets(top: contentInsetTop, left: 0, bottom: 0, right: 0)*/
    }
    @IBAction func didTapSettingsButton(_ sender:AnyObject?) {
        if let settingsNavigationController = UIStoryboard(name: "AppSettings", bundle: nil).instantiateViewController(withIdentifier: "settingsNavigationController") as? UINavigationController {
            if let settingsViewController = settingsNavigationController.topViewController as? SettingsViewController {
                settingsViewController.addDisplay(.playbackSettings)
                settingsViewController.addDisplay(.playback)
                settingsViewController.addDisplay(.playbackHLS)
                let viewModel = SettingsViewModel(sessionConfig: goCoderConfig)
                settingsViewController.viewModel = viewModel!
            }
            self.present(settingsNavigationController, animated: true, completion: nil)
        }
    }
    
    @objc func followBtnTapped(_ sender: UITapGestureRecognizer){
        if isFollowReq{
            self.statusAlert(msg: "already_request")
        }else{
            self.isFollowReq = true
            var updateStatus = String()
            if self.follow_status == "false" {
                updateStatus = "Follow"
            }else{
                updateStatus = "Unfollow"
            }
            let updateOBj = BaseWebService()
            let request = NSMutableDictionary()
            request.setValue(self.streamDict.value(forKey: "publisher_id"), forKey: "user_id")
            request.setValue(UserModel.shared.userId, forKey: "follower_id")
            request.setValue(updateStatus, forKey: "type")
            updateOBj.baseService(subURl: FOLLOW_API, params:  request as? Parameters, onSuccess: {response in
                let dict = response.result.value as? NSDictionary
                let status = dict?.value(forKey: "status") as! String
                self.isFollowReq = false
                if status == "true"{
                    if updateStatus == "Follow"{
                        self.follow_status =  "true"
                        self.followerCircleView()
                        
                    }else{
                        self.follow_status =  "false"
                        self.followerRectangleView()
                        
                    }
                }
            })
        }
    }
    @IBAction func closeBtnTapped(_ sender: Any) {
        let alertVC = CPAlertVC(title:APP_NAME, message: Utility.language.value(forKey: "want_to_exit") as! String)
        alertVC.animationType = .bounceUp
        alertVC.addAction(CPAlertAction(title: "Yes", type: .normal, handler: {
            self.stopStream()
        }))
        alertVC.addAction(CPAlertAction(title: "No", type: .cancel, handler: {
        }))
        alertVC.show(into: self)
    }
    
    
    @IBAction func previewCloseBtnTapped(_ sender: Any) {
        self.msgTxtView.resignFirstResponder()
        self.stopStream()
    }
    
    @IBAction func streamInfoBtnTapped(_ sender: Any) {
        self.msgTxtView.resignFirstResponder()
        
        menuView.isHidden = false
        menuUserListView.isHidden = true
        self.streamDetailContainerView.isHidden = false
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            self.streamDetailContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.chatUserContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.stickersView.frame.origin.y = FULL_HEIGHT
            self.stickersView.isHidden = true
            self.giftSendView.isHidden = true
        }, completion: nil)
    }
    
    @IBAction func streamUserInfoBtnTapped(_ sender: Any) {
        self.msgTxtView.resignFirstResponder()
        
        menuView.isHidden = true
        menuUserListView.isHidden = false
        self.streamDetailContainerView.isHidden = false
        self.streamDetailContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
        self.menuUserListView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
    }
    
    @IBAction func hidePropertiesBtnTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            self.msgTxtView.resignFirstResponder()
            self.chatUserContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.streamDetailContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.menuUserListView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.reportView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.giftSendView.isHidden = true
            self.stickersView.frame.origin.y = FULL_HEIGHT
            self.stickersView.isHidden = true
        }, completion: nil)
    }
    
    @IBAction func reportBtnTapped(_ sender: Any) {
        if menu_reportLabel.text == Utility.language.value(forKey: "undo_report") as? String{
            self.reportService(title: "",type:"undoreport")
        }else{
            
            menuView.isHidden = true
            menuUserListView.isHidden = true
            self.streamDetailContainerView.isHidden = false
            self.reportView.isHidden = false
            self.streamDetailContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.reportView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
        }
    }
    
    
    @IBAction func cmtSendBtnTapped(_ sender: Any) {
        self.sendComment(msg: msgTxtView.text)
    }
    
    @IBAction func giftBtnTapped(_ sender: Any) {
        
        self.msgTxtView.resignFirstResponder()
        if stickersView.isHidden {
            self.chatUserContainerView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
            self.stickersView.isHidden = false
            self.view.bringSubviewToFront(self.stickersView)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.stickersView.frame.origin.y = FULL_HEIGHT - self.stickersView.frame.size.height
                self.chatUserContainerView.frame.origin.y = self.chatUserContainerView.frame.origin.y - self.stickersView.frame.size.height
            }, completion: nil)
        }
    }
    
    
    @IBAction func giftSendBtnTapped(_ sender: Any) {
        
        self.checkGift()
        self.giftSendView.isHidden = true
        self.giftSendView.frame.origin.y = self.stickersView.frame.origin.y
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.giftSendView.frame.origin.y = self.stickersView.frame.origin.y-45
            self.chatUserContainerView.frame.origin.y = self.chatUserContainerView.frame.origin.y + self.giftSendView.frame.size.height
        }, completion: nil)
    }
    
    
    
    //MARK: Follow button animation view
    func followerCircleView()  {
        self.followView.frame = CGRect(x:FULL_WIDTH-40,y:self.userImageView.frame.origin.y+10,width:35,height:35)
        self.followImageView.frame = CGRect(x:7,y:7,width:20.5,height:20.5)
        self.followView.addSubview(self.followImageView)
        self.followImageView.image = #imageLiteral(resourceName: "FollowTic")
        self.followView.backgroundColor = .white
        self.followLbl.text = ""
    }
    
    func followerRectangleView()  {
        self.followView.frame = CGRect(x:FULL_WIDTH-115,y:self.userImageView.frame.origin.y+10,width:100,height:35)
        self.followImageView.frame = CGRect(x:10,y:7,width:20.5,height:20.5)
        self.followView.addSubview(self.followImageView)
        self.followImageView.image = #imageLiteral(resourceName: "follow_icon")
        self.followView.backgroundColor = .clear
        self.followLbl.text = (Utility.language.value(forKey: "follow") as! String)
    }
    // swipe reponse gesture
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.down:
                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
                    // Add the transformation in this block
                    self.msgTxtView.resignFirstResponder()
                    self.chatUserContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
                    self.streamDetailContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
                    self.menuUserListView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
                    self.giftSendView.isHidden = true
                    self.stickersView.frame.origin.y = FULL_HEIGHT
                    self.stickersView.isHidden = true
                    self.reportView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
                }, completion: nil)
            case UISwipeGestureRecognizer.Direction.up:
                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
                    // Add the transformation in this block
                    self.chatUserContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
                }, completion: nil)
            default:
                break
            }
        }
    }
    
    //MARK: Keyboard hide/show
    @objc func keyboardWillShow(sender: NSNotification) {
        if !isKeyboardEnable {
            isKeyboardEnable = true
            let info = sender.userInfo!
            let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            if !self.stickersView.isHidden{
                self.chatUserContainerView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
                self.stickersView.frame.origin.y = FULL_HEIGHT
                self.stickersView.isHidden = true
                self.giftSendView.isHidden = true
            }
            self.chatUserContainerView.frame.origin.y = self.chatUserContainerView.frame.origin.y - keyboardFrame.height
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
//        self.chatUserContainerView.frame.origin.y += keyboardFrame.height
        self.chatUserContainerView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
        isKeyboardEnable = false
        
    }
    func hideBottomInputs(){
        self.chatUserContainerView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
        self.msgTxtView.resignFirstResponder()
        self.giftSendView.isHidden = true
        self.stickersView.frame.origin.y = FULL_HEIGHT
        self.stickersView.isHidden = true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.giftSendView.isHidden = true
        self.stickersView.isHidden = true
        self.stickersView.frame.origin.y = FULL_HEIGHT
    }
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        //        UIView.animate(withDuration: 0.2) {
        self.bottomView.frame.origin.y += self.bottomView.frame.size.height
        self.bottomView.frame.size.height = height+20
        self.msgTxtView.frame.size.height = height
        self.msgView.frame.size.height = height+10
        
        self.bottomView.frame.origin.y -= self.bottomView.frame.size.height
        self.msgView.frame.origin.y = 5
        self.msgTxtView.frame.origin.y = 5
    }
    
    //textview delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        if Utility.shared.checkEmpty(str: textView.text!+text) {
            self.sendBtn.isHidden = true
        }else{
            if self.sendBtn.isHidden{
                self.sendBtn.frame.origin.x -= 30
                UIView.animate(withDuration: 0.5, animations: {
                    self.sendBtn.isHidden = false
                    self.sendBtn.frame.origin.x += 30
                })
            }
        }
        return true
    }
    //#MARK: Table view delegate & data source
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int{
        if (tableView == chatTableView){
            return self.commentArray.count
        }else if(tableView == userlistTableView){
            return self.userListArray.count
        }else if(tableView == reportTableview){
            return self.reportArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var customCell = UITableViewCell()
        if (tableView == chatTableView){
            
            let responseDict:NSDictionary = commentArray.object(at: indexPath.row) as! NSDictionary
            let type:String = responseDict.value(forKey: "type") as! String
            if(type == "text"){
                let cmtCell = tableView.dequeueReusableCell(withIdentifier: "ChatCommentCell", for: indexPath) as! ChatCommentCell
                cmtCell.cmtContainerView.alpha = 1.0
                cmtCell.comment(dict: responseDict)
                //hide cell with animation
                UIView.animate(withDuration: chat_Hide_Seconds, animations: {() -> Void in
                    cmtCell.cmtContainerView.alpha = 0
                })
                tableView.rowHeight = cmtCell.cmtContainerView.frame.size.height+5

                customCell = cmtCell
            }else if (type == "joined"){
                let joinCell = tableView.dequeueReusableCell(withIdentifier: "UserJoinedCell", for: indexPath) as! UserJoinedCell
                joinCell.joinUserLabel.alpha = 1.0
                joinCell.join(username: responseDict.value(forKey: "user_name") as! String)
                //hide cell with animation
                UIView.animate(withDuration: chat_Hide_Seconds, animations: {() -> Void in
                    joinCell.joinUserLabel.alpha = 0
                })
                tableView.rowHeight = 35

                customCell = joinCell
            }else if (type == "gift"){
                let gift = tableView.dequeueReusableCell(withIdentifier: "GiftCell", for: indexPath) as! GiftCell
                gift.containerView.alpha = 1.0
                gift.giftIcon.alpha = 1.0
                gift.config(dict: responseDict)
                //hide cell with animation
                UIView.animate(withDuration: chat_Hide_Seconds, animations: {() -> Void in
                    gift.containerView.alpha = 0
                    gift.giftIcon.alpha = 0
                })
                tableView.rowHeight = 65

                customCell = gift
            }
        }else if(tableView == userlistTableView){
            let userlistCell = tableView.dequeueReusableCell(withIdentifier: "UserListTableCell", for: indexPath) as! UserListTableCell
            let responseDict:NSDictionary = userListArray.object(at: indexPath.row) as! NSDictionary
            userlistCell.user(dict:responseDict)
            tableView.rowHeight = 55

            customCell = userlistCell
        }else if(tableView == reportTableview){
            let report = tableView.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
            let reportDict = reportArray.object(at: indexPath.row) as! NSDictionary
            report.reportNameLbl?.text = "\(reportDict.value(forKey: "title") as! String)"
            report.contentView.backgroundColor = .clear
            report.backgroundColor = .clear
            tableView.rowHeight = 50

            customCell = report
            
        }
        return customCell
    }
  /*  // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        if(tableView == chatTableView){
            let responseDict:NSDictionary = commentArray.object(at: indexPath.row) as! NSDictionary
            let responseType:String = responseDict.value(forKey: "type") as! String
            if(responseType == "text"){
                let msg:String = responseDict.value(forKey: "message") as! String
                let txtView = UITextView()
                txtView.font = lite
                txtView.text = msg
                var fixedWidth =  txtView.intrinsicContentSize.width
                if fixedWidth > 240{
                    fixedWidth = 240
                }
                txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                let newSize =  txtView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                var newFrame =  txtView.frame
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                return newFrame.size.height + 25
                
            }else if(responseType == "joined"){
                return 35
            }else if(responseType == "gift"){
                return 65
            }
        }else if(tableView == userlistTableView){
            return 55
        }else if(tableView == reportTableview){
            return 50
        }
        return 0
    }
    */
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath){
        
        
        if(tableView == userlistTableView){
            //            let responseDict:NSDictionary = userListArray.object(at: indexPath.row) as! NSDictionary
            //            let userDetailView = UserDetailPage()
            //            userDetailView.modalPresentationStyle = .overCurrentContext
            //            userDetailView.modalTransitionStyle = .crossDissolve
            //            let userID = responseDict.value(forKey: "id") as! Int
            //            userDetailView.viewType = "2"
            //            userDetailView.profileID = String(userID) as NSString
            //            self.navigationController?.present(userDetailView, animated: true, completion: nil)
            
        }else if(tableView == reportTableview){
            let reportDict = reportArray.object(at: indexPath.row) as! NSDictionary
            self.reportService(title: "\(reportDict.value(forKey: "title") as! String)",type:"report")
        }
    }
    //MARK: Collection view delegate & data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.stickerCollectionView {
            return giftArray.count
        }
        return self.userListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var customCell = UICollectionViewCell()
        if collectionView == self.stickerCollectionView {
            let giftcell : stickerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "stickerCell", for: indexPath) as! stickerCell
            let giftDict:NSDictionary =  giftArray.object(at: indexPath.row) as! NSDictionary
            giftcell.configGift(dict: giftDict)
            giftcell.giftBtn.tag = indexPath.row
            giftcell.giftBtn.addTarget(self, action: #selector(self.chooseGift(_:)), for: .touchUpInside)
            customCell = giftcell
        }else{
            let cell : UserListCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserListCollectionViewCell", for: indexPath) as! UserListCollectionViewCell
            let userDict:NSDictionary =  userListArray.object(at: indexPath.row) as! NSDictionary
            cell.config(image:userDict.value(forKey: "user_image") as! String)
            customCell = cell
        }
        return customCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.stickerCollectionView {
            let stickerSize = FULL_WIDTH/4
            return CGSize(width: stickerSize, height: stickerSize)
        }
        return CGSize(width: 40, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.stickerCollectionView {
            return 0.0
            
        }
        return 5.0
    }
    
    func reportService(title:String,type:String) {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        if type == "report"{
            requestDict.setValue(title, forKey: "report")
        }
        requestDict.setValue(self.stream_name, forKey: "name")
        Obj.baseService(subURl: REPORT_STREAM_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                if type == "report"{
                    self.statusAlert(msg: "reported_successfully")
                    self.menu_reportLabel.text = Utility.language.value(forKey: "undo_report") as? String
                }else{
                    self.statusAlert(msg: "reported_undo_successfully")
                    self.menu_reportLabel.text = Utility.language.value(forKey: "report") as? String
                }
                self.reportView.frame = CGRect.init(x: 0, y: FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
                self.chatUserContainerView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
                self.streamDetailContainerView.frame = CGRect.init(x: 0, y: FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
            }
        })
    }
    
    //send sticker
    @objc func chooseGift(_ sender: UIButton){
        selectedGift = giftArray.object(at: sender.tag) as! NSDictionary
        self.giftTitleLbl.text = selectedGift.value(forKey: "gft_title") as? String
        if self.giftSendView.isHidden {
            self.giftSendView.isHidden = false
            self.giftSendView.frame.origin.y = self.stickersView.frame.origin.y
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.giftSendView.frame.origin.y = self.stickersView.frame.origin.y-45
                self.chatUserContainerView.frame.origin.y = self.chatUserContainerView.frame.origin.y - self.giftSendView.frame.size.height
            }, completion: nil)
        }
    }
    //MARK: config Live screen
    func configLiveScreen(){
        self.socketConnect()
        UIView.animate(withDuration: 0.4, animations: {
            self.CustomPreview.alpha = 0
        }, completion: nil)
        
        self.customView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
        self.chatUserContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.makeHeartLike))
        chatUserContainerView.addGestureRecognizer(tap)
    }
    
    //MARK: wowza delegate
    func onWOWZStatus(_ status: WOWZStatus!) {
        switch (status.state) {
        case .idle:
            DispatchQueue.main.async { () -> Void in
                print("Broadcast status: The broadcast is idle");
            }
            break
        case .starting:
            DispatchQueue.main.async { () -> Void in
                print("Broadcast status: The broadcast is starting");
                self.player.playerView = self.previewView;
            }
            break;
        case .running:
            DispatchQueue.main.async { () -> Void in
                print("Broadcast status: The broadcast is running");
                self.configLiveScreen()
            }
            break;
        case .stopping:
            DispatchQueue.main.async { () -> Void in
                print("Broadcast status: The broadcast is stopping");
                self.stopStream()
            }
            break;
        case .buffering:
            DispatchQueue.main.async { () -> Void in
                print("Broadcast status: The broadcast is buffer");
                
            }
            break;
        default: break
        }
    }
    
    func onWOWZError(_ status: WOWZStatus!) {
        print("wowza error \(status)")
        print(status.error?.localizedDescription ?? "error occured")
      /*  let alertVC = CPAlertVC(title:APP_NAME, message: Utility.language.value(forKey: "broadcast_end") as! String)
        alertVC.animationType = .bounceUp
        alertVC.addAction(CPAlertAction(title: Utility.language.value(forKey: "yes") as! String, type: .normal, handler: {
        }))
        alertVC.show(into: self)*/
        self.stopStream()
        CRNotifications.showNotification(textColor: .white, backgroundColor: PRIMARY_COLOR, image: UIImage.init(named: "brodcastInfo_icon"), title: APP_NAME, message: Utility.language.value(forKey: "broadcast_end") as! String, dismissDelay: 2)


    }
    
    func showAlert(_ title:String, error:NSError) {
        let alertController = UIAlertController(title: title, message: error.localizedDescription, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    //MARK: SOCKET IO
    
    //connect socket
    func socketConnect()  {
        manager.defaultSocket.connect()
        self.addHandlers()
    }
    //disconnnect
    func disConnectSocket()  {
        manager.defaultSocket.off("_streamJoined")
        manager.defaultSocket.off("_msgReceived")
        manager.defaultSocket.disconnect()
    }
    
    //MARK: SOCKET EMIT
    
    // active socket
    func subscribeStream()  {
        let req = NSMutableDictionary()
        req.setValue(self.stream_name, forKey: "stream_name")
        req.setValue(UserModel.shared.userId, forKey: "user_id")
        req.setValue(profile.name, forKey: "user_name")
        req.setValue(PROFILE_IMAGE_URL+profile.user_image!, forKey: "user_image")
        manager.defaultSocket.emit("_subscribeStream", with: [req])
    }
    
    // unsubscribe
    func unsubscribeStream()  {
        let req = NSMutableDictionary()
        req.setValue(self.stream_name, forKey: "stream_name")
        req.setValue(UserModel.shared.userId, forKey: "user_id")
        manager.defaultSocket.emit("_unsubscribeStream", with: [req])
    }
    
    //send join msg to stream
    func joinMsg()  {
        let req = NSMutableDictionary()
        req.setValue(UserModel.shared.userId, forKey: "user_id")
        req.setValue(profile.name, forKey: "user_name")
        req.setValue(profile.user_image, forKey: "user_image")
        req.setValue(self.stream_name, forKey: "stream_name")
        req.setValue("joined", forKey: "type")
        manager.defaultSocket.emit("_sendMsg", with: [req])
        self.getStreamInfo()
    }
    //get stream info
    func getStreamInfo()  {
        let req = NSMutableDictionary()
        req.setValue(UserModel.shared.userId, forKey: "user_id")
        req.setValue(self.stream_name, forKey: "stream_name")
        manager.defaultSocket.emit("_getstreamInfo", with: [req])
    }
    //check if gems avail based on the gift selection
    func checkGift() {
        let req = NSMutableDictionary()
        req.setValue(UserModel.shared.userId, forKey: "gift_from")
        req.setValue(self.streamDict.value(forKey: "publisher_id"), forKey: "gift_to")
        req.setValue(selectedGift.value(forKey: "_id"), forKey: "gift_id")
        req.setValue(selectedGift.value(forKey: "gft_title"), forKey: "gift_title")
        req.setValue(selectedGift.value(forKey: "gft_icon"), forKey: "gift_icon")
        if UserModel.shared.isPremium{
            req.setValue(selectedGift.value(forKey: "gft_gems_prime"), forKey: "gems_count")
        }else {
            req.setValue(selectedGift.value(forKey: "gft_gems"), forKey: "gems_count")
        }
        req.setValue(selectedGift.value(forKey: "gft_gems"), forKey: "gems_earnings")
        manager.defaultSocket.emit("_sendGift", with: [req])
    }
    
    //send gift msg to stream
    func sendGiftMsg() {
        let req = NSMutableDictionary()
        req.setValue("gift", forKey: "type")
        req.setValue(UserModel.shared.userId, forKey: "user_id")
        req.setValue(profile.name, forKey: "user_name")
        req.setValue(self.stream_name, forKey: "stream_name")
        req.setValue(selectedGift.value(forKey: "gft_title"), forKey: "gift_title")
        req.setValue(selectedGift.value(forKey: "gft_icon"), forKey: "gift_icon")
        manager.defaultSocket.emit("_sendMsg", with: [req])
        self.commentArray.add(req)
        self.refreshCommentView()
    }
    
    //send like to stresm
    @objc func makeHeartLike()  {
        if !isKeyboardEnable && self.stickersView.isHidden{
            let req = NSMutableDictionary()
                  req.setValue(UserModel.shared.userId, forKey: "user_id")
                  req.setValue(self.stream_name, forKey: "stream_name")
                  req.setValue(like_color, forKey: "like_color")
                  req.setValue("liked", forKey: "type")
                  manager.defaultSocket.emit("_sendMsg", with: [req])
                  HeartTheme.selectedColor = like_color
                  let heart = HeartView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
                  self.chatUserContainerView.addSubview(heart)
                  heart.center = CGPoint(x: FULL_WIDTH-80, y: FULL_HEIGHT-80)
                  heart.animateInView(view: self.chatUserContainerView)
        }else{
            self.hideBottomInputs()
        }
      
    }
    
    //send comment to stream
    func sendComment(msg:String)  {
        let req = NSMutableDictionary()
        req.setValue(UserModel.shared.userId, forKey: "user_id")
        req.setValue(profile.name, forKey: "user_name")
        req.setValue(self.stream_name, forKey: "stream_name")
        req.setValue(msg, forKey: "message")
        req.setValue("text", forKey: "type")
        manager.defaultSocket.emit("_sendMsg", with: [req])
        self.commentArray.add(req)
        self.refreshCommentView()
        self.msgTxtView.text = ""
        self.sendBtn.isHidden = true
    }
    
    
    //MARK: SOCKET LISTNERS
    func addHandlers()  {
        //stream connect
        manager.defaultSocket.on(clientEvent: .connect) {data, ack in
            print("SOCKET_IO CONNECTED")
            self.subscribeStream()
            self.joinMsg()
        }
        //stream like ,joined,text
        manager.defaultSocket.on("_msgReceived", callback: {data, ack in
            print("SOCKET_IO MSG \(data)")
            let dict = data[0] as! NSDictionary
            let msg_type = dict.value(forKey: "type") as! String
            
            // heart like
            if msg_type == "liked"{
                HeartTheme.selectedColor = dict.value(forKey: "like_color") as! String
                self.showHeartLike()
            }else if msg_type == "joined"{
                self.commentArray.add(dict)
                self.refreshCommentView()
                self.getStreamInfo()
            }else if msg_type == "text"{
                self.commentArray.add(dict)
                self.refreshCommentView()
            }else if msg_type == "gift"{
                self.commentArray.add(dict)
                self.refreshCommentView()
            }
            
        })
        //stream user left
        manager.defaultSocket.on("_subscriberLeft", callback: {data, ack in
            print("SOCKET_IO LEFT \(data)")
            self.getStreamInfo()
        })
        
        //stream details
        manager.defaultSocket.on("_streamInfo", callback: {data, ack in
            print("SOCKET_IO STREAM INFO \(data)")
            let dict = data[0] as! NSDictionary
            self.setDetails(dict: dict)
        })
        
        //gift avail status
        manager.defaultSocket.on("_giftStatus", callback: {data, ack in
            print("SOCKET_IO GIFT STATUS \(data)")
            let dict = data[0] as! NSDictionary
            let status = dict.value(forKey: "status") as! String
            if status == "true"{
                self.sendGiftMsg()
                UserModel.shared.gemCount = (dict.value(forKey: "total_gems") as! NSNumber).intValue
            }else{
                self.statusAlert(msg: "not_enough_gems")
            }
            
        })
        
        manager.defaultSocket.on("_streamBlocked", callback: {data, ack in
            print("SOCKET_IO BLOCK STATUS \(data)")
            let alertVC = CPAlertVC(title:APP_NAME, message: Utility.language.value(forKey: "stream_blocked") as! String)
            alertVC.animationType = .bounceUp
            alertVC.addAction(CPAlertAction(title: Utility.language.value(forKey: "yes") as! String, type: .normal, handler: {
                self.stopStream()
            }))
            alertVC.show(into: self)
        })
    }
    
    //show heart
    @objc func showHeartLike()  {
        let heart = HeartView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        self.chatUserContainerView.addSubview(heart)
        heart.center = CGPoint(x: FULL_WIDTH-80, y: FULL_HEIGHT-80)
        heart.animateInView(view: self.chatUserContainerView)
    }
    
    
    
    
    
}




