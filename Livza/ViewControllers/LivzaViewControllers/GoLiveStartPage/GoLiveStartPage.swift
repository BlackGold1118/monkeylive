//
//  GoLiveStartPage.swift
//  HSLiveStream
//
//  Created by APPLE on 30/01/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit
import JTMaterialTransition
import GrowingTextView

class GoLiveStartPage: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,UITextViewDelegate,GrowingTextViewDelegate {
    var cameraPreview:UIImagePickerController?
    let captureSession = AVCaptureSession()

    var previewLayer:CALayer!
    var captureDevice:AVCaptureDevice!
    @IBOutlet weak var cameraPreView: UIView!
    @IBOutlet weak var goLiveBtn: UIButton!
    @IBOutlet weak var tipView: UIView!
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var title_TextView: GrowingTextView!
    @IBOutlet var loader: UIActivityIndicatorView!
    var transition: JTMaterialTransition?

    @IBOutlet var borderLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        self.configureView()
        self.previewScreen()
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.changeToRTL()
        self.initialSetup()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(UIApplication.didBecomeActiveNotification)
        NotificationCenter.default.removeObserver(UIApplication.willResignActiveNotification)
    }
    
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
                
            //self.goLiveBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
//            self.tipView.transform = CGAffineTransform(scaleX: -1, y: 1)
//            self.tipLabel.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.tipLabel.textAlignment = .right
//            self.title_TextView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.title_TextView.textAlignment = .right
            
        }
        else{
            self.tipView.transform = .identity
            self.tipLabel.transform = .identity
            self.tipLabel.textAlignment = .left
            self.title_TextView.transform = .identity
            self.title_TextView.textAlignment = .left
        }
    }
    
    func initialSetup(){
        self.transition = JTMaterialTransition(animatedView: self.centerPoint())
        self.fd_prefersNavigationBarHidden = true
        self.fd_interactivePopDisabled = true
        NotificationCenter.default.addObserver(self,selector: #selector(applicationDidBecomeActive),name: UIApplication.didBecomeActiveNotification,object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //MARK: Set up this view
    func configureView()  {
        //textview
            self.title_TextView.tintColor = PRIMARY_COLOR
             self.title_TextView.placeholder = (Utility.language.value(forKey: "whats_happening") as! String)
             self.title_TextView.placeholderColor = TEXT_SECONDARY_COLOR
             self.title_TextView.font = averageReg
             self.title_TextView.maxLength = 80
             self.title_TextView.minHeight = 40.0
             self.title_TextView.maxHeight = 150.0
             self.title_TextView.textColor = .white
             self.title_TextView.backgroundColor = .clear
             self.title_TextView.delegate = self
        borderLbl.backgroundColor = PRIMARY_COLOR
        borderLbl.frame.origin.y = self.title_TextView.frame.origin.y+45
        tipView.frame.origin.y = self.borderLbl.frame.origin.y+50
        //configure button using categories method
        goLiveBtn.cornerRoundRadius()
        goLiveBtn.config(color: .white, font: average, align: .center, title:"go_live")
        tipLabel.config(color: TEXT_COLOR, font: small, align: .natural, text: "tip_note")
        tipView.cornerViewRadius()
        //hide tab bar controller
//        self.tabBarController?.tabBar.isHidden = true
       //microphone permission
        checkMicPermission()
        //add tap gesture
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
    }
    
    
    //call when application from background
    @objc func applicationDidBecomeActive() {
        captureSession.startRunning()

    }
    @objc func appMovedToBackground() {
        captureSession.stopRunning()
    }
    
    //Tap to dismiss keyboard
    @objc func handleTap(_ recognizer : UITapGestureRecognizer) {
        title_TextView.resignFirstResponder()
    }
    
    //MARK: Close Action
    @IBAction func closeBtnTapped(_ sender: Any) {
        title_TextView.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
    func checkMicPermission() -> Bool {
        var permissionCheck: Bool = false
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            permissionCheck = true
        case AVAudioSession.RecordPermission.denied:
            permissionCheck = false
        case AVAudioSession.RecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                if granted {
                    permissionCheck = true
                } else {
                    permissionCheck = false
                }
            })
        default:
            break
        }
        return permissionCheck
    }
    //MARK: Go to publisher page
    @IBAction func goLiveBtnTapped(_ sender: Any) {
        
        if(checkMicPermission()){
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            print("already authorized")
            self.startLive()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    print("access granted")
                    self.startLive()
                } else {
                    print("access denied")
                }
            })
        }
        }else{
        }
    }
    
   
    
    //live start service
    func startLive()  {
        
        if(Utility().checkEmptyWithString(value: self.title_TextView.text)){
            Utility().showAlertWithTitle(alertTitle: Utility.language.value(forKey: "alert") as! NSString, alertMsg: Utility.language.value(forKey: "enter_stream_title") as! NSString, viewController: self)
        }else{
                loader.startAnimating()
                 let Obj = BaseWebService()
                //create stream api call
                 Obj.getDetails(subURl: "\(CREATE_STREAM_API)/\(UserModel.shared.userId ?? "")", onSuccess: {response in
                     let dict = response.result.value as? NSDictionary
                     let status = dict?.value(forKey: "status") as! String
                     if status == "true"{
                        
                        let stream_name = dict?.value(forKey: "stream_name") as! String
                        let requestDict = NSMutableDictionary.init()
                        requestDict.setValue(UserModel.shared.userId, forKey: "publisher_id")
                        requestDict.setValue(stream_name, forKey: "name")
                        requestDict.setValue(self.title_TextView.text!, forKey: "title")
                        requestDict.setValue("true", forKey: "recording")
                        
                        let publish = PublishLivePage()
                        publish.modalPresentationStyle = .custom
                        publish.transitioningDelegate = self.transition
                        publish.stream_name = stream_name
                        publish.startRequest = requestDict
                        publish.stream_des = self.title_TextView.text!
                        self.navigationController?.present(publish, animated: true, completion: nil)
                        self.title_TextView.text = ""
                        
                        self.loader.stopAnimating()

                     }else{
                        
                        self.loader.stopAnimating()

                        let alertVC = CPAlertVC(title:APP_NAME, message: Utility.language.value(forKey: "acc_blocked") as! String)
                                  alertVC.animationType = .bounceUp
                                  alertVC.addAction(CPAlertAction(title: "Ok", type: .normal, handler: {
                                    UserModel.shared.logoutFromAll()
                                    Utility.shared.goToLoginPage()
                                  }))
                                  alertVC.show(into: self)
                     
                    }
                 })
            
           
        }
    }
    
    //MARK: Alertview with action
       func showAlertWith(msg:String) {
           let alertVC = CPAlertVC(title:APP_NAME, message: msg)
           alertVC.animationType = .bounceUp
           alertVC.addAction(CPAlertAction(title: "Ok", type: .normal, handler: {
           }))
           alertVC.show(into: self)
       }
       
    
    @IBAction func tipCloseBtnTapped(_ sender: Any) {
        
        UIView.animate(withDuration: 2.0, delay: 0, options: [], animations: {
            self.tipView.alpha = 0
        }, completion: { _ in
            self.tipView.isHidden = true // Here you hide it when animation done
        })
    }
   
    
    //MARK: Textview delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == TEXT_SECONDARY_COLOR {
            textView.text = nil
            textView.textColor = .white
        }
    }
 
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        self.title_TextView.frame.size.height = height
        borderLbl.frame.origin.y = self.title_TextView.frame.origin.y+height+5
        tipView.frame.origin.y = self.borderLbl.frame.origin.y+50
    }
  
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return numberOfChars < 80;
    }


    //show preview camera screen
    func previewScreen(){
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back).devices
        captureDevice = availableDevices.first
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(captureDeviceInput)
        } catch {
            print(error.localizedDescription)
        }
        let pL = AVCaptureVideoPreviewLayer(session: captureSession)
        pL.videoGravity = .resizeAspectFill
        self.previewLayer = pL
        self.previewLayer.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
        self.cameraPreView.layer.addSublayer(self.previewLayer)
        //ADD CONSTRAINTS HERE
        captureSession.startRunning()
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString):NSNumber(value:kCVPixelFormatType_32BGRA)] as [String : Any]
        dataOutput.alwaysDiscardsLateVideoFrames = true
        if captureSession.canAddOutput(dataOutput) {
            captureSession.addOutput(dataOutput)
        }
        captureSession.commitConfiguration()
    }

}
