//
//  UserListCollectionViewCell.swift
//  HSLiveStream
//
//  Created by APPLE on 10/02/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit

class UserListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var userProfileImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userProfileImgView.makeItRound()
        colorView.cornerViewRadius()
        colorView.backgroundColor = UIColor().setRandomColor()
        colorView.alpha = 0.5;
    }
    func config(image:String)  {
        let imgURL = URL.init(string: image)
        userProfileImgView.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
    }
}
