//
//  UserJoinedCell.swift
//  HSLiveStream
//
//  Created by APPLE on 12/02/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit

class UserJoinedCell: UITableViewCell {

    @IBOutlet weak var joinUserLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        joinUserLabel.layer.cornerRadius = 10
        joinUserLabel.clipsToBounds = true
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func join(username: String) {
        joinUserLabel.config(color: .white, font: low, align: .center, text: "")
        joinUserLabel.text = "@\(username) joined"
        joinUserLabel.backgroundColor = LIVE_COLOR
        
        let attributes = [NSAttributedString.Key.font: low]
        let rect: CGRect = joinUserLabel.text!.boundingRect(with: CGSize(width: 255, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: attributes as [NSAttributedString.Key : Any], context: nil)
       
        if (rect.height>30) {
        joinUserLabel.frame.size = CGSize(width: rect.width+20, height: rect.height)
        }else{
        joinUserLabel.frame.size = CGSize(width: rect.width+20, height: 30)
        }
        joinUserLabel.cornerRadius()

        self.contentView.addSubview(joinUserLabel)
    }
}
