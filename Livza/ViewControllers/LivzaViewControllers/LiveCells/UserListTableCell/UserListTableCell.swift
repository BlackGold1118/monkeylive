//
//  UserListTableCell.swift
//  HSLiveStream
//
//  Created by APPLE on 18/02/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit

class UserListTableCell: UITableViewCell {

    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var colorView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImgView.makeItRound()
        colorView.cornerViewRadius()
        colorView.backgroundColor = UIColor().setRandomColor()
        colorView.alpha = 0.5;

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func user(dict:NSDictionary)  {
        userNameLbl.config(color: .white, font: average, align: .left, text: "")
        userNameLbl.text = dict.value(forKey: "user_name") as? String
        let imgName = dict.value(forKey: "user_image") as? String
        let imgURL = URL.init(string: PROFILE_IMAGE_URL+imgName!)

        userImgView.sd_setImage(with:imgURL, placeholderImage: #imageLiteral(resourceName: "user_placeholder"))
    }
}
