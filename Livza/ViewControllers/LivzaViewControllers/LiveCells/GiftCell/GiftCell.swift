//
//  GiftCell.swift
//  Livza
//
//  Created by HTS-Product on 19/09/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class GiftCell: UITableViewCell {
    @IBOutlet var titleLbl: UILabel!
    
    @IBOutlet var giftIcon: UIImageView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 10
        containerView.clipsToBounds = true
        self.titleLbl.config(color: .white, font: medium, align: .left, text: "")
        containerView.backgroundColor =  UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.8)
        nameLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .left, text: "")


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(dict:NSDictionary)  {
        let username:String = "@\(dict.value(forKey: "user_name") as! String) \(Utility.language.value(forKey: "sent") as! String)"
        let iconName = dict.value(forKey: "gift_icon") as! String
        let iconURL = URL.init(string: GIFT_IMAGE_URL+iconName)
        self.giftIcon.sd_setImage(with: iconURL, placeholderImage: #imageLiteral(resourceName: "gift_icon"))
        
        nameLbl.text = username
        let colorString = "\"\(dict.value(forKey: "gift_title") as! String)\""
        self.titleLbl.text =  " \(colorString)  \(Utility.language.value(forKey: "gift") as! String)"
        self.titleLbl.setCustomColor(colorString: colorString, color: .yellow)
    }
    
}
