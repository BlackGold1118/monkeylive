//
//  ChatCommentCell.swift
//  HSLiveStream
//
//  Created by APPLE on 10/02/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit
import Foundation

class ChatCommentCell: UITableViewCell {

    @IBOutlet weak var cmtContainerView: UIView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet var msgTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setUpThisView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setUpThisView()  {
        cmtContainerView.layer.cornerRadius = 10
        cmtContainerView.clipsToBounds = true
        cmtContainerView.backgroundColor =  UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.8)
        self.msgTextView.font = lite
        self.msgTextView.textColor = .white
        self.msgTextView.backgroundColor = .clear
        self.msgTextView.isUserInteractionEnabled = true
        self.msgTextView.isEditable = false
        self.msgTextView.isScrollEnabled = false
        self.msgTextView.textAlignment = .left
    }
    func comment(dict:NSDictionary) {
        let username:String = "@\(dict.value(forKey: "user_name") as! String)"
        let msg = dict.value(forKey: "message") as? String
        usernameLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .left, text: "")
        usernameLbl.text = username
        self.msgTextView.text = msg
        
        
        let attributes = [NSAttributedString.Key.font: lite]
        let rect: CGRect = usernameLbl.text!.boundingRect(with: CGSize(width: 255, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: attributes as [NSAttributedString.Key : Any], context: nil)
      
        var fixedWidth =  self.msgTextView.intrinsicContentSize.width
        if fixedWidth > 240{
            fixedWidth = 240
        }
        

        self.msgTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize =  self.msgTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame =  self.msgTextView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            if newFrame.size.width > 100{
                self.msgTextView.frame = CGRect.init(x: 7, y: 17, width: newFrame.size.width, height: newFrame.size.height)
            }else{
                self.msgTextView.frame = CGRect.init(x: 7, y: 17, width:100, height: newFrame.size.height)
            }
        
        let msgWidth = self.msgTextView.frame.size.width+20
        let nameWidth = rect.width+20
        
        if msgWidth<nameWidth{
            self.cmtContainerView.frame =  CGRect.init(x: 5, y: 5, width:nameWidth, height: self.msgTextView.frame.origin.y+self.msgTextView.frame.size.height)
        }else{
            self.cmtContainerView.frame =  CGRect.init(x: 5, y: 5, width:msgWidth, height: self.msgTextView.frame.origin.y+self.msgTextView.frame.size.height)
        }
    }
}
