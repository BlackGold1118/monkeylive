//
//  PublishLivePage.swift
//  Livza
//
//  Created by HTS-Product on 05/09/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import WowzaGoCoderSDK
import SocketIO

class PublishLivePage: UIViewController, WOWZStatusCallback, WOWZVideoSink,WOWZVideoEncoderSink, WOWZAudioSink,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var settingsButton:UIButton!
    @IBOutlet weak var customPreviewView: UIView!
    @IBOutlet weak var speakerIconView: UIImageView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var cameraTipLabel: UILabel!
    @IBOutlet weak var swapTipLabel: UILabel!
    @IBOutlet weak var tipInfoLabel: UILabel!
    @IBOutlet weak var liveLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var tipView: UIView!
    @IBOutlet weak var previewImgView: UIImageView!
    @IBOutlet weak var moreInfoView: UIView!
    @IBOutlet weak var chatUserContainerView: UIView!
    @IBOutlet weak var stopBroadcastBtn: UIButton!
    @IBOutlet weak var watchCountView: UIView!
    @IBOutlet weak var watchingCountLbel: UILabel!
    @IBOutlet weak var userlistCollectionView: UICollectionView!
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var hideLabel: UILabel!
    @IBOutlet weak var streamDetailContainerView: UIView!
    @IBOutlet weak var broadCastDetailsLabel: UILabel!
    @IBOutlet weak var menu_liveLabel: UILabel!
    @IBOutlet weak var menu_ViewCount: UILabel!
    @IBOutlet weak var menu_watchCountView: UIView!
    @IBOutlet weak var streamNameLabel: UILabel!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var menuUserListView: UIView!
    @IBOutlet weak var liveViewersLabel: UILabel!
    @IBOutlet weak var totalCountLbl: UILabel!
    @IBOutlet weak var totalViewersLabel: UILabel!
    @IBOutlet weak var userlistTableView: UITableView!
    
    @IBOutlet var loaderView: DotsLoader!
    @IBOutlet var loaderStatusLbl: UILabel!
    
    
    let chat_Hide_Seconds = 6.0
    var timer = Timer()
    var userListArray = NSMutableArray()
    var commentArray = NSMutableArray()
    var goCoder:WowzaGoCoder?
    var goCoderConfig:WowzaConfig!
    var receivedGoCoderEventCodes = Array<WOWZEvent>()
    var blackAndWhiteVideoEffect = false
    var bitmapOverlayVideoEffect = false /// Not supported in swift version
    var goCoderRegistrationChecked = false
    var profile:ProfileModel!
    var stream_name = String()
    var stream_des = String()
    var streamStarted : Bool = false
    var thumbnail_Captured : Bool = false
    var menuAppear : Bool = false
    var stream_duration = String()
    var startRequest = NSMutableDictionary()
    
    //MARK: - Class Member Variables
    let SDKSampleSavedConfigKey = "SDKSampleSavedConfigKey"
    let BlackAndWhiteEffectKey = "BlackAndWhiteKey"
    let BitmapOverlayEffectKey = "BitmapOverlayKey"
    
    let manager = SocketManager(socketURL: URL(string: SOCKET_IO_URL)!, config: [.log(true), .compress])
    
    var broadcastStartTime = CMTime()
    
    //MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.broadcastStartTime = CMTime.invalid
        // Reload any saved data
        blackAndWhiteVideoEffect = UserDefaults.standard.bool(forKey: BlackAndWhiteEffectKey)
        bitmapOverlayVideoEffect = UserDefaults.standard.bool(forKey: BitmapOverlayEffectKey)
        WowzaGoCoder.setLogLevel(.default)
        
        if let savedConfig:Data = UserDefaults.standard.object(forKey: SDKSampleSavedConfigKey) as? Data {
            if let wowzaConfig = NSKeyedUnarchiver.unarchiveObject(with: savedConfig) as? WowzaConfig {
                goCoderConfig = wowzaConfig
            }
            else {
                goCoderConfig = WowzaConfig()
            }
        }
        else {
            goCoderConfig = WowzaConfig()
        }
        // Log version and platform info
        //  print("WowzaGoCoderSDK version =\n major: \(WOWZVersionInfo.majorVersion())\n minor: \(WOWZVersionInfo.minorVersion())\n revision: \(WOWZVersionInfo.revision())\n build: \(WOWZVersionInfo.buildNumber())\n string: \(WOWZVersionInfo.string())\n verbose string: \(WOWZVersionInfo.verboseString())")
        
        // print("Platform Info:\n\(WOWZPlatformInfo.string())")
        
        let defaultDict:NSDictionary = UserModel.shared.getDefaults()!
        let sdk_license = defaultDict.value(forKeyPath: "stream_connection_info.ios_sdk_license") as? String
        print("sdk license \(sdk_license!)")
        if let goCoderLicensingError = WowzaGoCoder.registerLicenseKey(sdk_license!) {
            self.showAlert("GoCoder SDK Licensing Error", error: goCoderLicensingError as NSError)
        }
        //            self.getLiveDetails()
        
        self.socketConnect()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let savedConfigData = NSKeyedArchiver.archivedData(withRootObject: goCoderConfig)
        UserDefaults.standard.set(savedConfigData, forKey: SDKSampleSavedConfigKey)
        
        // Update the configuration settings in the GoCoder SDK
        if (goCoder != nil) {
            goCoder?.config = goCoderConfig
            
            blackAndWhiteVideoEffect = UserDefaults.standard.bool(forKey: BlackAndWhiteKey)
            bitmapOverlayVideoEffect = UserDefaults.standard.bool(forKey: BitmapOverlayKey)
        }
                    self.view.bringSubviewToFront(self.settingsButton)
        self.changeToRTL()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        goCoder?.cameraPreview?.previewLayer?.frame = view.bounds
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.configWowza()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.goCoder?.cameraPreview?.stop()
    }
    
    override var prefersStatusBarHidden:Bool {
        return true
    }
    
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
            self.streamNameLabel.textAlignment = .right
            self.hideLabel.textAlignment = .right
            self.broadCastDetailsLabel.textAlignment = .right
            self.menu_liveLabel.textAlignment = .right
            self.menuView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.streamNameLabel.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else{
            self.streamNameLabel.textAlignment = .left
            self.hideLabel.textAlignment = .left
            self.broadCastDetailsLabel.textAlignment = .left
            self.menu_liveLabel.textAlignment = .left
            self.menuView.transform = .identity
            self.view.transform = .identity
            self.streamNameLabel.transform = .identity
        }
    }
    
    //initial setup
    func initialSetup()  {
        
        UIApplication.shared.isIdleTimerDisabled = true
        isWatchingLive = true
        self.fd_prefersNavigationBarHidden = true
        self.fd_interactivePopDisabled = true
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchCamera(_:)))
        tap.numberOfTapsRequired = 2
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        
        self.configPreview()
        self.configChatView()
        chatUserContainerView.isHidden = true
        
    }
    
    //wowza configuration
    func configWowza() {
        print("called config")
        if !goCoderRegistrationChecked {
            let defaultDict:NSDictionary = UserModel.shared.getDefaults()!
            let sdk_license = defaultDict.value(forKeyPath: "stream_connection_info.ios_sdk_license") as? String
            if let goCoderLicensingError = WowzaGoCoder.registerLicenseKey(sdk_license!) {
                self.showAlert("GoCoder SDK Licensing Error", error: goCoderLicensingError as NSError)
            }
            else {
                goCoderRegistrationChecked = true
                
                // Initialize the GoCoder SDK
                if let goCoder = WowzaGoCoder.sharedInstance() {
                    self.goCoder = goCoder
                    
                    // Request camera and microphone permissions
                    WowzaGoCoder.requestPermission(for: .camera, response: { (permission) in
                        //      print("Camera permission is: \(permission == .authorized ? "authorized" : "denied")")
                    })
                    
                    WowzaGoCoder.requestPermission(for: .microphone, response: { (permission) in
                        //   print("Microphone permission is: \(permission == .authorized ? "authorized" : "denied")")
                    })
                    
                    self.goCoder?.register(self as WOWZAudioSink)
                    self.goCoder?.register(self as WOWZVideoSink)
                    self.goCoder?.register(self as WOWZVideoEncoderSink)
                    
                    
                    self.goCoderConfig.hostAddress = defaultDict.value(forKeyPath: "stream_connection_info.stream_host") as? String
                    self.goCoderConfig.applicationName = defaultDict.value(forKeyPath: "stream_connection_info.stream_application_name") as? String
                    self.goCoderConfig.streamName = self.stream_name
                    self.goCoderConfig.username = defaultDict.value(forKeyPath: "stream_connection_info.stream_username") as? String
                    self.goCoderConfig.password = defaultDict.value(forKeyPath: "stream_connection_info.stream_password") as? String
                    self.goCoderConfig.portNumber = defaultDict.value(forKeyPath: "stream_connection_info.stream_port") as! UInt
                    self.goCoderConfig.backgroundBroadcastEnabled = true
                    
                    self.goCoderConfig.videoWidth = 352
                    self.goCoderConfig.videoHeight = 288
                    self.goCoderConfig.load(WOWZFrameSizePreset.preset352x288)
                    
   
                    self.goCoder?.config = self.goCoderConfig
                    // Specify the view in which to display the camera preview
                    self.goCoder?.cameraView = self.view
                    //full screen
                    self.goCoder?.cameraPreview?.previewGravity = .resizeAspectFill
                    // Start the camera preview
                    self.goCoder?.cameraPreview?.start()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        //start publishing live video
                        self.startBroadCast()
                    }
                }
                
            }
        }
        else{
            // Start the camera preview
            self.goCoder?.cameraPreview?.start()
        }
    }
    
    //MARK: Alertview with action
    func showAlertWith(msg:String) {
        let alertVC = CPAlertVC(title:APP_NAME, message: msg)
        alertVC.animationType = .bounceUp
        alertVC.addAction(CPAlertAction(title: "Ok", type: .normal, handler: {
        }))
        alertVC.show(into: self)
    }
    
    
    //live preview screen configuration
    func configPreview()  {
        self.customPreviewView.isHidden = false
        do{
            let jsonDecoder = JSONDecoder()
            profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
            let imgURL = URL.init(string: PROFILE_IMAGE_URL+profile.user_image!)
            if imgURL != nil{
                let profile_data = try? Data(contentsOf: imgURL!)
                if profile_data != nil{
                    previewImgView.image = UIImage(data: profile_data!)
                }else{
                    previewImgView.image = #imageLiteral(resourceName: "user_placeholder")
                }
            }else{
                previewImgView.image = #imageLiteral(resourceName: "user_placeholder")
            }
            self.previewImgView.layer.minificationFilter = CALayerContentsFilter.trilinear
            self.previewImgView.layer.minificationFilterBias = 5.0
            
        }catch{
            
        }
        self.loaderView.tintColor = .white
        self.loaderView.dotsCount = 4
        self.loaderView.dotsRadius = 5
        self.loaderView.startAnimating()
        
        self.loaderStatusLbl.config(color: .white, font: mediumReg, align: .center, text: "initialise")
        
        customPreviewView.backgroundColor = .clear
        liveLabel.config(color: .white, font: high, align: .left, text: "live")
        infoLabel.config(color: .white, font: mediumReg, align: .left, text: "you_are_now")
        tipInfoLabel.config(color: TEXT_PRIMARY_COLOR, font: small, align: .left, text: "live_tips")
        cameraTipLabel.config(color: .white, font: mediumReg, align: .center, text: "double_tap_camera_hint")
        swapTipLabel.config(color: .white, font: mediumReg, align: .center, text: "swap_top_hint")
        tipView.cornerViewRadius()
        
        self.view.bringSubviewToFront(customPreviewView)
    }
    
    
    func configChatView()  {
        //            registerCustomCells()
        chatTableView.delegate = self
        chatTableView.dataSource = self
        //set collection view scroll horizontally
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        userlistCollectionView.collectionViewLayout = collectionViewFlowLayout
        watchCountView.cornerLeft()
        self.watchCountView.backgroundColor = CIRCLE_BG_COLOR
        self.moreInfoView.backgroundColor = CIRCLE_BG_COLOR
        
        moreInfoView.cornerViewRadius()
        self.chatUserContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
        self.chatTableView.frame = CGRect.init(x: 10, y: self.watchCountView.frame.origin.y-220, width: 270, height: 200)

        self.configStreamDetails()
        self.watchingCountLbel.config(color: .white, font: lite, align: .center, text: "")
        self.watchingCountLbel.text = "0"
        self.menu_ViewCount.text = "0"
        self.totalCountLbl.text = "0"
        self.stopBroadcastBtn.cornerRoundRadius()
        self.stopBroadcastBtn.config(color: .white, font: medium, align: .center, title: "stop_broadcasting")
        setupSwipeFunction()
        self.registerCustomCells()
    }
    
    //MARK: Register custom cells
    func registerCustomCells(){
        
        chatTableView.register(UINib(nibName: "ChatCommentCell", bundle: nil), forCellReuseIdentifier: "ChatCommentCell")
        chatTableView.register(UINib(nibName: "UserJoinedCell", bundle: nil), forCellReuseIdentifier: "UserJoinedCell")
        userlistCollectionView.register(UINib(nibName: "UserListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserListCollectionViewCell")
        userlistTableView.register(UINib(nibName: "UserListTableCell", bundle: nil), forCellReuseIdentifier: "UserListTableCell")
        chatTableView.register(UINib(nibName: "GiftCell", bundle: nil), forCellReuseIdentifier: "GiftCell")
        
    }
    //stream design
    func configStreamDetails()  {
        streamDetailContainerView.backgroundColor = .black
        streamDetailContainerView.alpha = 0.8
        hideLabel.config(color: .white, font: average, align: .left, text: "hide_chat")
        broadCastDetailsLabel.config(color: .white, font: average, align: .left, text: "broadcast_details")
        menu_liveLabel.config(color: .white, font: lowReg, align: .center, text: "live")
        menu_ViewCount.config(color: .white, font: lowReg, align: .center, text: "")
        menu_liveLabel.backgroundColor = .darkGray
        menu_watchCountView.cornerViewRadius()
        menu_watchCountView.backgroundColor = PRIMARY_COLOR
        streamNameLabel.config(color: .white, font: high, align: .left, text: "")
        streamNameLabel.text = self.stream_des
        streamDetailContainerView.isHidden = true
        totalCountLbl.config(color: .white, font: high, align: .center, text: "")
        
        // configure menu user list view
        totalViewersLabel.config(color: .white, font: averageReg, align: .center, text: "total_viewers")
        liveViewersLabel.config(color: .white, font: liteReg, align: .center, text: "live_viewers")
        
        menuUserListView.backgroundColor = .clear
    }
    
    //set stream details
    func setDetails(dict:NSDictionary) {
        self.menu_ViewCount.text = "\(dict.value(forKey: "watch_count") as! String)"
        self.watchingCountLbel.text = "\(dict.value(forKey: "watch_count") as! String)"
        self.totalCountLbl.text = "\(dict.value(forKey: "watch_count") as! String)"
        
        self.userListArray.removeAllObjects()
        self.userListArray.addObjects(from:dict.value(forKey: "live_viewers") as! [Any])
        self.userlistTableView.reloadData()
        self.userlistCollectionView.reloadData()
    }
    
    //MARK: Swipe Fucntion
    func setupSwipeFunction(){
        let swipeTop = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeTop.direction = UISwipeGestureRecognizer.Direction.up
        self.view.addGestureRecognizer(swipeTop)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    // swipe reponse gesture
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.down:
                self.menuAppear = false
                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
                    // Add the transformation in this block
                    self.chatUserContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
                    self.streamDetailContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
                    self.menuUserListView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
                }, completion: nil)
            case UISwipeGestureRecognizer.Direction.up:
                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
                    // Add the transformation in this block
                    self.chatUserContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
                }, completion: nil)
            default:
                break
            }
        }
    }
    
    //start wowza
    func startBroadCast()  {
        if let configError = goCoder?.config.validateForBroadcast() {
            //                self.showAlert("Incomplete Streaming Settings", error: configError as NSError)
            print(configError.localizedDescription)
        }
        else {
            receivedGoCoderEventCodes.removeAll()
            goCoder?.startStreaming(self)
            goCoder?.isAudioMuted = false
        }
    }
    //start stream api call
    func startStreamAPI(){
        let Obj = BaseWebService()
        Obj.baseService(subURl: START_STREAM_API, params: startRequest as? Parameters, onSuccess: {response in
        })
    }
    
    
    @objc func switchCamera(_ recognizer : UITapGestureRecognizer) {
        if let otherCamera = goCoder?.cameraPreview?.otherCamera() {
            if !otherCamera.supportsWidth(goCoderConfig.videoWidth) {
                goCoderConfig.load(otherCamera.supportedPresetConfigs.last!.toPreset())
                goCoder?.config = goCoderConfig
            }
            goCoder?.cameraPreview?.switchCamera()
        }
    }
    
    func enableTourch() {
        var newTorchState = goCoder?.cameraPreview?.camera?.isTorchOn ?? true
        newTorchState = !newTorchState
        goCoder?.cameraPreview?.camera?.isTorchOn = newTorchState
    }
    
    func muteAudio() {
        var newMutedState = self.goCoder?.isAudioMuted ?? true
        newMutedState = !newMutedState
        goCoder?.isAudioMuted = newMutedState
    }
    
    
    @IBAction func watchCountBtnTapped(_ sender: Any) {
        if userListArray.count != 0{
            self.userlistTableView.reloadData()
        }
        self.streamDetailContainerView.isHidden = false
        self.menuView.isHidden = true
        self.menuUserListView.isHidden = false
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            self.streamDetailContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.menuUserListView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
        }, completion: nil)
    }
    
    
    
    @IBAction func moreBtnTapped(_ sender: Any) {
        self.menuAppear = true
        menuUserListView.isHidden = true
        menuView.isHidden = false
        self.streamDetailContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            self.chatUserContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
            // Add the transformation in this block
            self.streamDetailContainerView.isHidden = false
            self.view.bringSubviewToFront(self.streamDetailContainerView)
        }, completion: nil)
    }
    
    @IBAction func hideBtnTapped(_ sender: Any) {
        self.menuAppear = false
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            self.chatUserContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.streamDetailContainerView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
            self.menuUserListView.frame = CGRect(x:0,y:FULL_HEIGHT,width:FULL_WIDTH,height:FULL_HEIGHT)
        }, completion: nil)
    }
    
    @IBAction func stopBroadcastBtnTapped(_ sender: Any) {
        let alertVC = CPAlertVC(title:APP_NAME, message: Utility.language.value(forKey: "want_to_stop_broadcast") as! String)
        alertVC.animationType = .bounceUp
        alertVC.addAction(CPAlertAction(title: Utility.language.value(forKey: "yes") as! String, type: .normal, handler: {
            self.stopBroadCasting()
        }))
        alertVC.addAction(CPAlertAction(title: Utility.language.value(forKey: "no") as! String, type: .cancel, handler: {
        }))
        alertVC.show(into: self)
    }
    
    
    
    func stopBroadCasting()  {
        UIApplication.shared.isIdleTimerDisabled = false
    
        if self.goCoder?.status.state == .running {
            self.goCoder?.endStreaming(self)
            self.goCoder?.unregisterAudioSink(self)
            self.goCoder?.unregisterVideoSink(self)
            self.goCoder?.unregisterVideoEncoderSink(self)
         
        }
        print("FINAL DURATION \(self.stream_duration)")
      
        self.timer.invalidate()
        let obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "publisher_id")
        requestDict.setValue(self.stream_name, forKey: "name")
        requestDict.setValue(self.stream_duration, forKey: "duration")
        
        obj.baseService(subURl: STOP_STREAM_API, params:  requestDict as? Parameters, onSuccess: {response in
            isWatchingLive = false
            self.dismiss(animated: true, completion: nil)
        });
    }
    @IBAction func broadcastDetailBtnTapped(_ sender: Any) {
        menuView.isHidden = true
        menuUserListView.isHidden = false
        self.menuUserListView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
    }
    
    func refreshCommentView()  {
             let indexPathArray = [IndexPath(row: self.commentArray.count - 1, section: 0)]
                 self.chatTableView.beginUpdates()
                 self.chatTableView.insertRows(at: indexPathArray, with: .automatic)
                 self.chatTableView.endUpdates()
          
          let indexPath = IndexPath(row: self.commentArray.count-1, section: 0)
              self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
              DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                  let indexPath = IndexPath(row: self.commentArray.count-1, section: 0)
                  self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
              }
          
    }
 
  
    @IBAction func didTapSettingsButton(_ sender:AnyObject?) {
        if let settingsNavigationController = UIStoryboard(name: "AppSettings", bundle: nil).instantiateViewController(withIdentifier: "settingsNavigationController") as? UINavigationController {
            
            if let settingsViewController = settingsNavigationController.topViewController as? SettingsViewController {
                settingsViewController.addAllSections()
                //                    settingsViewController.removeDisplay(.recordVideoLocally)
                //                    settingsViewController.removeDisplay(.backgroundMode)
                let viewModel = SettingsViewModel(sessionConfig: goCoderConfig)
                viewModel?.supportedPresetConfigs = goCoder?.cameraPreview?.camera?.supportedPresetConfigs
                settingsViewController.viewModel = viewModel!
            }
            
            self.present(settingsNavigationController, animated: true, completion: nil)
        }
    }
    
    
    
    
    //MARK: Upload thumbnail image to server
    func triggerThumbnailStatus()  {
        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.changeStatus), userInfo: nil, repeats: true)
    }
    @objc func changeStatus(){
        self.thumbnail_Captured = true
    }
    
    func screenshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions((self.goCoder?.cameraView?.view.superview?.bounds.size)!, (self.goCoder?.cameraView?.view.superview?.isOpaque)!, 0.0)
        defer { UIGraphicsEndImageContext() }
        if let context = UIGraphicsGetCurrentContext() {
            self.goCoder?.cameraView?.view.superview?.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            return image
        }
        return nil
    }
    
    
    //#MARK: Table view delegate & data source
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int{
        if (tableView == chatTableView){
            return self.commentArray.count
        }else if(tableView == userlistTableView){
            return self.userListArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var customCell = UITableViewCell()
        if (tableView == chatTableView){
            
            let responseDict:NSDictionary = commentArray.object(at: indexPath.row) as! NSDictionary
            let responseType:String = responseDict.value(forKey: "type") as! String
            if(responseType == "text"){
                let cmtCell = tableView.dequeueReusableCell(withIdentifier: "ChatCommentCell", for: indexPath) as! ChatCommentCell
                cmtCell.cmtContainerView.alpha = 1.0
                cmtCell.comment(dict: responseDict)
                //hide cell with animation
                UIView.animate(withDuration: chat_Hide_Seconds, animations: {() -> Void in
                    cmtCell.cmtContainerView.alpha = 0
                })
                tableView.rowHeight = cmtCell.cmtContainerView.frame.size.height+5
                customCell = cmtCell
            }else if (responseType == "joined"){
                let joinCell = tableView.dequeueReusableCell(withIdentifier: "UserJoinedCell", for: indexPath) as! UserJoinedCell
                joinCell.joinUserLabel.alpha = 1.0
                joinCell.join(username: responseDict.value(forKey: "user_name") as! String)
                //hide cell with animation
                UIView.animate(withDuration: chat_Hide_Seconds, animations: {() -> Void in
                    joinCell.joinUserLabel.alpha = 0
                })
                tableView.rowHeight = 35

                customCell = joinCell
            }else if (responseType == "gift"){
                let gift = tableView.dequeueReusableCell(withIdentifier: "GiftCell", for: indexPath) as! GiftCell
                gift.containerView.alpha = 1.0
                gift.giftIcon.alpha = 1.0
                gift.config(dict: responseDict)
                //hide cell with animation
                UIView.animate(withDuration: chat_Hide_Seconds, animations: {() -> Void in
                    gift.containerView.alpha = 0
                    gift.giftIcon.alpha = 0
                })
                tableView.rowHeight = 65

                customCell = gift
            }
        }else if(tableView == userlistTableView){
            let userlistCell = tableView.dequeueReusableCell(withIdentifier: "UserListTableCell", for: indexPath) as! UserListTableCell
            let responseDict:NSDictionary = userListArray.object(at: indexPath.row) as! NSDictionary
            userlistCell.user(dict:responseDict)
            tableView.rowHeight = 55
            customCell = userlistCell
        }
        return customCell
    }
    
    
   
    //MARK: Collection view delegate & datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.userListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : UserListCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserListCollectionViewCell", for: indexPath) as! UserListCollectionViewCell
        let userDict:NSDictionary =  userListArray.object(at: indexPath.row) as! NSDictionary
        cell.config(image:userDict.value(forKey: "user_image") as! String)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 40, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }

    
    
    //MARK: - WOWZStatusCallback Protocol Instance Methods
    func onWOWZStatus(_ status: WOWZStatus!) {
        switch (status.state) {
        case .idle:
            print("Broadcast status: The broadcast is idle");
        case .running:
            DispatchQueue.main.async { () -> Void in
                print("Broadcast status: The broadcast is running");
                self.startStreamAPI()
                UIView.animate(withDuration: 1.0, animations: {
                    self.customPreviewView.alpha = 0
                }, completion: {_ in
                    self.streamStarted = true
                    self.thumbnail_Captured = true
                    self.triggerThumbnailStatus()
                    UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
                        // Add the transformation in this block
                        self.chatUserContainerView.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
                        self.chatUserContainerView.isHidden =  false
                        self.view.bringSubviewToFront(self.chatUserContainerView)
                    }, completion: nil)
                })
            }
        case .stopping:
            print("Broadcast status: The broadcast is stopped");
            self.stopBroadCasting()
            self.dismiss(animated: true, completion: nil)
        case  .starting:
            self.broadcastStartTime = CMTime.invalid
            print("Broadcast status: The broadcast is starting");
        case .buffering:
            print("Broadcast status: The broadcast is buffering");
            break
        default: break
        }
    }
    
    func onWOWZEvent(_ status: WOWZStatus!) {
        print("WOWZA ERROR STATE \(status.event)")
        // If an event is reported by the GoCoder SDK, display an alert dialog describing the event,
        // but only if we haven't already shown an alert for this event
        
        DispatchQueue.main.async { () -> Void in
            if !self.receivedGoCoderEventCodes.contains(status.event) {
                self.receivedGoCoderEventCodes.append(status.event)
                //                    self.showAlert("Live Streaming Event", status: status)
            }
            
        }
    }
    
    func onWOWZError(_ status: WOWZStatus!) {
        // If an error is reported by the GoCoder SDK, display an alert dialog containing the error details
        DispatchQueue.main.async { () -> Void in
            //                self.showAlert("Live Streaming Error", status: status)
            print("Live Streaming Error \(status.error?.localizedDescription)")
            
        }
    }
    
    
    //MARK: - WOWZZVideoSink Protocol Methods
    
    func videoFrameWasCaptured(_ imageBuffer: CVImageBuffer, framePresentationTime: CMTime, frameDuration: CMTime) {
        
        DispatchQueue.main.async {
            if self.thumbnail_Captured && self.streamStarted {
                self.thumbnail_Captured = false
                let captureImage = CIImage(cvImageBuffer: imageBuffer)
                let context = CIContext(options:nil);
                let tempImage:CGImage = context.createCGImage(captureImage, from: captureImage.extent)!
                let image = UIImage(cgImage: tempImage)
                let imageData: Data? = image.jpegData(compressionQuality: 0.0)
                
                if imageData != nil{
                    let uploadObj = BaseWebService()
                    uploadObj.uploadThumbnail(thumbnail: imageData!, name: self.stream_name, onSuccess: {response in
                        let dict = response.result.value as? NSDictionary
                        let status = dict?.value(forKey: "status") as! String
                        if status == "true"{
                        }
                    })
                }
            }
        }
        
        
        if goCoder != nil && goCoder!.isStreaming && blackAndWhiteVideoEffect {
            // convert frame to b/w using CoreImage tonal filter
            var frameImage = CIImage(cvImageBuffer: imageBuffer)
            if let grayFilter = CIFilter(name: "CIPhotoEffectTonal") {
                grayFilter.setValue(frameImage, forKeyPath: "inputImage")
                if let outImage = grayFilter.outputImage {
                    frameImage = outImage
                    let context = CIContext(options: nil)
                    context.render(frameImage, to: imageBuffer)
                }
            }
        }
        
    }
 
  
    func videoCaptureInterruptionStarted() {
        goCoder?.endStreaming(self)
    }
    
    
    //MARK: - WOWZAudioSink Protocol Methods
    
    func audioLevelDidChange(_ level: Float) {
        //        print("Audio level did change: \(level)");
    }
    
    //MARK: - WOWZVideoEncoderSink Protocol Methods

    func videoFrameWasEncoded(_ data: CMSampleBuffer) {
        
        if CMTimeCompare(broadcastStartTime, CMTime.invalid) == 0 {
            broadcastStartTime = CMSampleBufferGetPresentationTimeStamp(data)
        } else {
            let diff = CMTimeSubtract(CMSampleBufferGetPresentationTimeStamp(data), broadcastStartTime)
            let seconds = CMTimeGetSeconds(diff)
            let duration = Int(seconds-1)
            let timeString = String(format: "%02ld:%02ld", duration / 60, duration % 60)

            DispatchQueue.main.async(execute: {
                self.stream_duration = timeString
            })
        }
    }
    
    //MARK: - Alerts
    func showAlert(_ title:String, status:WOWZStatus) {
        let alertController = UIAlertController(title: title, message: status.description, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(_ title:String, error:NSError) {
        let alertController = UIAlertController(title: title, message: error.localizedDescription, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    //MARK: SOCKET IO
    @objc func showHeartLike()  {
        let heart = HeartView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        self.chatUserContainerView.addSubview(heart)
        heart.center = CGPoint(x: FULL_WIDTH-80, y: FULL_HEIGHT-80)
        heart.animateInView(view: self.chatUserContainerView)
    }
    
    //connect socket
    func socketConnect()  {
        manager.defaultSocket.connect()
        self.addHandlers()
    }
    //join room
    func joinRoom()  {
        let req = NSMutableDictionary()
        req.setValue(self.stream_name, forKey: "stream_name")
        req.setValue(UserModel.shared.userId, forKey: "user_id")
        manager.defaultSocket.emit("_publishStream", with: [req])
    }
    
    //send join msg to stream
    func joinMsg()  {
        let req = NSMutableDictionary()
        req.setValue(UserModel.shared.userId, forKey: "user_id")
        req.setValue(profile.name, forKey: "user_name")
        req.setValue(profile.user_image, forKey: "user_image")
        req.setValue(self.stream_name, forKey: "stream_name")
        req.setValue("joined", forKey: "type")
        manager.defaultSocket.emit("_sendMsg", with: [req])
    }
    //get stream info
    func getStreamInfo()  {
        let req = NSMutableDictionary()
        req.setValue(UserModel.shared.userId, forKey: "user_id")
        req.setValue(self.stream_name, forKey: "stream_name")
        manager.defaultSocket.emit("_getstreamInfo", with: [req])
    }
    //listners
    func addHandlers()  {
        manager.defaultSocket.on(clientEvent: .connect) {data, ack in
            print("SOCKET_IO CONNECTED")
            self.joinRoom()
            self.joinMsg()
        }
        manager.defaultSocket.on("_msgReceived", callback: {data, ack in
            print("SOCKET_IO MSG \(data)")
            let dict = data[0] as! NSDictionary
            let msg_type = dict.value(forKey: "type") as! String
            // heart like
            if msg_type == "liked"{
                HeartTheme.selectedColor = dict.value(forKey: "like_color") as! String
                self.showHeartLike()
            }else if msg_type == "joined"{
                self.commentArray.add(dict)
                self.refreshCommentView()
                self.getStreamInfo()
            }else if msg_type == "text"{
                self.commentArray.add(dict)
                self.refreshCommentView()
            }else if msg_type == "gift"{
                self.commentArray.add(dict)
                self.refreshCommentView()
            }
            
        })
        
        //stream details
        manager.defaultSocket.on("_streamInfo", callback: {data, ack in
            print("SOCKET_IO STREAM INFO \(data)")
            let dict = data[0] as! NSDictionary
            self.setDetails(dict: dict)
        })
        
        //stream user left
        manager.defaultSocket.on("_subscriberLeft", callback: {data, ack in
            print("SOCKET_IO LEFT \(data)")
            self.getStreamInfo()
        })
        
        //stream block action
        manager.defaultSocket.on("_streamBlocked", callback: {data, ack in
            print("SOCKET_IO BLOCK STATUS \(data)")
            if self.goCoder?.status.state == .running {
                self.goCoder?.endStreaming(self)
            }
            let alertVC = CPAlertVC(title:APP_NAME, message: Utility.language.value(forKey: "stream_blocked") as! String)
            alertVC.animationType = .bounceUp
            alertVC.addAction(CPAlertAction(title: Utility.language.value(forKey: "yes") as! String, type: .normal, handler: {
                self.stopBroadCasting()
            }))
            alertVC.show(into: self)
        })
    }
}

