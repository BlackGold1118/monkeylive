//
//  UserListPageCell.swift
//  HSLiveStream
//
//  Created by APPLE on 19/02/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit

class UserListPageCell: UITableViewCell {
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var separatorLine: UILabel!
    @IBOutlet weak var followersLbl: UILabel!
    @IBOutlet weak var followingLbl: UILabel!
    @IBOutlet weak var fullNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func config(userDict:NSDictionary) {
        userImgView.makeItRound()
        //adjust width based on username text
        separatorLine.backgroundColor = TEXT_SECONDARY_COLOR
        //setup all details
        followingLbl.config(color: TEXT_SECONDARY_COLOR, font: small, align: .left, text: EMPTY_STRING)
        fullNameLbl.config(color: TEXT_COLOR, font: medium, align: .left, text: EMPTY_STRING)

        let followCount:NSNumber = userDict.value(forKey: "followers") as! NSNumber
        followersLbl.config(color: TEXT_SECONDARY_COLOR, font: small, align: .left, text: EMPTY_STRING)
        
        let followingsCount:NSNumber = userDict.value(forKey: "followings") as! NSNumber
        followingLbl.text = "\(followingsCount.stringValue) \(Utility.language.value(forKey: "followings")as! String)"
        self.fullNameLbl.text = (userDict.value(forKey: "name") as! String)
        followersLbl.text = "\(followCount.stringValue) \((Utility.language.value(forKey: "followers")as! String))"
        
        let img_name = userDict.value(forKey: "user_image") as? String        
        userImgView.sd_setImage(with: URL(string: img_name!), placeholderImage: #imageLiteral(resourceName: "user_placeholder"))
        
    }
    
}
