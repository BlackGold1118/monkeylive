//
//  RecentVideoCell.swift
//  HSLiveStream
//
//  Created by APPLE on 21/02/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit

class RecentVideoCell: UITableViewCell {
    
    @IBOutlet weak var video_thumbnailView: UIImageView!
    @IBOutlet weak var viewIcon: UIImageView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var videoDescriptionLbl: UILabel!
    @IBOutlet weak var watchCountLbl: UILabel!
    @IBOutlet weak var TypeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCellWithDetails(videoDict:NSDictionary,viewType:String)  {
        
        TypeLbl.cornerRadius()
        viewIcon.image = #imageLiteral(resourceName: "home_view_icon")
        TypeLbl.backgroundColor = LIVE_COLOR
        TypeLbl.config(color: .white, font: small, align: .center, text: "live")
        TypeLbl.config(color: .white, font: small, align: .center, text: "live")
        watchCountLbl.config(color: .white, font: small, align: .left, text: EMPTY_STRING)
        let count = videoDict.value(forKey: "watch_count") as! NSNumber
        watchCountLbl.text = count.stringValue
        video_thumbnailView.cornerViewMiniumRadius()
        video_thumbnailView.sd_setImage(with: URL(string: (videoDict.value(forKey: "publisher_image") as? String)!), placeholderImage: #imageLiteral(resourceName: "placeholder_video"))
        
        videoDescriptionLbl.config(color: .white, font: average, align: .left, text: EMPTY_STRING)
        videoDescriptionLbl.text = (videoDict.value(forKey: "title") as! String)
        //set time
        timeLbl.config(color: TEXT_SECONDARY_COLOR, font: small, align: .left, text:EMPTY_STRING)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let localDate = formatter.date(from: videoDict.value(forKey: "streamed_on") as! String)
        if localDate != nil{
            self.timeLbl.text = Utility.shared.makeFormat(to: localDate!)
        }
        
        if viewType == "search" {
            self.contentView.backgroundColor = .white
            self.backgroundColor = .white
            TypeLbl.cornerRadius()
            viewIcon.image = #imageLiteral(resourceName: "view_icon_gray")
            TypeLbl.backgroundColor = LIVE_COLOR
            videoDescriptionLbl.textColor = TEXT_COLOR
            watchCountLbl.textColor = TEXT_SECONDARY_COLOR
        }
    }
    
    
}
