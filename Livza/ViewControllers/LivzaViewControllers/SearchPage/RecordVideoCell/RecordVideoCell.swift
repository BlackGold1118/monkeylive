//
//  RecordVideoCell.swift
//  HSLiveStream
//
//  Created by APPLE on 08/03/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit

class RecordVideoCell: UITableViewCell {

    @IBOutlet weak var video_thumbnailView: UIImageView!
    @IBOutlet weak var createdTimeLbl: UILabel!
    @IBOutlet weak var postedbyLbl: UILabel!
    @IBOutlet weak var videoDescriptionLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configureCellWithDetails(videoDict:NSDictionary,viewType:String)  {

        video_thumbnailView.cornerViewMiniumRadius()
        video_thumbnailView.sd_setImage(with: URL(string: (videoDict.value(forKey: "publisher_image") as? String)!), placeholderImage: #imageLiteral(resourceName: "placeholder_video"))
        videoDescriptionLbl.config(color: .white, font: average, align: .left, text: EMPTY_STRING)
        
        videoDescriptionLbl.text = videoDict.value(forKey: "title") as? String        //set time
//        let dateNew = Date(timeIntervalSince1970:createdDate!)
        createdTimeLbl.config(color: TEXT_SECONDARY_COLOR, font: small, align: .left, text:EMPTY_STRING )
        
            let formatter = DateFormatter()
             formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
             let localDate = formatter.date(from: videoDict.value(forKey: "streamed_on") as! String)
             if localDate != nil{
                 self.createdTimeLbl.text = Utility.shared.makeFormat(to: localDate!)
             }
        
        postedbyLbl.config(color: .white, font: lite, align: .left, text: EMPTY_STRING)
        postedbyLbl.text = videoDict.value(forKey: "posted_by") as? String
        if viewType == "search" {
            self.contentView.backgroundColor = .white
            self.backgroundColor = .white
            videoDescriptionLbl.textColor = TEXT_COLOR
            postedbyLbl.textColor = TEXT_COLOR
            createdTimeLbl.textColor = TEXT_SECONDARY_COLOR
        }
    }
    
}
