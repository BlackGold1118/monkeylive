//
//  SearchPage.swift
//  HSLiveStream
//
//  Created by APPLE on 22/02/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit
import JTMaterialTransition

class SearchListPage: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var broadcastBtn: UIButton!
    @IBOutlet weak var broadcastTableView: UITableView!
    @IBOutlet weak var textCloseView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var userBtn: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var noItemView: UIView!
    @IBOutlet weak var noUserImgView: UIImageView!
    @IBOutlet weak var noUserLbl: UILabel!
    @IBOutlet weak var userBorder: UILabel!
    @IBOutlet weak var broadcastBorder: UILabel!
    @IBOutlet weak var userTableView: UITableView!
    @IBOutlet var loader: UIActivityIndicatorView!
    
    var userArray = NSMutableArray()
    var broadcastArray = NSMutableArray()
    var viewType = String()
    var keyboardEnable = Bool()
    var userServiceEnable = Bool()
    let BROADCAST_VIEW = "0"
    let USER_VIEW = "1"
    var transition: JTMaterialTransition?
    var broadcastCount = Int()
    var userCount = Int()
    var isFirst = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpThisview()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.fd_prefersNavigationBarHidden = true
        self.initialSetup()
        self.changeToRTL()
    }
    override func viewDidLayoutSubviews() {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func changeToRTL() {
        
        if UserModel.shared.language == "Arabic" {
        self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
        self.searchTextField.transform = CGAffineTransform(scaleX: -1, y: 1)
        self.searchTextField.textAlignment = .right
        }
        else{
            self.view.transform = .identity
            self.searchTextField.transform = .identity
            self.searchTextField.textAlignment = .left
        }
    }
    
    //MARK:initial setup
    func setUpThisview(){
        loader.color = PRIMARY_COLOR;
        keyboardEnable = false
        userServiceEnable = false
        self.navigationView.backgroundColor = BLACK_COLOR
        self.textCloseView.isHidden = true
        //add loader
        broadcastTableView.register(UINib(nibName: "RecentVideoCell", bundle: nil), forCellReuseIdentifier: "RecentVideoCell")
        broadcastTableView.register(UINib(nibName: "RecordVideoCell", bundle: nil), forCellReuseIdentifier: "RecordVideoCell")
        userTableView.register(UINib(nibName: "UserListPageCell", bundle: nil), forCellReuseIdentifier: "UserListPageCell")
        
        noItemView.isHidden = true
        self.configureBroadCastView()
        self.loadMoreBroadCastVideos()
    }
    
    func initialSetup()  {
        self.navigationController?.isNavigationBarHidden = true
        self.transition = JTMaterialTransition(animatedView: self.centerPoint())
        self.navigationView.backgroundColor = BLACK_COLOR
        self.fd_prefersNavigationBarHidden = true
        self.fd_interactivePopDisabled = true
        self.pullDownRefresh()
       }
       
    //MARK: Pulldown refresh
       func pullDownRefresh()  {
        //video pulldown
           let loadingView = DGElasticPullToRefreshLoadingViewCircle()
           broadcastTableView.tintColor = BLACK_COLOR
           broadcastTableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
               self?.broadcastTableView.removeInfiniteScroll()
            self!.broadcastCount = 0
            self!.getVideoService(offset: "0",searchKey:self!.searchTextField.text!)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                   self?.broadcastTableView.dg_stopLoading()
               })
               }, loadingView: loadingView)
        broadcastTableView.dg_setPullToRefreshFillColor(.white)
           broadcastTableView.dg_setPullToRefreshBackgroundColor(broadcastTableView.backgroundColor!)
        
        //user table
        let userloadingView = DGElasticPullToRefreshLoadingViewCircle()
                 userTableView.tintColor = BLACK_COLOR
                 userTableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
                     self?.userTableView.removeInfiniteScroll()
                    self!.userCount = 0
                    self!.getUserService(offset: "0",searchKey:self!.searchTextField.text! as NSString)
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                         self?.userTableView.dg_stopLoading()
                     })
                     }, loadingView: userloadingView)
                 userTableView.dg_setPullToRefreshFillColor(.white)
                 userTableView.dg_setPullToRefreshBackgroundColor(userTableView.backgroundColor!)
                
       }
    
    //MARK:Load more broadcast
    func loadMoreBroadCastVideos()  {
        noItemView.isHidden = true
        loader.startAnimating()
        let frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        broadcastTableView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: frame)
        broadcastTableView.beginInfiniteScroll(false)
        broadcastTableView.addInfiniteScroll { (collectionView) -> Void in
            let offset : Int = self.broadcastArray.count
            let offsetString = String(offset)
            if(self.broadcastArray.count == 0){
                self.broadcastCount = 0
                self.getVideoService(offset: "0",searchKey:self.searchTextField.text!)
            }else{
                self.broadcastCount = self.broadcastArray.count
                if self.broadcastArray.count%20 == 0{
                    self.getVideoService(offset: offsetString,searchKey:self.searchTextField.text!)
                }
            }
            // finish infinite scroll animations
            self.broadcastTableView.finishInfiniteScroll()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //MARK:Load more user
    func loadMoreUserList()  {
        noItemView.isHidden = true
        loader.startAnimating()
        let frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        userTableView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: frame)
        userTableView.beginInfiniteScroll(false)
        userTableView.addInfiniteScroll { (collectionView) -> Void in
            let offset : Int = self.userArray.count
            let offsetString = String(offset)
            if(self.userArray.count == 0){
                self.userCount = 0
                self.getUserService(offset: "0",searchKey:self.searchTextField.text! as NSString)
            }else{
                self.userCount = self.userArray.count
                if self.userArray.count%20 == 0  {
                    self.getUserService(offset: offsetString,searchKey:self.searchTextField.text! as NSString)
                }
            }
            // finish infinite scroll animations
            self.userTableView.finishInfiniteScroll()
        }
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.searchTextField.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func textClearBtnTapped(_ sender: Any) {
        self.searchTextField.resignFirstResponder()
        noItemView.isHidden = true
        searchTextField.text = EMPTY_STRING
        loader.startAnimating()
        self.textCloseView.isHidden = true
        self.broadcastArray.removeAllObjects()
        self.broadcastCount = 0
        self.userCount = 0
        self.getVideoService(offset: "0",searchKey:EMPTY_STRING)
        self.userArray.removeAllObjects()
        self.getUserService(offset: "0",searchKey:EMPTY_STRING as NSString)
        
    }
    
    @IBAction func broadcastBtnTapped(_ sender: Any) {
        keyboardEnable = false
        self.searchTextField.resignFirstResponder()
        self.configureBroadCastView()
        self.isHideBroadcast()
    }
    
    @IBAction func userBtnTapped(_ sender: Any) {
        self.noItemView.isHidden = true
        keyboardEnable = false
        self.searchTextField.resignFirstResponder()
        if !userServiceEnable{
            self.loadMoreUserList()
            userServiceEnable = true
        }
        self.configureUserView()
        if isFirst{
            self.isHideUser()
            isFirst = true
        }
    }
    
    //MARK: check if broadcast is empty
    func checkBroadCastAvailability() {
        let zoomAnimation = AnimationType.from(direction: .right, offset: 30.0)
        //        let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/6)
        
        if (broadcastArray.count == 0) {
            self.broadcastTableView.isHidden = true;
            self.noItemView.isHidden = false
            self.loader.stopAnimating()
        }else{
            if viewType == BROADCAST_VIEW{
                self.broadcastTableView.isHidden = false;
                self.noItemView.isHidden = true
            }
            self.broadcastTableView.reloadData()
            let indexPath = IndexPath(item: self.broadcastCount, section: 0)
            self.broadcastTableView.scrollToRow(at: indexPath, at: .top, animated: false)
            
            if #available(iOS 11.0, *) {
                broadcastTableView?.performBatchUpdates({
                    UIView.animate(views: self.broadcastTableView.visibleCells,
                                   animations: [zoomAnimation], completion: nil)
                }, completion: nil)
            } else {
                // Fallback on earlier versions
            }
            
        }
    }
    
    func isHideBroadcast(){
        if (broadcastArray.count == 0) {
            self.broadcastTableView.isHidden = true;
            self.noItemView.isHidden = false
            self.loader.stopAnimating()
        }else{
            self.broadcastTableView.isHidden = false;
            self.noItemView.isHidden = true
        }
    }
    func isHideUser(){
        if (userArray.count == 0) {
            self.userTableView.isHidden = true;
            self.noItemView.isHidden = false
            self.loader.stopAnimating()
        }else{
            self.userTableView.isHidden = false;
            self.noItemView.isHidden = true
        }
    }
    //MARK: check if users is empty
    func checkUsersAvailability() {
        let zoomAnimation = AnimationType.from(direction: .right, offset: 30.0)
        //        let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/6)
        if (userArray.count == 0) {
            self.userTableView.isHidden = true;
            self.noItemView.isHidden = false
            self.loader.stopAnimating()
        }else{
            if viewType == USER_VIEW{
                self.userTableView.isHidden = false;
                self.noItemView.isHidden = true
            }
            self.userTableView.reloadData()
            let indexPath = IndexPath(item: self.userCount, section: 0)
            self.userTableView.scrollToRow(at: indexPath, at: .top, animated: false)
            if #available(iOS 11.0, *) {
                userTableView?.performBatchUpdates({
                    UIView.animate(views: self.userTableView.visibleCells,
                                   animations: [zoomAnimation], completion: nil)
                }, completion: nil)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    //MARK: Broad cast video view
    func configureBroadCastView() {
        viewType = BROADCAST_VIEW
        if UserModel.shared.language == "Arabic"{
        searchTextField.config(color: .white, font: medium, align: .right, place: "search_people")
        }
        else{
            searchTextField.config(color: .white, font: medium, align: .left, place: "search_people")
        }
        searchTextField.delegate = self
        searchTextField.attributedPlaceholder = NSAttributedString(string:Utility.language.value(forKey: "search_broadcast") as! String, attributes: [NSAttributedString.Key.foregroundColor: TEXT_SECONDARY_COLOR])
        
        broadcastBtn.config(color: .white, font: medium, align: .center, title: "broadcast")
        userBtn.config(color: TEXT_SECONDARY_COLOR, font: medium, align: .center, title: "people")
        
        userBorder.isHidden = true
        broadcastBorder.isHidden = false
        userTableView.isHidden = true
        broadcastTableView.isHidden = false
        noUserImgView.image = #imageLiteral(resourceName: "no_user_icon")
        noUserLbl.config(color: TEXT_SECONDARY_COLOR, font: medium, align: .center, text: "no_videos")
    }
    
    //MARK: user list view
    func configureUserView() {
        viewType = USER_VIEW
        if UserModel.shared.language == "Arabic"{
        searchTextField.config(color: .white, font: medium, align: .right, place: "search_people")
        }
        else{
            searchTextField.config(color: .white, font: medium, align: .left, place: "search_people")
        }
        searchTextField.delegate = self
        searchTextField.attributedPlaceholder = NSAttributedString(string:Utility.language.value(forKey: "search_people") as! String, attributes: [NSAttributedString.Key.foregroundColor: TEXT_SECONDARY_COLOR])
        broadcastBtn.config(color: TEXT_SECONDARY_COLOR, font: medium, align: .center, title: "broadcast")
        userBtn.config(color: .white, font: medium, align: .center, title: "people")
        broadcastBorder.isHidden = true
        userBorder.isHidden = false
        userTableView.isHidden = false
        broadcastTableView.isHidden = true
        noUserImgView.image = #imageLiteral(resourceName: "no_user_icon")
        noUserLbl.config(color: TEXT_SECONDARY_COLOR, font: medium, align: .center, text: "no_user")
    }
    
    //MARK: Textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userServiceEnable = true
        noItemView.isHidden = true
        self.broadcastArray.removeAllObjects()
        self.userArray.removeAllObjects()
        self.broadcastTableView.reloadData()
        self.userTableView.reloadData()
        self.searchTextField.resignFirstResponder()
        loader.startAnimating()
        self.getAllDetailsFromServer()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboardEnable = true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text?.count)! == 0) {
            if textField.text ==  EMPTY_STRING{
                self.textCloseView.isHidden = false
            }else{
                self.textCloseView.isHidden = true
            }
        }else{
            if((textField.text?.count == 1) && textField.text != EMPTY_STRING){
                self.textCloseView.isHidden = true
            }else{
                self.textCloseView.isHidden = false
            }
        }
        return true
    }
    
    // Finish Editing The Text Field
    func textFieldDidEndEditing(_ textField: UITextField) {
        keyboardEnable = false
    }
    
    //MARK: Service call to get details based on searchfield text
    func getAllDetailsFromServer()  {
        self.broadcastArray.removeAllObjects()
        self.userArray.removeAllObjects()
        self.getVideoService(offset: "0",searchKey:self.searchTextField.text!)
        self.getUserService(offset: "0",searchKey:self.searchTextField.text! as NSString)
    }
    
    //MARK: Get user video List
    func getVideoService(offset:String,searchKey:String)   {
        
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "profile_id")
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue(searchKey, forKey: "search_key")
        requestDict.setValue("recent", forKey: "sort_by")
        requestDict.setValue(offset, forKey: "offset")
        requestDict.setValue("20", forKey: "limit")
        requestDict.setValue("live", forKey: "type")
        requestDict.setValue("false", forKey: "filter_applied")
        requestDict.setValue("", forKey: "gender")
        requestDict.setValue("", forKey: "min_age")
        requestDict.setValue("", forKey: "max_age")
        
        let Obj = BaseWebService()
        Obj.baseService(subURl: LIVE_STREAM_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            self.loader.stopAnimating()
          
            if status == "true"{
              if offset == "0" {
                self.broadcastArray.removeAllObjects()
                    }
                self.broadcastArray.addObjects(from: dict?.value(forKey: "result") as! [Any])
                self.checkBroadCastAvailability()
            }else{
                self.checkBroadCastAvailability()
            }
        })
    }
    
    
    //MARK: get userlist
    func getUserService(offset:String,searchKey:NSString)   {
        
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue("20", forKey: "limit")
        requestDict.setValue(offset, forKey: "offset")
        requestDict.setValue(searchKey, forKey: "search_key")
        
        let Obj = BaseWebService()
        Obj.baseService(subURl: ALL_USER_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            self.loader.stopAnimating()
            if status == "true"{
                let resultArray = dict?.value(forKey: "result") as! NSArray
                if offset == "0" {
                        self.userArray.removeAllObjects()
                    }
                if resultArray.count == 0{
                    self.checkUsersAvailability()
                }else{
                    self.userArray.addObjects(from: resultArray as! [Any])
                    self.checkUsersAvailability()
                }
            }else{
                self.checkUsersAvailability()
            }
        })
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int{
        if (tableView == broadcastTableView) {
            return broadcastArray.count
        }else if(tableView == userTableView){
            return userArray.count
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var customCell = UITableViewCell()
        if (tableView == broadcastTableView) {
            let videoDict:NSDictionary =  broadcastArray.object(at: indexPath.row) as! NSDictionary
            let type:String = (videoDict.value(forKey: "type") as? String)!
            if type == "recorded" {
                let recordCell = tableView.dequeueReusableCell(withIdentifier: "RecordVideoCell", for: indexPath) as! RecordVideoCell
                recordCell.configureCellWithDetails(videoDict: videoDict,viewType:"search")
                customCell = recordCell
                if UserModel.shared.language == "Arabic"{
                
                    recordCell.videoDescriptionLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                    recordCell.videoDescriptionLbl.textAlignment = .right
                    recordCell.createdTimeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                    recordCell.createdTimeLbl.textAlignment = .right
                    recordCell.postedbyLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                    recordCell.postedbyLbl.textAlignment = .right
                    recordCell.video_thumbnailView.transform = CGAffineTransform(scaleX: -1, y: 1)
                }
                else{
                    recordCell.videoDescriptionLbl.transform = .identity
                    recordCell.videoDescriptionLbl.textAlignment = .left
                    recordCell.createdTimeLbl.transform = .identity
                    recordCell.createdTimeLbl.textAlignment = .left
                    recordCell.postedbyLbl.transform = .identity
                    recordCell.postedbyLbl.textAlignment = .left
                    recordCell.video_thumbnailView.transform = .identity
                }
            }else{
                let liveCell = tableView.dequeueReusableCell(withIdentifier: "RecentVideoCell", for: indexPath) as! RecentVideoCell
                liveCell.configureCellWithDetails(videoDict: videoDict,viewType:"search")
                customCell = liveCell
                if UserModel.shared.language == "Arabic"{
                
                   liveCell.videoDescriptionLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                   liveCell.videoDescriptionLbl.textAlignment = .right
                    liveCell.watchCountLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                    liveCell.watchCountLbl.textAlignment = .right
                    liveCell.TypeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                    liveCell.TypeLbl.textAlignment = .center
                    liveCell.timeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                    liveCell.timeLbl.textAlignment = .right
                    liveCell.video_thumbnailView.transform = CGAffineTransform(scaleX: -1, y: 1)
                    liveCell.viewIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
                }
                else{
                    liveCell.videoDescriptionLbl.transform = .identity
                    liveCell.videoDescriptionLbl.textAlignment = .left
                    liveCell.watchCountLbl.transform = .identity
                    liveCell.watchCountLbl.textAlignment = .left
                    liveCell.TypeLbl.transform = .identity
                    liveCell.TypeLbl.textAlignment = .center
                    liveCell.timeLbl.transform = .identity
                    liveCell.timeLbl.textAlignment = .left
                    liveCell.video_thumbnailView.transform = .identity
                    liveCell.viewIcon.transform = .identity
                }
            }
            return customCell
            
        }else if(tableView == userTableView){
            let userCell = tableView.dequeueReusableCell(withIdentifier: "UserListPageCell", for: indexPath) as! UserListPageCell
            let userDict:NSDictionary =  userArray.object(at: indexPath.row) as! NSDictionary
            userCell.config(userDict: userDict)
            customCell = userCell
            if UserModel.shared.language == "Arabic"{
            
               userCell.fullNameLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
               userCell.fullNameLbl.textAlignment = .right
                userCell.followersLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                userCell.followersLbl.textAlignment = .right
                userCell.followingLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                userCell.followingLbl.textAlignment = .right
                userCell.userImgView.transform = CGAffineTransform(scaleX: -1, y: 1)
                userCell.separatorLine.textAlignment = .right
            }
            else{
                userCell.fullNameLbl.transform = .identity
                userCell.fullNameLbl.textAlignment = .left
                userCell.followersLbl.transform = .identity
                userCell.followersLbl.textAlignment = .left
                userCell.followingLbl.transform = .identity
                userCell.followingLbl.textAlignment = .left
                userCell.userImgView.transform = .identity
                userCell.separatorLine.textAlignment = .left
            }
        }
        if UserModel.shared.language == "Arabic" {
        self.searchTextField.transform = CGAffineTransform(scaleX: -1, y: 1)
        self.searchTextField.textAlignment = .right
        }
        else{
            self.searchTextField.transform = .identity
            self.searchTextField.textAlignment = .left
        }
        return customCell
    }
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        if (tableView == broadcastTableView) {
            return 110
        }else if(tableView == userTableView){
            return 80
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath){
        if !loader.isAnimating{
        if (tableView == broadcastTableView) {
            let videoDetails =  broadcastArray.object(at: indexPath.row) as! NSDictionary
            var videoType:String
            videoType = (videoDetails.value(forKey: "type") as? String)!
            if (videoType == "recorded") {
                let videoObj = VideoPlayer()
                videoObj.modalPresentationStyle = .custom
                videoObj.transitioningDelegate = self.transition
                videoObj.videoDict = videoDetails
                videoObj.selected_index = indexPath.row
                videoObj.deletedInfo = { index in
                    print("deleted index \(index)")
                    self.broadcastArray.removeObject(at: index)
                    self.broadcastTableView.reloadData()
                }
                self.navigationController?.present(videoObj, animated: true, completion: nil)
            }else{
                let subscriberObj = SubscriberLivePage()
                subscriberObj.modalPresentationStyle = .custom
                subscriberObj.transitioningDelegate = self.transition
                subscriberObj.streamDict = videoDetails
                subscriberObj.stream_name = videoDetails.value(forKey: "name") as! String
                self.navigationController?.present(subscriberObj, animated: true, completion: nil)
            }
        }else if(tableView == userTableView){
            let userDict:NSDictionary =  userArray.object(at: indexPath.row) as! NSDictionary
            let user_id = userDict.value(forKey: "user_id") as! String
            let profile = ProfilePage()
            if user_id != UserModel.shared.userId{
                profile.viewType = "fromMsg"
            }else{
                profile.viewType = "ownProfile"
                profile.additionalInfo = "needback"
            }
            profile.profileId = userDict.value(forKey: "user_id") as! String
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
    }
}
