//
//  GemListPage.swift
//  Randoo
//
//  Created by HTS-Product on 13/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import StoreKit
import GoogleMobileAds
import SwiftyReceiptValidator

class GemListPage: UIViewController,APParallaxViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    var gemsListArray = NSMutableArray()
    let gemsTempArray = NSMutableArray()
    var currency = String()
    var selectedProduct = SKProduct()
    var pullDownRefresh = CarbonSwipeRefresh()
    var productIDs: Array<String?> = []
    var productsArray: Array<SKProduct?> = []
    var gemPurchase = true
    var isRequested = false
    
    @IBOutlet var bgImgView: UIImageView!
    
    @IBOutlet var loader: UIActivityIndicatorView!
    @IBOutlet var loaderView: UIView!
    
    @IBOutlet var topView: UIView!
    @IBOutlet var gemsCollectionView: UICollectionView!
    @IBOutlet var freeGemBtn: UIButton!
    @IBOutlet var navigationBg: UIImageView!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var gemcountLbl: UILabel!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var gemIcon: UIImageView!
    @IBOutlet var profileBg: UIImageView!
    @IBOutlet weak var backbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(UserModel.shared.userId)
        self.initialSetup()
    }
    
    
    
    override func viewDidLayoutSubviews() {
        self.gemsCollectionView.addParallax(with: self.topView, andHeight: self.topView.frame.size.height)
        //        self.profileBg.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: self.topView.frame.size.height)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.changeToRTL()
    }
    
    
    func changeToRTL() {
        if UserModel.shared.language == "Arabic"{
            self.titleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.backbtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.backbtn.frame = CGRect.init(x: FULL_WIDTH-50, y: 25, width: 40, height: 40)
        }
        else{
            self.titleLbl.transform = .identity
            self.backbtn.transform = .identity
            self.backbtn.frame = CGRect.init(x: 10, y: 25, width: 40, height: 40)
        }
    }
    
    func initialSetup()  {
        self.loaderView.isHidden = true
        self.fd_prefersNavigationBarHidden = true
        self.fd_interactivePopDisabled = false
        self.titleLbl.config(color: .white, font: averageReg, align: .center, text: "gems")
        self.titleLbl.isHidden = true
        self.navigationBg.isHidden = true
        self.gemcountLbl.config(color: .white, font:average , align: .center, text: "")
        self.gemcountLbl.text = "\(UserModel.shared.gemCount) \(Utility.language.value(forKey: "gems")as! String)"
        self.descriptionLbl.config(color: .white, font:lite , align: .center, text: "gem_list_des")
        self.freeGemBtn.config(color: PRIMARY_COLOR, font: mediumReg, align: .center, title: "free_gems")
        self.freeGemBtn.backgroundColor = .white
        self.freeGemBtn.cornerRoundRadius()
        gemsCollectionView.register(UINib(nibName: "GemListCell", bundle: nil), forCellWithReuseIdentifier: "GemListCell")
        gemsCollectionView.register(UINib(nibName: "GemPrimeCell", bundle: nil), forCellWithReuseIdentifier: "GemPrimeCell")
        
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .vertical
        gemsCollectionView.collectionViewLayout = collectionViewFlowLayout
        self.setTopPageDesign()
        self.getGemsFromAppHud()
        self.gemsCollectionView.reloadData()
        
        if IS_LIVZA{
            self.bgImgView.image = #imageLiteral(resourceName: "live_chat_bg")
        }else{
            self.bgImgView.image = #imageLiteral(resourceName: "chat_bg")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        gemPurchase = false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func refresh(sender: AnyObject) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            self.pullDownRefresh.endRefreshing()
        })
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func freegemBtnTapped(_ sender: Any) {
        let makeFreeGem = InvitePage()
        self.navigationController?.pushViewController(makeFreeGem, animated: true)
    }
    
    //scroll view delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(" offset \(scrollView.contentOffset.y) ")
        if scrollView.contentOffset.y > -260{
            print(" HIDE ")
            UIView.animate(withDuration: 1.0, animations: {
                self.gemcountLbl.alpha = 0
                self.descriptionLbl.alpha = 0
                self.freeGemBtn.alpha = 0
                self.gemIcon.alpha = 0
            })
        }else{
            print(" SHOW ")
            self.gemcountLbl.alpha = 1
            self.descriptionLbl.alpha = 1
            self.freeGemBtn.alpha = 1
            self.gemIcon.alpha = 1
            self.titleLbl.isHidden = true
            self.navigationBg.isHidden = true
            
        }
        if scrollView.contentOffset.y > -100{
            UIView.animate(withDuration: 1.0, animations: {
                self.titleLbl.isHidden = false
                self.navigationBg.isHidden = false
            })
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.setTopPageDesign()
    }
    
    func setTopPageDesign()  {
        self.topView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: 300)
        self.profileBg.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: 300)
        self.gemIcon.frame = CGRect.init(x: (FULL_WIDTH/2)-35, y: 70, width: 60, height: 60)
        self.gemcountLbl.frame = CGRect.init(x: 0, y: 141, width: FULL_WIDTH, height: 30)
        self.descriptionLbl.frame = CGRect.init(x: 20, y: 172, width: FULL_WIDTH-40, height: 60)
        self.freeGemBtn.frame = CGRect.init(x: (FULL_WIDTH/2)-75, y: 236, width: 150, height: 35)
        
    }
    
    //get gems list from Apphud
    func getGemsFromAppHud() {
        if Apphud.products() == nil {
            Apphud.productsDidFetchCallback({(products) in
                
                for product in products {
                    if GEMIDS.contains(product.productIdentifier) {
                        self.gemsTempArray.insert(product, at: 0)
                    }
                }
                self.productsArray = products
            })
        } else {
            for product in Apphud.products()! {
                print(product.productIdentifier)
                print(product.subscriptionGroupIdentifier as Any)
                if GEMIDS.contains(product.productIdentifier) {
                    self.gemsTempArray.insert(product, at: 0)
                }
            }
            self.productsArray = Apphud.products()!
        }
        self.appendStoreInfo()
    }
    
    //append store information
    func appendStoreInfo()  {
        DispatchQueue.main.async {
            let arrangedArray = NSMutableArray()
            let gemsTempListArray = NSMutableArray()
            var count = 0
            for gem in self.gemsTempArray {
                count += 1
                let skproduct = gem as! SKProduct
                self.currency = skproduct.priceLocale.currencySymbol!
                var gemdict = Dictionary<String, Any>()
                gemdict["gem_price"] = skproduct.price.floatValue
                gemdict["type"] = "gem"
                gemdict["gem_title"] = skproduct.productIdentifier
                gemdict["platform"] = "ios"
                for index in 0..<GEMIDS.count {
                    if skproduct.productIdentifier == GEMIDS[index]{
                        gemdict["gem_icon"] = GEMICONS[index]
                        gemdict["gem_count"] = GEMIDS[index].components(separatedBy: ".").last
                    }
                }
                
                let gemDict = gemdict as NSDictionary
                
                let dict = NSMutableDictionary.init(dictionary: gemDict)
                arrangedArray.add(dict)
                
            }
            gemsTempListArray.addObjects(from: arrangedArray as! [Any])
            
            for productID in GEMIDS{
                for i in 0..<gemsTempListArray.count {
                    let dict = gemsTempListArray.object(at: i) as! NSDictionary
                    if productID == (dict.value(forKey: "gem_title") as? String ?? "") {
                        self.gemsListArray.add(dict)
                    }
                }
            }
            // append prime information inside the array
            if self.gemsTempArray.count > 10 {
                if (count%10) == 0 {
                    let primeDict = NSMutableDictionary()
                    primeDict.setValue("prime", forKey: "type")
                    self.gemsListArray.add(primeDict)
                }
            }else{
                if count == self.gemsTempArray.count {
                    let primeDict = NSMutableDictionary()
                    primeDict.setValue("prime", forKey: "type")
                    self.gemsListArray.add(primeDict)
                }
            }
            
            self.gemsCollectionView.reloadData()
        }
    }
    
    func purchaseProduct(product: SKProduct) {
        Apphud.purchase(product) { result in
            
            if result.error != nil {
                self.isRequested = false
                self.fd_interactivePopDisabled = false
                self.loader.stopAnimating()
                self.loaderView.isHidden = true
                if let error = result.transaction?.error as? SKError {
                    switch error.code {
                        case .paymentCancelled:
                            let alert = UIAlertController(title: "Purchase is canceled", message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                            self.present(alert, animated: true)
                        default:
                            break
                    }
                }
            } else {
//                self.loaderView.isHidden = true
//                self.fd_interactivePopDisabled = false
//                self.isRequested = false
//                self.gemcountLbl.text = "\(UserModel().gemCount) \(Utility.language.value(forKey: "gems")as! String)"
//                let alert = AlertPopup()
//                alert.viewType = "gems_updated"
//                alert.modalPresentationStyle = .overCurrentContext
//                alert.modalTransitionStyle = .crossDissolve
//                alert.dismissed = {
//                    self.dismiss(animated: true, completion: nil)
//                    self.navigationController?.popViewController(animated: true)
//                }
//                self.present(alert, animated: true, completion: nil)
                self.updatePaymentInfo(tID: (result.transaction?.transactionIdentifier)!)
            }
        }
    }
    
    //MARK: Collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if gemsListArray.count == 0{
            return 2 //increase this based on orignal gems list
        }else{
            return gemsListArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var customCell = UICollectionViewCell()
        if gemsListArray.count == 0{
            let cell : GemListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GemListCell", for: indexPath) as! GemListCell
            cell.gemIcon.makeItRound()
            cell.startShimmer()
            customCell = cell
        }else{
            let dict =  gemsListArray.object(at: indexPath.row) as! NSDictionary
            let type =  dict.value(forKey: "type") as! String
            if type == "gem"{
                let cell : GemListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GemListCell", for: indexPath) as! GemListCell
                cell.configGem(dict: dict,currency:self.currency)
                customCell = cell
            }else{
                
                let primeCell : GemPrimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GemPrimeCell", for: indexPath) as! GemPrimeCell
                primeCell.configCell()
                primeCell.payBtn.addTarget(self, action: #selector(goPrimePage), for: .touchUpInside)
                customCell = primeCell
                if UserModel.shared.language == "Arabic"{
                    primeCell.primeView.transform = CGAffineTransform(scaleX: -1, y: 1)
                    primeCell.payBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
                    primeCell.titleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                    primeCell.titleLbl.textAlignment = .right
                    primeCell.priceValidLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                    primeCell.priceValidLbl.textAlignment = .right
                }
                else{
                    primeCell.primeView.transform = .identity
                    primeCell.titleLbl.transform = .identity
                    primeCell.titleLbl.textAlignment = .left
                    primeCell.payBtn.transform = .identity
                    primeCell.priceValidLbl.transform = .identity
                    primeCell.priceValidLbl.textAlignment = .left
                }
            }
        }
        return customCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if gemsListArray.count != 0{
            let dict =  gemsListArray.object(at: indexPath.row) as! NSDictionary
            let type =  dict.value(forKey: "type") as! String
            if type == "gem"{
                if Utility.shared.isConnectedToNetwork(){
                    if !isRequested{
                        self.loaderView.isHidden = false
                        self.fd_interactivePopDisabled = true
                        self.loader.startAnimating()
                        isRequested = true
                        let product_id =  dict.value(forKey: "gem_title") as! String
                        if GEMIDS.contains(product_id) {
                            for product in productsArray {
                                var prd =  SKProduct()
                                prd = product!
                                if prd.productIdentifier == product_id{
                                    self.selectedProduct = prd
                                    self.purchaseProduct(product: prd)
                                }
                            }
                        }else{
                            isRequested = false
                            self.loaderView.isHidden = true
                            self.loader.stopAnimating()
                            self.statusAlert(msg: "package_not_available")
                        }
                    } else {
                        self.statusAlert(msg: "already_request")
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (gemsCollectionView.frame.size.width-10)/2
        if gemsListArray.count == 0{
            return CGSize(width: width, height: 140)
        }else{
            let dict =  gemsListArray.object(at: indexPath.row) as! NSDictionary
            let type =  dict.value(forKey: "type") as! String
            if type == "gem"{
                return CGSize(width: width, height: 140)
            }else{
                return CGSize(width: FULL_WIDTH-10, height: 80)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5)
    }
    
    @objc func goPrimePage() {
        if Utility.shared.isConnectedToNetwork(){
            OfferPresentingViewController.RemoteConfiguredPlansViewController.requestProductsAndShow(from: self)
        }
    }
    
    //update to service
    func updatePaymentInfo(tID:String)  {
        let updateOBj = BaseWebService()
        let request = NSMutableDictionary()
        request.setValue(tID, forKey: "transaction_id")
        request.setValue(UserModel.shared.userId, forKey: "user_id")
        request.setValue(selectedProduct.productIdentifier, forKey: "gem_id")
        request.setValue(selectedProduct.price.stringValue, forKey: "paid_amount")
        
        updateOBj.baseService(subURl: GEMS_PURCHASE_API, params:  request as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                self.loaderView.isHidden = true
                self.fd_interactivePopDisabled = false
                self.isRequested = false
                SKPaymentQueue.default().restoreCompletedTransactions()
                UserModel.shared.gemCount = (dict?.value(forKey: "available_gems") as! NSNumber).intValue
                self.gemcountLbl.text = "\(UserModel().gemCount) \(Utility.language.value(forKey: "gems")as! String)"
                let alert = AlertPopup()
                alert.viewType = "gems_updated"
                alert.modalPresentationStyle = .overCurrentContext
                alert.modalTransitionStyle = .crossDissolve
                alert.dismissed = {
                    self.dismiss(animated: true, completion: nil)
                }
                self.present(alert, animated: true, completion: nil)
            } else {
                print("aaaa")
            }
        })
    }
}

