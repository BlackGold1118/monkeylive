//
//  GemPrimeCell.swift
//  Randoo
//
//  Created by HTS-Product on 25/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class GemPrimeCell: UICollectionViewCell {
    
    @IBOutlet var primeAnimationView: UIView!
    
    @IBOutlet var primeView: UIView!
    @IBOutlet var priceValidLbl: UILabel!
    @IBOutlet var payBtn: UIButton!
    
    @IBOutlet var titleLbl: UILabel!
    
    @IBOutlet var crownView: UIView!
    @IBOutlet var tickBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.primeAnimationView.cornerViewMiniumRadius()
        self.priceValidLbl.config(color: .white, font: averageReg, align: .left, text: "")
        self.titleLbl.config(color: .white, font: mediumReg, align: .left, text: "prime_benefits")
        self.payBtn.config(color: #colorLiteral(red: 1, green: 0.6, blue: 0.2549019608, alpha: 1), font: liteReg, align: .center, title: "subscribe")
        self.payBtn.cornerRoundRadius()
        self.payBtn.backgroundColor = .white
        self.tickBtn.cornerRoundRadius()
        self.crownView.cornerViewRadius()
        if IS_LIVZA {
            self.primeAnimationView.isHidden = true
            self.primeView.backgroundColor = PRIMARY_COLOR
            self.primeView.cornerViewMiniumRadius()
        }

    }
    func configCell(){
        self.priceValidLbl.isHidden = true
        if UserModel.shared.isPremium {
            do{
                let jsonDecoder = JSONDecoder()
                let profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
                self.primeAnimationView.addLOT(lot_name: "subscribe", w: self.primeAnimationView.frame.size.width, h: 70)
                self.titleLbl.config(color: .white, font: medium, align: .left, text: "premium_member")
                let valideUntil = Utility.language.value(forKey: "valid_until") as! String
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                let expDate = formatter.date(from: profile.premium_expiry_date!)
                self.priceValidLbl.text = "\(valideUntil) \(Utility.shared.timeStamp(time:expDate! , format: "dd/MM/yyyy"))"
                self.payBtn.isHidden = true
            }catch{
            }
        }else{
            if UserModel.shared.alreadySubscribed { // already prime
                self.primeAnimationView.addLOT(lot_name: "renewal", w: self.primeAnimationView.frame.size.width, h: 70)
                self.payBtn.config(color: .red, font: liteReg, align: .center, title: "renewal")
                self.titleLbl.config(color: .white, font: mediumReg, align: .left, text: "expired")
                self.priceValidLbl.config(color: .white, font: averageReg, align: .left, text: "your_membership")
            }else{
                self.primeAnimationView.addLOT(lot_name: "subscribe", w: self.primeAnimationView.frame.size.width, h: 70)
                self.payBtn.config(color: #colorLiteral(red: 1, green: 0.6, blue: 0.2549019608, alpha: 1), font: liteReg, align: .center, title: "subscribe")
                self.titleLbl.config(color: .white, font: liteReg, align: .left, text: "prime_benefits")
                if Utility.shared.primePackage != nil{
                    let dict = Utility.shared.primePackage
                    let currency = dict?.value(forKey: "currency") as! String
                    let validity = dict?.value(forKey: "validity") as! String
                    let price = dict?.value(forKey: "price") as! String
                    self.priceValidLbl.text = "\(currency) \(price)/\(validity)"
                    
                }
            }
            self.crownView.isHidden = true
            self.tickBtn.isHidden = true
        }
    }
}
