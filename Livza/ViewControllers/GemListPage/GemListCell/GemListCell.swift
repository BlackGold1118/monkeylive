//
//  GemListCell.swift
//  Randoo
//
//  Created by HTS-Product on 25/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class GemListCell: UICollectionViewCell {

    @IBOutlet var bgView: UIView!
    @IBOutlet var gemIcon: UIImageView!
    @IBOutlet var gemcountLbl: UILabel!
    @IBOutlet var gempriceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgView.cornerViewMiniumRadius()
        self.bgView.applyCardEffect()
        self.gemcountLbl.config(color: TEXT_PRIMARY_COLOR, font: mediumReg, align: .center, text: "")
        self.gempriceLbl.config(color: PRIMARY_COLOR, font: mediumReg, align: .center, text: "")
        let Width = (FULL_WIDTH-10)/2
        let cellWidth = Width-10
        self.bgView.frame = CGRect.init(x: 5, y: 5, width:cellWidth , height: 130)
        self.gemIcon.frame = CGRect.init(x:(cellWidth/2)-28 , y: 8, width: 56, height: 56)
        self.gemcountLbl.frame = CGRect.init(x: 20, y: 67, width: cellWidth-40, height: 23)
        self.gempriceLbl.frame = CGRect.init(x: 20, y: 93, width: cellWidth-40, height: 23)

    }
    func startShimmer()  {
        let Width = (FULL_WIDTH-10)/2
        let cellWidth = Width-10
        self.gemcountLbl.frame = CGRect.init(x: 35, y: 72, width: cellWidth-70, height: 10)
        self.gempriceLbl.frame = CGRect.init(x: 20, y: 93, width: cellWidth-40, height: 10)
        UserDefaults.standard.set("gems", forKey: "shimmer_view_from")
        self.gemcountLbl.cornerRadius()
        self.gempriceLbl.cornerRadius()
        AMShimmer.start(for: bgView)
    }

    func configGem(dict:NSDictionary,currency:String) {
        let Width = (FULL_WIDTH-10)/2
        let cellWidth = Width-10
        self.gemcountLbl.frame = CGRect.init(x: 20, y: 67, width: cellWidth-40, height: 23)
        self.gempriceLbl.frame = CGRect.init(x: 20, y: 93, width: cellWidth-40, height: 23)
        AMShimmer.stop(for: bgView)
        self.gemIcon.image = UIImage(named: dict.value(forKey: "gem_icon") as! String)
        self.gemcountLbl.text = (dict.value(forKey: "gem_count") as! String)
        self.gempriceLbl.text = "\(currency) \(NSNumber.init(value: (dict.value(forKey: "gem_price") as! Float)).stringValue)"
    }
}
