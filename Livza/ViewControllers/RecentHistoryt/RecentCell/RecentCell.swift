//
//  RecentCell.swift
//  Randoo
//
//  Created by HTS-Product on 29/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class RecentCell: UICollectionViewCell {

    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var userImgView: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var moreBtn: UIButton!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var bgview: UIView!
    @IBOutlet var animationView: UIView!
    @IBOutlet var crownIcon: UIImageView!

    @IBOutlet var moreBtnAnimatedLbl: UILabel!
    @IBOutlet var genderIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        crownIcon.isHidden = true

        self.timeLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .center, text: "")
        self.nameLbl.config(color: .white, font: mediumReg, align: .center, text: "")
        self.locationLbl.config(color: TEXT_SECONDARY_COLOR, font: medium, align: .center, text: "")
        self.moreBtn.config(color: .white, font: medium, align: .center, title: "more_info")
        bgview.layer.cornerRadius = 10
        bgview.clipsToBounds = true
        bgview.backgroundColor = .clear
        self.moreBtn.backgroundColor = PRIMARY_COLOR
        self.moreBtn.cornerRoundRadius()
        self.userImgView.makeItRound()
        self.userImgView.frame = CGRect.init(x: 91, y: 44, width: 100, height: 100)

    }
    
    func startShimmer()  {
        self.moreBtn.isHidden = true
        self.genderIcon.isHidden = true
        self.nameLbl.frame.size.height = 15
        self.locationLbl.frame.size.height = 15
        self.moreBtnAnimatedLbl.frame.size.height = 25
        self.moreBtnAnimatedLbl.cornerViewRadius()
        self.nameLbl.cornerRadius()
        self.locationLbl.cornerRadius()
        UserDefaults.standard.set("recent", forKey: "shimmer_view_from")
        AMShimmer.start(for: self.animationView, except:[self.bgview,self.timeLbl], isToLastView: false)
        AMShimmer.start(for: self.nameLbl)
        AMShimmer.start(for: self.locationLbl)
        AMShimmer.start(for: self.moreBtnAnimatedLbl)
    }

    func config(model:Users_list)  {
        bgview.border(color: TEXT_SECONDARY_COLOR)
        AMShimmer.stop(for: self.animationView)
        AMShimmer.stop(for: self.nameLbl)
        AMShimmer.stop(for: self.locationLbl)
        AMShimmer.stop(for: self.moreBtnAnimatedLbl)
        self.genderIcon.isHidden = false
        self.moreBtn.isHidden = false
        self.nameLbl.text = model.name
        self.locationLbl.text = model.location
        let imgURL = URL.init(string: PROFILE_IMAGE_URL+model.user_image!)
        self.userImgView.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        let localDate = formatter.date(from: model.last_chat!)
//        if localDate != nil{
//            self.timeLbl.text = Utility.shared.makeFormat(to: localDate!)
//        }
        if model.premium_member == "false"{
            crownIcon.isHidden = true
        }else{
            crownIcon.isHidden = false
        }
        
        if model.gender == "male"{
            genderIcon.image = UIImage(named: "male_sel")
        }else{
            genderIcon.image = UIImage(named: "female_sel")
        }
        var username = String()
        if (model.name?.count)! > 20{
            username  = "\(String((model.name?.prefix(18))!)).."
        }else{
            username = model.name!
        }
        if model.privacy_age!{
            self.nameLbl.text = "\(username)"
        }else{
            self.nameLbl.text = "\(username), \(model.age!)"
        }
        let nameWidth = (self.nameLbl.intrinsicContentSize.width/2)+10
        self.nameLbl.frame = CGRect.init(x:(self.bgview.frame.size.width/2)-nameWidth , y: (self.userImgView.frame.size.height/2)+10, width: self.nameLbl.intrinsicContentSize.width, height: 25)
        self.genderIcon.frame = CGRect.init(x:self.nameLbl.frame.size.width+self.nameLbl.frame.origin.x+10 , y: (self.userImgView.frame.size.height/2)+14, width: 17, height: 17)
     
       
    }
}
