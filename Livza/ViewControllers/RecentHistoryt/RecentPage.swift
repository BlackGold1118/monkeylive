//
//  RecentPage.swift
//  Randoo
//
//  Created by HTS-Product on 29/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import GoogleMobileAds


class RecentPage: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,GADBannerViewDelegate {
    
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var recentCollectionView: UICollectionView!
    @IBOutlet var deleteBtn: UIButton!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var premiumView: UIView!
    @IBOutlet var premiumAnimationView: UIView!
    @IBOutlet var crownView: UIView!
    @IBOutlet var tickBtn: UIButton!
    @IBOutlet var noResultLbl: UILabel!
    @IBOutlet var primeBtn: UIButton!
    @IBOutlet var primeSubTitleLbl: UILabel!
    @IBOutlet var primeTitlelbl: UILabel!
    @IBOutlet var bannerView: GADBannerView!
    
    var recentArray = NSMutableArray()
    var recentList = [Users_list]()
    var lottieName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    func initialSetup()  {
        
        self.fd_prefersNavigationBarHidden = true
        self.titleLbl.config(color: .white, font: averageReg, align: .center, text: "recent_history")
        recentCollectionView.register(UINib(nibName: "RecentCell", bundle: nil), forCellWithReuseIdentifier: "RecentCell")
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        recentCollectionView.collectionViewLayout = collectionViewFlowLayout
        self.recentCollectionView.reloadData()
        
        self.getRecentList(offset: "0")
        self.deleteBtn.isHidden = true
        
        self.primeTitlelbl.config(color: .white, font: liteReg, align: .left, text: "expired")
        self.primeSubTitleLbl.config(color: .white, font: averageReg, align: .left, text: "your_membership")
        self.primeBtn.config(color: #colorLiteral(red: 1, green: 0.6, blue: 0.2549019608, alpha: 1), font: liteReg, align: .center, title: "subscribe")
        self.primeBtn.cornerRoundRadius()
        self.primeBtn.elevationEffect()
        self.primeBtn.backgroundColor = .white
        self.noResultLbl.config(color: TEXT_SECONDARY_COLOR, font: medium, align: .center, text: "no_recent")
        self.noResultLbl.isHidden = true
        self.crownView.cornerViewRadius()
        self.tickBtn.cornerRoundRadius()
        self.configPrimeView()
        if Utility.shared.isAdsEnable() {
            self.configAds()
        }else{
            self.bannerView.isHidden = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.changeToRTL()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLayoutSubviews() {
        if IS_LIVZA {
            self.premiumView.cornerViewMiniumRadius()
            self.premiumView.backgroundColor = PRIMARY_COLOR
            
        }else{
            self.premiumAnimationView.addLOT(lot_name: self.lottieName, w: self.premiumView.frame.size.width, h: 80)
            self.premiumAnimationView.cornerViewMiniumRadius()
            self.premiumView.applyCardEffect()
        }
    }
    
    //config banner view
    func configAds()  {
        bannerView.adUnitID = Utility.shared.adUnitId
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    //banner view delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    
    func configPrimeView()  {
        self.primeSubTitleLbl.isHidden = true
        if UserModel.shared.isPremium {
            do{
                let jsonDecoder = JSONDecoder()
                let profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
                self.lottieName = "subscribe"
                self.primeTitlelbl.text = Utility.language.value(forKey: "premium_member") as? String
                let valideUntil = Utility.language.value(forKey: "valid_until") as! String
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                let expDate = formatter.date(from: profile.premium_expiry_date!)
                self.primeSubTitleLbl.text = "\(valideUntil) \(Utility.shared.timeStamp(time:expDate! , format: "dd/MM/yyyy"))"
                self.primeBtn.isHidden = true
                self.crownView.isHidden = false
                self.tickBtn.isHidden = false
            }catch{
            }
        }else{
            if UserModel.shared.alreadySubscribed { // already prime
                self.lottieName = "renewal"
                self.primeBtn.config(color: .red, font: liteReg, align: .center, title: "renewal")
                self.primeTitlelbl.text = Utility.language.value(forKey: "expired") as? String
                self.primeSubTitleLbl.text = Utility.language.value(forKey: "your_membership") as? String
            }else{
                self.lottieName = "subscribe"
                self.primeBtn.config(color: #colorLiteral(red: 1, green: 0.6, blue: 0.2549019608, alpha: 1), font: liteReg, align: .center, title: "subscribe")
                self.primeTitlelbl.text = Utility.language.value(forKey: "prime_benefits") as? String
                if Utility.shared.primePackage != nil{
                    let dict = Utility.shared.primePackage
                    let currency = dict?.value(forKey: "currency") as! String
                    let validity = dict?.value(forKey: "validity") as! String
                    let price = dict?.value(forKey: "price") as! String
                    self.primeSubTitleLbl.text = "\(currency) \(price)/\(validity)"
                    
                }
            }
            self.crownView.isHidden = true
            self.tickBtn.isHidden = true
        }
        
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func subscribtBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            OfferPresentingViewController.RemoteConfiguredPlansViewController.requestProductsAndShow(from: self)
        }
    }
    
    
    
    func changeToRTL() {
        if UserModel.shared.language == "Arabic"{
            self.titleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.backBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.deleteBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.deleteBtn.frame = CGRect.init(x: 10, y: 25, width: 40, height: 40)
            self.backBtn.frame = CGRect.init(x: FULL_WIDTH-50, y: 25, width: 40, height: 40)
            self.premiumView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.premiumAnimationView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.crownView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.tickBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.primeTitlelbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.primeSubTitleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.primeBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.noResultLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.primeSubTitleLbl.textAlignment = .right
            self.primeTitlelbl.textAlignment = .right
            self.recentCollectionView.transform = CGAffineTransform(scaleX: -1, y: 1)

        }
        else{
            self.titleLbl.transform = .identity
            self.backBtn.transform = .identity
            self.backBtn.frame = CGRect.init(x: 10, y: 25, width: 40, height: 40)
            self.deleteBtn.frame = CGRect.init(x: FULL_WIDTH-50, y: 25, width: 40, height: 40)
            self.deleteBtn.transform = .identity
            self.premiumView.transform = .identity
            self.premiumAnimationView.transform = .identity
            self.crownView.transform = .identity
            self.tickBtn.transform = .identity
            self.primeTitlelbl.transform = .identity
            self.primeSubTitleLbl.transform = .identity
            self.primeBtn.transform = .identity
            self.noResultLbl.transform = .identity
            self.primeSubTitleLbl.textAlignment = .left
            self.primeTitlelbl.textAlignment = .left
            self.recentCollectionView.transform = .identity

        }
    }
    
    
    @IBAction func deleteBtnTapped(_ sender: Any) {
        let otherAlert = UIAlertController(title: nil, message: Utility.language.value(forKey: "want_to_delete") as? String, preferredStyle: UIAlertController.Style.alert)
        let dismiss = UIAlertAction(title: Utility.language.value(forKey: "cancel") as? String, style: UIAlertAction.Style.cancel, handler: nil)
        otherAlert.addAction(dismiss)
        
        let okayBtn = UIAlertAction(title: Utility.language.value(forKey: "ok") as? String, style: UIAlertAction.Style.default, handler:deleteList)
        otherAlert.addAction(okayBtn)
        present(otherAlert, animated: true, completion: nil)
    }
    
    
    
    
    //get recent list from service
    func getRecentList(offset:String)  {
        let Obj = BaseWebService()
        Obj.getDetails(subURl: "\(RECENT_VIDEOCHATS_API)/\(UserModel.shared.userId)/\(offset)/20", onSuccess: {response in
            do{
                let jsonDecoder = JSONDecoder()
                let model = try jsonDecoder.decode(FollowModel.self, from: response.data!)
                print(model)
                if model.status == "true"{
                    self.recentArray.addObjects(from: model.users_list!)
                    self.recentList = self.recentArray as! [Users_list]
                    self.recentCollectionView.reloadData()
                    if self.recentList.count != 0{
                        self.recentCollectionView.isHidden = false
                        self.deleteBtn.isHidden = false
                    }else{
                        self.noResultLbl.isHidden = false
                        self.recentCollectionView.isHidden = true
                    }
                }else{
                    self.noResultLbl.isHidden = false
                    self.recentCollectionView.isHidden = true
                }
            }catch{
                
            }
            
        })
    }
    //get recent list from service
    func deleteList(alert: UIAlertAction)  {
        let Obj = BaseWebService()
        Obj.getDetails(subURl: "\(DELETE_VIDEOCHATS_API)/\(UserModel.shared.userId)", onSuccess: {response in
            do{
                let jsonDecoder = JSONDecoder()
                let model = try jsonDecoder.decode(RecentModel.self, from: response.data!)
                if model.status == "true"{
                    self.deleteBtn.isHidden = true
                    self.recentList.removeAll()
                    self.noResultLbl.isHidden = false
                    self.recentCollectionView.isHidden = true
                    self.dismiss(animated: true, completion: nil)
                }else{
                    
                }
            }catch{
            }
        })
    }
    //MARK: Collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if recentList.count == 0 {
            return 3
        }else{
            return recentList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var customCell = UICollectionViewCell()
        if recentList.count == 0 {
            let cell : RecentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentCell", for: indexPath) as! RecentCell
            cell.startShimmer()
            customCell = cell
        }else{
            let cell : RecentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentCell", for: indexPath) as! RecentCell
            cell.config(model: recentList[indexPath.row])
            cell.moreBtn.tag = indexPath.row
            cell.moreBtn.addTarget(self, action: #selector(self.moreInfo(_:)), for: .touchUpInside)
            
            customCell = cell
        }
        if UserModel.shared.language == "Arabic"{
            customCell.transform = CGAffineTransform(scaleX: -1, y: 1)

        }
        return customCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if Utility.shared.isConnectedToNetwork(){
            
            if recentList.count != 0 {
                let profile = ProfilePage()
                let userModal = recentList[indexPath.row]
                profile.viewType = "fromFollow"
                profile.userModal = userModal
                profile.profileId = userModal.user_id!
                self.navigationController?.pushViewController(profile, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 280, height: 290)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == recentList.count - 1 {  //numberofitem count
            if recentList.count%20 == 0{
                self.getRecentList(offset: "\(recentList.count)")
            }
        }
    }
    
    
    //move profile
    @objc func moreInfo(_ sender: UIButton){
        let profile = ProfilePage()
        let userModal = recentList[sender.tag]
        profile.viewType = "fromFollow"
        profile.userModal = userModal
        profile.profileId = userModal.user_id!
        self.navigationController?.pushViewController(profile, animated: true)
    }
}
