//
//  LoginPage.swift
//  Randoo
//
//  Created by HTS-Product on 08/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import FDFullscreenPopGesture
import FBSDKLoginKit
import Firebase
import FirebaseUI
import SafariServices
import AuthenticationServices

enum Signin: String {
    case apple
    case phonenumber
    case facebook
}

let reachability = Reachability()

class LoginPage: UIViewController,UIGestureRecognizerDelegate,FUIAuthDelegate,ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var videoPreview: UIView!
    @IBOutlet weak var phoneBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var tosBtn: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var phoneloginUB: UIButton!
    
    @IBOutlet var applogo: UIImageView!
    var player: AVPlayer?
    var termDict = NSDictionary()
    
    @IBOutlet var appleBtn: UIButton!
    override func viewDidLoad() {
        
        phoneloginUB.isHidden = true
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialSetup()
        
    }
//    override func viewWillAppear(_ animated: Bool) {
//    self.changeToRTL()
//    }
    override func viewDidLayoutSubviews() {
        self.appleBtn.cornerRoundRadius()
        self.phoneBtn.cornerRoundRadius()
        self.fbBtn.cornerRoundRadius()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.initializeVideoPlayerWithVideo()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //config inital details
    func initialSetup()  {
        
        self.navigationController?.fd_fullscreenPopGestureRecognizer.isEnabled = true
        self.fd_prefersNavigationBarHidden = true
        bgView.backgroundColor = .black
        bgView.alpha = 0.8
        //phone_button
        phoneBtn.isHidden = true
        self.phoneBtn.setBorder(color: .white)
        self.phoneBtn.config(color: .white, font: medium, align: .center, title:"login_with_mobile")
        //old_apple_button
        //self.appleBtn.setBorder(color: .white)
        //self.appleBtn.config(color: .white, font: medium, align: .center, title:"login_with_apple")
        //new_apple_button
        self.appleBtn.backgroundColor = .white
        self.appleBtn.config(color: TEXT_PRIMARY_COLOR, font: medium, align: .center, title: "login_with_apple")
        //fb_apple_button
        self.fbBtn.backgroundColor = BG_FACEBOOK_COLOR
        self.fbBtn.config(color: .white, font: medium, align: .center, title: "facebook")
        self.tosBtn.config(color: .white, font: lite, align: .center, title: "tos")
        
        let tosStr = Utility.language.value(forKey: "tos")
        let range = (tosStr as! NSString).range(of: "Privacy Policy")
        let attributedString = NSMutableAttributedString(string: tosStr as! String)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range)
        tosBtn.titleLabel?.attributedText = attributedString
        
        if IS_LIVZA{
            self.applogo.image  =  #imageLiteral(resourceName: "welcome_logo")
        }else{
            self.applogo.image  =  #imageLiteral(resourceName: "app_logo_icon")
        }
        
        self.getPrivacyService()
        
          if #available(iOS 13.0, *) {
          }else{
            self.appleBtn.isHidden = true
            }
     

    }
    
    override func viewWillLayoutSubviews() {
     /*   if #available(iOS 13.0, *) {
            let appleBtn = ASAuthorizationAppleIDButton.init(type: .signIn, style: .whiteOutline)
                 appleBtn.frame = CGRect.init(x: self.fbBtn.frame.origin.x, y: self.fbBtn.frame.origin.y+self.fbBtn.frame.size.height+10, width: self.fbBtn.frame.size.width, height: self.fbBtn.frame.size.height+10)
                 appleBtn.addTarget(self, action: #selector(appleBtnTapped), for: .touchUpInside)
                appleBtn.cornerRadius = self.fbBtn.frame.size.height/2
                 self.view.addSubview(appleBtn)
                 
             } else {
                 // Fallback on earlier versions
             } */
    }
    
    
    @IBAction func appleBtnTapped(_ sender: Any) {
        if #available(iOS 13.0, *) {

             let appleIDProvider = ASAuthorizationAppleIDProvider()
                let request = appleIDProvider.createRequest()
                request.requestedScopes = [.fullName, .email]
                let authorizationController = ASAuthorizationController(authorizationRequests: [request])
                authorizationController.delegate = self
                authorizationController.presentationContextProvider = self
                authorizationController.performRequests()
             }
    }
    
  
    
    // ASAuthorizationControllerDelegate function for successful authorization
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            self.loader.startAnimating()
            if appleIDCredential.fullName?.familyName != nil{
                print("new signin")
                let firstName = appleIDCredential.fullName?.givenName
                let secondName = appleIDCredential.fullName?.familyName
                UserModel.shared.appleName = "\(firstName!) \(secondName!)"
                if let email = appleIDCredential.email {
                    UserModel.shared.email = email
                }
                
                self.signin(type: .apple, id: appleIDCredential.user)
            }else{
                print("already signin")
                self.signin(type: .apple, id: appleIDCredential.user)
            }
            // Create an account in your system.

            
        }
    }
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @IBAction func fbBtnTapped(_ sender: Any) {
        
        if Utility.shared.isConnectedToNetwork(){
            if AccessToken.current != nil {
                // User is logged in, do work such as go to next view controller.
                self.getFBUserData()
            }else{
                let login = LoginManager()
                login.logIn(permissions: ["public_profile","email"], from: self, handler: { result, error in
                    
                    if error != nil {
                        print("Process error ")
                    } else if (result?.isCancelled)! {
                        print("Cancelled")
                    } else {
                        self.loader.startAnimating()
                        self.getFBUserData()
                    }
                })
            }
            
        }else{
            self.statusAlert(msg: "network_error")
        }
        
    }
    
    //MARK: Get facebook user details based on accesstoken
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id,email,name,picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let responseDict = result as! NSDictionary
                    let fb_id = responseDict.value(forKey: "id") as! String
                    if (responseDict.value(forKey: "email") != nil){
                        let mail = responseDict.value(forKey: "email") as! String
                        UserModel.shared.email = mail
                    }
                    
                    print("fb data \(responseDict)")
                    UserModel.shared.fbDetails = responseDict
                    self.signin(type: .facebook, id:fb_id)
                }
            })
        }
    }
    //phone number
    @IBAction func phoneBtnTapped(_ sender: Any) {
//        self.signin(type:"phonenumber" , id:"8124205746") // Testing Purpose - Quick login without verification
//        FUIAuth.defaultAuthUI()?.delegate = self
//        let phoneProvider = FUIPhoneAuth.init(authUI: FUIAuth.defaultAuthUI()!)
//        FUIAuth.defaultAuthUI()?.providers = [phoneProvider]
//        let phoneProviderNew = FUIAuth.defaultAuthUI()?.providers.first as! FUIPhoneAuth
//
//        phoneProviderNew.signIn(withPresenting: self, phoneNumber: nil)
        let flowNavigation = UIStoryboard(name: "PhoneLogin", bundle: .main).instantiateInitialViewController() as! UINavigationController
        (flowNavigation.viewControllers.first as! PhoneLoginViewController).completion = { [weak self] phoneNumber in
            self?.signin(type: .phonenumber, id: phoneNumber)
        }
        
        present(flowNavigation, animated: true)
    }
    

    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        if authDataResult?.user != nil{
            if let myInteger = Int((authDataResult?.user.phoneNumber)!) {
                let phone_no = NSNumber(value:myInteger)
                self.signin(type: .phonenumber, id:(authDataResult?.user.phoneNumber)!)
            }
        }
    }
    func authUI(_ authUI: FUIAuth, didFinish operation: FUIAccountSettingsOperationType, error: Error?) {
    }
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, url: URL?, error: Error?) {
    }
    
    @IBAction func tosBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            let webObj =  WebPage()
            webObj.resultDict = self.termDict
            self.navigationController?.pushViewController(webObj, animated: true)
        }else{
            self.statusAlert(msg: "network_error")
        }
    }
    
    
    
    //background preview video
    func initializeVideoPlayerWithVideo() {
        // get the path string for the video from assets
        var videoString:String?
        if IS_LIVZA{
            videoString = Bundle.main.path(forResource: "stream_bgvideo", ofType: "mp4")
        }else{
            videoString = Bundle.main.path(forResource: "loginbgvideo", ofType: "mp4")
        }
        print("video path \(String(describing: videoString))")
        
        guard let unwrappedVideoPath = videoString else {return}
        // convert the path string to a url
        let videoUrl = URL(fileURLWithPath: unwrappedVideoPath)
        
        // initialize the video player with the url
        self.player = AVPlayer(url: videoUrl)
        
        // create a video layer for the player
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        
        // make the layer the same size as the container view
        layer.frame = CGRect(x:0,y:0,width:FULL_WIDTH,height:FULL_HEIGHT)
        
        // make the video fill the layer as much as possible while keeping its aspect size
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        // add the layer to the container view
        self.videoPreview.layer.addSublayer(layer)
        player?.play()
        
        // repeat video
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: .main) { _ in
            self.player?.seek(to: CMTime.zero)
            self.player?.play()
        }
    }
    //sign in service
    func signin(type: Signin ,id: String) {
        type.event.log()
        
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(type.rawValue, forKey: "type")
        requestDict.setValue(id, forKey: "login_id")
        Obj.baseService(subURl: SIGNIN_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "accountblocked"{
                UserModel.shared.logoutFromAll()
                self.loader.stopAnimating()
                let alertVC = CPAlertVC(title:APP_NAME, message: Utility.language.value(forKey: "acc_blocked") as! String)
                alertVC.animationType = .bounceUp
                alertVC.addAction(CPAlertAction(title: Utility.language.value(forKey: "yes") as! String, type: .normal, handler: {
                }))
                alertVC.show(into: self)
            }else{
                let isExist = dict?.value(forKey: "accountexists") as! String
                if status == SUCCESS{
                    self.fd_interactivePopDisabled = true
                    
                    if isExist == "false"{
                        UserModel.shared.isFirstLogin = true
                        UserModel.shared.setLogindetails(type: type.rawValue, id: id)
                        appDelegate.connectSockets()
                        let homeObj = TabBarPage()
                        self.navigationController?.pushViewController(homeObj, animated: true)
                    }else{
                        UserModel.shared.accessToken = dict?.value(forKey: "auth_token") as! String
                        UserModel.shared.userId = dict?.value(forKey: "user_id") as! String
                        print(UserModel.shared.userId)
                        
                        Apphud.startManually(apiKey: APPHUB_SDK_TOKEN, userID: UserModel.shared.userId, deviceID: UserModel.shared.userId, observerMode: false)
                        UserModel.shared.profileComplete = true
                        Utility.shared.pushsignIn()
                        appDelegate.connectSockets()
                        self.fetchInitialDetails()
                    }
                    
                    self.loader.stopAnimating()
                }
            }
        })
    }
    //GET terms & conditions
    func getPrivacyService()  {
        let serviceObj = BaseWebService()
        serviceObj.getDetails(subURl: TERMS_API, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == SUCCESS
            {
                self.termDict = dict?.value(forKey: "terms") as! NSDictionary //ANIL
                let dict = response.result.value as? NSDictionary
                _ = dict?.value(forKey: "terms") as? String
               // self.loader.stopAnimating()
            }
            
        })
    }
    
    //update initial details
    func fetchInitialDetails()  {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue(UserModel.shared.userId, forKey: "profile_id")
        Obj.baseService(subURl: PROFILE_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.userData = response.data!
                let premium_member = dict?.value(forKey: "premium_member") as! String
                UserModel.shared.isPremium = premium_member == "true"
                let available_gems = dict?.value(forKey: "available_gems") as! NSNumber
                UserModel.shared.gemCount = available_gems.intValue
                let homeObj = TabBarPage()
                self.navigationController?.pushViewController(homeObj, animated: true)
            }
        })
    }
}
extension SFSafariViewController {
    override open var modalPresentationStyle: UIModalPresentationStyle {
        get { return .fullScreen}
        set { super.modalPresentationStyle = newValue }
    }
}
