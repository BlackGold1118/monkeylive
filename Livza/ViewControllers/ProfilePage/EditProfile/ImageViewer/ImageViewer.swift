//
//  ImageViewer.swift
//  Randoo
//
//  Created by HTS-Product on 20/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class ImageViewer: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate ,UIScrollViewDelegate{
    
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var swipeLbl: UILabel!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var imgName = String()
    var imageUploaded: (() -> Void)?
    var viewType = String()
    var imgData:Data?
    var imageType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        self.animationView.addLOT(lot_name: "swipeup", w: self.animationView.frame.size.width, h: self.animationView.frame.size.height)
        //        self.animationView.rotate(angle: 180)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.changeToRTL()
        self.initialSetup()
    }
    
    func loadGifImage(url: String) {
          // self.gifimageview.isHidden = false
           //self.profileImg.isHidden = true
          // self.containerView.backgroundColor = .clear
           if let imgUrl = URL(string: url) {
               self.profileImg.isHidden = false
//               self.profileImg.setGifFromURL(imgUrl)
               self.profileImg.contentMode = .scaleAspectFit
           }
       }
    
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.profileImg.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.swipeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.swipeLbl.textAlignment = .center
        }
        else{
            self.view.transform = .identity
            self.profileImg.transform = .identity
            self.swipeLbl.transform = .identity
            self.swipeLbl.textAlignment = .center
        }
    }
    
    //config initial setup
    func initialSetup()  {
        if viewType == "2" { // own profile
            self.imagePicker.delegate = self
            self.swipeLbl.config(color: .white, font: medium, align: .center, text: "swipe_to_change")
            //swipe up
            let swipeUp = UISwipeGestureRecognizer(target: self, action:#selector(self.respondToSwipeGesture(sender:)))
            swipeUp.direction = UISwipeGestureRecognizer.Direction.up
            self.view.addGestureRecognizer(swipeUp)            
            self.animationView.isHidden = false
            self.swipeLbl.isHidden = false
        }else{ // other profile
            self.animationView.isHidden = true
            self.swipeLbl.isHidden = true
        }
        
        //swipe down
        let swipeDown = UISwipeGestureRecognizer(target: self, action:#selector(self.respondToSwipeGesture(sender:)))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
        if viewType == "3" {
            if(imageType == "gif")
                           {
                               self.loadGifImage(url: self.imgName)
                           }
            else
            {// chat page
            if imgData != nil{
             
                let image = UIImage(data: imgData!)
                self.profileImg.image = image
             
            } 
            }
            
        }else{
            let imgURL = URL.init(string:PROFILE_IMAGE_URL+self.imgName)
            self.profileImg.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        }
        
        loader.stopAnimating()
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 10.0
        self.scrollView.delegate = self
        self.scrollView.alwaysBounceVertical = false
        self.scrollView.alwaysBounceHorizontal = false
    }
    //scrollview delegate
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.profileImg
    }
    
    //close view
    @IBAction func backBtnTapped(_ sender: Any) {
        if !loader.isAnimating {
            self.dismiss(animated: true, completion: nil)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //swipe
    @objc func respondToSwipeGesture(sender: UIGestureRecognizer? = nil) {
        
        if !loader.isAnimating {
            if let swipeGesture = sender as? UISwipeGestureRecognizer {
                switch swipeGesture.direction {
                case UISwipeGestureRecognizer.Direction.down:
                    self.dismiss(animated: true, completion: nil)
                case UISwipeGestureRecognizer.Direction.up:
                    if Utility.shared.isConnectedToNetwork(){
                        self.animationView.isHidden = true
                        self.swipeLbl.isHidden = true
                        
                        self.choosePicture()
                    }
                default:
                    break
                }
            }
        }
        
        
    }
    //gallery popup
    func choosePicture()  {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: Utility.shared.appLanguage.value(forKey: "camera") as? String, style: .default)
        { (action) in
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                //already authorized
                self.moveToCamera()
            } else {
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                    if granted {
                        //access allowed
                        self.moveToCamera()
                    } else {
                        //access denied
                        DispatchQueue.main.async {
                            self.privateCameraPermissionAlert()
                        }
                    }
                })
            }
        }
        let gallery = UIAlertAction(title: Utility.shared.appLanguage.value(forKey: "gallery") as? String, style: .default) { (action) in
            self.animationView.isHidden = false
            self.swipeLbl.isHidden = false
            
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: Utility.shared.appLanguage.value(forKey: "cancel") as? String, style: .cancel){(action) in
            self.animationView.isHidden = false
            self.swipeLbl.isHidden = false
        }
        alertController.addAction(camera)
        alertController.addAction(gallery)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //move to camera
    func moveToCamera()
    {
        if UIImagePickerController.availableCaptureModes(for: .front) != nil
        {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            imagePicker.cameraDevice = .front
            imagePicker.delegate = self
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker, animated: true, completion: nil)
        }
        else
        { print("Your device does not have camera") }
        
        

    }
    
    // THIS METHDO WILL BE USED SINCE WE CAN'T PRESENT MULTIPLE ALERT ON APP DELEGATE AT ONCE
    
    func privateCameraPermissionAlert()  {
        
        let otherAlert = UIAlertController(title: nil, message: Utility.language.value(forKey: "camera_permission") as? String, preferredStyle: UIAlertController.Style.alert)
        let dismiss = UIAlertAction(title: Utility.language.value(forKey: "cancel") as? String, style: UIAlertAction.Style.cancel, handler: nil)
        otherAlert.addAction(dismiss)
        
        let setting = UIAlertAction(title: Utility.language.value(forKey: "setting") as? String, style: UIAlertAction.Style.default, handler:goSetting)
        otherAlert.addAction(setting)
        
        //ANIL
        self.present(otherAlert, animated: true, completion: nil)
    }
    
    /*func moveToCamera()   {
        self.animationView.isHidden = false
        self.imagePicker.allowsEditing = false
        self.swipeLbl.isHidden = false
        self.imagePicker.sourceType = .camera

        if !UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            print(">>>Device has no camera.")
        }
        else
        {
            //self.present(self.imagePicker, animated: true, completion: nil)
        }
    }*/
    
    // MARK: - UIImagePickerControllerDelegate Methods
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print("infor \(info)")
        if let pickedImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            if Utility.shared.isConnectedToNetwork(){
                self.loader.startAnimating()
                self.profileImg.image = pickedImage
                self.profileImg.layer.minificationFilterBias = 5.0
                
                let imageData: Data = pickedImage.jpegData(compressionQuality: 0.5)!
                let uploadObj = BaseWebService()
                uploadObj.uploadProfilePic(profileimage: imageData, onSuccess: {response in
                    let dict = response.result.value as? NSDictionary
                    let status = dict?.value(forKey: "status") as? String
                    if status == "true"{
                        self.updateProfile(img: pickedImage)
                    }
                })
            }
        }else{
            self.showAlert(msg: "choose_image")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateProfile(img:UIImage)  {
        let updateObj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue(UserModel.shared.userId, forKey: "profile_id")
        updateObj.baseService(subURl: PROFILE_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.userData = response.data!
                self.profileImg.image = img
                self.imageUploaded!()
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.loader.stopAnimating()
                    self.profileImg.layer.minificationFilterBias = 0.0
                    self.statusAlert(msg: "profile_updated")
                    self.dismiss(animated: true, completion: nil)
                }
            }else  if status == "rejected"{
                let msg = dict?.value(forKey: "message") as! String
                self.statusServer(alert: msg)
            }
        })
    }
}
