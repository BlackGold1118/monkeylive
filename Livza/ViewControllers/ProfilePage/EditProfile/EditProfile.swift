//
//  EditProfile.swift
//  Randoo
//
//  Created by HTS-Product on 19/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import JTMaterialTransition
import GoogleMobileAds

class EditProfile: UIViewController,UITextFieldDelegate,GADBannerViewDelegate {
    
    
    @IBOutlet var bannerView: GADBannerView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var premiumView: UIView!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var paypalView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var primeTitleLbl: UILabel!
    @IBOutlet weak var primeSubTitleLbl: UILabel!
    @IBOutlet weak var nameHeaderLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var paypalLbl: UILabel!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var crownView: UIView!
    @IBOutlet weak var paypalTF: UITextField!
    @IBOutlet weak var genderStatusLbl: UILabel!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet var tickBtn: UIButton!
    
    @IBOutlet var primeBtn: UIButton!
    var changesDone: (() -> Void)?
    var transition: JTMaterialTransition?
    var imageName = String()
    var lottieName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
            self.changeToRTL()
    }
    
    override func viewDidLayoutSubviews() {
        if IS_LIVZA {
            self.premiumView.backgroundColor = PRIMARY_COLOR
        }else{
            self.animationView.cornerViewMiniumRadius()
            self.animationView.addLOT(lot_name: "editprofilelottie", w: self.animationView.frame.size.width, h: self.animationView.frame.size.height)
        }
    }
    
    func changeToRTL(){
        
        if UserModel.shared.language == "Arabic"{
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.userImg.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.nameHeaderLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.nameHeaderLbl.textAlignment = .right
            self.genderLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.genderLbl.textAlignment = .right
            self.dobLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.dobLbl.textAlignment = .right
            self.paypalLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.paypalLbl.textAlignment = .right
            self.nameTF.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.nameTF.textAlignment = .left
            self.dateLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.dateLbl.textAlignment = .left
            self.paypalTF.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.paypalTF.textAlignment = .left
            self.genderStatusLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.genderStatusLbl.textAlignment = .left
            self.submitBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.primeTitleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.primeTitleLbl.textAlignment = .right
            self.primeSubTitleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.primeSubTitleLbl.textAlignment = .right
            
        }
        else{
            self.view.transform = .identity
            self.userImg.transform = .identity
            self.nameHeaderLbl.transform = .identity
            self.nameHeaderLbl.textAlignment = .left
            self.genderLbl.transform = .identity
            self.genderLbl.textAlignment = .left
            self.dobLbl.transform = .identity
            self.dobLbl.textAlignment = .left
            self.paypalLbl.transform = .identity
            self.paypalLbl.textAlignment = .left
            self.nameTF.transform = .identity
            self.nameTF.textAlignment = .right
            self.dateLbl.transform = .identity
            self.dateLbl.textAlignment = .right
            self.paypalTF.transform = .identity
            self.paypalTF.textAlignment = .right
            self.genderStatusLbl.transform = .identity
            self.genderStatusLbl.textAlignment = .right
            self.submitBtn.transform = .identity
            self.primeTitleLbl.transform = .identity
            self.primeTitleLbl.textAlignment = .left
            self.primeSubTitleLbl.transform = .identity
            self.primeSubTitleLbl.textAlignment = .left
        }
    }
    
    //initial set up
    func initialSetup()  {
        self.fd_interactivePopDisabled = false
        self.fd_prefersNavigationBarHidden = true
        self.loader.stopAnimating()
        self.premiumView.cornerViewMiniumRadius()
        self.nameView.cornerViewMiniumRadius()
        self.genderView.cornerViewMiniumRadius()
        self.dobView.cornerViewMiniumRadius()
        self.paypalView.cornerViewMiniumRadius()
        self.premiumView.applyCardEffect()
        self.nameView.applyCardEffect()
        self.genderView.applyCardEffect()
        self.dobView.applyCardEffect()
        self.paypalView.applyCardEffect()
        
        self.tickBtn.cornerViewRadius()
        self.crownView.cornerViewRadius()
        self.primeTitleLbl.config(color: .white, font: liteReg, align: .left, text: "expired")
        self.primeSubTitleLbl.config(color: .white, font: averageReg, align: .left, text: "your_membership")
        self.primeBtn.config(color: #colorLiteral(red: 1, green: 0.6, blue: 0.2549019608, alpha: 1), font: liteReg, align: .center, title: "subscribe")
        self.primeBtn.backgroundColor = .white
        self.primeBtn.cornerRoundRadius()
        self.primeBtn.elevationEffect()
        
        self.nameHeaderLbl.config(color:TEXT_PRIMARY_COLOR, font: lite, align: .left, text: "name")
        self.genderLbl.config(color:TEXT_PRIMARY_COLOR, font: lite, align: .left, text: "gender")
        self.dobLbl.config(color:TEXT_PRIMARY_COLOR, font: lite, align: .left, text: "dob")
        self.paypalLbl.config(color:TEXT_PRIMARY_COLOR, font: lite, align: .left, text: "paypal")
        self.nameTF.config(color: TEXT_PRIMARY_COLOR, font: liteReg, align: .right, place: "")
        nameTF.delegate = self
        self.dateLbl.config(color:TEXT_SECONDARY_COLOR, font: liteReg, align: .right, text: "")
        self.genderStatusLbl.config(color:TEXT_SECONDARY_COLOR, font: liteReg, align: .right, text: "")
        
        self.paypalTF.config(color: TEXT_PRIMARY_COLOR, font: liteReg, align: .right, place: "paypal")
        self.paypalTF.placeholder = (Utility.language.value(forKey: "paypal") as! String)
        self.paypalTF.delegate = self
        self.crownView.cornerViewRadius()
        self.submitBtn.cornerViewMiniumRadius()
        self.submitBtn.backgroundColor = PRIMARY_COLOR
        self.submitBtn.config(color: .white, font: medium, align: .center, title: "save")
        
        self.transition = JTMaterialTransition(animatedView: self.centerPoint())
        
        self.setDetails()
        self.configPrimeView()
        
        if Utility.shared.isAdsEnable() {
            self.configAds()
        }else{
            self.bannerView.isHidden = true
        }
        
        let defaultDict = UserModel.shared.getDefaults()
        let moneyEnable = defaultDict?.value(forKey: "show_money_conversion") as! NSNumber
        if moneyEnable == 0{
            self.paypalView.isHidden = true
        }
    }
    
    func configPrimeView()  {
        self.primeSubTitleLbl.isHidden = true
        if UserModel.shared.isPremium {
            do{
                let jsonDecoder = JSONDecoder()
                let profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
                self.lottieName = "subscribe"
                self.primeTitleLbl.text = Utility.language.value(forKey: "premium_member") as? String
                let valideUntil = Utility.language.value(forKey: "valid_until") as! String
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                let expDate = formatter.date(from: profile.premium_expiry_date!)
                self.primeSubTitleLbl.text = "\(valideUntil) \(Utility.shared.timeStamp(time:expDate! , format: "dd/MM/yyyy"))"
                self.primeBtn.isHidden = true
                self.crownView.isHidden = false
                self.tickBtn.isHidden = false
            }catch{
            }
        }else{
            if UserModel.shared.alreadySubscribed { // already prime
                self.lottieName = "renewal"
                self.primeBtn.config(color: .red, font: liteReg, align: .center, title: "renewal")
                self.primeTitleLbl.text = Utility.language.value(forKey: "expired") as? String
                self.primeSubTitleLbl.text = Utility.language.value(forKey: "your_membership") as? String
            }else{
                self.lottieName = "subscribe"
                self.primeBtn.config(color: #colorLiteral(red: 1, green: 0.6, blue: 0.2549019608, alpha: 1), font: liteReg, align: .center, title: "subscribe")
                self.primeTitleLbl.text = Utility.language.value(forKey: "prime_benefits") as? String
                if Utility.shared.primePackage != nil{
                    let dict = Utility.shared.primePackage
                    let currency = dict?.value(forKey: "currency") as! String
                    let validity = dict?.value(forKey: "validity") as! String
                    let price = dict?.value(forKey: "price") as! String
                    self.primeSubTitleLbl.text = "\(currency) \(price)/\(validity)"
                    
                }
            }
            self.crownView.isHidden = true
            self.tickBtn.isHidden = true
        }
    }
    
    //config banner view
    func configAds()  {
        bannerView.adUnitID = Utility.shared.adUnitId
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    //banner view delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func tapProfilePic(_ sender: Any) {
        let viewObj = ImageViewer()
        viewObj.viewType = "2"
        viewObj.modalPresentationStyle = .custom
        viewObj.transitioningDelegate = self.transition
        viewObj.imgName = self.imageName
        viewObj.imageUploaded = {
            self.setDetails()
            self.changesDone!()
        }
        self.navigationController?.present(viewObj, animated: true, completion: nil)
    }
    
    //set details
    func setDetails()  {
        do{
            let jsonDecoder = JSONDecoder()
            let profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
            self.nameTF.text = profile.name
            let imgURL = URL.init(string: PROFILE_IMAGE_URL+profile.user_image!)
            self.imageName = profile.user_image!
            self.userImg.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
            if profile.gender == "male"{
                genderStatusLbl.text = "Male"
            }else{
                genderStatusLbl.text = "Female"
            }
            
            self.dateLbl.text = profile.dob
            self.paypalTF.text = profile.paypal_id
        }catch{
            
        }
    }
    
    //close view
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func primeBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            OfferPresentingViewController.RemoteConfiguredPlansViewController.requestProductsAndShow(from: self)
        }
    }
    
    //save details
    @IBAction func saveBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            if !self.loader.isAnimating{
                if self.nameTF.isEmptyValue() {
                    self.showAlert(msg: "enter_name")
                }else{
                    self.loader.startAnimating()
                    let updateOBj = BaseWebService()
                    let request = NSMutableDictionary()
                    request.setValue(UserModel.shared.userId, forKey: "user_id")
                    request.setValue(UserModel.shared.userId, forKey: "profile_id")
                    request.setValue(self.nameTF.text, forKey: "name")
                    request.setValue(self.paypalTF.text, forKey: "paypal_id")
                    updateOBj.baseService(subURl: PROFILE_API, params:  request as? Parameters, onSuccess: {response in
                        let dict = response.result.value as? NSDictionary
                        let status = dict?.value(forKey: "status") as! String
                        if status == "true"{
                            UserModel.shared.userData = response.data!
                            self.setDetails()
                            self.changesDone!()
                            self.statusAlert(msg: "profile_updated")
                            self.loader.stopAnimating()
                        }
                    })
                }
            }
        }
    }
    //text field delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn _: NSRange, replacementString string: String) -> Bool {
        
        if (textField.text?.count)! >= 20 {
            if string.isEmpty{
                return true
            }else{
                return false
            }
        }
        if !string.isEmpty{
            let set = CharacterSet(charactersIn: RISTRICTED_CHARACTERS)
            let inverted = set.inverted
            let filtered = string.components(separatedBy: inverted).joined(separator: "")
            return filtered != string
        }else{
            return true
        }
    }
}
