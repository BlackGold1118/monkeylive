//
//  ProfilePage.swift
//  Randoo
//
//  Created by HTS-Product on 31/01/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import Lottie
import SDWebImage
import JTMaterialTransition
import GoogleMobileAds
import IQKeyboardManagerSwift

class ProfilePage: UIViewController,GADBannerViewDelegate,storeDelegate,socketDelegate {
    
    
    
    @IBOutlet var bannerView: GADBannerView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var gemView: UIView!
    @IBOutlet weak var giftView: UIView!
    @IBOutlet weak var premiumView: UIView!
    @IBOutlet weak var followViwe: UIView!
    @IBOutlet weak var followersCountLbl: UILabel!
    @IBOutlet weak var followersLbl: UILabel!
    @IBOutlet weak var followingCountLbl: UILabel!
    @IBOutlet weak var followingLbl: UILabel!
    @IBOutlet weak var separatorLbl: UILabel!
    @IBOutlet weak var gemCountLbl: UILabel!
    @IBOutlet weak var giftCountLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var expiredLbl: UILabel!
    @IBOutlet weak var renewBtn: UIButton!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var membershipLbl: UILabel!
    @IBOutlet weak var otherUserView: UIView!
    @IBOutlet weak var msgBtn: UIButton!
    @IBOutlet weak var videocall_btn: UIButton!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var settingBtn: UIButton!
    @IBOutlet weak var crownIcon: UIImageView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet var gemLbl: UILabel!
    @IBOutlet var giftLbl: UILabel!
    @IBOutlet var crownView: UIView!
    @IBOutlet var tickBtn: UIButton!
    @IBOutlet var genderIcon: UIImageView!
    @IBOutlet var profileBg: UIImageView!
    
    @IBOutlet var videosView: UIView!
    @IBOutlet var videoCountLbl: UILabel!
    @IBOutlet var videoLbl: UILabel!
    @IBOutlet weak var youtube_icon: UIImageView!
    
    var partner:ProfileModel!
    var userModal:Users_list!
    var isFollowReq = Bool()
    var viewType = String()
    var dismissType = String()
    var imageName = String()
    var transition: JTMaterialTransition?
    var profileId = String()
    var followStatus = String()
    var lottieName = String()
    var isLoadData = false
    var username = String()
    var additionalInfo = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //        self.transitioningDelegate = self
        self.initalSetup()
        //        self.changeToRTL()
    }
    
    override func viewDidLayoutSubviews() {
        self.gemView.cornerViewMiniumRadius()
        self.giftView.cornerViewMiniumRadius()
        self.gemView.applyCardEffect()
        self.giftView.applyCardEffect()
        self.videosView.cornerViewMiniumRadius()
        self.videosView.applyCardEffect()
        
        if IS_LIVZA{
            self.premiumView.backgroundColor = PRIMARY_COLOR
            self.premiumView.cornerViewMiniumRadius()
        }else{
            self.animationView.addLOT(lot_name: lottieName, w: self.premiumView.frame.size.width, h: 80)
            self.animationView.cornerViewMiniumRadius()
            self.premiumView.applyCardEffect()
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        //        self.changeToRTL()
        Entities.sharedInstance.delegate = self
        HSChatSocket.sharedInstance.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        self.fd_prefersNavigationBarHidden = true
        
        if viewType == "ownProfile" {
            self.isLoadData = true
            self.getDetails()
        }else{
            if !isLoadData {
                self.isLoadData = false
                self.getDetails()
            }
        }
        self.initalSetup()
        self.configPrimeView()
        self.changeToRTL()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if viewType == "livepage"{
            self.isLoadData = false
        }
    }
    
    
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.profilePic.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.editBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.crownIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.genderIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.gemLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.youtube_icon.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.gemLbl.textAlignment = .right
            self.gemCountLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.gemCountLbl.textAlignment = .left
            self.videoLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.videoLbl.textAlignment = .right
            self.videoCountLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.videoCountLbl.textAlignment = .left
            self.giftLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftLbl.textAlignment = .right
            self.giftCountLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftCountLbl.textAlignment = .left
            self.nameLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.nameLbl.textAlignment = .right
            self.locationLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.locationLbl.textAlignment = .center
            self.followersLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.followersCountLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.followingLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.followingCountLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.membershipLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.renewBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.expiredLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.videocall_btn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.msgBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.followBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.expiredLbl.textAlignment = .right
            self.membershipLbl.textAlignment = .right
        }
        else{
            self.view.transform = .identity
            self.gemLbl.transform = .identity
            self.gemLbl.textAlignment = .left
            self.profilePic.transform = .identity
            self.editBtn.transform = .identity
            self.crownIcon.transform = .identity
            self.genderIcon.transform = .identity
            self.gemCountLbl.transform = .identity
            self.gemCountLbl.textAlignment = .right
            self.videoLbl.transform = .identity
            self.videoLbl.textAlignment = .left
            self.videoCountLbl.transform = .identity
            self.videoCountLbl.textAlignment = .right
            self.giftLbl.transform = .identity
            self.giftLbl.textAlignment = .left
            self.giftCountLbl.transform = .identity
            self.giftCountLbl.textAlignment = .right
            self.nameLbl.transform = .identity
            self.locationLbl.transform = .identity
            self.locationLbl.textAlignment = .center
            self.followersLbl.transform = .identity
            self.followersCountLbl.transform = .identity
            self.followingLbl.transform = .identity
            self.followingCountLbl.transform = .identity
            self.membershipLbl.transform = .identity
            self.membershipLbl.textAlignment = .left
            self.renewBtn.transform = .identity
            self.videocall_btn.transform = .identity
            self.msgBtn.transform = .identity
            self.followBtn.transform = .identity
            self.expiredLbl.transform = .identity
            self.expiredLbl.textAlignment = .left
        }
    }
    //config design properties
    func initalSetup() {
        if IS_LIVZA{
            self.profileBg.image = #imageLiteral(resourceName: "live_profile_bg")
            if IS_IPHONE_X{
                bannerView.frame.origin.y = self.followBtn.frame.origin.y+210
            }else{
                bannerView.frame.origin.y = self.followBtn.frame.origin.y+90
            }
        }else{
            self.profileBg.image = #imageLiteral(resourceName: "profile_bg")
        }
        
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        self.genderIcon.isHidden = true
        self.crownIcon.isHidden = true
        if viewType != "ownProfile"{
            self.tabBarController?.tabBar.isHidden = true
            if Utility.shared.isAdsEnable() {
                print("banner not hidden")
                self.configAds()
            }else{
                print("banner hidden")
                self.bannerView.isHidden = true
            }
        }
        self.fd_prefersNavigationBarHidden = true
        self.isFollowReq = false
        self.fd_interactivePopDisabled = false
        self.profilePic.makeItRound()
        self.nameLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text: "")
        self.locationLbl.config(color: TEXT_PRIMARY_COLOR, font: mediumReg, align: .center, text: "")
        self.followersCountLbl.config(color: TEXT_PRIMARY_COLOR, font: mediumReg, align: .center, text: "")
        self.followingCountLbl.config(color: TEXT_PRIMARY_COLOR, font: mediumReg, align: .center, text: "")
        self.followersLbl.config(color: TEXT_SECONDARY_COLOR, font: mediumReg, align: .center, text: "followers")
        self.followingLbl.config(color: TEXT_SECONDARY_COLOR, font: mediumReg, align: .center, text: "followings")
        self.separatorLbl.backgroundColor = LINE_COLOR
        self.gemCountLbl.config(color: TEXT_PRIMARY_COLOR, font: mediumReg, align: .right, text: "")
        self.giftCountLbl.config(color: TEXT_PRIMARY_COLOR, font: mediumReg, align: .right, text: "")
        self.videoCountLbl.config(color: TEXT_PRIMARY_COLOR, font: mediumReg, align: .right, text: "")
        self.gemLbl.config(color: TEXT_SECONDARY_COLOR, font: mediumReg, align: .left, text: "gems")
        self.giftLbl.config(color: TEXT_SECONDARY_COLOR, font: mediumReg, align: .left, text: "gifts")
        if viewType == "ownProfile"{
            self.videoLbl.config(color: TEXT_SECONDARY_COLOR, font: mediumReg, align: .left, text: "my_videos")
        }else{
            self.videoLbl.config(color: TEXT_SECONDARY_COLOR, font: mediumReg, align: .left, text: "videos")
        }
        self.expiredLbl.config(color: .white, font: liteReg, align: .left, text: "expired")
        self.membershipLbl.config(color: .white, font: averageReg, align: .left, text: "your_membership")
        self.renewBtn.config(color: #colorLiteral(red: 1, green: 0.6, blue: 0.2549019608, alpha: 1), font: liteReg, align: .center, title: "renewal")
        self.renewBtn.cornerRoundRadius()
        self.renewBtn.elevationEffect()
        self.crownView.cornerViewRadius()
        self.tickBtn.cornerRoundRadius()
        
        self.renewBtn.backgroundColor = .white
        self.transition = JTMaterialTransition(animatedView: self.centerPoint())
        self.loader.color = PRIMARY_COLOR
        self.loader.stopAnimating()
        self.getDetails()
        
        if !IS_LIVZA && viewType == "ownProfile"{
            self.gemView.frame.origin.y = self.followViwe.frame.origin.y+self.followViwe.frame.size.height+15
            self.giftView.frame.origin.y = self.gemView.frame.origin.y+self.gemView.frame.size.height+10
            self.premiumView.frame.origin.y = self.giftView.frame.origin.y+self.giftView.frame.size.height+10
        }
        if IS_LIVZA {
            self.videosView.isHidden = false
        }else{
            self.videosView.isHidden = true
        }
    }
    //get details based on the view
    func getDetails(){
        if viewType == "livepage"{
            self.configOtherUserUI() //local set
            do{
                let jsonDecoder = JSONDecoder()
                partner = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.partnerModel)
                self.setLiveUserInfo()
            }catch{
            }
            self.editBtn.isHidden = true
            self.settingBtn.isHidden = true
            self.backBtn.isHidden = true
        }else if viewType == "fromFollow"{
            //            self.loader.startAnimating()
            self.configOtherUserUI() //local set
            self.setOtherUserInfo()
            self.editBtn.isHidden = true
            self.settingBtn.isHidden = true
            self.backBtn.isHidden = false
        }else if viewType == "ownProfile"{
            
            setProfileDetails() //local set
            DispatchQueue.global(qos: .background).async {
                self.refreshProfileInfo()//get from server
            }
            self.otherUserView.isHidden = true
            if additionalInfo == "needback"{
                self.backBtn.isHidden = false
            }else{
                self.backBtn.isHidden = true
            }
            
        }else if viewType == "fromMsg"{
            //            self.loader.startAnimating()
            self.configOtherUserUI() //local set
            DispatchQueue.global(qos: .background).async {
                self.refreshProfileInfo()//get from server
            }
            self.editBtn.isHidden = true
            self.settingBtn.isHidden = true
            self.backBtn.isHidden = false
        }
        
        UserModel.shared.scrollEnabled = true
    }
    
    func configPrimeView()  {
        membershipLbl.isHidden = true
        if UserModel().isPremium {
            do{
                let jsonDecoder = JSONDecoder()
                let profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
                self.lottieName = "subscribe"
                self.expiredLbl.text = Utility.language.value(forKey: "premium_member") as? String
                let valideUntil = Utility.language.value(forKey: "valid_until") as! String
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                let expDate = formatter.date(from: profile.premium_expiry_date ?? "")
                if let expDate = expDate {
                    self.membershipLbl.text = "\(valideUntil) \(Utility.shared.timeStamp(time:expDate , format: "dd/MM/yyyy"))"
                }
                self.renewBtn.isHidden = true
            }catch{
            }
            self.crownView.isHidden = false
            self.tickBtn.isHidden = false
        }else{
            if UserModel.shared.alreadySubscribed { // already prime
                self.renewBtn.isHidden = false
                self.lottieName = "renewal"
                self.renewBtn.config(color: .red, font: liteReg, align: .center, title: "renewal")
                self.expiredLbl.text = Utility.language.value(forKey: "expired") as? String
                self.membershipLbl.text = Utility.language.value(forKey: "your_membership") as? String
            }else{
                self.renewBtn.isHidden = false
                self.lottieName = "subscribe"
                self.renewBtn.config(color: #colorLiteral(red: 1, green: 0.6, blue: 0.2549019608, alpha: 1), font: liteReg, align: .center, title: "subscribe")
                self.expiredLbl.text = Utility.language.value(forKey: "prime_benefits") as? String
                if Utility.shared.primePackage != nil{
                    let dict = Utility.shared.primePackage
                    let currency = dict?.value(forKey: "currency") as! String
                    let validity = dict?.value(forKey: "validity") as! String
                    let price = dict?.value(forKey: "price") as! String
                    self.membershipLbl.text = "\(currency) \(price)/\(validity)"
                }
                
            }
            self.crownView.isHidden = true
            self.tickBtn.isHidden = true
        }
    }
    
    //refresh profile information
    func refreshProfileInfo()  {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        var userid = String()
        if self.viewType == "ownProfile" {
            userid = UserModel.shared.userId
            self.profileId = UserModel.shared.userId
        }else if self.viewType == "fromFollow" || self.viewType == "livepage" {
            userid = self.profileId
        }else if self.viewType == "fromMsg"{
            userid = self.profileId
        }
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue(userid, forKey: "profile_id")
        Obj.baseService(subURl: PROFILE_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                if self.viewType == "ownProfile" {
                    UserModel.shared.userData = response.data!
                    self.setProfileDetails()
                    let premium_member = dict?.value(forKey: "premium_member") as! String
                    UserModel.shared.isPremium = premium_member == "true"
                    self.configPrimeView()
                }else{
                    do{
                        let jsonDecoder = JSONDecoder()
                        self.partner = try jsonDecoder.decode(ProfileModel.self, from: response.data!)
                        self.setModel(profile: self.partner)
                        if self.viewType == "livepage"{
                            UserModel.shared.partnerModel = response.data!
                        }
                    }catch{
                    }
                }
                self.loader.stopAnimating()
                self.isLoadData = true
            }
        })
    }
    
    
    //other user profile UI
    func configOtherUserUI()  {
        self.followingCountLbl.text = "-"
        self.followersCountLbl.text = "-"
        if IS_LIVZA{
            self.videosView.isHidden = false
            self.otherUserView.frame.origin.y = self.videosView.frame.origin.y+self.videosView.frame.size.height+5
        }
        self.otherUserView.isHidden = false
        self.msgBtn.setCorner(radius: 20)
        self.videocall_btn.setCorner(radius: 20)
        self.msgBtn.applyCardEffect()
        self.videocall_btn.applyCardEffect()
        let msgIcon = #imageLiteral(resourceName: "msg_sel").withRenderingMode(.alwaysTemplate)
        self.msgBtn.setImage(msgIcon, for: .normal)
        self.msgBtn.tintColor = PRIMARY_COLOR
        self.followBtn.cornerRoundRadius()
        self.followBtn.setBorder(color: LINE_COLOR)
    }
    //set details
    func setLiveUserInfo()  {
        self.imageName = partner.user_image!
        self.genderIcon.isHidden = false
        if partner.gender == "male"{
            genderIcon.image = UIImage(named: "male_sel")
        }else{
            genderIcon.image = UIImage(named: "female_sel")
        }
        var username = String()
        if (partner.name?.count)! > 25{
            username  = "\(String((partner.name?.prefix(25))!)).."
        }else{
            username  = partner.name!
        }
        
        if partner.privacy_age! {
            self.nameLbl.text = "\(username)"
        }else{
            self.nameLbl.text = "\(username), \(partner.age!)"
        }
        
        let nameWidth = (self.nameLbl.intrinsicContentSize.width/2)+10
        self.nameLbl.frame = CGRect.init(x:(FULL_WIDTH/2)-nameWidth , y: self.profilePic.frame.size.height+self.profilePic.frame.origin.y+10, width: self.nameLbl.intrinsicContentSize.width, height: 35)
        self.genderIcon.frame = CGRect.init(x:self.nameLbl.frame.size.width+self.nameLbl.frame.origin.x+10 , y: self.profilePic.frame.size.height+self.profilePic.frame.origin.y+17.5, width: 20, height: 20)
        
        
        let imgURL = URL.init(string: PROFILE_IMAGE_URL+partner.user_image!)
        self.profilePic.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        self.locationLbl.text = partner.location
        self.profileId = partner.user_id!
        
        self.followBtn.isHidden = false
        self.followingCountLbl.text = "\(partner.followings!)"
        self.followersCountLbl.text = "\(partner.followers!)"
        if partner.follow == "false"{
            self.followBtn.config(color: .white, font: medium, align: .center, title: "follow")
            self.followBtn.backgroundColor = PRIMARY_COLOR
        }else{
            self.followBtn.config(color: TEXT_PRIMARY_COLOR, font: medium, align: .center, title: "unfollow")
            self.followBtn.backgroundColor = .white
        }
        
        self.videocall_btn.isHidden = true
        self.msgBtn.isHidden = true
        self.followBtn.frame.origin.y = 20
        self.refreshProfileInfo()
    }
    
    // set profile values
    func setProfileDetails()  {
        do{
            let jsonDecoder = JSONDecoder()
            let profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
            self.setModel(profile: profile)
        }catch{
            
        }
    }
    
    
    //set values from model
    func setModel(profile:ProfileModel)  {
        if profile.user_image != nil{
            self.imageName = profile.user_image!
        }else{
            self.imageName = ""
        }
        self.locationLbl.text = profile.location
        self.followingCountLbl.text = "\(profile.followings!)"
        self.followersCountLbl.text = "\(profile.followers!)"
        
        var username = String()
        if (profile.name?.count)! > 25{
            username  = "\(String((profile.name?.prefix(25))!)).."
        }else{
            username  = profile.name!
        }
        
        if self.viewType == "ownProfile"{
            self.gemCountLbl.text = "\(profile.available_gems!)"
            //            self.gemCountLbl.text = "\(UserModel.shared.gemCount)"
            self.giftCountLbl.text = "\(profile.available_gifts!)"
            self.nameLbl.text = "\(username), \(profile.age!)"
            if IS_LIVZA{
                self.videoCountLbl.text = "\(profile.videos_count!)"
            }
            
        }else{
            self.profileId = profile.user_id!
            if profile.follow == "false"{
                self.followBtn.config(color: .white, font: medium, align: .center, title: "follow")
                self.followBtn.backgroundColor = PRIMARY_COLOR
            }else{
                self.followBtn.config(color: TEXT_PRIMARY_COLOR, font: medium, align: .center, title: "unfollow")
                self.followBtn.backgroundColor = .white
            }
            self.followStatus = profile.follow!
            
            if profile.privacy_age! {
                self.nameLbl.text = "\(username)"
            }else{
                self.nameLbl.text = "\(username), \(profile.age!)"
            }
            
            if profile.privacy_contactme! || self.viewType == "livepage" {
                self.videocall_btn.isHidden = true
                self.msgBtn.isHidden = true
                self.followBtn.frame.origin.y = 20
            }else{
                self.videocall_btn.isHidden = false
                self.msgBtn.isHidden = false
                self.followBtn.frame.origin.y = self.videocall_btn.frame.origin.y+self.videocall_btn.frame.size.height+20
            }
            
        }
        if IS_LIVZA{
            self.videoCountLbl.text = "\(profile.videos_count!)"
        }
        
        
        let imgURL = URL.init(string: PROFILE_IMAGE_URL+self.imageName)
        self.profilePic.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        
        if profile.gender == "male"{
            genderIcon.image = UIImage(named: "male_sel")
        }else{
            genderIcon.image = UIImage(named: "female_sel")
        }
        self.genderIcon.isHidden = false
        let nameWidth = (self.nameLbl.intrinsicContentSize.width/2)+10
        print("setnameLbl 2")
        self.nameLbl.frame = CGRect.init(x:(FULL_WIDTH/2)-nameWidth , y: self.profilePic.frame.size.height+self.profilePic.frame.origin.y+10, width: self.nameLbl.intrinsicContentSize.width, height: 35)
        self.genderIcon.frame = CGRect.init(x:self.nameLbl.frame.size.width+self.nameLbl.frame.origin.x+10 , y: self.profilePic.frame.size.height+self.profilePic.frame.origin.y+17.5, width: 20, height: 20)
        
        /*
         let nameWidth = (self.nameLbl.intrinsicContentSize.width/2)+10
         self.nameLbl.frame = CGRect.init(x:(FULL_WIDTH/2)-nameWidth , y: self.profilePic.frame.size.height+self.profilePic.frame.origin.y+10, width: self.nameLbl.intrinsicContentSize.width, height: 35)
         self.genderIcon.frame = CGRect.init(x:self.nameLbl.frame.size.width+self.nameLbl.frame.origin.x+10 , y: self.profilePic.frame.size.height+self.profilePic.frame.origin.y+17.5, width: 20, height: 20)
         */
        
        
        if profile.premium_member == "true"{
            self.crownIcon.isHidden = false
        }else{
            self.crownIcon.isHidden = true
        }
        self.followBtn.isHidden = false
        
    }
    //config banner view
    func configAds()  {
        
        bannerView.adUnitID = Utility.shared.adUnitId
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    //banner view delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    @IBAction func settingBtnTapped(_ sender: Any) {
        let settingObj = MainSettingPage()
        self.navigationController?.pushViewController(settingObj, animated: true)
        
    }
    @IBAction func editBtnTapped(_ sender: Any) {
        let editObj = EditProfile()
        editObj.changesDone = {
            self.setProfileDetails()
        }
        self.navigationController?.pushViewController(editObj, animated: true)
    }
    
    @IBAction func followersBtnTapped(_ sender: Any) {
        if self.isLoadData {
            
            if viewType == "livepage" {
                self.statusAlert(msg: "another_live")
            }else{
                let follow = ContainerPage()
                follow.viewType = "2"
                follow.profileID = self.profileId
                self.navigationController?.pushViewController(follow, animated: true)
            }
        }
    }
    
    @IBAction func followingBtnTapped(_ sender: Any) {
        if self.isLoadData {
            if viewType == "livepage" {
                self.statusAlert(msg: "another_live")
            }else{
                let follow = ContainerPage()
                follow.viewType = "3"
                follow.profileID = self.profileId
                self.navigationController?.pushViewController(follow, animated: true)
            }
        }
    }
    
    
    @IBAction func picViewTapped(_ sender: Any){
        if self.isLoadData {
            let viewObj = ImageViewer()
            if self.profileId == UserModel.shared.userId{
                viewObj.viewType = "2"
            }else{
                viewObj.viewType = self.viewType
            }
            viewObj.modalPresentationStyle = .custom
            viewObj.transitioningDelegate = self.transition
            viewObj.imgName = self.imageName
            viewObj.imageUploaded = {
                self.setProfileDetails()
            }
            self.present(viewObj, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func videoBtnTapped(_ sender: Any) {
        
        let recentObj = UserVideoListPage()
        recentObj.backImgURL = ""
        if viewType == "ownProfile" {
            recentObj.userName = EMPTY_STRING as String
        }else{
            recentObj.userName = username
        }
        recentObj.profileID = self.profileId as String
        self.navigationController?.pushViewController(recentObj, animated: true)
    }
    
    @IBAction func gemBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            
            if viewType == "livepage" {
                self.statusAlert(msg: "another_live")
            }else{
                let gemObj =  GemListPage()
                self.navigationController?.pushViewController(gemObj, animated: true)
            }
        }
    }
    
    @IBAction func giftBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            
            if viewType == "livepage" {
                self.statusAlert(msg: "another_live")
            }else{
                if self.giftCountLbl.text == "0"{
                    let alert = AlertPopup()
                    alert.viewType = "no_gift"
                    alert.modalPresentationStyle = .overCurrentContext
                    alert.modalTransitionStyle = .crossDissolve
                    alert.dismissed = {
                        self.dismiss(animated: true, completion: nil)
                    }
                    self.present(alert, animated: true, completion: nil)
                }else{
                    let giftObj =  GiftPage()
                    giftObj.updateProfile = {
                        self.setProfileDetails()
                    }
                    self.navigationController?.pushViewController(giftObj, animated: true)
                }
            }
        }
    }
    
    @IBAction func renewalBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            if Utility.shared.isConnectedToNetwork(){
                OfferPresentingViewController.RemoteConfiguredPlansViewController.requestProductsAndShow(from: self)
            }
        }
        
    }
    
    @IBAction func msgBtnTapped(_ sender: Any) {
        if self.isLoadData {
            
            let msgDetail = MessageDetailPage()
            msgDetail.contactID = profileId
            msgDetail.contactName = self.partner.name!
            msgDetail.contactImg = self.partner.user_image!
            if self.partner.blocked_by_me == "true" {
                msgDetail.isBlocked = true
            }else{
                msgDetail.isBlocked = false
            }
            msgDetail.newChanges = {
                self.refreshProfileInfo()
            }
            self.navigationController?.pushViewController(msgDetail, animated: true)
        }
    }
    @IBAction func videoCallBtnTapped(_ sender: Any) {
        if self.isLoadData {
            let defaultDict:NSDictionary = UserModel.shared.getDefaults()!
            let call_charge = defaultDict.value(forKey: "video_calls") as! Int
            if call_charge <= UserModel.shared.gemCount{
                DispatchQueue.main.async {
                    let videoObj = VideoCallPage()
                    videoObj.user_name = self.partner.name!
                    videoObj.user_img = self.partner.user_image!
                    videoObj.sender = true
                    videoObj.platform = "ios"
                    videoObj.user_id = self.partner.user_id!
                    videoObj.modalPresentationStyle = .fullScreen
                    self.navigationController?.present(videoObj, animated: true, completion: nil)
                    Event.VideoCall.log()
                }
            }else{
                self.statusAlert(msg: "not_enough_gems")
            }
        }
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        if self.dismissType == "dismiss" {
            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func followBtnTapped(_ sender: Any) {
        if self.isLoadData {
            if isFollowReq{
                self.statusAlert(msg: "already_request")
            }else{
                self.isFollowReq = true
                var updateStatus = String()
                if self.followStatus == "false" {
                    updateStatus = "Follow"
                    Event.FollowFriend.log()
                }else{
                    updateStatus = "Unfollow"
                    Event.UnfollowFriend.log()
                }
                let updateOBj = BaseWebService()
                let request = NSMutableDictionary()
                request.setValue(self.profileId, forKey: "user_id")
                request.setValue(UserModel.shared.userId, forKey: "follower_id")
                request.setValue(updateStatus, forKey: "type")
                updateOBj.baseService(subURl: FOLLOW_API, params:  request as? Parameters, onSuccess: {response in
                    let dict = response.result.value as? NSDictionary
                    let status = dict?.value(forKey: "status") as! String
                    if status == "true"{
                        if updateStatus == "Follow"{
                            self.followStatus = "true"
                            self.followBtn.config(color: TEXT_PRIMARY_COLOR, font: medium, align: .center, title: "unfollow")
                            self.followBtn.backgroundColor = .white
                            UserDefaults.standard.set("Unfollow", forKey: "follow_instant_status")
                        }else{
                            self.followStatus = "false"
                            self.followBtn.config(color: .white, font: medium, align: .center, title: "follow")
                            self.followBtn.backgroundColor = PRIMARY_COLOR
                            
                            UserDefaults.standard.set("Follow", forKey: "follow_instant_status")
                        }
                        self.refreshProfileInfo()
                        self.isFollowReq = false
                    }
                })
            }
        }
    }
    
    //from recent
    func setOtherUserInfo()  {
        self.imageName = userModal.user_image!
        if userModal.gender == "male"{
            genderIcon.image = UIImage(named: "male_sel")
        }else{
            genderIcon.image = UIImage(named: "female_sel")
        }
        if (userModal.name?.count)! > 25{
            username  = "\(String((userModal.name?.prefix(25))!)).."
            
        }else{
            username  = userModal.name!
        }
        if userModal.privacy_age! {
            self.nameLbl.text = "\(username)"
        }else{
            self.nameLbl.text = "\(username), \(userModal.age!)"
        }
        
        let nameWidth = (self.nameLbl.intrinsicContentSize.width/2)+10
        
        self.nameLbl.frame = CGRect.init(x:(FULL_WIDTH/2)-nameWidth , y: self.profilePic.frame.size.height+self.profilePic.frame.origin.y+10, width: self.nameLbl.intrinsicContentSize.width, height: 35)
        self.genderIcon.frame = CGRect.init(x:self.nameLbl.frame.size.width+self.nameLbl.frame.origin.x+10 , y: self.profilePic.frame.size.height+self.profilePic.frame.origin.y+17.5, width: 20, height: 20)
        
        let imgURL = URL.init(string: PROFILE_IMAGE_URL+userModal.user_image!)
        self.profilePic.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        self.locationLbl.text = userModal.location
        self.profileId = userModal.user_id!
        
        self.followBtn.isHidden = false
        self.followingCountLbl.text = "-"
        self.followersCountLbl.text = "-"
        self.followBtn.isHidden = true
        if userModal.privacy_contactme! {
            self.videocall_btn.isHidden = true
            self.msgBtn.isHidden = true
            self.followBtn.frame.origin.y = 20
        }
        self.refreshProfileInfo()
    }
    func gotStoredInfo(type: String, msg: Messages?) {
        
    }
    func gotSocketInfo(type: String, dict: NSDictionary?) {
        
    }
    //    func selectedLanguage(language: String) {
    //        DispatchQueue.main.async {
    //            //setup language
    //            UserModel.shared.setAppLanguage(Language: language)
    //            Utility.shared.configLanguage()
    //            self.initalSetup()
    //            self.changeToRTL()
    //
    //        }
    //    }
}

