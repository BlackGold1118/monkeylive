//
//  GiftPage.swift
//  Randoo
//
//  Created by HTS-Product on 31/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class GiftPage: UIViewController {
    
    @IBOutlet var gemDetailLbl: UILabel!
    @IBOutlet var convertBtn: UIButton!
    @IBOutlet var gemBtn: UIButton!
    @IBOutlet var gemSelBtn: UIButton!
    @IBOutlet var moneySelBtn: UIButton!
    @IBOutlet var moneyBtn: UIButton!
    @IBOutlet var moneySelView: UIView!
    @IBOutlet var gemSelView: UIView!
    @IBOutlet var bgImgView: UIImageView!
    @IBOutlet var loader: UIActivityIndicatorView!
    
    var select_type = String()
    var updateProfile: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        // Do any additional setup after loading the view.
    }
     override func viewWillAppear(_ animated: Bool) {
        self.changeToRTL()
        self.initialSetup()
    }
    
    func changeToRTL(){
        
        if UserModel.shared.language == "Arabic"{
            self.gemBtn.frame = CGRect.init(x: self.gemSelView.frame.origin.x+self.gemSelView.frame.size.width+20, y: 345, width: 115, height: 45)
            self.moneyBtn.frame = CGRect.init(x: self.moneySelView.frame.origin.x+self.moneySelView.frame.size.width+20, y: 345, width: 115, height: 45)
        }
        else{
            self.gemBtn.frame = CGRect.init(x: self.gemSelView.frame.origin.x+self.gemSelView.frame.size.width-20, y: 345, width: 115, height: 45)
            self.moneyBtn.frame = CGRect.init(x: self.moneySelView.frame.origin.x+self.moneySelView.frame.size.width-20, y: 345, width: 115, height: 45)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        let defaultDict = UserModel.shared.getDefaults()
        let moneyEnable = defaultDict?.value(forKey: "show_money_conversion") as! NSNumber
        if moneyEnable == 0{
            select_type = "gem"
            self.moneySelView.isHidden = true
            self.gemSelView.isHidden = true
            self.gemBtn.isHidden = true
            self.moneyBtn.isHidden = true
            self.convertBtn.config(color: TEXT_PRIMARY_COLOR , font: mediumReg, align: .center, title: "convert_gem")
            self.convertBtn.frame = CGRect.init(x: 40, y: self.gemDetailLbl.frame.origin.y+self.gemDetailLbl.frame.size.height+15, width: FULL_WIDTH-80, height:50 )
        }
    }
    func initialSetup()  {
        self.fd_prefersNavigationBarHidden = true
        self.gemDetailLbl.config(color: .white, font: high, align: .center, text: "")
        self.gemBtn.config(color: .white, font: medium, align: .left, title: "gems")
        self.moneyBtn.config(color: .white, font: medium, align: .left, title: "money")
        moneyBtn.setTitle("     \(Utility.language.value(forKey: "money") as! String)", for: .normal)
        gemBtn.setTitle("       \(Utility.language.value(forKey: "gems") as! String)", for: .normal)
        self.selected(type: "gem")
        self.convertBtn.cornerRoundRadius()
        
        do{
            let jsonDecoder = JSONDecoder()
            let model = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
            let havStr = Utility.language.value(forKey: "you_have") as! String
            let giftStr = Utility.language.value(forKey: "gifts_worth") as! String
            let gemsStr = Utility.language.value(forKey: "gems") as! String
            
            self.gemDetailLbl.text = "\(havStr) \(NSNumber.init(value: model.available_gifts!).stringValue) \(giftStr) (\(NSNumber.init(value: model.gift_earnings!).stringValue) \(gemsStr))"
        }catch{
            
        }
        if IS_LIVZA {
            self.bgImgView.image = #imageLiteral(resourceName: "live_chat_bg")
        }else{
            self.bgImgView.image = #imageLiteral(resourceName: "chat_bg")
        }
        
    }
    
    func selected(type:String)  {
        self.moneySelBtn.cornerRoundRadius()
        self.gemSelBtn.cornerRoundRadius()
        self.moneySelView.cornerViewRadius()
        self.gemSelView.cornerViewRadius()
        self.moneySelView.border(color: .white)
        self.gemSelView.border(color: .white)
        
        if type == "gem" {
            select_type = "gem"
            self.convertBtn.config(color: TEXT_PRIMARY_COLOR , font: mediumReg, align: .center, title: "convert_gem")
            self.gemSelBtn.backgroundColor = .white
            self.gemSelBtn.setBorder(color: .white)
            self.moneySelBtn.backgroundColor = .clear
            self.moneySelBtn.isHidden = true
            self.gemSelBtn.isHidden = false
            
        }else{
            select_type = "gift"
            self.convertBtn.config(color: TEXT_PRIMARY_COLOR , font: mediumReg, align: .center, title: "withdraw_money")
            self.moneySelBtn.backgroundColor = .white
            self.moneySelBtn.setBorder(color: .white)
            self.gemSelBtn.backgroundColor = .clear
            self.moneySelBtn.isHidden = false
            self.gemSelBtn.isHidden = true
        }
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func convertBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            
            if select_type == "gem" {
                let otherAlert = UIAlertController(title: nil, message: Utility.language.value(forKey: "want_to_convert_gem") as? String, preferredStyle: UIAlertController.Style.alert)
                let dismiss = UIAlertAction(title: Utility.language.value(forKey: "cancel") as? String, style: UIAlertAction.Style.cancel, handler: nil)
                otherAlert.addAction(dismiss)
                
                let okayBtn = UIAlertAction(title: Utility.language.value(forKey: "ok") as? String, style: UIAlertAction.Style.default, handler:convertGem)
                otherAlert.addAction(okayBtn)
                present(otherAlert, animated: true, completion: nil)
            }else{
                self.statusAlert(msg: "payment_not_available")
            }
        }
    }
    
    func convertGem(alert: UIAlertAction)  {
        let updateOBj = BaseWebService()
        let request = NSMutableDictionary()
        request.setValue(UserModel.shared.userId, forKey: "user_id")
        request.setValue("gems", forKey: "type")
        updateOBj.baseService(subURl: GIFT_TO_GEM_API, params:  request as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.updateProfile()
                let alert = AlertPopup()
                alert.viewType = "gems_updated"
                alert.modalPresentationStyle = .overCurrentContext
                alert.modalTransitionStyle = .crossDissolve
                alert.dismissed = {                    
                    self.dismiss(animated: true, completion: nil)
                    self.updateProfile!()
                    self.navigationController?.popViewController(animated: true)
                }
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func gemBtnTapped(_ sender: Any) {
        self.selected(type: "gem")
    }
    @IBAction func moneyBtnTapped(_ sender: Any) {
        self.selected(type: "money")
        
    }
    
}
