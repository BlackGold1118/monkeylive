//
//  stickerCell.swift
//  Randoo
//
//  Created by HTS-Product on 16/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import Lottie

class stickerCell: UICollectionViewCell {

    @IBOutlet var stickerView: AnimationView!
    @IBOutlet var giftView: UIView!
    @IBOutlet var giftIcon: UIImageView!
    @IBOutlet var gemCount: UILabel!
    @IBOutlet var gemIcon: UIImageView!
    @IBOutlet var stickerBtn: UIButton!
    @IBOutlet var giftBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.gemCount.config(color: .white, font: lite, align: .left, text: "")
    }
    
    func configCell(name:String)  {
        self.stickerView.isHidden = false
        self.giftView.isHidden = true
        self.stickerView.removeFromSuperview()
            let stickerSize = FULL_WIDTH/4
            self.stickerView = AnimationView()
            self.stickerView.clearsContextBeforeDrawing = true
            self.stickerView = AnimationView(name: name)
            self.stickerView.frame = CGRect.init(x: 10, y: 10, width: stickerSize-20, height: stickerSize-20)

        self.stickerView.loopMode = .loop
            //        stickerView.play{ (finished) in
            //            // Do Something
            //        }
        self.contentView.addSubview( self.stickerView)
        self.stickerBtn.frame = CGRect.init(x: 10, y: 10, width: stickerSize-20, height: stickerSize-20)
        self.contentView.bringSubviewToFront(self.stickerBtn)

    }
    func configGift(dict:NSDictionary)  {
        self.stickerView.isHidden = true
        self.giftView.isHidden = false
        let iconName = dict.value(forKey: "gft_icon") as! String
        let imgURL = URL.init(string: GIFT_IMAGE_URL+iconName)
        self.giftIcon.sd_setImage(with: imgURL, placeholderImage: UIImage.init(named: "gift_icon"))
        if UserModel.shared.isPremium{
            let gemPrime = dict.value(forKey: "gft_gems_prime") as! NSNumber
            self.gemCount.text = gemPrime.intValue.kmFormatted
        }else{
            let gem = dict.value(forKey: "gft_gems") as! NSNumber
            self.gemCount.text = gem.intValue.kmFormatted
        }
        let stickerSize = FULL_WIDTH/4

        self.giftBtn.frame = CGRect.init(x: 10, y: 10, width: stickerSize-20, height: stickerSize-20)
        self.contentView.bringSubviewToFront(self.giftBtn)

    }

}
