//
//  LocationListPage.swift
//  Randoo
//
//  Created by HTS-Product on 08/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import GoogleMobileAds

class LocationListPage: UIViewController,UITableViewDelegate,UITableViewDataSource,GADBannerViewDelegate {
 

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var locationTableView: UITableView!
    @IBOutlet var bannerView: GADBannerView!

    var changesDone: (() -> Void)?

    var locationArray = NSMutableArray()
    var selectedLocation = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialSetup()
        self.changeToRTL()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if selectedLocation.count == 0 {
            UserDefaults.standard.setValue(locationArray, forKey: "location")
        }else{
            UserDefaults.standard.setValue(selectedLocation, forKey: "location")
        }
    }
    
    func changeToRTL(){
    if UserModel.shared.language == "Arabic"{
        self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
        self.titleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
        self.titleLbl.textAlignment = .center
        }
    else{
        self.view.transform = .identity
        self.titleLbl.transform = .identity
        self.titleLbl.textAlignment = .center
        }
    }

    //MARK: Configure initial design
    func initialSetup() {
        self.locationArray.removeAllObjects()
        self.fd_prefersNavigationBarHidden = true
        self.titleLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text: "location")
        locationTableView.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
        let defaultDict:NSDictionary = UserModel.shared.getDefaults()!
        locationArray.add(Utility.language.value(forKey: "select_all") as Any)
        locationArray.addObjects(from: defaultDict.value(forKey: "locations") as! [Any])
        if Utility.shared.isAdsEnable() {
            self.configAds()
        }else{
            self.bannerView.isHidden = true
        }
        var previousArray = NSArray()
        previousArray = (UserDefaults.standard.value(forKeyPath: "location") as! NSArray).mutableCopy() as! NSArray
        selectedLocation.addObjects(from:previousArray as! [Any])
        if selectedLocation.count == locationArray.count-1 {
            self.selectedLocation.removeAllObjects()
            self.selectedLocation = locationArray.mutableCopy() as! NSMutableArray
        }
        locationTableView.reloadData()
    }
    
    //config banner view
    func configAds()  {
        bannerView.adUnitID = Utility.shared.adUnitId
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    //banner view delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "location")
        if selectedLocation.count == 0 {
                   UserDefaults.standard.setValue(locationArray, forKey: "location")
               }else{
                   UserDefaults.standard.setValue(selectedLocation, forKey: "location")
               }
        
        if IS_LIVZA {
            self.changesDone!()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath) as! LocationCell
        let location:NSString = locationArray.object(at: indexPath.row) as! NSString
        cell.locationLbl.text = location as String
        if selectedLocation.contains(location){
            cell.tickView.backgroundColor = PRIMARY_COLOR
        }else{
            cell.tickView.backgroundColor = .clear
        }
        if UserModel.shared.language == "Arabic"{
        
            cell.locationLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell.locationLbl.textAlignment = .right
            cell.selectionImg.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else{
            cell.locationLbl.transform = .identity
            cell.locationLbl.textAlignment = .left
            cell.selectionImg.transform = .identity
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let location:NSString = locationArray.object(at: indexPath.row) as! NSString
        if !IS_LIVZA{
            UserDefaults.standard.setValue("1", forKey: "filter_applied")
        }
        if  location.isEqual(to: Utility.language.value(forKey: "select_all") as! String) {
            if selectedLocation.contains(location){
                selectedLocation.removeAllObjects()
            }else{
                selectedLocation.removeAllObjects()
                selectedLocation = locationArray.mutableCopy() as! NSMutableArray
            }
        }else{
            // check all countries selected or not
            if selectedLocation.count == locationArray.count{
                selectedLocation.remove(Utility.language.value(forKey: "select_all") as! String)
            }
            //check if already added
            if selectedLocation.contains(location){
                selectedLocation.remove(location)
            }else{
                selectedLocation.add(location)
            }
            //check all sub countries are selected
            if selectedLocation.count == locationArray.count-1{
                selectedLocation.removeAllObjects()
                selectedLocation = locationArray.mutableCopy() as! NSMutableArray
            }
        }
        self.locationTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
