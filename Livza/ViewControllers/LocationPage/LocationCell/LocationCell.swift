//
//  LocationCell.swift
//  Randoo
//
//  Created by HTS-Product on 08/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {

    @IBOutlet weak var separatorLbl: UILabel!
    
    @IBOutlet weak var locationLbl: UILabel!
    
    @IBOutlet weak var selectionImg: UIImageView!
    
    @IBOutlet weak var tickView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.tickView.cornerViewRadius()
        self.tickView.setViewBorder(color: PRIMARY_COLOR)
        self.locationLbl.config(color: TEXT_PRIMARY_COLOR, font: medium, align: .left, text: "")
        self.separatorLbl.backgroundColor = LINE_COLOR

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
