//
//  TabBarPage.swift
//  Randoo
//
//  Created by HTS-Product on 08/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import FDFullscreenPopGesture

class TabBarPage: SVCSwipeViewController,SVCTabBarMoveDelegate {
    
    let homeObj = HomePage()
    let messageObj = MessagePage()
    let profileObj = ProfilePage()
    let listObj = LiveListPage()
    let searchObj = SearchListPage()
    let addObj = GoLiveStartPage()
    
    let defaultTabBar = SVCTabBar()
    let firstItem = SVCTabItem(type: .system)
    let secondItem = SVCTabItem(type: .system)
    let thirdItem = SVCTabItem(type: .system)
    let fourthItem = SVCTabItem(type: .system)
    let fifthItem = SVCTabItem(type: .system)
    
    var darkMode = Bool()
    let transparent_color = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        darkMode = true
        profileObj.viewType = "ownProfile"
//        UserModel.shared.updateProfile()
        self.addViewControllers()
        self.view.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.view.isUserInteractionEnabled = true
            
        }
    }
    
    override func viewWillLayoutSubviews() {
        self.navigationController?.isNavigationBarHidden = true
        
        
    }
    
    private func addViewControllers() {
        if IS_LIVZA {
            viewControllers = [listObj,searchObj,addObj,messageObj,profileObj]
        }else{
            viewControllers = [homeObj,messageObj,profileObj]
        }
        self.addTabBarItems()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if darkMode{
            return .lightContent
        }else{
            return .default
        }
    }
    
    //add number tabbar items
    private func addTabBarItems() {
        tabBarType = .bottom
        //tabbar animation
        firstItem.imageViewAnimators = [SVCTransitionAnimator(transitionOptions: .transitionFlipFromTop)]
        secondItem.imageViewAnimators = [SVCTransitionAnimator(transitionOptions: .transitionFlipFromRight)]
        thirdItem.imageViewAnimators = [SVCTransitionAnimator(transitionOptions: .transitionFlipFromTop)]
        fourthItem.imageViewAnimators = [SVCTransitionAnimator(transitionOptions: .transitionFlipFromLeft)]
        fifthItem.imageViewAnimators = [SVCTransitionAnimator(transitionOptions: .transitionFlipFromBottom)]
        
        if IS_LIVZA {
            self.addLivzaIcons(type: "blacktab")
            defaultTabBar.items = [firstItem, secondItem, thirdItem,fourthItem,fifthItem]
        }else{
            self.addRandouIcons(type: "blacktab")
            defaultTabBar.items = [firstItem, secondItem, thirdItem]
        }
        tabBar?.backgroundColor = transparent_color
        // inject tab bar
        tabBar = defaultTabBar
        tabBar?.moveDelegate = self
        tabBar?.height = 200.0
        tabBar?.layer.borderWidth = 0.5
        tabBar?.layer.borderColor = transparent_color.cgColor
        tabBar?.clipsToBounds = true
    }
    
    
    
    //MARK: tab bar delegate
    func move(toIndex: Int, fromIndex: Int, percent: CGFloat, isTap: Bool, duration: TimeInterval) {
        darkMode = true
        if percent == 1.0 {
            if IS_LIVZA{
                if toIndex==0  || toIndex==2{
                    self.addLivzaIcons(type: "blacktab")
                    tabBar?.backgroundColor = transparent_color
                }else if toIndex==1 || toIndex==3 || toIndex==4 {
                    self.addLivzaIcons(type: "whitetab")
                    tabBar?.backgroundColor = .white
                }
                if toIndex == 3{
                    darkMode = false
                }
            }else{
                if toIndex==0{
                    self.addRandouIcons(type: "blacktab")
                    tabBar?.backgroundColor = transparent_color
                }else if toIndex==1 || toIndex==2{
                    self.addRandouIcons(type: "whitetab")
                    tabBar?.backgroundColor = .white
                }
                if toIndex == 1{
                    darkMode = false
                }
            }
        }
        setNeedsStatusBarAppearanceUpdate()
    }
    
    //add livza tab icons
    func addLivzaIcons(type:String)  {
        //selected
        firstItem.setImage(UIImage(named: "tab_list_sel")?.withRenderingMode(.alwaysOriginal), for: .selected)
        secondItem.setImage(UIImage(named: "tab_search_sel")?.withRenderingMode(.alwaysOriginal), for: .selected)
        thirdItem.setImage(UIImage(named: "tab_add_sel")?.withRenderingMode(.alwaysOriginal), for: .selected)
        fourthItem.setImage(UIImage(named: "tab_msg_sel")?.withRenderingMode(.alwaysOriginal), for: .selected)
        fifthItem.setImage(UIImage(named: "tab_profile_sel")?.withRenderingMode(.alwaysOriginal), for: .selected)
        //unselected
        thirdItem.setImage(UIImage(named: "tab_add_unsel")?.withRenderingMode(.alwaysOriginal), for: .normal)
        if type == "blacktab" {
            firstItem.setImage(UIImage(named: "tab_list_unsel")?.withRenderingMode(.alwaysOriginal), for: .normal)
            secondItem.setImage(UIImage(named: "tab_search_unsel")?.withRenderingMode(.alwaysOriginal), for: .normal)
            fourthItem.setImage(UIImage(named: "tab_msg_unsel")?.withRenderingMode(.alwaysOriginal), for: .normal)
            fifthItem.setImage(UIImage(named: "tab_profile_unsel")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }else{
            firstItem.setImage(UIImage(named: "tab_list_gray")?.withRenderingMode(.alwaysOriginal), for: .normal)
            secondItem.setImage(UIImage(named: "tab_search_gray")?.withRenderingMode(.alwaysOriginal), for: .normal)
            fourthItem.setImage(UIImage(named: "tab_msg_gray")?.withRenderingMode(.alwaysOriginal), for: .normal)
            fifthItem.setImage(UIImage(named: "tab_profile_gray")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
    }
    //add randou tab icons
    func addRandouIcons(type:String)  {
        //selected
        firstItem.setImage(UIImage(named: "home_sel")?.withRenderingMode(.alwaysOriginal), for: .selected)
        secondItem.setImage(UIImage(named: "msg_sel")?.withRenderingMode(.alwaysOriginal), for: .selected)
        thirdItem.setImage(UIImage(named: "profile_sel")?.withRenderingMode(.alwaysOriginal), for: .selected)
        //unselected
        if type == "blacktab" {
            firstItem.setImage(UIImage(named: "home_unsel")?.withRenderingMode(.alwaysOriginal), for: .normal)
            secondItem.setImage(UIImage(named: "msg_unsel")?.withRenderingMode(.alwaysOriginal), for: .normal)
            thirdItem.setImage(UIImage(named: "profile_unsel")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }else{
            firstItem.setImage(UIImage(named: "home_gray")?.withRenderingMode(.alwaysOriginal), for: .normal)
            secondItem.setImage(UIImage(named: "msg_gray")?.withRenderingMode(.alwaysOriginal), for: .normal)
            thirdItem.setImage(UIImage(named: "profile_gray")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
    }
    
}

