//
//  FilterPage.swift
//  Randoo
//
//  Created by HTS-Product on 19/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import SwiftyJSON

class FilterPage: UIViewController,RangeSeekSliderDelegate {
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterLbl: UILabel!
    @IBOutlet weak var filterMaleBtn: UIButton!
    @IBOutlet weak var filterFemaleBtn: UIButton!
    @IBOutlet weak var filterGenderBothBtn: UIButton!
    @IBOutlet weak var filterLocationBtn: UIButton!
    @IBOutlet weak var ageLbl: UILabel!
    @IBOutlet weak var ageRangeLbl: UILabel!
    @IBOutlet weak var rangeView: RangeSeekSlider!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet var gemCountLbl: UILabel!
    
    var didDismiss: ((_ type:String) ->())?
    var defaultDict = NSDictionary()
    var gender = String()
    var minAge = String()
    var maxAge = String()
    var filterApplied = String()
    var gemSubLimit = NSString()
    var gemUnsubLimit = NSString()
    var isShowToast = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.configFilterView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.changeToRTL()
    }
    
    
    
    override func viewDidLayoutSubviews() {
        UIView.animate(withDuration:0.5,
                       delay: 0.0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.0,
                       options: .curveEaseIn,
                       animations: {
                        //Do all animations here
                        self.filterView.frame.origin.y = FULL_HEIGHT -  self.filterView.frame.size.height
        }, completion: {
            //Code to run after animating
            (value: Bool) in
        })
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = filterView.frame
        rectShape.position = filterView.center
        rectShape.path = UIBezierPath(roundedRect: filterView.bounds, byRoundingCorners: [ .topRight , .topLeft], cornerRadii: CGSize(width: 5, height: 5)).cgPath
        filterView.layer.mask = rectShape
        
        refreshLockedFeatures()
    }
    
    func configFilterView()  {
        defaultDict = UserModel.shared.getDefaults()!
        gemSubLimit = defaultDict.value(forKeyPath: "filter_gems.sub") as! NSString
        gemUnsubLimit = defaultDict.value(forKeyPath: "filter_gems.unsub") as! NSString
        
        self.filterMaleBtn.cornerRoundRadius()
        self.filterFemaleBtn.cornerRoundRadius()
        self.filterGenderBothBtn.cornerRoundRadius()
        self.filterLocationBtn.cornerRoundRadius()
        
        gender = UserDefaults.standard.value(forKeyPath: "gender") as! String
        minAge =  UserDefaults.standard.value(forKeyPath: "min_age") as! String
        maxAge =  UserDefaults.standard.value(forKeyPath: "max_age") as! String
        filterApplied =  UserDefaults.standard.value(forKeyPath: "filter_applied") as! String
        isShowToast = false
        if gender == "male" {
            self.selectBtn(btn: self.filterMaleBtn)
        }else if gender == "female"{
            self.selectBtn(btn: self.filterFemaleBtn)
        }else{
            self.selectBtn(btn: self.filterGenderBothBtn)
        }
        filterApplied = "0"
        self.gemCountLbl.config(color: .white, font: average, align: .left, text: "")
        self.filterLbl.config(color: .white, font: average, align: .left, text: "filter")
        self.ageLbl.config(color: .white, font: lite, align: .left, text: "age")
        self.ageRangeLbl.config(color: .white, font: lite, align: .right, text: "")
        self.ageRangeLbl.text = "\(minAge)-\(maxAge)"
        self.rangeView.minValue = 18
        self.rangeView.maxValue = 99
        self.rangeView.minDistance = 10
        self.rangeView.delegate = self
        self.rangeView.hideLabels = true
        self.rangeView.selectedMinValue = CGFloat((minAge as NSString).floatValue)
        self.rangeView.selectedMaxValue = CGFloat((maxAge as NSString).floatValue)
        self.rangeView.lineHeight = 2.0
        self.rangeView.handleColor = .white
        self.rangeView.colorBetweenHandles = PRIMARY_COLOR
        if UserModel.shared.isPremium{
            self.gemCountLbl.text = gemSubLimit as String
        }else{
            self.gemCountLbl.text = gemUnsubLimit as String
        }
        //tap to dismiss
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        self.bgView.addGestureRecognizer(tap)
    }
    
    private var lockImageViews = [UIImageView]()
    func refreshLockedFeatures() {
        lockImageViews.forEach { $0.removeFromSuperview() }
        lockImageViews.removeAll()
        
        guard !UserModel.shared.isPremium else { return }
        
        ageLbl.sizeToFit()
        let rect = ageLbl.bounds
        let referenceFrame = rect.offsetBy(dx: rect.width, dy: 0).insetBy(dx: 0, dy: 2)
        let referenceSize = referenceFrame.size
        
        let lockImage = UIImage(named: "padlock")
        func createImvw() -> UIImageView {
            let imvw = UIImageView(image: lockImage)
            imvw.contentMode = .scaleAspectFit
            imvw.alpha = 0.6
            lockImageViews.append(imvw)
            return imvw
        }
        
        if !filterAvailability(type: "age") {
            let imvw = createImvw()
            imvw.frame = referenceFrame
            ageLbl.addSubview(imvw)
        }
        
        func blockButton(_ button: UIButton) {
            let imvw = createImvw()
            imvw.frame.origin = CGPoint(x: button.frame.maxX - 18, y: button.frame.origin.y - 5)
            imvw.frame.size = referenceSize
            button.superview?.addSubview(imvw)
        }
        
        if !filterAvailability(type: "gender") {
            blockButton(filterMaleBtn)
            blockButton(filterFemaleBtn)
        }
        
        if !filterAvailability(type: "location") {
            blockButton(filterLocationBtn)
        }
    }
    
    func selectBtn(btn:UIButton)  {
        
        filterApplied = "1"
        self.unselectAll()
        if isShowToast{
            self.filterView.toast(alert: gender)
        }
        UIView.animate(withDuration: 0.2,
                       animations: {
                        btn.backgroundColor = PRIMARY_COLOR
                        btn.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },completion: { _ in
            UIView.animate(withDuration: 0.2) {
                btn.transform = CGAffineTransform.identity
            }
        })
    }
    
    func unselectAll()  {
        self.filterMaleBtn.backgroundColor = CIRCLE_BG_COLOR
        self.filterGenderBothBtn.backgroundColor = CIRCLE_BG_COLOR
        self.filterLocationBtn.backgroundColor = CIRCLE_BG_COLOR
        self.filterFemaleBtn.backgroundColor = CIRCLE_BG_COLOR
    }
    
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
            self.view.transform = .identity
            self.filterView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.filterLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.ageLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.ageRangeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.rangeView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.gemCountLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.filterMaleBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.filterFemaleBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.filterGenderBothBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.filterLocationBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.gemCountLbl.textAlignment = .right
            self.ageLbl.textAlignment = .right
            self.ageRangeLbl.textAlignment = .left
            self.filterLbl.textAlignment = .right
            
            
        }else{
            self.gemCountLbl.textAlignment = .left
            self.ageLbl.textAlignment = .left
            self.ageRangeLbl.textAlignment = .right
            self.filterLbl.textAlignment = .left
            self.view.transform = .identity
            self.filterView.transform = .identity
            self.filterLbl.transform = .identity
            self.ageLbl.transform = .identity
            self.ageRangeLbl.transform = .identity
            self.rangeView.transform = .identity
            self.gemCountLbl.transform = .identity
            self.filterMaleBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.filterFemaleBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.filterGenderBothBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.filterLocationBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    @IBAction func maleBtnTapped(_ sender: Any) {
        
        if UserModel.shared.isPremium {
            if UserModel.shared.gemCount >= gemSubLimit.integerValue{ //check premimum member gem limit
                gender = "male"
                isShowToast = true
                Event.MaleFilterApplied.log()
                self.selectBtn(btn: self.filterMaleBtn)
            }else{
                self.gemAlert()
            }
        }else{
            if UserModel.shared.gemCount >= gemUnsubLimit.integerValue{ //check non premimum member gem limit
                if self.filterAvailability(type: "gender"){
                    gender = "male"
                    isShowToast = true
                    Event.MaleFilterApplied.log()
                    self.selectBtn(btn: self.filterMaleBtn)
                }else{
                    self.goPrime()
                }
            }else{
                self.goPrime()
            }
        }
    }
    
    @IBAction func femaleBtnTapped(_ sender: Any) {
        if UserModel.shared.isPremium {
            if UserModel.shared.gemCount >= gemSubLimit.integerValue{ //check premimum member gem limit
                gender = "female"
                isShowToast = true
                Event.FemaleFilterApplied.log()
                self.selectBtn(btn: self.filterFemaleBtn)
            }else{
                self.gemAlert()
            }
        }else{
            if UserModel.shared.gemCount >= gemUnsubLimit.integerValue{ //check non premimum member gem limit
                if self.filterAvailability(type: "gender"){
                    gender = "female"
                    isShowToast = true
                    Event.FemaleFilterApplied.log()
                    self.selectBtn(btn: self.filterFemaleBtn)
                }else{
                    self.goPrime()
                }
            }else{
                self.goPrime()
            }
        }
    }
    
    @IBAction func genderBothBtnTapped(_ sender: Any) {
        if UserModel.shared.isPremium {
            if UserModel.shared.gemCount >= gemSubLimit.integerValue{ //check premimum member gem limit
                gender = "both"
                isShowToast = true
                self.selectBtn(btn: self.filterGenderBothBtn)
                filterApplied = "0"
            }else{
                self.gemAlert()
            }
        }else{
            if UserModel.shared.gemCount >= gemUnsubLimit.integerValue{ //check non premimum member gem limit
                if self.filterAvailability(type: "gender"){
                    gender = "both"
                    isShowToast = true
                    self.selectBtn(btn: self.filterGenderBothBtn)
                    filterApplied = "0"
                }else{
                    self.goPrime()
                }
            }else{
                self.goPrime()
            }
        }
    }
    @IBAction func locationBtnTapped(_ sender: Any) {
        
        if UserModel.shared.isPremium {
            if UserModel.shared.gemCount >= gemSubLimit.integerValue{ //check premimum member gem limit
                self.dismiss(animated: true, completion: nil)
                self.didDismiss!("location")
            }else{
                self.gemAlert()
            }
        }else{
            if UserModel.shared.gemCount >= gemUnsubLimit.integerValue{ //check non premimum member gem limit
                if self.filterAvailability(type: "location"){
                    self.dismiss(animated: true, completion: nil)
                    self.didDismiss!("location")
                }else{
                    self.goPrime()
                }
            }else{
                self.goPrime()
            }
        }
    }
    
    func goPrime()  {
        self.dismiss(animated: true, completion: nil)
        self.didDismiss!("prime")
    }
    
    //MARK: slider delegate
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if UserModel.shared.isPremium {
            if UserModel.shared.gemCount >= gemSubLimit.integerValue{ //check non premimum member gem limit
                minAge = "\(Int.init(minValue.rounded()))"
                maxAge = "\(Int.init(maxValue.rounded()))"
                filterApplied = "1"
                Event.AgeFilterApplied.log()
                self.ageRangeLbl.text = "\(Int.init(minValue.rounded()))-\(Int.init(maxValue.rounded()))"
            }else{
                self.gemAlert()
            }
        }else{
            
            if UserModel.shared.gemCount >= gemUnsubLimit.integerValue{ //check non premimum member gem limit
                if self.filterAvailability(type: "age"){
                    minAge = "\(Int.init(minValue.rounded()))"
                    maxAge = "\(Int.init(maxValue.rounded()))"
                    filterApplied = "1"
                    Event.AgeFilterApplied.log()
                    self.ageRangeLbl.text = "\(Int.init(minValue.rounded()))-\(Int.init(maxValue.rounded()))"
                }else{
                    self.goPrime()
                }
            }else{
                self.goPrime()
            }
        }
    }
    
    func gemAlert() {
        self.didDismiss!("gem")
        self.filterView.frame.origin.y = FULL_HEIGHT
        self.dismiss(animated: false, completion: nil)
        
    }
    
    //dismiss view
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        UIView.animate(withDuration:0.5,
                       delay: 0.0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.0,
                       options: .curveEaseIn,
                       animations: {
                        UserDefaults.standard.setValue(self.gender, forKey: "gender")
                        UserDefaults.standard.setValue(self.filterApplied, forKey: "filter_applied")
                        UserDefaults.standard.setValue(self.minAge, forKey: "min_age")
                        UserDefaults.standard.setValue(self.maxAge, forKey: "max_age")
                        self.didDismiss!("dismiss")
                        self.filterView.frame.origin.y = FULL_HEIGHT
                        self.dismiss(animated: true, completion: nil)
                        
        }, completion: {(value: Bool) in
        })
    }
    
    func filterAvailability(type:String)->Bool  {
        var filterDict = NSDictionary()
        filterDict = defaultDict.value(forKey: "filter_options") as! NSDictionary
        if type == "location"{
            let locationAvailable = filterDict.value(forKeyPath: "location.non-prime") as! String
            if locationAvailable == "1"{
                return true
            }
        }else if type == "gender"{
            let genderAvailable = filterDict.value(forKeyPath: "gender.non-prime") as! String
            if genderAvailable == "1"{
                return true
            }
        }else if type == "age"{
            let ageAvailable = filterDict.value(forKeyPath: "age.non-prime") as! String
            if ageAvailable == "1"{
                return true
            }
        }
        return false
    }
}

