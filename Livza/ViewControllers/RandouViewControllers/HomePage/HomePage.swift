//
//  HomePage.swift
//  Randoo
//
//  Created by HTS-Product on 28/01/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import AVKit
import Lottie
import JTMaterialTransition
import SwiftyJSON
import CoreTelephony
import StoreKit
import FirebaseRemoteConfig
import FirebaseAnalytics

class HomePage: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVCaptureVideoDataOutputSampleBufferDelegate,UIViewControllerTransitioningDelegate,UITextFieldDelegate {
    
    var transition: JTMaterialTransition?
    var cameraView:UIImagePickerController?
    var cameraPreview:UIImagePickerController?
    let captureSession = AVCaptureSession()
    var previewLayer:CALayer!
    var captureDevice:AVCaptureDevice!
    var layoutEnable =  Bool()
    var gender = String()
    var age = Int()
    var dobDate = Date()
    let imagePicker = UIImagePickerController()
    var profileImg:UIImage?
    let network = NetworkManager.sharedInstance
    var country_name = String()
    var freeGem = Int()
    
    @IBOutlet var logoView: UIImageView!
    @IBOutlet var logoViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var logoViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var sideView: UIView!
    @IBOutlet weak var preview: UIView!
    @IBOutlet weak var gemBtn: UIButton!
    @IBOutlet weak var recentBtn: UIButton!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var gemCountLbl: UILabel!
    @IBOutlet weak var filterStatusIcon: UIButton!
    @IBOutlet weak var infoView: ShimmeringView!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var offerView: UIView!
    let alert = BPStatusBarAlert()
    
    let primeTempArray = NSMutableArray()
    var productIDs: Array<String?> = []
    var selectedProduct = SKProduct()
    
    @OptionalUserDefault(.offerCountdown)
    private var offerCountdown: Int?
    
    @OptionalUserDefault(.lastCountdownDate)
    private var lastCountdownDate: Date?
    
    private weak var offerViewController: OfferViewController?
    private var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialSetup()
        subscribeNotifications()
        invokeTimerIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let configs = RemoteConfig.remoteConfig()
        if UserModel.shared.isFirstLaunch, configs.configValue(forKey: .show_in_first_launch).boolValue {
            UserModel.shared.isFirstLaunch = false
            
            if !UserModel.shared.isPremium {
                OfferPresentingViewController.RemoteConfiguredPlansViewController.requestProductsAndShow(from: self)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshLogo()
        appDelegate.isLive = false
        if isSocketConnected{
            self.checkNotificationRedirect()
            }
        self.navigationController?.isNavigationBarHidden = true
        self.fd_prefersNavigationBarHidden = true
        self.gemCountLbl.text = UserModel().gemCount.kmFormatted
        if !UserModel.shared.profileComplete { // all basic details updated
            let obj = RegisterInfo()
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.completedInfo = { gem_count in
                self.navigationController?.dismiss(animated:false, completion: nil)
                if gem_count != nil{
                    self.gemCountLbl.text = gem_count?.kmFormatted
                }else{
                    self.gemCountLbl.text = "0"
                }
            }
            self.navigationController?.present(obj, animated: false, completion: nil)
        }else{ // all basic details updated
            self.tabBarController?.tabBar.isHidden = false
            UserModel.shared.getTurnServers()
            UserModel.shared.updateProfile()
            Utility.shared.getAdminMsg()
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
            self.view.addGestureRecognizer(tap)
            self.infoLbl.isHidden = false
            self.infoView.isHidden = false
        }
        
        self.changeToRTL()
    }
    override func viewDidLayoutSubviews() {
   
    }
    override func viewWillDisappear(_ animated: Bool) {
        print("will disappear")
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return countryCode
        }
    }
    
    @objc private func refreshLogo() {
        logoView.image = UserModel.shared.isPremium ? UIImage(named: "app_header_logo_prime") : UIImage(named: "app_header_logo")
        logoViewWidthConstraint.constant = UserModel.shared.isPremium ? 185 : 140
        logoViewHeightConstraint.constant = UserModel.shared.isPremium ? 60 : 45
    }
    
    //initial details
    func initialSetup()  {
        offerView.alpha = 0
        let infor = CTTelephonyNetworkInfo()
        let carrier = infor.subscriberCellularProvider
        let currentLocale = NSLocale.current
        if carrier?.isoCountryCode != nil, let isoCode = carrier?.isoCountryCode, let description = currentLocale.localizedString(forRegionCode: isoCode) {
            country_name = description
        }else{
            country_name = "global"
        }
        
        UserDefaults.standard.set(APP_RTC_URL, forKey: "web_rtc_web")
        Utility.shared.setDefaultFilter()
        self.fd_prefersNavigationBarHidden = true
        self.fd_interactivePopDisabled = true
        self.imagePicker.delegate = self
        self.previewScreen()
        self.transition = JTMaterialTransition(animatedView: self.centerPoint())
        //        let entity  = Entities()
        self.transitioningDelegate = self
        //        Entities.sharedInstance.addNewChat()
        //        if !Entities.sharedInstance.checkIfUserExist(user_id: "5") {
        //
        //        }
        self.gemBtn.cornerRoundRadius()
        self.filterBtn.cornerRoundRadius()
        self.recentBtn.cornerRoundRadius()
        self.gemCountLbl.config(color: .white, font: low, align: .center, text: "")
        self.gemCountLbl.cornerRadius()
        self.gemCountLbl.text = UserModel().gemCount.kmFormatted
        self.gemCountLbl.backgroundColor = PRIMARY_COLOR
        
        self.filterStatusIcon.cornerRoundRadius()
        self.filterStatusIcon.backgroundColor = PRIMARY_COLOR
        self.filterBtn.backgroundColor = CIRCLE_BG_COLOR
        self.recentBtn.backgroundColor = CIRCLE_BG_COLOR
        self.gemBtn.backgroundColor = CIRCLE_BG_COLOR
        filterStatusIcon.isHidden = true
        
        
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive),
                                               name: UIApplication.didBecomeActiveNotification, // UIApplication.didBecomeActiveNotification for swift 4.2+
            object: nil)
        
        //network not available
        network.reachability.whenUnreachable = { reachability in
            self.alert.message(message: Utility.language.value(forKey: "network_error") as! String)
            self.alert.hideDisable = true
            self.alert.show()
            
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
        
        //available
        network.reachability.whenReachable = { reachability in
            self.alert.finishAnimating()
        }
        
        self.primeStoreInfo()
        self.infoView.frame = CGRect.init(x: 20, y: (FULL_HEIGHT/2)-15, width: FULL_WIDTH - 40, height: 30)
           self.infoLbl.frame = CGRect.init(x: 20, y: (FULL_HEIGHT/2)-15, width: FULL_WIDTH - 40, height: 30)
    }
    
    func changeToRTL() {
         if UserModel.shared.language == "Arabic"{
            self.sideView.frame = CGRect.init(x: 0, y: 0, width: 100, height: FULL_HEIGHT)
            self.logoView.frame.origin.x = FULL_WIDTH-160
         }else{
            self.sideView.frame = CGRect.init(x: FULL_WIDTH-100, y: 0, width: 100, height: FULL_HEIGHT)
            self.logoView.frame.origin.x = 20
        }
        self.infoLbl.config(color: .white, font:medium, align: .center, text: "touch_to_start")

     }
    
    func infoShimmer()  {
        infoView.contentView = self.infoLbl
        infoView.isShimmering = true
        infoView.shimmerAnimationOpacity = 0.1
        infoView.shimmerSpeed = 120
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        Event.StartLiveChat.log()
        DispatchQueue.main.async {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                        if granted {
                            DispatchQueue.main.async {
                                if Utility.shared.isConnectedToNetwork(){
                                    HSWebSocket.sharedInstance.update(status: IN_SEARCH)
                                    let liveObj = ContainerPage()
                                    liveObj.modalPresentationStyle = .custom
                                    liveObj.transitioningDelegate = self.transition
                                    liveObj.viewType = "1"
                                    liveObj.changesDone = {
                                        self.gemCountLbl.text = UserModel().gemCount.kmFormatted
                                    }
                                    self.navigationController?.present(liveObj, animated: true, completion: nil)
                                }
                            }
                        } else {
                            
                            DispatchQueue.main.async {
                                
                                self.microPhonePermissionAlert()
                            }

                        }
                    })
                    
                } else {
                    
                    DispatchQueue.main.async {
                        
                        self.cameraPermissionAlert()
                    }
                    
                }
            })
        }
    }
    
    @IBAction func showOfferScreen() {
        OfferViewController.requestProductsAndShow(from: self)
    }
    
    @IBAction func gemBtnTapped(_ sender: Any) {
//        appDelegate.endCall()
        if Utility.shared.isConnectedToNetwork(){
            let gemList = GemListPage()
            self.navigationController?.pushViewController(gemList, animated: false)
        }
    }
    
    @IBAction func filterBtnTapped(_ sender: Any) {
        
        self.tabBarController?.tabBar.isHidden = true
        let filter = FilterPage()
        filter.modalPresentationStyle = .overCurrentContext
        filter.modalTransitionStyle = .crossDissolve
        filter.didDismiss = { type in
            let filter_applied = UserDefaults.standard.value(forKeyPath: "filter_applied") as! String
            if  filter_applied == "1"{
                self.filterStatusIcon.isHidden = false
            }
            self.tabBarController?.tabBar.isHidden = false
            if type == "location" {
                let locObj = LocationListPage()
                self.navigationController?.pushViewController(locObj, animated: true)
            }else if type == "prime"{
                if Utility.shared.isConnectedToNetwork(){
                    OfferPresentingViewController.RemoteConfiguredPlansViewController.requestProductsAndShow(from: self)
                }
            }else if type == "gem"{
                
                let alert = AlertPopup()
                alert.viewType = "insufficient_funds"
                alert.modalPresentationStyle = .overCurrentContext
                alert.modalTransitionStyle = .crossDissolve
                alert.dismissed = {
                    let gemObj = GemListPage()
                    self.navigationController?.pushViewController(gemObj, animated: true)
                }
                self.present(alert, animated: true, completion: nil)
                
            }
        }
        self.navigationController?.present(filter, animated: true, completion: nil)
    }
    
    @IBAction func recentBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            let recentObj = RecentPage()
            self.navigationController?.pushViewController(recentObj, animated: true)
        }
    }
    
    
    //move to camera
    func moveToCamera()   {
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .camera
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    
    //show preview camera screen
    func previewScreen(){
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .front).devices
        captureDevice = availableDevices.first
        
        //ANIL
        if captureDevice != nil
        {
            do
            {
                let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
                captureSession.addInput(captureDeviceInput)
            }
            catch
            { print(error.localizedDescription) }
        }
        
        let pL = AVCaptureVideoPreviewLayer(session: captureSession)
        pL.videoGravity = .resizeAspectFill
        self.previewLayer = pL
        self.previewLayer.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
        self.preview.layer.addSublayer(self.previewLayer)
        //ADD CONSTRAINTS HERE
        if captureDevice != nil
        { captureSession.startRunning() }
        
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString):NSNumber(value:kCVPixelFormatType_32BGRA)] as [String : Any]
        dataOutput.alwaysDiscardsLateVideoFrames = true
        
        if captureDevice != nil
        {
            if captureSession.canAddOutput(dataOutput) {
                captureSession.addOutput(dataOutput)
            }
            captureSession.commitConfiguration()
        }
    }
    
    
    //call when application from background
    @objc func applicationDidBecomeActive() {
        captureSession.startRunning()
        infoView.isShimmering = true
        
        AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
            if granted {
            } else {
                DispatchQueue.main.async {
                    
                    self.cameraPermissionAlert()
                }
            }
        })
        
        //check reachability
        let reachability = Reachability()
        switch reachability!.connection {
        case .wifi:
            alert.hideDisable = false
            alert.finishAnimating()
        case .cellular:
            alert.finishAnimating()
        case .none:
            alert.message(message: Utility.language.value(forKey: "network_error") as! String)
            alert.hideDisable = true
            alert.show()
        }
    }
    @objc func appMovedToBackground() {
        infoView.isShimmering = false
        captureSession.stopRunning()
    }
    
    
    //update profile details
    func updateProfile()  {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue(UserModel.shared.userId, forKey: "profile_id")
        Obj.baseService(subURl: PROFILE_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.userData = response.data!
                let premium_member = dict?.value(forKey: "premium_member") as! String
                UserModel.shared.isPremium = premium_member == "true"
//                let available_gems = dict?.value(forKey: "available_gems") as! NSNumber
//                UserModel.shared.gemCount = available_gems.intValue
//                self.gemCountLbl.text = available_gems.intValue.kmFormatted
                for purchasedItme in Apphud.nonRenewingPurchases()! {
                    print(purchasedItme.productId)
                }
            }
        })
    }
}

//MARK: GET PRIME INFO FROM STORE
extension HomePage: SKProductsRequestDelegate,SKPaymentTransactionObserver,SKRequestDelegate{
    
    func primeStoreInfo()  {
        let defaultDict = UserModel.shared.getDefaults()
        
        (defaultDict?.value(forKey: "membership_packages") as? [Any]).map(primeTempArray.addObjects)
        for prime in self.primeTempArray {
            let gemdict = prime as! NSDictionary
            let productid = gemdict.value(forKey: "subs_title")
            self.productIDs.append((productid as! String))
        }
        self.requestProductInfo()
        if !UserModel.shared.userId.isEmpty
        {
            //ANIL
            if UserModel.shared.isPremium || UserModel.shared.alreadySubscribed { // already prime
                DispatchQueue.main.async {
                   Utility.shared.iapActiveStatus()
                }
            }
            
        }
    }
    
    //check in app product information by using productids
    func requestProductInfo() {
        SKPaymentQueue.default().add(self)
        if SKPaymentQueue.canMakePayments() {
            let productIdentifiers = NSSet(array: productIDs as [Any])
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as Set<NSObject> as! Set<String>)
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            print("Cannot perform In App Purchases.")
        }
    }
    
    //refresh delegate
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        for transaction in queue.transactions {
            var trans = SKPaymentTransaction()
            trans = transaction
            if transaction.transactionState == .purchased {
                // Pro Purchased
                print("check already done \(transaction.payment.productIdentifier)")
            }
            
        }
    }
    
    //store kit delegates
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if response.products.count != 0 {
            productIDs.removeAll()
            let product = response.products[0]
            let primeDict = NSMutableDictionary()
            primeDict.setValue(product.price.stringValue, forKey: "price")
            primeDict.setValue(product.priceLocale.currencySymbol!, forKey: "currency")
            if #available(iOS 11.2, *) {
                primeDict.setValue("\((NSNumber.init(value: (product.subscriptionPeriod?.numberOfUnits)!)).stringValue)\(unitName(unitRawValue: (product.subscriptionPeriod?.unit.rawValue)!))", forKey: "validity")
            } else {
                // Fallback on earlier versions
            }
            
            Utility.shared.primePackage = primeDict
            
        }
        else {
            print("There are no products.")
        }
    }
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            print("iap details \(transaction.transactionState), \(transaction.payment.productIdentifier)")
            if transaction.transactionState == .purchased {
                // Pro Purchased
                print("check  \(transaction.payment.productIdentifier)")
            }
            switch transaction.transactionState {
            case .purchasing: break
            default: queue.finishTransaction(transaction)
            }
        }
    }
    
    func unitName(unitRawValue:UInt) -> String {
        switch unitRawValue {
        case 0: return "D"
        case 1: return "W"
        case 2: return "M"
        case 3: return "Y"
        default: return ""
        }
    }
    
    //redirect notification
    func checkNotificationRedirect()  {
        if Utility().isNotificationRedirect {
            let msgDict = Utility.shared.getNotificationInfo()
            let scope = msgDict?.value(forKey: "scope") as! String
            if scope == "txtchat" || scope == "admin"{
                let user_id = msgDict?.value(forKey: "user_id") as! String
                let user_image = msgDict?.value(forKey: "user_image") as! String
                let user_name = msgDict?.value(forKey: "user_name") as! String
                let msgDetail = MessageDetailPage()
                msgDetail.contactID = user_id
                msgDetail.contactName = user_name
                msgDetail.contactImg = user_image
                msgDetail.isBlocked = false
                msgDetail.newChanges = {
                }
                self.navigationController?.pushViewController(msgDetail, animated: true)
            }else if scope == "follow"{
                let user_id = msgDict?.value(forKey: "user_id") as! String
                let profile = ProfilePage()
                profile.viewType = "fromMsg"
                profile.profileId = user_id
                self.navigationController?.pushViewController(profile, animated: true)
            }else if scope == "video_call"{
                DispatchQueue.main.async {
//                    appDelegate.endCall()
                    UserModel.shared.alreadyInCall = true
                    let videoObj = VideoCallPage()
                    videoObj.user_name = msgDict!.value(forKey: "user_name") as! String
                    videoObj.user_img = msgDict!.value(forKey: "user_image") as! String
                    videoObj.sender = false
                    videoObj.viewFrom = "callkit"
                    videoObj.platform =  msgDict!.value(forKey: "platform") as! String
                    videoObj.modalPresentationStyle = .fullScreen
                    videoObj.user_id = msgDict!.value(forKey: "user_id") as! String
                    videoObj.room_id = msgDict!.value(forKey: "room_id") as! String
                    self.navigationController?.present(videoObj, animated: true, completion: nil)
                }
                
            }
            if scope != "video_call"{
                self.infoShimmer()
            }
            
            Utility().clearRedirectionInfo()
        }else{
            self.infoShimmer()
        }
    }
}

// MARK: - Helpers
private extension HomePage {
    func subscribeNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(refreshLogo),
                                       name: .primeUpdated,
                                       object: nil)
        notificationCenter.addObserver(self, selector: #selector(dismissedOneMonthOffer),
                                       name: .offerViewControllerDismissed(OneMonthSubscriptionViewController.self),
                                       object: nil)
        notificationCenter.addObserver(self, selector: #selector(dismissedOffer),
                                       name: .offerViewControllerDismissed(OfferViewController.self),
                                       object: nil)
        notificationCenter.addObserver(self, selector: #selector(invokeTimerIfNeeded),
                                       name: UIApplication.willEnterForegroundNotification,
                                       object: nil)
        notificationCenter.addObserver(self, selector: #selector(invalidateTimerIfNeeded),
                                       name: UIApplication.didEnterBackgroundNotification,
                                       object: nil)
        notificationCenter.addObserver(self, selector: #selector(offerWillAppear),
                                       name: .offerViewControllerWillAppear,
                                       object: nil)
        notificationCenter.addObserver(self, selector: #selector(subscriptionPurchased),
                                       name: .subscriptionPurchased,
                                       object: nil)
    }
}

// MARK: - Offer
private extension HomePage {
    var curremtTime: String? {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.zeroFormattingBehavior = .pad
        return offerCountdown.flatMap { formatter.string(from: Double($0)) }
    }
    
    func startCountdownTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
            guard let self = self, self.offerCountdown != nil else { return timer.invalidate() }
            defer { self.offerCountdown! -= 1 }
            self.countdownLabel.text = self.curremtTime
            self.offerViewController?.countdownLabel?.text = self.curremtTime
            self.lastCountdownDate = Date()
            
            if self.offerCountdown! <= 0 {
                timer.invalidate()
                self.offerView.isHidden = true
                self.offerViewController?.dismiss(animated: true)
            }
        }
        
        RunLoop.main.add(timer!, forMode: .default)
    }
    
    @objc func subscriptionPurchased(_ notification: Notification) {
        guard let transaction = notification.object as? SKPaymentTransaction else { return }
        Analytics.logEvent("subscription_purchased:\(transaction.payment.productIdentifier)", parameters: [
            "transactionIdentifier" : transaction.transactionIdentifier,
            "productIdentifier" : transaction.payment.productIdentifier
        ])
        offerView.isHidden = true
    }
    
    @objc func offerWillAppear(_ notification: Notification) {
        offerViewController = notification.object as? OfferViewController
        UIView.animate(withDuration: 0.3) {
            self.offerView.alpha = 0
        }
    }
    
    @objc func invokeTimerIfNeeded() {
        guard !UserModel.shared.isPremium else { return }
        if let lastDate = lastCountdownDate, offerCountdown != nil, offerCountdown! > 0 {
            let difference = Int(Date().timeIntervalSince(lastDate))
            offerCountdown! -= max(difference, 0)
            countdownLabel.text = curremtTime
            startCountdownTimer()
            dismissedOffer()
        }
    }
    
    @objc func invalidateTimerIfNeeded() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc func dismissedOneMonthOffer() {
        let configs = RemoteConfig.remoteConfig()
        guard offerCountdown == nil,
              !UserModel.shared.isPremium,
              let countdownSeconds = configs.configValue(forKey: .countdown_seconds).numberValue?.intValue,
              configs.configValue(forKey: .enable_offer).boolValue
        else { return }
        
        offerCountdown = countdownSeconds
        startCountdownTimer()
        OfferPresentingViewController.RemoteConfiguredPlansViewController.requestProductsAndShow(from: navigationController?.topViewController)
    }
    
    @objc func dismissedOffer() {
        UIView.animate(withDuration: 0.3) {
            self.offerView.alpha = 1
        }
    }
}
