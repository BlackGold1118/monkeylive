//
//  LivePage.swift
//  Randoo
//
//  Created by HTS-Product on 31/01/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import AVKit
import Lottie
import SwiftyJSON
import ContentSheet

class LivePage: ARDVideoCallViewController,UIScrollViewDelegate,SRWebSocketDelegate,ARDVideoCallViewControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var enlargeBtn: UIButton!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var propertiesView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var sideToolsView: UIView!
    @IBOutlet weak var magicBtn: UIButton!
    @IBOutlet weak var reportBtn: UIButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var muteBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var arrowView: UIView!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var infoView: ShimmeringView!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var ageLbl: UILabel!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var searchingLbl: UILabel!
    @IBOutlet weak var touchInfoView: UIView!
    @IBOutlet weak var touchInfoLbl: UILabel!
    @IBOutlet weak var stickersView: UIView!
    @IBOutlet weak var stickerCollectionView: UICollectionView!
    @IBOutlet weak var stickerBtn: UIButton!
    @IBOutlet weak var giftBtn: UIButton!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var emojiView: AnimationView!
    @IBOutlet var giftSendView: UIView!
    @IBOutlet var giftTitleLbl: UILabel!
    @IBOutlet var giftSendBtn: UIButton!
    @IBOutlet var giftAnimationView: UIView!
    @IBOutlet var giftLblBg: UIView!
    @IBOutlet var giftAnimatedLbl: UILabel!
    @IBOutlet var giftAnimatedIcon: UIImageView!
    @IBOutlet var giftReceiverAnimationView: UIView!
    @IBOutlet var giftReceiverLblBg: UIView!
    @IBOutlet var giftReceiverAnimatedLbl: UILabel!
    @IBOutlet var giftReceiverAnimatedIcon: UIImageView!
    @IBOutlet var liveInfoLbl: UILabel!
    @IBOutlet var profileBtn: UIButton!
    @IBOutlet var crownIcon: UIImageView!
    @IBOutlet var receiverEmojiView: AnimationView!
    
    @IBOutlet var loadCloseBtn: UIButton!
    var filter_applied = false
    var isViewAppear = Bool()
    var isFollowReq = Bool()
    var hideEnabled = Bool()
    var isStickerEnabled = Bool()
    var isGiftSendEnabled = Bool()
    var myStatus = String()
    var closeClicked = Bool()
    var partner:PartnerModel!
    var profile:ProfileModel!
    var followStatus = String()
    var isLive = Bool()
    var speakerEnable = Bool()
    var searchCount = Int()
    var popUpEnabled = Bool()
    var emojiArray = NSMutableArray()
    var giftArray = NSMutableArray()
    var stickerType = String()
    var partnerID = String()
    var selectedGift = NSDictionary()
    var pageDismissed: (() -> Void)?
    var findDesArray:[String]?
    var randomIndex = Int()
    var isReport = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialSetup()
        UserDefaults.standard.setValue(false, forKey: "partner_available")
        
    }
    
    override func viewDidLayoutSubviews() {
        
        infoView.contentView = self.infoLbl
        infoView.isShimmering = true
        infoView.shimmerAnimationOpacity = 0.1
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDelegate.isLive = true
        isViewAppear = true
        let details_avail = UserDefaults.standard.value(forKey: "partner_available") as! Bool
        if details_avail{
            do{
                let jsonDecoder = JSONDecoder()
                self.profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.partnerModel)
                self.setUserDetails()
            }catch{
                
            }
        }
        self.changeToRTL()
    }
    override func viewWillDisappear(_ animated: Bool) {
        appDelegate.isLive = true
        isViewAppear = false
        UIApplication.shared.isIdleTimerDisabled = false
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    
    
    //config initial details
    func initialSetup()  {
        UIApplication.shared.isIdleTimerDisabled = true
        myStatus = IN_SEARCH
        searchCount = 0
        HSWebSocket.webSocket.delegate = self
        closeClicked = false
        isLive = false
        isFollowReq = false
        popUpEnabled = false
        HSWebSocket.sharedInstance.findPartner()
        self.loaderView.backgroundColor = PRIMARY_COLOR
        self.configWebRTC()
        self.speakerEnable = true
        self.muteBtn.setImage(UIImage.init(named: "unmuted_icon"), for: .normal)
        self.configScrollView(type: "1")
        
        Utility.shared.addLoader(customView: loaderView)
        Utility.shared.showLoader()
        hideEnabled = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideProperties(sender:)))
        self.view.addGestureRecognizer(tap)
        self.configToolsView()
        self.configBottomView()
        self.touchInfoLbl.config(color: .white, font: lite, align: .center, text: "touch_full")
        self.scrlView.addSubview(self.touchInfoView)
        self.delegate = self
        self.scrlView.addSubview(self.enlargeBtn)
        self.scrlView.bringSubviewToFront(self.enlargeBtn)
        self.configStickersView()
        self.findDesArray = ["find_des1","find_des2","find_des3","find_des4","find_des5","find_des6"]
        //add observer for getting active state
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        self.configSearchLbl()
        self.liveInfoLbl.config(color: .white, font: medium, align: .center, text: "")
        self.liveInfoLbl.isHidden = true
        self.scrlView.addSubview(liveInfoLbl)
    }
    //config side tools
    func configToolsView()  {
        self.sideToolsView.frame.size.height = FULL_HEIGHT
        self.scrlView.addSubview(sideToolsView)
        self.scrlView.bringSubviewToFront(sideToolsView)
        
        self.sideToolsView.frame.origin.x = FULL_WIDTH-100
        self.bottomView.frame.origin.y = FULL_HEIGHT - self.bottomView.frame.size.height
        
        self.magicBtn.cornerRoundRadius()
        self.cameraBtn.cornerRoundRadius()
        self.muteBtn.cornerRoundRadius()
        self.reportBtn.cornerRoundRadius()
        self.closeBtn.cornerRoundRadius()
        
        self.magicBtn.backgroundColor = CIRCLE_BG_COLOR
        self.cameraBtn.backgroundColor = CIRCLE_BG_COLOR
        self.reportBtn.backgroundColor = CIRCLE_BG_COLOR
        self.muteBtn.backgroundColor = CIRCLE_BG_COLOR
        self.closeBtn.backgroundColor = CIRCLE_BG_COLOR
    }
    
    func configStickersView()  {
        isStickerEnabled = false
        isGiftSendEnabled = false
        self.stickerBtn.config(color: .white, font: medium, align: .center, title: "sticker")
        self.giftBtn.config(color: TEXT_SECONDARY_COLOR, font: medium, align: .center, title: "gifts")
        self.giftBtn.setBorder(color:STICKERVIEW_BORDER)
        self.stickerBtn.setBorder(color:STICKERVIEW_BORDER)
        self.view.bringSubviewToFront(self.stickersView)
        stickerCollectionView.register(UINib(nibName: "stickerCell", bundle: nil), forCellWithReuseIdentifier: "stickerCell")
        let height = FULL_WIDTH/2
        stickerCollectionView.frame = CGRect.init(x: 0, y: 45, width: FULL_WIDTH, height:height)
        stickersView.addSubview(stickerCollectionView)
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        stickerCollectionView.collectionViewLayout = collectionViewFlowLayout
        pageControl.hidesForSinglePage = true
        pageControl.currentPageIndicatorTintColor = PRIMARY_COLOR
        pageControl.pageIndicatorTintColor = .white
        self.loadStickers(type: "emoji")
        self.stickersView.bringSubviewToFront(stickerCollectionView)
        self.view.bringSubviewToFront(self.stickersView)
        self.stickersView.isHidden = true
        self.stickersView.frame.origin.y = FULL_HEIGHT
        giftSendView.isHidden = true
        self.giftTitleLbl.config(color: .white, font: mediumReg, align: .left, text: "")
        self.giftSendBtn.config(color: .yellow, font: mediumReg, align: .center, title: "send")
        self.giftLblBg.crapView(radius: 10)
        self.giftLblBg.backgroundColor = UIColor.init(red: 0.0/250, green: 0.0/250, blue: 0.0/250, alpha: 0.2)
        self.giftReceiverLblBg.crapView(radius: 10)
        self.giftReceiverLblBg.backgroundColor = UIColor.init(red: 0.0/250, green: 0.0/250, blue: 0.0/250, alpha: 0.2)
        self.giftAnimatedLbl.config(color: .white, font: medium, align: .left, text: "")
        self.giftReceiverAnimatedLbl.config(color: .white, font: medium, align: .left, text: "")
        self.giftAnimationView.isHidden = true
        self.giftReceiverAnimationView.isHidden = true
        self.emojiView.isHidden = true
        self.receiverEmojiView.isHidden = true
    }
    
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
            self.view.transform = .identity
            self.loaderView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.loadCloseBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.searchingLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.propertiesView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.sideToolsView.frame.origin.x = -15
            self.bottomView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.profileImgView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.crownIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.profileNameLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.ageLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.followBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.profileBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftSendView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftTitleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftSendBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.stickersView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.stickerCollectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.stickerBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.pageControl.transform = CGAffineTransform(scaleX: -1, y: 1)
            let padding =  FULL_WIDTH - self.bottomView.frame.size.width
            self.bottomView.frame.origin.x = padding/2
            self.videoCallView.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            self.giftAnimatedLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftAnimatedIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftAnimationView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftReceiverAnimatedLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftReceiverAnimatedIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.giftReceiverAnimationView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }else{
            self.view.transform = .identity
            self.loaderView.transform = .identity
            self.loadCloseBtn.transform = .identity
            self.searchingLbl.transform = .identity
            self.propertiesView.transform =  .identity
            self.bottomView.transform =  .identity
            self.touchInfoView.transform =  .identity
            self.liveInfoLbl.transform =  .identity
            self.closeBtn.transform =  .identity
            self.profileImgView.transform =  .identity
            self.crownIcon.transform =  .identity
            self.profileNameLbl.transform =  .identity
            self.ageLbl.transform =  .identity
            self.followBtn.transform =  .identity
            self.profileBtn.transform =  .identity
            self.arrowView.transform =  .identity
            self.infoLbl.transform =  .identity
            self.infoView.transform =  .identity
            self.touchInfoView.transform =  .identity
            self.touchInfoLbl.transform =  .identity
            self.giftSendView.transform =  .identity
            self.giftTitleLbl.transform =  .identity
            self.giftSendBtn.transform =  .identity
            self.stickersView.transform =  .identity
            self.stickerCollectionView.transform =  .identity
            self.stickerBtn.transform =  .identity
            self.giftBtn.transform =  .identity
            self.pageControl.transform =  .identity
            self.giftAnimationView.transform = .identity
            self.giftAnimatedLbl.transform =  .identity
            self.giftAnimatedIcon.transform =  .identity
            self.giftAnimationView.transform =  .identity
            self.giftReceiverAnimatedLbl.transform =  .identity
            self.giftReceiverAnimatedIcon.transform =  .identity
            self.giftReceiverAnimationView.transform =  .identity
        }
    }
    
    //load data
    func loadStickers(type:String)  {
        stickerType = type
        if type == "emoji"{
            self.emojiArray.removeAllObjects()
            self.emojiArray.addObjects(from: ["angry","cry","lol","smile","tired","wow","angry","cry",
                                              "lol","smile","tired","wow","angry","cry","lol","smile",
                                              "tired","wow","angry","cry","lol","smile","tired","wow",
                                              "angry","cry","lol","smile","tired","wow","smile","tired",
            ])
            let emojiCount = self.emojiArray.count/8
            if self.emojiArray.count > 8{
                pageControl.numberOfPages = emojiCount
            }else{
                pageControl.numberOfPages = 0
            }
        }else{
            self.giftArray.removeAllObjects()
            let defaultDict = UserModel.shared.getDefaults()
            self.giftArray.addObjects(from: defaultDict?.value(forKey: "gifts") as! [Any])
            let giftCount = self.giftArray.count/8
            if self.giftArray.count > 8{
                pageControl.numberOfPages = giftCount
            }else{
                pageControl.numberOfPages = 0
            }
        }
        let Padding = FULL_WIDTH/2
        pageControl.frame = CGRect.init(x:Padding - (self.pageControl.frame.width/2), y: Padding+45, width: self.pageControl.frame.width, height:37)
        
        self.stickerCollectionView.reloadData()
    }
    //config bottom profile view
    func configBottomView(){
        self.bottomView.backgroundColor = .clear
        self.profileView.crapView(radius: 15)
        self.profileView.backgroundColor = CIRCLE_BG_COLOR
        self.profileImgView.makeItRound()
        self.profileNameLbl.config(color: .white, font: medium, align: .left, text: "")
        self.ageLbl.config(color: .white, font: lite, align: .left, text: "")
        //        if UserModel.shared.touchToolTip() == nil{
        self.infoLbl.config(color: .white, font: lite, align: .center, text: "swipe_skip")
        self.arrowView.addFitLOT(lot_name: "swipeup", w: self.arrowView.frame.size.width, h: self.arrowView.frame.size.height)
        //        }else{
        //            self.infoLbl.isHidden = true
        //            self.arrowView.isHidden = true
        //        }
        //
        self.followBtn.cornerRoundRadius()
        self.followBtn.backgroundColor = PRIMARY_COLOR
        scrlView.addSubview(bottomView)
        self.bottomView.frame.origin.y = FULL_HEIGHT - self.bottomView.frame.size.height
        self.bottomView.isHidden = true
        
    }
    func setUserDetails()  {
        let imgURL = URL.init(string: PROFILE_IMAGE_URL+profile.user_image!)
        self.profileImgView.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        self.profileNameLbl.text = profile.name
        if profile.premium_member == "true" {
            self.crownIcon.isHidden = false
        }else{
            self.crownIcon.isHidden = true
        }
        self.ageLbl.text = "\(Utility.language.value(forKey: "age") as! String) \(profile.age!)"
        if profile.follow == "false"{
            self.followStatus = "Follow"
            self.followBtn.setImage(UIImage.init(named: "follow_icon"), for: .normal)
            self.followBtn.isHidden = false
            self.profileView.frame = CGRect.init(x: 15, y: 75, width: 220, height: 70)
            
        }else{
            self.followStatus = "Unfollow"
            self.followBtn.setImage(UIImage.init(named: "unfollow_icon"), for: .normal)
            self.followBtn.isHidden = true
            self.profileView.frame = CGRect.init(x: 40, y: 75, width: 170, height: 70)
            
        }
    }
    
    //config scroll view
    func configScrollView(type:String)  {
        scrlView.contentSize = CGSize.init(width: FULL_WIDTH, height: 2*FULL_HEIGHT)
        loaderView.frame = CGRect.init(x: 0, y: FULL_HEIGHT, width: FULL_WIDTH, height: FULL_HEIGHT)
        scrlView.addSubview(loaderView)
        scrlView.showsVerticalScrollIndicator = false
        if type == "1"{
            scrlView.contentOffset = CGPoint.init(x: 0, y:FULL_HEIGHT)
            if isViewAppear{
                UserModel.shared.scrollEnabled = false
            }
        }
        scrlView.delegate = self
        scrlView.isPagingEnabled = true
        
    }
    func configSearchLbl()  {
        randomIndex = Int(arc4random_uniform(UInt32(findDesArray!.count)))
        
        self.searchingLbl.config(color: .white, font: medium, align: .center, text: self.findDesArray![randomIndex])
        self.searchingLbl.sizeThatFits(CGSize(width: FULL_WIDTH-40, height: CGFloat.greatestFiniteMagnitude))
        let newSize =  self.searchingLbl.sizeThatFits(CGSize(width: FULL_WIDTH-40, height: CGFloat.greatestFiniteMagnitude))
        var newFrame =  self.searchingLbl.frame
        newFrame.size = CGSize(width: max(newSize.width, FULL_WIDTH-40), height: newSize.height)
        self.searchingLbl.frame = CGRect.init(x: 20, y: FULL_HEIGHT/2+75, width: FULL_WIDTH-40, height: newFrame.size.height)
        
    }
    //center zoom tool view
    func toolTipAnimation()  {
        UIView.animate(withDuration: 0.6,
                       animations: {
                        self.touchInfoView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.6, animations: {
                            self.touchInfoView.transform = CGAffineTransform.identity
                        }, completion: { _ in
                            self.toolTipAnimation()
                        })
        })
        
    }
    //show tips info
    func showToolTip()  {
        self.touchInfoView.isHidden = false
        self.infoLbl.isHidden = false
        self.arrowView.isHidden = false
        self.toolTipAnimation()
    }
    //hide tool tip
    func hideToolTip()  {
        DispatchQueue.main.asyncAfter(deadline: .now() + 6.0) {
            UIView.animate(withDuration: 1.0, animations: {
                self.touchInfoView.isHidden = true
                self.infoLbl.isHidden = true
                self.arrowView.isHidden = true
            })
        }
        
        
    }
    //dismiss stream view
    @IBAction func dismissBtnTapped(_ sender: Any) {
        let otherAlert = UIAlertController(title: nil, message: Utility.language.value(forKey: "want_to_exit") as? String, preferredStyle: UIAlertController.Style.alert)
        let dismiss = UIAlertAction(title: Utility.language.value(forKey: "cancel") as? String, style: UIAlertAction.Style.cancel, handler: nil)
        otherAlert.addAction(dismiss)
        
        let okayBtn = UIAlertAction(title: Utility.language.value(forKey: "ok") as? String, style: UIAlertAction.Style.default, handler:closePage)
        otherAlert.addAction(okayBtn)
        present(otherAlert, animated: true, completion: nil)
        
    }
    //close page from alert
    func closePage(alert: UIAlertAction){
        self.closeView()
    }
    func closeView()  {
        closeClicked = true
        self.hangup()
        self.pageDismissed!()
        appDelegate.isLive = false
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func profileBtnTapped(_ sender: Any) {
        let secondVC = ProfilePage()
        secondVC.viewType = "livepage"
        self.navigationController?.pushViewController(secondVC, animated: true)
    }
    
    
    //follow / unfollow action
    @IBAction func followBtnTapped(_ sender: Any) {
        if isFollowReq{
            self.statusAlert(msg: "already_request")
        }else{
            isFollowReq = true
            let updateOBj = BaseWebService()
            let request = NSMutableDictionary()
            request.setValue(partnerID, forKey: "user_id")
            request.setValue(UserModel.shared.userId, forKey: "follower_id")
            request.setValue(self.followStatus, forKey: "type")
            updateOBj.baseService(subURl: FOLLOW_API, params:  request as? Parameters, onSuccess: {response in
                let dict = response.result.value as? NSDictionary
                let status = dict?.value(forKey: "status") as! String
                let msg = dict?.value(forKey: "message") as! String
                
                if status == "true"{
                    if msg == "Unfollowed successfully"{
                        self.followBtn.setImage(UIImage.init(named: "follow_icon"), for: .normal)
                        self.followStatus = "Follow"
                        self.profileView.frame = CGRect.init(x: 15, y: 75, width: 220, height: 70)
                        
                    }else{
                        self.followBtn.setImage(UIImage.init(named: "unfollow_icon"), for: .normal)
                        self.followStatus = "Unfollow"
                        self.followBtn.isHidden = true
                        self.profileView.frame = CGRect.init(x: 40, y: 75, width: 170, height: 70)
                    }
                }
                self.isFollowReq = false
            })
        }
    }
    
    
    //open stickers view
    @IBAction func magicBtnTapped(_ sender: Any) {
        isStickerEnabled = true
        self.stickersView.isHidden = false
        self.view.bringSubviewToFront(self.stickersView)
        self.emojiBtnAction()
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.stickersView.frame.origin.y = FULL_HEIGHT - self.stickersView.frame.size.height
        }, completion: nil)
    }
    
    @IBAction func cameraBtnTapped(_ sender: Any) {
        if checkAvail() {
            UIView.transition(with: self.cameraBtn, duration: 0.5, options: .transitionFlipFromRight, animations: nil, completion: nil)
            self.switchCamera()
        }else{
            self.statusAlert(msg: "upgrade_prime")
        }
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        Event.StopLiveChat.log()
        searchCount = 0
        closeClicked = true
        self.hangup()
        self.pageDismissed!()
        appDelegate.isLive = false
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func checkAvail() -> Bool {
        var filterDict = NSDictionary()
        filterDict =  UserModel.shared.getDefaults()!.value(forKey: "filter_options") as! NSDictionary
        let locationAvailable = filterDict.value(forKeyPath: "bcamera.non-prime") as! String
        
        if UserModel.shared.isPremium {
            return true
        }else{
            if locationAvailable == "1" {
                return true
            }
        }
        return false
    }
    @IBAction func muteBtnTapped(_ sender: Any) {
        if self.speakerEnable {
            self.muteOff()
            self.speakerEnable = false
            UIView.transition(with: self.muteBtn, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.muteBtn.setImage(UIImage.init(named: "muted_icon"), for: .normal)
            }, completion: nil)
            
        }else{
            self.muteOn()
            self.speakerEnable = true
            UIView.transition(with: self.muteBtn, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.muteBtn.setImage(UIImage.init(named: "unmuted_icon"), for: .normal)
            }, completion: nil)
            
        }
    }
    @IBAction func reportBtnTapped(_ sender: Any) {
        let secondVC = ReportPage()
        secondVC.partner_id = self.partnerID
        secondVC.getReport = { id in
            self.uploadService(reportid: id)
        }
        if #available(iOS 13.0, *) {
            secondVC.modalPresentationStyle = .formSheet
            self.present(secondVC, animated: true, completion: nil)
        }else{
            self.present(inContentSheet: secondVC, animated: true)
        }
        
        
    }
    
    @IBAction func enlarge(_ sender: Any) {
        self.enlargeView("")
        self.view.bringSubviewToFront(self.scrlView)
    }
    
    @IBAction func giftSendBtnTapped(_ sender: Any) {
        
        var gemValue = NSInteger()
        
        if UserModel.shared.isPremium{
            let primeGem = selectedGift.value(forKey: "gft_gems_prime") as! NSNumber
            gemValue = primeGem.intValue
        }else {
            let normalGem = selectedGift.value(forKey: "gft_gems") as! NSNumber
            gemValue = normalGem.intValue
        }
        print("gemValue \(gemValue) my gem \(UserModel.shared.gemCount)")
        if UserModel.shared.gemCount  >= gemValue  {
            HSWebSocket.sharedInstance.sendGift(partner: self.partnerID, giftDict: selectedGift)
        }else{
            self.statusAlert(msg: "not_enough_gems")
        }
    }
    
    @IBAction func giftBtnTapped(_ sender: Any) {
        self.stickerBtn.setTitleColor(TEXT_SECONDARY_COLOR, for: .normal)
        self.giftBtn.setTitleColor(.white, for: .normal)
        self.loadStickers(type: "gift")
    }
    
    @IBAction func emojBtnTapped(_ sender: Any) {
        self.stickersView.isHidden = false
        self.emojiBtnAction()
    }
    
    
    
    func emojiBtnAction()  {
        self.stickerBtn.setTitleColor(.white, for: .normal)
        if isGiftSendEnabled {
            self.isGiftSendEnabled = false
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.giftSendView.frame.origin.y = self.stickersView.frame.origin.y
            }, completion: {_ in
                self.giftSendView.isHidden = true
            })
        }
        
        self.giftBtn.setTitleColor(TEXT_SECONDARY_COLOR, for: .normal)
        self.loadStickers(type: "emoji")
    }
    
    //handle
    @objc func hideProperties(sender: UITapGestureRecognizer? = nil) {
        // handling code
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            if self.isStickerEnabled {
                self.stickersView.frame.origin.y = FULL_HEIGHT
                self.isStickerEnabled = false
                self.giftSendView.isHidden = true
            }else{
                if self.hideEnabled {
                    self.hideEnabled = false
                    if UserModel.shared.language == "Arabic"{
                        self.sideToolsView.frame.origin.x = -15
                    }else{
                        self.sideToolsView.frame.origin.x = FULL_WIDTH-100
                    }
                    self.bottomView.frame.origin.y = FULL_HEIGHT - self.bottomView.frame.size.height
                }else{
                    self.hideEnabled = true
                    self.bottomView.frame.origin.y = FULL_HEIGHT
                    if UserModel.shared.language == "Arabic"{
                        self.sideToolsView.frame.origin.x = -FULL_WIDTH
                    }else{
                        self.sideToolsView.frame.origin.x = FULL_WIDTH
                    }
                }
            }
        }, completion: nil)
    }
    
    //hide stickerview
    func hideStickersView()  {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            if self.isStickerEnabled {
                self.stickersView.frame.origin.y = FULL_HEIGHT
                self.isStickerEnabled = false
                self.giftSendView.isHidden = true
            }
        }, completion: nil)
    }
    //MARK: Scrollview delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrlView.isScrollEnabled = false
        }else{
            scrlView.isScrollEnabled = true
            let height = FULL_HEIGHT/2
            if scrollView.contentOffset.y>height {//loader
                if isViewAppear{
                    UserModel.shared.scrollEnabled = false
                }
                self.scrlView.contentOffset = CGPoint.init(x: 0, y: FULL_HEIGHT)
            }
        }
        
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.moveViewBasedOnScroll(top: scrollView.contentOffset.y)
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.moveViewBasedOnScroll(top: scrollView.contentOffset.y)
    }
    func moveViewBasedOnScroll(top:CGFloat)  {
        let height = FULL_HEIGHT/2
        if top>height {//loader
            self.bottomView.isHidden = true
            self.closeClicked = false
            self.hangup()
            self.isLive = false
            searchCount = 0
            HSWebSocket.sharedInstance.findPartner()
            self.configScrollView(type: "2")
        }else{//live
            self.scrlView.contentOffset = CGPoint.init(x: 0, y: 0)
            
        }
    }
    
    
    //MARK: SOCKET DELEGATE
    func webSocketDidOpen(_ webSocket: SRWebSocket!) {
        print("HERE CONNECTED \(webSocket.url.absoluteString)")
        if !self.isChat(socket: webSocket.url.absoluteString) {
            print("check connection")
        }
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didReceiveMessage message: Any!) {
        let jsonResponse =  JSON.init(message)
        print("RECEIVE SOCKET: \(jsonResponse)")
        let jsonString = message as! String
        let data = jsonString.data(using: .utf8)!
        do{
            let jsonDecoder = JSONDecoder()
            partner = try jsonDecoder.decode(PartnerModel.self, from: data)
            if partner.type == "_Searchback"{ // no user
                if  !isLive && !popUpEnabled{
                    searchCount += 1
                    if searchCount > 20 {
                        
                        DispatchQueue.main.async {
                            Event.NoUserFound.log()
                            self.popUpEnabled = true
                            self.hangup()
                            HSWebSocket.sharedInstance.update(status: IN_CHAT)
                            Utility.shared.hideLoader()
                            let alert = AlertPopup()
                            alert.viewType = "no_user_found"
                            alert.modalPresentationStyle = .overCurrentContext
                            alert.modalTransitionStyle = .crossDissolve
                            alert.dismissed = {
                                self.pageDismissed!()
                                appDelegate.isLive = false
                                self.dismiss(animated: true, completion: nil)
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                            HSWebSocket.sharedInstance.findPartner() //find again
                        }
                    }
                }
            } else if partner.type == "_Joinback"{
                //Free, so connect partner
                if partner.partner_id != UserModel.shared.userId{
                    let previousUser = UserDefaults.standard.value(forKeyPath: "partner_list") as! NSArray
                    if !isLive && !previousUser.contains(partner.partner_id!){
                        
                        let partnerArray = NSMutableArray()
                        partnerArray.addObjects(from: UserDefaults.standard.value(forKeyPath: "partner_list") as! [Any])
                        partnerArray.add(self.partner.partner_id!)
                        self.partnerID = self.partner.partner_id!
                        self.getPartnerProfileDetails()
                        self.isLive = true
                        self.join(toNextRoom: self.partner.randomKey, platform: self.partner.platform)
                        
                        DispatchQueue.main.async {
                            
                            UserDefaults.standard.removeObject(forKey: "partner_list")
                            UserDefaults.standard.set(partnerArray, forKey: "partner_list")
                            HSWebSocket.sharedInstance.update(status: IN_CHAT)
                            UserDefaults.standard.setValue(self.partner.platform, forKey: "partner_platform")
                            UserDefaults().synchronize()
                            
                            Event.MatchedOnLiveChat.log()
                        }
                    }
                }
            } else if partner.type == "_Connectback"{ // available partner details received
                if partner.partner_id != UserModel.shared.userId {
                    
                    if !isLive{
                        
                        HSWebSocket.sharedInstance.update(status: IN_CHAT)
                        self.myStatus = IN_CHAT
                        HSWebSocket.sharedInstance.joinRoom(partner: self.partner.partner_id!)
                        let  msg = try JSON(data: data)
                        filter_applied = msg["filter_search_result"].boolValue
                    }
                }
                
            }else if partner.type == "_leavePartner"{
                if !isReport{
                    self.statusAlert(msg: "partner_left")
                }
                
                self.closeClicked = false
                self.hangup()
            }else if partner.type == "_receiveSticker"{
                let  msg = try JSON(data: data)
                let sname = msg["sticker_id"].stringValue
                self.receiveEmoji(s_name: sname)
            }else if partner.type == "_receiveGift"{
                self.receiveGiftFromPartner(data:data)
            }else if partner.type == "_sendGiftSucceed"{
                self.sendGiftToPartner()
                let  msg = try JSON(data: data)
                UserModel.shared.gemCount = msg["total_gems"].numberValue.intValue
            }else if partner.type == "_outofGems"{
                self.statusAlert(msg: "not_enough_gems")
            }else if partner.type == "_Userblocked"{
                self.statusAlert(msg: "acc_blocked")
                UserModel.shared.logoutFromAll()
                Utility.shared.goToLoginPage()
            }else if partner.type == "_listenNotify"{
                let  msg = try JSON(data: data)
                let type = msg["msg_type"].stringValue
                if type == "0"{
                    self.liveInfoLbl.isHidden = false
                    let awaytxt = Utility.language.value(forKey: "away_from_live") as! String
                    self.liveInfoLbl.text = "\(self.profileNameLbl.text ?? "") \(awaytxt)" //ANIL
                    self.scrlView.bringSubviewToFront(self.liveInfoLbl)
                }else{
                    self.liveInfoLbl.isHidden = true
                }
            }else if partner.type == "_filterCharged"{
                let  msg = try JSON(data: data)
                let user_id = msg["user_id"].stringValue
                if user_id == UserModel.shared.userId{
                    let gems = msg["total_gems"].numberValue
                    UserModel.shared.gemCount = gems.intValue
                }
            }
        }catch{
        }
    }
    
    //check its matching filter result
    func checkFilterStatus()  {
        let filterEnable = UserDefaults.standard.value(forKeyPath: "filter_applied") as! String
        if filterEnable == "1"{
            if filter_applied{
                HSWebSocket.sharedInstance.filterUpdated(status: "true")
                self.statusAlert(msg: "filter_result_success")
            }else{
                self.statusAlert(msg: "filter_result_failure")
            }
        }
    }
    
    //MARK: APP RTC DELEGATE
    func streamDetails(_ state: Int) {
        print("state in live page \(state)")
        if state == 2 { // CONNECTED STATE
            
            DispatchQueue.main.async {
                
                HSWebSocket.sharedInstance.setRecent(partner: self.partnerID) // for recent history
                self.checkFilterStatus()
                self.bottomView.isHidden = false
                self.isReport = false
                self.muteOn()
                self.speakerOn()
                self.muteBtn.setImage(UIImage.init(named: "unmuted_icon"), for: .normal)
                UserModel.shared.scrollEnabled = true
                self.liveInfoLbl.isHidden = true
                self.showToolTip()
                self.hideToolTip()
                self.searchingLbl.text = ""
            }
            
        }else if state == 3{
            self.liveInfoLbl.isHidden = true
        }else if state == 5{ //away from chat
            
        }else if state == 6 { // DISCONNECTED STATE
            self.bottomView.isHidden = true
            self.stickersView.frame.origin.y = FULL_HEIGHT
            self.giftSendView.frame.origin.y = FULL_HEIGHT
            
            //cut current partner
            HSWebSocket.sharedInstance.breakupPartner(partner: self.profile?.user_id ?? "")
            if self.closeClicked { // close btn clicked
                HSWebSocket.sharedInstance.update(status: ONLINE)
                self.myStatus = ONLINE
            }else{ // search another user
                if Utility.shared.isConnectedToNetwork() {
                    self.myStatus = IN_SEARCH
                    HSWebSocket.sharedInstance.update(status: IN_SEARCH)
                    self.isLive = false
                    self.searchCount = 0
                    HSWebSocket.sharedInstance.findPartner()
                    self.configScrollView(type: "1")
                    self.scrlView.contentOffset = CGPoint.init(x: 0, y: FULL_HEIGHT)
                    self.configSearchLbl()
                }else{
                    isSocketConnected = false
                    self.closeView()
                }
            }
        }
    }
    @objc func appMovedToBackground() {
        print("bg state")
        //send offline
        DispatchQueue.main.async {
            HSWebSocket.sharedInstance.notifyUser(partner_id:self.partnerID, type: "0")
        }
    }
    @objc func appBecomeActive() {
        print("active state")
        //send  online
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            HSWebSocket.sharedInstance.notifyUser(partner_id: self.partnerID, type: "1")
        }
        
    }
    
    func getPartnerProfileDetails()  {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue(partner.partner_id, forKey: "profile_id")
        Obj.baseService(subURl: PROFILE_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.partnerModel = response.data!
                do{
                    let jsonDecoder = JSONDecoder()
                    self.profile = try jsonDecoder.decode(ProfileModel.self, from: response.data!)
                    self.setUserDetails()
                    UserDefaults.standard.setValue(true, forKey: "partner_available")
                    
                    if self.profile.gender == "male" {
                        Event.MatchedMale.log()
                    } else if self.profile.gender == "female" {
                        Event.MatchedFemale.log()
                    }
                }catch{
                    
                }
            }
        })
    }
    
    //MARK: Collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if stickerType == "emoji"{
            return emojiArray.count
        }else{
            return giftArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : stickerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "stickerCell", for: indexPath) as! stickerCell
        
        if stickerType == "emoji"{
            cell.configCell(name: emojiArray.object(at: indexPath.row) as! String)
            cell.stickerBtn.tag = indexPath.row
            cell.stickerBtn.addTarget(self, action: #selector(self.sendEmoji(_:)), for: .touchUpInside)
        }else{
            let giftDict:NSDictionary =  giftArray.object(at: indexPath.row) as! NSDictionary
            cell.configGift(dict: giftDict)
            cell.giftBtn.tag = indexPath.row
            cell.giftBtn.addTarget(self, action: #selector(self.chooseGift(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let stickerSize = FULL_WIDTH/4
        return CGSize(width: stickerSize, height: stickerSize)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
    //send emoji
    @objc func sendEmoji(_ sender: UIButton){ //<- needs `@objc`{
        print("partner modal \(self.partnerID)")
        HSWebSocket.sharedInstance.sendSticker(partner:  self.partnerID , s_name:self.emojiArray.object(at: sender.tag) as! String )
        self.clearEmojies()
        self.emojiView.isHidden = false
        self.stickersView.isHidden = true
        self.emojiView = AnimationView()
        self.emojiView.clearsContextBeforeDrawing = true
        self.emojiView = AnimationView(name: self.emojiArray.object(at: sender.tag) as! String)
        self.emojiView.frame =  CGRect.init(x: (FULL_WIDTH/2)-75, y: FULL_HEIGHT, width: 150, height: 150)
        self.emojiView.contentMode = .scaleAspectFill
        self.view.addSubview(emojiView)
        self.view.bringSubviewToFront(self.emojiView)
        
        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.emojiView.frame =  CGRect.init(x: (FULL_WIDTH/2)-75, y: (FULL_HEIGHT/2)-75, width: 150, height: 150)
                        
        }, completion: { _ in
            self.emojiView.play{ (finished) in
                // Do Something
                UIView.animate(withDuration: 2.0, animations: {
                    self.emojiView.alpha = 0
                }, completion: nil)
            }
        })
    }
    //clear previous emojies
    func clearEmojies()  {
        self.receiverEmojiView.removeFromSuperview()
        self.emojiView.removeFromSuperview()
    }
    //recieve emoji
    func receiveEmoji(s_name:String){
        
        self.clearEmojies()
        self.receiverEmojiView.isHidden = false
        self.receiverEmojiView = AnimationView()
        self.receiverEmojiView.clearsContextBeforeDrawing = true
        self.receiverEmojiView = AnimationView(name:s_name)
        self.receiverEmojiView.frame =  CGRect.init(x: (FULL_WIDTH/2)-75, y: -150, width: 150, height: 150)
        self.receiverEmojiView.contentMode = .scaleAspectFill
        self.view.addSubview(receiverEmojiView)
        self.view.bringSubviewToFront(self.receiverEmojiView)
        
        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.receiverEmojiView.frame =  CGRect.init(x: (FULL_WIDTH/2)-75, y: (FULL_HEIGHT/2)-75, width: 150, height: 150)
        }, completion: { _ in
            self.receiverEmojiView.play{ (finished) in
                // Do Something
                UIView.animate(withDuration: 2.0, animations: {
                    self.receiverEmojiView.alpha = 0
                }, completion: {_ in
                    self.receiverEmojiView.isHidden = true
                })
            }
        })
    }
    
    //send sticker
    @objc func chooseGift(_ sender: UIButton){
        selectedGift = giftArray.object(at: sender.tag) as! NSDictionary
        self.giftTitleLbl.text = selectedGift.value(forKey: "gft_title") as? String
        if !isGiftSendEnabled {
            isGiftSendEnabled = true
            self.giftSendView.isHidden = false
            self.giftSendView.frame.origin.y = self.stickersView.frame.origin.y
            self.view.addSubview(giftSendView)
            self.view.bringSubviewToFront(self.giftSendView)
            self.view.bringSubviewToFront(self.stickersView)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.giftSendView.frame.origin.y = self.stickersView.frame.origin.y-45
            }, completion: nil)
        }
    }
    //send gift
    func sendGiftToPartner()  {
        self.giftAnimationView.removeFromSuperview()
        
        self.giftLblBg.backgroundColor = CIRCLE_BG_COLOR
        self.giftSendView.isHidden = true
        self.giftAnimationView.isHidden = false
        self.stickersView.isHidden = true
        if UserModel.shared.language == "Arabic"{
            self.giftAnimationView.frame.origin.x = FULL_WIDTH
        }else{
            self.giftAnimationView.frame.origin.x = -(self.giftAnimationView.frame.size.width+50)
        }
        self.view.addSubview(giftAnimationView)
        self.view.bringSubviewToFront(self.giftAnimationView)
        
        let colorString = "\"\(selectedGift.value(forKey: "gft_title") as? String ?? "")\""
        self.giftAnimatedLbl.text =  "\(Utility.language.value(forKey: "sent_gift") as! String)  \(colorString)  \(Utility.language.value(forKey: "gift") as! String)"
        self.giftAnimatedLbl.setCustomColor(colorString: colorString, color: .yellow)
        let iconName = selectedGift.value(forKey: "gft_icon") as! String
        let iconURL = URL.init(string: GIFT_IMAGE_URL+iconName)
        self.giftAnimatedIcon.sd_setImage(with: iconURL, placeholderImage: #imageLiteral(resourceName: "gift_icon"))
        
        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseIn],
                       animations: {
                        if UserModel.shared.language == "Arabic"{
                            self.giftAnimationView.frame.origin.x = FULL_WIDTH - self.giftAnimationView.frame.size.width
                        }else{
                            self.giftAnimationView.frame.origin.x = -8
                        }
        }, completion: { _ in
            UIView.animate(withDuration: 1.0, delay: 3.0, options: [.curveEaseIn], animations:{
                if UserModel.shared.language == "Arabic"{
                    self.giftAnimationView.frame.origin.x = FULL_WIDTH
                }else{
                    self.giftAnimationView.frame.origin.x = -(self.giftAnimationView.frame.size.width+50)
                }
            } , completion: nil)
        })
    }
    //recieve gift
    func receiveGiftFromPartner(data:Data){
        
        do {
            let  giftJson = try JSON(data: data)
            self.giftSendView.isHidden = true
            self.giftReceiverAnimationView.isHidden = false
            
            if UserModel.shared.language == "Arabic"{
                self.giftReceiverAnimationView.frame.origin.x = FULL_WIDTH
            }else{
                self.giftReceiverAnimationView.frame.origin.x = -(self.giftReceiverAnimationView.frame.size.width+50)
            }
            
            self.view.addSubview(giftReceiverAnimationView)
            self.view.bringSubviewToFront(self.giftReceiverAnimationView)
            
            let colorString = "\"\(giftJson["gift_title"].stringValue)\""
            self.giftReceiverAnimatedLbl.text =  "\(Utility.language.value(forKey: "received_gift") as! String)  \(colorString)  \(Utility.language.value(forKey: "gift") as! String)"
            self.giftReceiverAnimatedLbl.setCustomColor(colorString: colorString, color: .yellow)
            let iconName = giftJson["gift_icon"].stringValue
            let iconURL = URL.init(string: GIFT_IMAGE_URL+iconName)
            self.giftReceiverAnimatedIcon.sd_setImage(with: iconURL, placeholderImage: #imageLiteral(resourceName: "gift_icon"))
            self.giftReceiverLblBg.backgroundColor = CIRCLE_BG_COLOR
            
            
            UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseIn],
                           animations: {
                            if UserModel.shared.language == "Arabic"{
                                self.giftReceiverAnimationView.frame.origin.x = FULL_WIDTH - self.giftReceiverAnimationView.frame.size.width
                            }else{
                                self.giftReceiverAnimationView.frame.origin.x = -8
                            }
            }, completion: { _ in
                UIView.animate(withDuration: 1.0, delay: 3.0, options: [.curveEaseIn], animations:{
                    if UserModel.shared.language == "Arabic"{
                        self.giftReceiverAnimationView.frame.origin.x = FULL_WIDTH
                    }else{
                        self.giftReceiverAnimationView.frame.origin.x = -(self.giftReceiverAnimationView.frame.size.width+50)
                    }
                    
                } , completion: nil)
            })
            
        }catch{
            
        }
        
    }
    
    //upload remote view image to server
    func uploadService(reportid:String) {
        let imageData: Data = videoCallView.remoteVideoView.takeScreenshot().jpegData(compressionQuality: 0.1)! //ANIL
        let uploadObj = BaseWebService()
        uploadObj.uploadReport(profileimage: imageData, reportid: reportid, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                DispatchQueue.main.async {
                    self.statusAlert(msg: "reported_successfully")
                    self.isReport = true
                    self.myStatus = IN_SEARCH
                    self.hangup()
                    HSWebSocket.sharedInstance.update(status: IN_SEARCH)
                    self.isLive = false
                    HSWebSocket.sharedInstance.findPartner()
                    self.configScrollView(type: "1")
                    self.scrlView.contentOffset = CGPoint.init(x: 0, y: FULL_HEIGHT)
                    self.configSearchLbl()
                }
            }
        })
        
    }
    
}

