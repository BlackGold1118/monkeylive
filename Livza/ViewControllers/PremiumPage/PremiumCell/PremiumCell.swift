//
//  PremiumCell.swift
//  Randoo
//
//  Created by HTS-Product on 28/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class PremiumCell: UICollectionViewCell {

    @IBOutlet var bgView: UIView!
    @IBOutlet var validityLbl: UILabel!
    @IBOutlet var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgView.cornerViewMiniumRadius()
        self.bgView.elevationEffect()
        self.validityLbl.config(color: TEXT_PRIMARY_COLOR, font: mediumReg, align: .center, text: "")
        self.priceLbl.config(color: PRIMARY_COLOR, font: averageReg, align: .center, text: "")
    }

}
