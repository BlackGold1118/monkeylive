//
//  PremiumPage.swift
//  Randoo
//
//  Created by HTS-Product on 09/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import StoreKit
import GoogleMobileAds

class PremiumPage: UIViewController,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SKProductsRequestDelegate,SKPaymentTransactionObserver,GADBannerViewDelegate, UITextViewDelegate {
    
    
    @IBOutlet var bannerView: GADBannerView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageIndicator: UIPageControl!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var primeCollectionView: UICollectionView!
    @IBOutlet weak var termsTextView: UITextView!
    
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet var subBtn: UIButton!
    
    @IBOutlet var loader: UIActivityIndicatorView!
    
    @IBOutlet var loaderView: UIView!
    
    @IBOutlet var paymentLoader: UIActivityIndicatorView!
    var sliderArray = NSMutableArray()
    var primeArray = NSMutableArray()
    var primeTempArray = NSMutableArray()
    var selectedPrimeID = String()
    var selectedPrimeDict = NSDictionary()
    
    var productIDs: Array<String?> = []
    var productsArray: Array<SKProduct?> = []
    var selectedProduct = SKProduct()
    var currency = String()
    var primePurchase = true
    var isRequested = false
    var helpArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.changeToRTL()

    }

    
    func changeToRTL(){
        if UserModel.shared.language == "Arabic"{
//            self.scrollView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.backbtn.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.backbtn.frame = CGRect.init(x: FULL_WIDTH-50, y: 25, width: 40, height: 40)
        }
        else{
            self.scrollView.transform = .identity
            self.backbtn.frame = CGRect.init(x: 10, y: 25, width: 40, height: 40)
        }
    }
    //initial basic design
    func initialSetup()  {
        self.loaderView.isHidden = true
        loader.color = PRIMARY_COLOR
        loader.startAnimating()
        self.fd_prefersNavigationBarHidden = true
        self.titleLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text: "prime")
        self.subBtn.cornerRoundRadius()
        self.subBtn.config(color: .white, font: medium, align: .center, title:"subscribe")
        self.subBtn.backgroundColor =  PRIMARY_COLOR
        
        let defaultDict = UserModel.shared.getDefaults()
        sliderArray.addObjects(from: defaultDict?.value(forKeyPath: "prime_details.prime_benefits") as! [Any])
        self.configSliderPage()
        self.configurePageControl()
        
        if Utility.shared.isAdsEnable() {
            self.configAds()
        }else{
            self.bannerView.isHidden = true
        }
        
        primeCollectionView.register(UINib(nibName: "PremiumCell", bundle: nil), forCellWithReuseIdentifier: "PremiumCell")
        
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        primeCollectionView.collectionViewLayout = collectionViewFlowLayout
        
        (defaultDict?.value(forKey: "membership_packages") as? [Any]).map(primeTempArray.addObjects)
        for prime in self.primeTempArray {
            let gemdict = prime as! NSDictionary
            let productid = gemdict.value(forKey: "subs_title")
            self.productIDs.append((productid as! String))
        }
        
        requestProductInfo()
        
        SKPaymentQueue.default().restoreCompletedTransactions()
        setupTermsTextView()
        getHelpDetails()

    }
    override func viewWillDisappear(_ animated: Bool) {
        primePurchase = false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func getHelpDetails()  {
        
        DispatchQueue.main.async {
            
            let serviceObj = BaseWebService()
            serviceObj.getDetails(subURl: HELP_API, onSuccess: {response in
                let dict = response.result.value as? NSDictionary
                let status = dict?.value(forKey: "status") as! String
                if status == SUCCESS{
                    self.helpArray.addObjects(from: dict?.value(forKey: "help_list") as! [Any])
                    self.loader.stopAnimating()
                }
            })
        }
        
        
    }
    
    func setupTermsTextView() {
        let text = NSMutableAttributedString(string: "Recurring billing, cancel anytime.\n\nBy tapping Continue, your payment will be charged to Your iTunes account, and your subscription will automatically renew for the same package length at the same price until you cancel in settings in the iTunes at least 24 hours prior to the end of the current period.\n\nBy tapping Continue, you agree to our ")
        text.addAttribute(NSAttributedString.Key.font,
                          value: UIFont.systemFont(ofSize: 12),
                          range: NSRange(location: 0, length: text.length))

        let privacyPolicy = NSMutableAttributedString(string: "Privacy Policy")
        privacyPolicy.addAttribute(NSAttributedString.Key.font,
                                      value: UIFont.systemFont(ofSize: 12),
                                      range: NSRange(location: 0, length: privacyPolicy.length))


        privacyPolicy.addAttribute(NSAttributedString.Key.link,
                                      value: "privacy",
                                      range: NSRange(location: 0, length: privacyPolicy.length))

        
        let andText = NSMutableAttributedString(string: " and ")
        andText.addAttribute(NSAttributedString.Key.font,
                                      value: UIFont.systemFont(ofSize: 12),
                                      range: NSRange(location: 0, length: andText.length))

            
        let tosText = NSMutableAttributedString(string: "Terms of Service.")
        tosText.addAttribute(NSAttributedString.Key.font,
                                      value: UIFont.systemFont(ofSize: 12),
                                      range: NSRange(location: 0, length: tosText.length))
        
        tosText.addAttribute(NSAttributedString.Key.link,
        value: "tos",
        range: NSRange(location: 0, length: tosText.length))
        
  
        text.append(privacyPolicy)
        text.append(andText)
        text.append(tosText)


        termsTextView.attributedText = text
        termsTextView.textAlignment = .left
        termsTextView.isEditable = false
        termsTextView.isSelectable = true
        termsTextView.delegate = self
    }

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
      
        if URL.absoluteString == "tos" {
            
            if Utility.shared.isConnectedToNetwork(){
                let webObj =  WebPage()
                webObj.resultDict = self.helpArray.object(at: 2) as! NSDictionary
                self.navigationController?.pushViewController(webObj, animated: true)
            }else{
                self.statusAlert(msg: "network_error")
            }
            
        } else {
            
            if Utility.shared.isConnectedToNetwork(){
                let webObj =  WebPage()
                webObj.resultDict = self.helpArray.object(at: 1) as! NSDictionary
                self.navigationController?.pushViewController(webObj, animated: true)
            }else{
                self.statusAlert(msg: "network_error")
            }
        }
        
        return false
    }
    
    
    @IBAction func subBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork(){
            if !loader.isAnimating {
                if !isRequested{
                    self.loaderView.isHidden = false
                    self.paymentLoader.startAnimating()
                    self.fd_interactivePopDisabled = true
                    isRequested = true
                    let product_id =  selectedPrimeDict.value(forKey: "subs_title") as! String
                    if self.productIDs.contains(product_id) {
                        for product in productsArray {
                            var prd =  SKProduct()
                            prd = product!
                            if prd.productIdentifier == product_id{
                                self.selectedProduct = prd
                                let payment = SKPayment(product: prd)
                                SKPaymentQueue.default().add(payment)
                            }
                        }
                    }else{
                        isRequested = false
                        self.loaderView.isHidden = true
                        DispatchQueue.main.async {

                        self.paymentLoader.stopAnimating()
                        self.statusAlert(msg: "package_not_available")
                        }
                    }
                }else{
                    self.statusAlert(msg: "already_request")
                }
            }
        }
    }
    
    //page indicator
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        self.pageIndicator.numberOfPages = sliderArray.count
        self.pageIndicator.currentPage = 0
        self.pageIndicator.tintColor = PRIMARY_COLOR
        self.pageIndicator.pageIndicatorTintColor = .lightGray
        self.pageIndicator.currentPageIndicatorTintColor =  PRIMARY_COLOR
    }
    
    func configSliderPage()  {
        self.scrollView.frame = CGRect.init(x: 0, y: self.navigationView.frame.size.height+1, width: FULL_WIDTH, height: self.scrollView.frame.height)
        self.automaticallyAdjustsScrollViewInsets = false
        
        for  i in stride(from: 0, to: sliderArray.count, by: 1) {
            let sliderDict = sliderArray.object(at: i) as! NSDictionary
            var frame = CGRect.zero
            frame.origin.x = self.scrollView.frame.size.width * CGFloat(i)
            frame.origin.y = 0
            frame.size = self.scrollView.frame.size
            self.scrollView.isPagingEnabled = true
            
            let myImageView:UIImageView = UIImageView()
            myImageView.tag = i
            myImageView.contentMode = UIView.ContentMode.scaleAspectFit
            myImageView.frame = CGRect.init(x: frame.origin.x+125, y: 0, width: FULL_WIDTH-250, height: FULL_WIDTH-250)
            let iconName = sliderDict.value(forKey: "image") as! String
            let iconURL = URL.init(string: PRIME_IMAGE_URL+iconName)
            myImageView.sd_setImage(with: iconURL, placeholderImage: #imageLiteral(resourceName: "crown_icon"))
            scrollView.addSubview(myImageView)
            
            let titleLbl:UILabel = UILabel()
            titleLbl.frame = CGRect.init(x: frame.origin.x, y: myImageView.frame.size.height, width: FULL_WIDTH, height:45)
            titleLbl.config(color: TEXT_PRIMARY_COLOR, font: averageReg, align: .center, text:"")
            titleLbl.text = sliderDict.value(forKey: "title") as? String
            scrollView.addSubview(titleLbl)
            
            let titleDes:UITextView = UITextView()
            
            let des = sliderDict.value(forKey: "description") as? String
            //            let height = Utility.shared.height(text: des!, font: light!, width: FULL_WIDTH-40)
            titleDes.frame = CGRect.init(x: frame.origin.x+20, y:titleLbl.frame.size.height+titleLbl.frame.origin.y-5, width: FULL_WIDTH-40, height:100)
            titleDes.textContainerInset = .zero
            titleDes.textContainer.lineFragmentPadding = 0
            titleDes.font = lite
            titleDes.textColor = TEXT_SECONDARY_COLOR
            titleDes.textAlignment = .center
            titleDes.isEditable = false
            titleDes.text = des
            scrollView.addSubview(titleDes)
            
            if i == (self.sliderArray.count-1){
                self.scrollView.contentSize = CGSize(width: FULL_WIDTH * CGFloat(sliderArray.count), height: titleDes.frame.origin.y+100)
                self.scrollView.frame = CGRect.init(x: 0, y: self.navigationView.frame.size.height+1, width: FULL_WIDTH+20, height: 100+titleDes.frame.origin.y)
                if IS_IPHONE_X{
                    self.pageIndicator.frame.origin.y = scrollView.frame.size.height+scrollView.frame.origin.y-60
                }else{
                    self.pageIndicator.frame.origin.y = scrollView.frame.size.height+scrollView.frame.origin.y-20
                }
                
            }
        }
    }
    //config banner view
    func configAds()  {
        bannerView.adUnitID = Utility.shared.adUnitId
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    //banner view delegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    //check in app product information by using productids
    func requestProductInfo() {
        
        SKPaymentQueue.default().add(self)
        
        if SKPaymentQueue.canMakePayments() {
            let productIdentifiers = NSSet(array: productIDs as [Any])
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as Set<NSObject> as! Set<String>)
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            print("Cannot perform In App Purchases.")
        }
    }
    
    
    //in app purchase product request delage
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if response.products.count != 0 {
            print("product infor \(productsArray)")
            productIDs.removeAll()
            for product in response.products {
                productIDs.append(product.productIdentifier)
                productsArray.append(product)
            }
            self.appendStoreInfo()
            
        }
        else {
            print("There are no products.")
        }
    }
    
    
    //inapp payment
    private func paymentQueue(queue: SKPaymentQueue!, updatedTransactions transactions: [AnyObject]!) {
        for transaction in transactions as! [SKPaymentTransaction] {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                print("Transaction completed successfully.")
                SKPaymentQueue.default().finishTransaction(transaction)
                
            case SKPaymentTransactionState.failed:
                print("Transaction Failed");
                SKPaymentQueue.default().finishTransaction(transaction)
                
            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
    
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction:AnyObject in transactions{
            let trans = transaction as! SKPaymentTransaction
            switch trans.transactionState{
            case .purchased:
                if primePurchase{
                    self.isRequested = false
                    self.fd_interactivePopDisabled = false
                    self.loaderView.isHidden = true
                    print ("Buy success")
                    self.updatePrimeInfo(tID: trans.transactionIdentifier!)
                }
                queue.finishTransaction(trans)
                break
            case .failed:
                print ("Buy error")
                self.loaderView.isHidden = true
                self.isRequested = false
                self.fd_interactivePopDisabled = false
                queue.finishTransaction(trans)
                break
            default:
                print("default: Error")
                break
            }
        }
    }
    
    //append store information
    func appendStoreInfo()  {
        let arrangedArray = NSMutableArray()
        var count = 0
        
        for prime in self.primeTempArray {
            count += 1
            let primedict = prime as! NSDictionary
            let dict = NSMutableDictionary.init(dictionary: primedict)
            let productid = primedict.value(forKey: "subs_title") as! String
            
            if productIDs.contains(productid){
                //get price & currency information from store
                dict.removeObject(forKey: "subs_price")
                dict.removeObject(forKey: "subs_validity")
                
                for product in productsArray {
                    var prd =  SKProduct()
                    prd = product!
                    if prd.productIdentifier == productid{
                        
                        currency = prd.priceLocale.currencySymbol!
                        dict.setValue(prd.price.stringValue, forKey: "subs_price")
                        if #available(iOS 11.2, *) {
                            dict.setValue("\((NSNumber.init(value: (prd.subscriptionPeriod?.numberOfUnits)!)).stringValue) \(unitName(unitRawValue: (prd.subscriptionPeriod?.unit.rawValue)!))", forKey: "subs_validity")
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                }
            }else{
                dict.removeObject(forKey: "subs_price")
                dict.setValue(NSNumber.init(value: (primedict.value(forKey: "subs_price") as! Int)).stringValue, forKey: "subs_price")
            }
            arrangedArray.add(dict)
            
        }
        
        self.primeArray.addObjects(from: arrangedArray as! [Any])
        let selectDict = self.primeArray.object(at: 0) as! NSDictionary
        let pID = selectDict.value(forKey: "_id") as! String
        selectedPrimeID =  pID
        selectedPrimeDict = selectDict
        DispatchQueue.main.async {
            self.primeCollectionView.reloadData()
            self.loader.stopAnimating()
        }
    }
    
    func unitName(unitRawValue:UInt) -> String {
        switch unitRawValue {
        case 0: return "Day"
        case 1: return "Week"
        case 2: return "Month"
        case 3: return "Year"
        default: return ""
        }
    }
    //scroll view delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageIndicator.currentPage = Int(pageNumber)
    }
    
    //MARK: Collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return primeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : PremiumCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PremiumCell", for: indexPath) as! PremiumCell
        let primeDict = primeArray.object(at: indexPath.row) as! NSDictionary
        let price = primeDict.value(forKey: "subs_price") as! String
        cell.priceLbl.text = "\(currency) \(price)"
        cell.validityLbl.text = primeDict.value(forKey: "subs_validity") as? String
        
        let primeID = primeDict.value(forKey: "_id") as! String
        if primeID == selectedPrimeID{
            cell.bgView.border(color: PRIMARY_COLOR)
        }else{
            cell.bgView.border(color: .clear)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let primeDict = primeArray.object(at: indexPath.row) as! NSDictionary
        selectedPrimeID = primeDict.value(forKey: "_id") as! String
        selectedPrimeDict = primeDict
        self.primeCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 140, height: 110)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
    }
    
    //update to service
    func updatePrimeInfo(tID:String)  {
        let updateOBj = BaseWebService()
        let request = NSMutableDictionary()
        request.setValue(tID, forKey: "transaction_id")
        request.setValue(UserModel.shared.userId, forKey: "user_id")
        request.setValue(selectedProduct.productIdentifier, forKey: "membership_id")
        request.setValue(selectedProduct.price.stringValue, forKey: "paid_amount")
        request.setValue(Utility.shared.getUTCTime(), forKey: "payment_time")
        
        updateOBj.baseService(subURl: PRIME_PURCHASE_API, params:  request as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.isPremium = true
                UserModel.shared.alreadySubscribed = true
                UserModel.shared.updateProfile()
                let alert = AlertPopup()
                alert.viewType = "premium_alert"
                alert.modalPresentationStyle = .overCurrentContext
                alert.modalTransitionStyle = .crossDissolve
                alert.dismissed = {
                    self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
}
