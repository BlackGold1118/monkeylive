//
//  PhoneLoginViewController.swift
//  Livza
//
//  Created by Serge Vysotsky on 21.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import UIKit
import PhoneNumberKit
import FirebaseAuth
import FirebaseFunctions

final class PhoneLoginViewController: UIViewController, AuthUIDelegate {
    @IBOutlet private weak var verifyButton: UIBarButtonItem!
    private weak var phoneTextField: PhoneNumberTextField!
    
    @UserDefault(.authVerificationID)
    private var verificationId: String
    var completion: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTextField.withFlag = true
        phoneTextField.withPrefix = true
        phoneTextField.withDefaultPickerUI = true
        phoneTextField.isPartialFormatterEnabled = true
        phoneTextField.addTarget(self, action: #selector(phoneNumberChanged(_:)), for: .editingChanged)
        phoneTextField.becomeFirstResponder()
    }
    
    @objc func phoneNumberChanged(_ sender: PhoneNumberTextField) {
        verifyButton.isEnabled = sender.isValidNumber
    }
    
    @IBAction func dismiss(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @IBAction func verify(_ sender: UIBarButtonItem) {
        guard let text = phoneTextField.text else { return }
        let number: PhoneNumber
        do {
            number = try phoneTextField.phoneNumberKit.parse(text)
        } catch {
            return showAlertForError(error, okAction: nil)
        }
        
        let phone = phoneTextField.phoneNumberKit.format(number, toType: .international)
        let progress = navigationController!.view.showProgressView()
        progress.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        checkIfUserRegistered(phone) { [weak self, weak sender] isRegistered in
            if isRegistered {
                self?.sendVerificationCode(phone) { [weak self, weak sender] result in
                    progress.removeFromSuperview()
                    switch result {
                    case let .success(verificationId):
                        self?.verificationId = verificationId
                        self?.pushToCodeVerification(sender)
                    case let .failure(error):
                        self?.showAlertForError(error, okAction: nil)
                    }
                }
            } else {
                progress.removeFromSuperview()
                let alert = UIAlertController(title: "You can't sign up with phone number", message: "Please create your account either with apple sign in or Facebook.", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default) { [weak self] _ in
                    self?.dismiss(animated: true)
                }
                
                alert.addAction(ok)
                self?.present(alert, animated: true)
            }
        }
    }
    
    func sendVerificationCode(_ phone: String, _ completion: @escaping (Result<String, Error>) -> Void) {
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { verificationId, error in
            if let verificationId = verificationId {
                completion(.success(verificationId))
            } else if let error = error {
                completion(.failure(error))
            }
        }
    }
    
    func checkIfUserRegistered(_ phone: String, _ completion: @escaping (Bool) -> Void) {
        let dict = ["phoneNumber": phone]
        Functions.functions().httpsCallable("isPhoneNumberRegistered").call(dict) { result, error in
            completion((result?.data as? String)?.isEmpty == false)
        }
    }
    
    func pushToCodeVerification(_ sender: Any?) {
        view.endEditing(true)
        performSegue(withIdentifier: "enterVerificationCode", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        (segue.destination as? PhoneInputTableViewController).map { controller in
            controller.loadViewIfNeeded()
            phoneTextField = controller.phoneTextField
        }
        
        (segue.destination as? CodeInputTableViewController).map { controller in
            controller.completion = completion
        }
    }
}

final class PhoneInputTableViewController: UITableViewController {
    @IBOutlet fileprivate weak var phoneTextField: PhoneNumberTextField!
}

final class CodeInputTableViewController: UITableViewController, UITextFieldDelegate {
    @IBOutlet private weak var confirmButton: UIBarButtonItem!
    @IBOutlet private weak var codeTextField: UITextField!
    
    @UserDefault(.authVerificationID)
    private var verificationId: String
    var completion: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        codeTextField.becomeFirstResponder()
    }
    
    @IBAction func textDidChange(_ sender: UITextField) {
        if let text = sender.text, text.allSatisfy({ $0.isWholeNumber }) {
            confirmButton.isEnabled = text.count == 6
        } else {
            confirmButton.isEnabled = false
        }
    }
    
    @IBAction func confirmCode(_ sender: UIBarButtonItem) {
        let progress = navigationController!.view.showProgressView()
        progress.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let creds = PhoneAuthProvider.provider().credential(withVerificationID: verificationId, verificationCode: codeTextField.text!)
        Auth.auth().signIn(with: creds) { [weak self] result, error in
            progress.removeFromSuperview()
            if let phoneNumber = result?.user.phoneNumber {
                let completion = self?.completion
                self?.dismiss(animated: true) {
                    completion?(phoneNumber)
                }
            } else if let error = error {
                self?.showAlertForError(error, okAction: nil)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let replaced = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        return replaced.count <= 6
    }
}

private extension UIViewController {
    func showAlertForError(_ error: Error, okAction: (() -> Void)?) {
        let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { _ in okAction?() }
        alert.addAction(ok)
        present(alert, animated: true)
    }
}
