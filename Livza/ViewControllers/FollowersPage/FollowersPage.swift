//
//  FollowersPage.swift
//  Randoo
//
//  Created by HTS-Product on 19/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class FollowersPage: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var InfoView: ShimmeringView!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var followersCollectionView: UICollectionView!
    @IBOutlet weak var noLbl: UILabel!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var pullDownRefresh = CarbonSwipeRefresh()
    var usersList = [Users_list]()
    var profileID = String()
    var usersListArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loader.startAnimating()
        self.initialSetup()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        InfoView.contentView = self.infoLbl
        InfoView.isShimmering = true
        InfoView.shimmerAnimationOpacity = 0.1
        pullDownRefresh.setMarginTop(0)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getFollowers(offset: "0")
    }
    
    
    func initialSetup() {
        self.fd_interactivePopDisabled = false
        self.fd_prefersNavigationBarHidden = true
        followersCollectionView.register(UINib(nibName: "FollowersCell", bundle: nil), forCellWithReuseIdentifier: "FollowersCell")
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .vertical
        followersCollectionView.collectionViewLayout = collectionViewFlowLayout
        self.infoLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .center, text: "swipe_right_followings")
        self.noLbl.config(color: TEXT_SECONDARY_COLOR, font: lite, align: .center, text: "no_followers")
        self.noLbl.isHidden = true
        
        self.titleLbl.config(color: .white, font: averageReg, align: .center, text: "followers")
        self.followersCollectionView.reloadData()
        self.configLoadMore()
    }
    
    
    func configLoadMore() {
        let frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        followersCollectionView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: frame)
        followersCollectionView.addInfiniteScroll { (collectionView) -> Void in
            self.followersCollectionView.performBatchUpdates({ () -> Void in
                // update collection view
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                self.followersCollectionView.finishInfiniteScroll()
            });
        }
        
        pullDownRefresh = CarbonSwipeRefresh(scrollView: self.followersCollectionView)
        pullDownRefresh.colors = [UIColor.blue, UIColor.red, UIColor.orange, UIColor.green]
        self.view.addSubview(pullDownRefresh)
        pullDownRefresh.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        
    }
    @objc func refresh(sender: AnyObject) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            self.pullDownRefresh.endRefreshing()
        })
    }
    
    
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Collection view delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.usersListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard indexPath.row < usersListArray.count else { return UICollectionViewCell() }
        let cell : FollowersCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FollowersCell", for: indexPath) as! FollowersCell
        let dict = usersListArray.object(at: indexPath.row) as! NSDictionary
        cell.config(dict: dict)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row < usersListArray.count else { return }
        let dict = usersListArray.object(at: indexPath.row) as! NSDictionary
        let profile = ProfilePage()
        let user_id = dict.value(forKey: "user_id") as! String

        if user_id != UserModel.shared.userId{
            profile.viewType = "fromMsg"
        }else{
            profile.viewType = "ownProfile"
            profile.additionalInfo = "needback"
            
        }
        self.usersListArray.removeAllObjects()
        profile.profileId = user_id
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewSize = followersCollectionView.frame.size.width
        return CGSize(width: collectionViewSize/2-5, height: 120)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func getFollowers(offset:String)  {
        let followObj = BaseWebService()
        followObj.getDetails(subURl: "\(FOLLOWERS_API)/\(self.profileID)/\(offset)/20", onSuccess: {response in
            self.usersListArray.removeAllObjects()
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                self.usersListArray.addObjects(from: dict?.value(forKey: "users_list") as! [Any])
                self.noLbl.isHidden = true
                self.followersCollectionView.reloadData()
            }else{
                self.noLbl.isHidden = false
            }
            self.loader.stopAnimating()
                    
        })
    }
}
