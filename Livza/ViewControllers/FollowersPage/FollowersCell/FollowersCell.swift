//
//  FollowersCell.swift
//  Randoo
//
//  Created by HTS-Product on 21/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class FollowersCell: UICollectionViewCell {
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var transparentView: UIView!
    
    @IBOutlet var crownIcon: UIImageView!
    @IBOutlet weak var genderImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.nameLbl.config(color: .white, font: mediumReg, align: .right, text: "")

    }

    func config(dict:NSDictionary)  {
        self.userImg.cornerViewMiniumRadius()
        self.transparentView.cornerViewMiniumRadius()
        var username = String()
        let name = dict.value(forKey: "name") as! String
        if (name.count) > 7{
            username = "\(String((name.prefix(7)))).."
        }else{
            username = name
        }
        let privacy_age = dict.value(forKey: "privacy_age") as! Bool
        let age = dict.value(forKey: "age") as! NSNumber

        if privacy_age {
            self.nameLbl.text = username
        }else{
            self.nameLbl.text = "\(username),\(age.stringValue)"
        }
        
        let gender = dict.value(forKey: "gender") as! String

        if gender == "male"{
            self.genderImgView.image = UIImage(named: "male_sel")
        }else{
            self.genderImgView.image = UIImage(named: "female_sel")
        }
        
        let premium_member = dict.value(forKey: "premium_member") as! String

        if premium_member == "true"{
            self.crownIcon.isHidden = false
        }else{
            self.crownIcon.isHidden = true
        }
        
        let user_image = dict.value(forKey: "user_image") as! String

        let imgURL = URL.init(string: PROFILE_IMAGE_URL+user_image)
        self.userImg.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        let padding = (self.nameLbl.intrinsicContentSize.width/2)
        print(FULL_WIDTH)
        let left = (FULL_WIDTH-30)/2

        self.nameLbl.frame = CGRect.init(x:(left/2)-padding , y: self.crownIcon.frame.origin.y+25, width: self.nameLbl.intrinsicContentSize.width, height: 21)
        
        self.genderImgView.frame = CGRect.init(x: self.nameLbl.frame.size.width+self.nameLbl.frame.origin.x+5 , y: self.nameLbl.frame.origin.y+3, width: 15, height: 15)

     
     
    }

}
