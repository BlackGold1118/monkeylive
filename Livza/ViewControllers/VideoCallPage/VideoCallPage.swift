//
//  VideoCallPage.swift
//  Randoo
//
//  Created by HTS-Product on 30/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import SwiftyJSON
import AVFoundation
import AudioToolbox

class VideoCallPage: ARDVideoCallViewController,AVCaptureVideoDataOutputSampleBufferDelegate, ARDVideoCallViewControllerDelegate,socketDelegate{
    
    @IBOutlet var enlargeBtn: UIButton!
    @IBOutlet var previewView: UIView!
    @IBOutlet var sideBarView: UIView!
    @IBOutlet var propertiesView: UIView!
    @IBOutlet var muteBtn: UIButton!
    @IBOutlet var cutBtn: UIButton!
    @IBOutlet var cameraBtn: UIButton!
    @IBOutlet var acceptBtn: UIButton!
    @IBOutlet var userImgView: UIImageView!
    @IBOutlet var usernameLbl: UILabel!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var statusLbl: UILabel!
    
    var cameraView:UIImagePickerController?
    var cameraPreview:UIImagePickerController?
    let captureSession = AVCaptureSession()
    var previewLayer:CALayer!
    var captureDevice:AVCaptureDevice!
    var hideEnabled = false
    var user_name = String()
    var user_id = String()
    var room_id = String()
    var platform = String()

    var user_img = String()
    var sender = Bool()
    var stopLayout = false
    var call_status = String()
    var muteEnable = true
    var hideSideview = true
    var av_Player : AVAudioPlayer!
    var viewFrom = String()
    var isAttened = false
    var isCharged = false
    var isJoined = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        // Do any additional setup after loading the view.
    }
    
  
    override func viewDidLayoutSubviews() {
        if !isAttened{
            if sender {
                UserModel.shared.alreadyInCall = true
                self.senderDesign()
            }else{
                self.receiverDesign()
            }
            self.view.bringSubviewToFront(self.propertiesView)
            self.view.bringSubviewToFront(enlargeBtn)
            if hideSideview {
                self.sideBarView.frame.origin.x = FULL_WIDTH
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        appDelegate.isLive = false
        self.disConnectCall()
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    //inital set up
    func initialSetup() {
        self.performSelector(onMainThread: #selector(self.makeRinging), with: nil, waitUntilDone: false)
        //        self.performSelector(inBackground: #selector(self.makeRinging), with: nil)
        appDelegate.isLive = true
        HSChatSocket.sharedInstance.delegate = self
        UIApplication.shared.isIdleTimerDisabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideProperties(sender:)))
        self.view.addGestureRecognizer(tap)
        self.delegate = self
        self.cameraBtn.cornerRoundRadius()
        self.cameraBtn.backgroundColor = CIRCLE_BG_COLOR
        self.muteBtn.cornerRoundRadius()
        self.muteBtn.backgroundColor = CIRCLE_BG_COLOR
        self.cutBtn.setCorner(radius: 20)
        self.acceptBtn.setCorner(radius: 20)
        acceptBtn.setImage(UIImage.init(named: "video_accept_icon"), for: .highlighted)
        cutBtn.setImage(UIImage.init(named: "video_decline_icon"), for: .highlighted)
        self.configWebRTC()
        self.view.bringSubviewToFront(self.propertiesView)
        self.userImgView.makeItRound()
        let imgURL = URL.init(string: PROFILE_IMAGE_URL+self.user_img)
        self.userImgView.sd_setImage(with: imgURL, placeholderImage: PLACE_HOLDER_IMG)
        self.usernameLbl.config(color: .white, font: medium, align: .left, text: "")
        self.usernameLbl.text = self.user_name
        call_status = "waiting"
        if sender {
            room_id =  Utility.shared.randomID()
            HSChatSocket.sharedInstance.manageCall(receiverID: user_id, room: room_id, type: "created")
            print("MAKECALL INITIAL")
            self.makeCall(room_id, platform: self.platform)
            self.previewView.isHidden = true
            self.showPreview()
            self.view.bringSubviewToFront(self.propertiesView)
        }else{
            self.previewScreen()
        }
        self.perform(#selector(automaticallyDisConnectCall), with: nil, afterDelay: 30.0)
        
    }
    
    
    @objc func automaticallyDisConnectCall(){
        if (call_status == "waiting"){
            self.disConnectCall()
            self.notifyMissed()
            if sender{
                HSChatSocket.sharedInstance.manageCall(receiverID: user_id, room: room_id, type: "ended")
            }
        }
    }
    
    
    //change design based on the user
    func senderDesign()  {
        self.acceptBtn.isHidden = true
        self.cutBtn.frame = CGRect.init(x: (FULL_WIDTH/2)-35, y: FULL_HEIGHT - 90, width: 70, height: 70)
        self.statusLbl.config(color: .white, font: lite, align: .left, text: "calling")
        self.bottomView.frame = CGRect.init(x: (FULL_WIDTH/2)-90, y: self.cutBtn.frame.origin.y-90, width: 250, height: 70)
        
    }
    func receiverDesign()  {
        if self.viewFrom == "callkit" {
            
            self.joinToCall()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                if !self.isJoined{
                    print("MAKECALL RECEIVER")
                    self.makeCall(self.room_id, platform: self.platform)
                    self.isJoined = true
                }
            }
        }else{
            self.acceptBtn.isHidden = false
            self.acceptBtn.frame = CGRect.init(x: (FULL_WIDTH/2)-85, y: FULL_HEIGHT - 90, width: 70, height: 70)
            if call_status == "connected"{
                self.cutBtn.frame = CGRect.init(x: (FULL_WIDTH/2)-35, y: FULL_HEIGHT - 90, width: 70, height: 70)
            }else{
                self.cutBtn.frame = CGRect.init(x: self.acceptBtn.frame.origin.x+100, y: FULL_HEIGHT - 90, width: 70, height: 70)
            }
        }
        
        self.bottomView.frame = CGRect.init(x: 10, y: 50, width: 250, height: 70)
        self.statusLbl.config(color: .white, font: lite, align: .left, text: "incoming")
    }
    
    //disconnect call
    func disConnectCall()  {
        call_status = "disconnected"
        self.hangup()
        if self.av_Player.isPlaying{
            av_Player.stop()
        }
        self.dismiss(animated: true, completion: nil)
        if sender {
            UserModel.shared.alreadyInCall = false
        }
    }
    
    @objc func hideProperties(sender: UITapGestureRecognizer? = nil) {
        // handling code
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            if self.hideEnabled {
                self.hideEnabled = false
                self.sideBarView.frame.origin.x = FULL_WIDTH-100
                self.cutBtn.frame.origin.y = FULL_HEIGHT - 90
                self.acceptBtn.frame.origin.y = FULL_HEIGHT - 90
                
            }else{
                self.hideEnabled = true
                self.sideBarView.frame.origin.x = FULL_WIDTH
                self.cutBtn.frame.origin.y = FULL_HEIGHT
                self.acceptBtn.frame.origin.y = FULL_HEIGHT
                
            }
        }, completion: nil)
    }
    
    
    @IBAction func enlargeBtn(_ sender: Any) {
        self.enlargeView("")
        self.view.bringSubviewToFront(self.propertiesView)
        self.view.bringSubviewToFront(self.enlargeBtn)

    }
    
    
    @IBAction func muteBtnTapped(_ sender: Any) {
        if self.muteEnable {
            self.muteOff()
            self.muteEnable = false
            UIView.transition(with: self.muteBtn, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.muteBtn.setImage(UIImage.init(named: "muted_icon"), for: .normal)
            }, completion: nil)
            
        }else{
            self.muteOn()
            self.muteEnable = true
            UIView.transition(with: self.muteBtn, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.muteBtn.setImage(UIImage.init(named: "unmuted_icon"), for: .normal)
            }, completion: nil)
            
        }
    }
    @IBAction func cameraBtnTapped(_ sender: Any) {
        UIView.transition(with: self.cameraBtn, duration: 0.5, options: .transitionFlipFromRight, animations: nil, completion: nil)
        self.switchCamera()
    }
    @IBAction func cutBtnTapped(_ sender: Any) {
        appDelegate.endCall()
        HSChatSocket.sharedInstance.manageCall(receiverID: user_id, room: room_id, type: "ended")
        self.notifyMissed() //missed msg
        self.disConnectCall()
    }
    
   
    //send missed msg
    func notifyMissed()  {
        if sender{
            print("call status \(call_status)")
            if call_status != "connected"{
            HSChatSocket.sharedInstance.sendMsg(receiverID: user_id, msgType: "missed", msg: "missed",name:self.user_name,img:self.user_img,attachment:nil)
            }
        }
    }
    @IBAction func acceptBtnTapped(_ sender: Any) {
        self.joinToCall()
        self.makeCall(self.room_id, platform: self.platform)
    }
    
    func joinToCall()  {
        DispatchQueue.main.async {
            if self.av_Player.isPlaying{
                self.av_Player.stop()
            }
            self.isAttened = true
            self.cutBtn.frame = CGRect.init(x: (FULL_WIDTH/2)-35, y: FULL_HEIGHT - 90, width: 70, height: 70)
            self.acceptBtn.alpha = 0
            self.previewView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
            self.view.addSubview(self.previewView)
            self.view.bringSubviewToFront(self.previewView)
        }
    }
    
    //show preview camera screen
    func previewScreen(){
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .front).devices
        captureDevice = availableDevices.first
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(captureDeviceInput)
        } catch {
            print(error.localizedDescription)
        }
        let pL = AVCaptureVideoPreviewLayer(session: captureSession)
        pL.videoGravity = .resizeAspectFill
        self.previewLayer = pL
        self.previewLayer.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
        self.previewView.layer.addSublayer(self.previewLayer)
        //ADD CONSTRAINTS HERE
        captureSession.startRunning()
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString):NSNumber(value:kCVPixelFormatType_32BGRA)] as [String : Any]
        dataOutput.alwaysDiscardsLateVideoFrames = true
        if captureSession.canAddOutput(dataOutput) {
            captureSession.addOutput(dataOutput)
        }
        captureSession.commitConfiguration()
    }
    
    //MARK: APP RTC DELEGATE
    func streamDetails(_ state: Int) {
        if !sender{
            self.previewView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
            self.view.bringSubviewToFront(self.previewView)
        }
        self.propertiesView.frame = CGRect.init(x: 0, y: 0, width: FULL_WIDTH, height: FULL_HEIGHT)
        self.sideBarView.frame.origin.x = FULL_WIDTH
        self.view.bringSubviewToFront(self.propertiesView)
        self.view.bringSubviewToFront(enlargeBtn)
        
        if state == 2 { // CONNECTED STATE
            if sender{
                if self.av_Player.isPlaying{
                    av_Player.stop()
                }
                self.hidePreview()
               //reduce gem for sender
                if !self.isCharged {
                    self.reduceGems()
                    self.isCharged = true
                }
            }
            call_status = "connected"
            hideSideview = false
            self.speakerOn()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                UIView.animate(withDuration: 1.0, animations: {
                    self.previewView.alpha = 0.0
                    self.bottomView.alpha = 0.0
                }, completion: {_ in
                    UIView.animate(withDuration: 0.3, animations: {
                        self.hideEnabled = false
                        self.sideBarView.frame.origin.x = FULL_WIDTH-100
                        self.cutBtn.frame.origin.y = FULL_HEIGHT - 90
                    }, completion: nil)
                })
            }
        }else if state == 6{
            self.disConnectCall()
        }
    }
    
    //charges for call
    func reduceGems(){
        let Obj = BaseWebService()
        Obj.getDetails(subURl: "\(CHARGE_CALL_API)/\(UserModel.shared.userId)", onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.updateProfile()
            }
        })
    }
    //enable call sound
    @objc func makeRinging(){
        var audioName = String()
        var audioType = String()
        
        if sender{
            audioName = "DialerTone"
            audioType = "caf"
        }else{
            audioName = "RingTone"
            audioType = "mp3"
        }
        
        let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: audioName, ofType: audioType)!)
        let session = AVAudioSession.sharedInstance()
        
        var availbleInput = NSArray()
        availbleInput = AVAudioSession.sharedInstance().availableInputs! as NSArray
        var port = AVAudioSessionPortDescription()
        port = availbleInput.object(at: 0) as! AVAudioSessionPortDescription
        
        var _: Error?
        try? session.setPreferredInput(port)
        
        //        try? session.setCategory(AVAudioSession.Category.playAndRecord)
        try? AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .default)
        try? AVAudioSession.sharedInstance().setActive(true)
        try? session.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        
        //        if !sender {
        //            try? session.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        //        } else {
        //            try? session.overrideOutputAudioPort(AVAudioSession.PortOverride.none)
        //        }
        try? session.setActive(true)
        try! av_Player = AVAudioPlayer(contentsOf: alertSound)
        av_Player!.prepareToPlay()
        av_Player.numberOfLoops = -1
        av_Player!.play()
        
        
    }
    
    //MARK: SOCKET LISTENERS
    
    func gotSocketInfo(type: String, dict: NSDictionary?) {
        if type == "_callRejected" {
            self.statusAlert(msg: "call_mutual_alert")
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    
    
}
