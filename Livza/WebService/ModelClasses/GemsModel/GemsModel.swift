/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation


struct gemsListModel : Codable {
    var gems_list : [Gems_list]?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case gems_list = "gems_list"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gems_list = try values.decodeIfPresent([Gems_list].self, forKey: .gems_list)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
    
}

struct Gems_list : Codable {
	var gem_price : Int?
	var gem_title : String?
	var gem_count : Int?
	var gem_icon : String?
    var type : String?


	enum CodingKeys: String, CodingKey {

		case gem_price = "gem_price"
		case gem_title = "gem_title"
		case gem_count = "gem_count"
		case gem_icon = "gem_icon"
        case type = "type"

	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		gem_price = try values.decodeIfPresent(Int.self, forKey: .gem_price)
		gem_title = try values.decodeIfPresent(String.self, forKey: .gem_title)
		gem_count = try values.decodeIfPresent(Int.self, forKey: .gem_count)
		gem_icon = try values.decodeIfPresent(String.self, forKey: .gem_icon)
        type = try values.decodeIfPresent(String.self, forKey: .type)

	}

}
