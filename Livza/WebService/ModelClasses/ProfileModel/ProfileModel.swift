/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct ProfileModel : Codable {
    let show_notification : Bool?
    let paypal_id : String?
    let available_gifts : Int?
    let user_id : String?
    let premium_member : String?
    let followers : Int?
    let followings : Int?
    let age : Int?
    let privacy_contactme : Bool?
    let follow_notification : Bool?
    let follow : String?
    let status : String?
    let privacy_age : Bool?
    let gender : String?
    let location : String?
    let available_gems : Int?
    let premium_expiry_date : String?
    let gift_earnings : Int?
    let referal_link : String?
    let dob : String?
    let user_image : String?
    let chat_notification : Bool?
    let name : String?
    let blocked_by_me : String?
    let created_at : String?
    let videos_count : Int?


    
    enum CodingKeys: String, CodingKey {
        
        case show_notification = "show_notification"
        case paypal_id = "paypal_id"
        case available_gifts = "available_gifts"
        case user_id = "user_id"
        case premium_member = "premium_member"
        case followers = "followers"
        case followings = "followings"
        case age = "age"
        case privacy_contactme = "privacy_contactme"
        case follow_notification = "follow_notification"
        case follow = "follow"
        case status = "status"
        case privacy_age = "privacy_age"
        case gender = "gender"
        case location = "Location"
        case available_gems = "available_gems"
        case premium_expiry_date = "premium_expiry_date"
        case gift_earnings = "gift_earnings"
        case referal_link = "referal_link"
        case dob = "dob"
        case user_image = "user_image"
        case chat_notification = "chat_notification"
        case name = "name"
        case blocked_by_me = "blocked_by_me"
        case created_at = "created_at"
        case videos_count = "videos_count"


    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        show_notification = try values.decodeIfPresent(Bool.self, forKey: .show_notification)
        paypal_id = try values.decodeIfPresent(String.self, forKey: .paypal_id)
        available_gifts = try values.decodeIfPresent(Int.self, forKey: .available_gifts)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        premium_member = try values.decodeIfPresent(String.self, forKey: .premium_member)
        followers = try values.decodeIfPresent(Int.self, forKey: .followers)
        followings = try values.decodeIfPresent(Int.self, forKey: .followings)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        privacy_contactme = try values.decodeIfPresent(Bool.self, forKey: .privacy_contactme)
        follow_notification = try values.decodeIfPresent(Bool.self, forKey: .follow_notification)
        follow = try values.decodeIfPresent(String.self, forKey: .follow)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        privacy_age = try values.decodeIfPresent(Bool.self, forKey: .privacy_age)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        available_gems = try values.decodeIfPresent(Int.self, forKey: .available_gems)
        premium_expiry_date = try values.decodeIfPresent(String.self, forKey: .premium_expiry_date)
        gift_earnings = try values.decodeIfPresent(Int.self, forKey: .gift_earnings)
        referal_link = try values.decodeIfPresent(String.self, forKey: .referal_link)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        user_image = try values.decodeIfPresent(String.self, forKey: .user_image)
        chat_notification = try values.decodeIfPresent(Bool.self, forKey: .chat_notification)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        blocked_by_me = try values.decodeIfPresent(String.self, forKey: .blocked_by_me)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        videos_count = try values.decodeIfPresent(Int.self, forKey: .videos_count)

    }
    
}
