/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Users_list : Codable {
    let user_id : String?
    let dob : String?
    let login_id : String?
    let follow : String?
    let location : String?
    let chat_notification : Bool?
    let referal_link : String?
    let show_notification : Bool?
    let age : Int?
    let gender : String?
    let privacy_age : Bool?
    let follow_notification : Bool?
    let privacy_contactme : Bool?
    let name : String?
    let user_image : String?
    let premium_member : String?
    let last_chat : String?

    enum CodingKeys: String, CodingKey {

        case user_id = "user_id"
        case dob = "dob"
        case login_id = "login_id"
        case follow = "follow"
        case location = "location"
        case chat_notification = "chat_notification"
        case referal_link = "referal_link"
        case show_notification = "show_notification"
        case age = "age"
        case gender = "gender"
        case privacy_age = "privacy_age"
        case follow_notification = "follow_notification"
        case privacy_contactme = "privacy_contactme"
        case name = "name"
        case user_image = "user_image"
        case premium_member = "premium_member"
        case last_chat = "last_chat"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        login_id = try values.decodeIfPresent(String.self, forKey: .login_id)
        follow = try values.decodeIfPresent(String.self, forKey: .follow)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        chat_notification = try values.decodeIfPresent(Bool.self, forKey: .chat_notification)
        referal_link = try values.decodeIfPresent(String.self, forKey: .referal_link)
        show_notification = try values.decodeIfPresent(Bool.self, forKey: .show_notification)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        privacy_age = try values.decodeIfPresent(Bool.self, forKey: .privacy_age)
        follow_notification = try values.decodeIfPresent(Bool.self, forKey: .follow_notification)
        privacy_contactme = try values.decodeIfPresent(Bool.self, forKey: .privacy_contactme)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        user_image = try values.decodeIfPresent(String.self, forKey: .user_image)
        premium_member = try values.decodeIfPresent(String.self, forKey: .premium_member)
        last_chat = try values.decodeIfPresent(String.self, forKey: .last_chat)
    }

}
