/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct PartnerModel : Codable {
    let type : String?
    let partner_id : String?
    let randomKey : String?
    let user_id : String?
    let user_image : String?
    let name : String?
    let age : Int?
    let dob : String?
    let gender : String?
    let premium_member : String?
    let location : String?
    let follow : String?
    let privacy_age : Bool?
    let privacy_contactme : Bool?
    let show_notification : Bool?
    let chat_notification : Bool?
    let follow_notification : Bool?
    let referal_link : String?
    let followings : Int?
    let followers : Int?
    let platform : String?
    
    enum CodingKeys: String, CodingKey {
        
        case type = "type"
        case partner_id = "partner_id"
        case randomKey = "randomKey"
        case user_id = "user_id"
        case user_image = "user_image"
        case name = "name"
        case age = "age"
        case dob = "dob"
        case gender = "gender"
        case premium_member = "premium_member"
        case location = "location"
        case follow = "follow"
        case privacy_age = "privacy_age"
        case privacy_contactme = "privacy_contactme"
        case show_notification = "show_notification"
        case chat_notification = "chat_notification"
        case follow_notification = "follow_notification"
        case referal_link = "referal_link"
        case followings = "followings"
        case followers = "followers"
        case platform = "platform"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        partner_id = try values.decodeIfPresent(String.self, forKey: .partner_id)
        randomKey = try values.decodeIfPresent(String.self, forKey: .randomKey)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        user_image = try values.decodeIfPresent(String.self, forKey: .user_image)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        premium_member = try values.decodeIfPresent(String.self, forKey: .premium_member)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        follow = try values.decodeIfPresent(String.self, forKey: .follow)
        privacy_age = try values.decodeIfPresent(Bool.self, forKey: .privacy_age)
        privacy_contactme = try values.decodeIfPresent(Bool.self, forKey: .privacy_contactme)
        show_notification = try values.decodeIfPresent(Bool.self, forKey: .show_notification)
        chat_notification = try values.decodeIfPresent(Bool.self, forKey: .chat_notification)
        follow_notification = try values.decodeIfPresent(Bool.self, forKey: .follow_notification)
        referal_link = try values.decodeIfPresent(String.self, forKey: .referal_link)
        followings = try values.decodeIfPresent(Int.self, forKey: .followings)
        followers = try values.decodeIfPresent(Int.self, forKey: .followers)
        platform = try values.decodeIfPresent(String.self, forKey: .platform)
    }
    
}

struct RecentModel : Codable {
    let recent_list : [Recent_list]?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case recent_list = "users_list"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        recent_list = try values.decodeIfPresent([Recent_list].self, forKey: .recent_list)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
    
}
struct Recent_list : Codable {
    let dob : String?
    let name : String?
    let last_chat : String?
    let gender : String?
    let privacy_age : Bool?
    let follow_notification : Bool?
    let follow : String?
    let show_notification : Bool?
    let premium_member : String?
    let login_id : Int?
    let location : String?
    let chat_notification : Bool?
    let age : Int?
    let referal_link : String?
    let user_id : String?
    let privacy_contactme : Bool?
    let user_image : String?

    enum CodingKeys: String, CodingKey {
        
        case dob = "dob"
        case name = "name"
        case last_chat = "last_chat"
        case gender = "gender"
        case privacy_age = "privacy_age"
        case follow_notification = "follow_notification"
        case follow = "follow"
        case show_notification = "show_notification"
        case premium_member = "premium_member"
        case login_id = "login_id"
        case location = "location"
        case chat_notification = "chat_notification"
        case age = "age"
        case referal_link = "referal_link"
        case user_id = "user_id"
        case privacy_contactme = "privacy_contactme"
        case user_image = "user_image"

    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        last_chat = try values.decodeIfPresent(String.self, forKey: .last_chat)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        privacy_age = try values.decodeIfPresent(Bool.self, forKey: .privacy_age)
        follow_notification = try values.decodeIfPresent(Bool.self, forKey: .follow_notification)
        follow = try values.decodeIfPresent(String.self, forKey: .follow)
        show_notification = try values.decodeIfPresent(Bool.self, forKey: .show_notification)
        premium_member = try values.decodeIfPresent(String.self, forKey: .premium_member)
        login_id = try values.decodeIfPresent(Int.self, forKey: .login_id)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        chat_notification = try values.decodeIfPresent(Bool.self, forKey: .chat_notification)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        referal_link = try values.decodeIfPresent(String.self, forKey: .referal_link)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        privacy_contactme = try values.decodeIfPresent(Bool.self, forKey: .privacy_contactme)
        user_image = try values.decodeIfPresent(String.self, forKey: .user_image)
    }
    
}

