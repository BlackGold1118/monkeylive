//
//  BaseWebService.swift
//  Randoo
//
//  Created by HTS-Product on 15/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import SwiftyJSON
import SwiftyReceiptValidator

public typealias Parameters = [String: Any]

enum RestoreError: Error {
    case noActiveSubscriptions
}

class BaseWebService: NSObject {

   
    public func baseService(subURl: String, params: Parameters!, onSuccess success: @escaping (DataResponse<Any>) -> Void) {
        let BaseUrl = URL(string: BASE_URL+subURl)
        print("BASE URL : \(BASE_URL+subURl)")
        print("PARAMETER : \(params!)")
        if Utility.shared.isConnectedToNetwork(){
            var header:HTTPHeaders? =  nil
            if(!UserModel.shared.accessToken.isEmpty) {
                header = self.getHeaders()
            }
            
//            var alamofireManager = SessionManager()
//            
//            if subURl == START_STREAM_API{
//                print("TIME OUT SET")
//            let configuration = URLSessionConfiguration.default
//            configuration.timeoutIntervalForRequest = 60 // seconds
//            alamofireManager = Alamofire.SessionManager(configuration: configuration)
//            }
            //webservice call
            Alamofire.request(BaseUrl!, method:.post, parameters: params!, encoding: URLEncoding.httpBody, headers: header).responseJSON { response in
                //sucesss block
                print("print response : \(response)")

                switch response.result {
                case .success:
                    do {
                        let jsonResponse = try JSON.init(data: response.data!)
                        print("JSON RESPONSE: \(jsonResponse)")
                    } catch {
                        print("JSONSerialization error:", error)
                    }
                    success(response)
                    break
                case .failure(let error):
                    print("FAILURE RESPONSE: \(error.localizedDescription)")
                    if error._code == NSURLErrorTimedOut{
                    }else if error._code == NSURLErrorNotConnectedToInternet{
                    }else{
                    }
                }
            }
        }
    }
    
    func restore(_ completion: @escaping (Swift.Result<JSON, Error>) -> Void) {
        let completion: (Swift.Result<JSON, Error>) -> Void = { res in DispatchQueue.main.async { completion(res) } }
        var parameters = Parameters()
        
        parameters["user_id"] = UserModel.shared.userId
        
        let validator = SwiftyReceiptValidator(configuration: .standard, isLoggingEnabled: true)
        let request = SRVSubscriptionValidationRequest(
            sharedSecret: APP_SPECIFIC_SHARED_SECRET,
            refreshLocalReceiptIfNeeded: true,
            excludeOldTransactions: true,
            now: Date()
        )
        validator.validate(request) { result in
            switch result {
            case let .success(response):
                if let valid = response.validSubscriptionReceipts.first, let latestReceipt = response.receiptResponse.latestReceipt {
                    parameters["receipt"] = latestReceipt.base64EncodedString()
                    parameters["transaction_id"] = valid.transactionId
                    parameters["membership_id"] = valid.productId
                    
                    Alamofire.request(
                        "\(SITE_URL):3000/payments/restoreMembership/",
                        method: .post,
                        parameters: parameters,
                        headers: self.getHeaders()
                    ).responseJSON { response in
                        switch response.result {
                        case let .success(json):
                            completion(.success(JSON(json)))
                        case let .failure(error):
                            completion(.failure(error))
                        }
                    }
                } else {
                    completion(.failure(RestoreError.noActiveSubscriptions))
                }
            case let .failure(error):
                completion(.failure(error))
            }
            
            _ = validator
        }
    }
  
    //MARK: upload profile pic service
    public func uploadProfilePic(profileimage:Data, onSuccess success: @escaping (DataResponse<Any>) -> Void) {
        
        if Utility.shared.isConnectedToNetwork(){
        let BaseUrl = URL(string: BASE_URL+PROFILE_PIC_API)
        print("BASE URL : \(BASE_URL+PROFILE_PIC_API)")
        let parameters = ["user_id": "\(UserModel.shared.userId)"]
        print("REQUEST : \(parameters)")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(profileimage, withName: "profile_image", fileName: "profilepic.jpeg", mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append((value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!), withName: key)
            }
        }, to:BaseUrl!,method:.post,headers:self.getHeaders())
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                })
                upload.responseJSON { response in
                    print("RESPONSE \(response)")
                    success(response)
                }
            case .failure(let error):
                print("FAILURE RESPONSE: \(error.localizedDescription)")
                if error._code == NSURLErrorTimedOut{
                }else if error._code == NSURLErrorNotConnectedToInternet{
                }else{
                }
            }
        }
        }
    }
    
    // GET METHOD
    public func getDetails(subURl: String,onSuccess success: @escaping (DataResponse<Any>) -> Void) {

        let BaseUrl = URL(string: BASE_URL+subURl)
        print("BASE URL : \(BASE_URL+subURl)")
        if Utility().isConnectedToNetwork(){
            var header:HTTPHeaders? =  nil
            if(!UserModel.shared.accessToken.isEmpty) {
                header = self.getHeaders()
            }
            //webservice call
            Alamofire.request(BaseUrl!, method:.get, parameters: nil, encoding: URLEncoding.httpBody, headers: header).responseJSON { response in
                //sucesss block
                switch response.result {
                case .success:
                    do {
                        let jsonResponse = try JSON.init(data: response.data!)
                        //print("SUCCESS RESPONSE: \(jsonResponse)")
                    } catch {
                        print("JSONSerialization error:", error)
                    }
                    success(response)
                    break
                case .failure(let error):
                    print("FAILURE RESPONSE: \(error.localizedDescription)")
                }
            }
        }
    }
    
    // GET METHOD
    public func delete(subURl: String, onFailure failure: ((Error?) -> Void)? = nil, onSuccess success: @escaping (DataResponse<Any>) -> Void) {
        
        let BaseUrl = URL(string: BASE_URL+subURl)
        print("BASE URL : \(BASE_URL+subURl)")
        if Utility().isConnectedToNetwork(){
            var header:HTTPHeaders? =  nil
            if(!UserModel.shared.accessToken.isEmpty) {
                header = self.getHeaders()
            }
            //webservice call
            Alamofire.request(BaseUrl!, method:.delete, parameters: nil, encoding: URLEncoding.httpBody, headers: header).responseJSON { response in
                //sucesss block
                switch response.result {
                case .success:
                    do {
                        let jsonResponse = try JSON.init(data: response.data!)
                        print("SUCCESS RESPONSE: \(jsonResponse)")
                    } catch {
                        print("JSONSerialization error:", error)
                    }
                    success(response)
                    break
                case .failure(let error):
                    print("FAILURE RESPONSE: \(error.localizedDescription)")
                    failure?(error)
                }
            }
        }
    }
    
    
    
    //MARK: upload report pic service
    public func uploadReport(profileimage:Data,reportid:String, onSuccess success: @escaping (DataResponse<Any>) -> Void) {
        
        if Utility.shared.isConnectedToNetwork(){
            let BaseUrl = URL(string: BASE_URL+REPORT_ATTACHMENT_API)
            print("BASE URL : \(BASE_URL+PROFILE_PIC_API)")
            let parameters = ["report_id": reportid]
            print("REQUEST : \(parameters)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(profileimage, withName: "report_image", fileName: "profilepic.jpeg", mimeType: "image/jpeg")
                for (key, value) in parameters {
                    multipartFormData.append((value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!), withName: key)
                }
            }, to:BaseUrl!,method:.post,headers:self.getHeaders())
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                    })
                    upload.responseJSON { response in
                        print("RESPONSE \(response)")
                        success(response)
                    }
                case .failure(let error):
                    print("FAILURE RESPONSE: \(error.localizedDescription)")
                    if error._code == NSURLErrorTimedOut{
                    }else if error._code == NSURLErrorNotConnectedToInternet{
                    }else{
                    }
                }
            }
        }
    }
    
    //MARK: upload chat pic service
    public func uploadChat(imgData:Data,type:String, onSuccess success: @escaping (DataResponse<Any>) -> Void) {
        
        if Utility.shared.isConnectedToNetwork(){
            let BaseUrl = URL(string: BASE_URL+UPLOAD_CHAT_API)
            print("BASE URL : \(BASE_URL+UPLOAD_CHAT_API)")
            let parameters = ["user_id": "\(UserModel.shared.userId)"]
            print("REQUEST : \(parameters)")
            
            var mime_type = String()
            var file_name = String()

            if type == "audio"{
                mime_type = "audio/vnd.wav"
                file_name = "audio.wav"
            }else{
                mime_type = "image/jpeg"
                file_name = "msg.jpeg"
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(imgData, withName: "chat_image", fileName: file_name, mimeType: mime_type)
                for (key, value) in parameters {
                    multipartFormData.append((value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!), withName: key)
                }
            }, to:BaseUrl!,method:.post,headers:self.getHeaders())
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                    })
                    upload.responseJSON { response in
                        print("RESPONSE \(response)")
                        success(response)
                    }
                case .failure(let error):
                    print("FAILURE RESPONSE: \(error.localizedDescription)")
                    if error._code == NSURLErrorTimedOut{
                    }else if error._code == NSURLErrorNotConnectedToInternet{
                    }else{
                    }
                }
            }
        }
    }
    
    
    //MARK: upload  thumbnail
    public func uploadThumbnail(thumbnail:Data,name:String, onSuccess success: @escaping (DataResponse<Any>) -> Void) {
        
        if Utility.shared.isConnectedToNetwork(){
            let BaseUrl = URL(string: BASE_URL+UPLOAD_THUMBNAIL_API)
            print("BASE URL : \(BASE_URL+UPLOAD_THUMBNAIL_API)")
            let parameters = ["publisher_id": "\(UserModel.shared.userId ?? "")","name":"\(name)"]
            print("REQUEST : \(parameters)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(thumbnail, withName: "stream_image", fileName: "\(name).jpeg", mimeType: "image/jpeg")
                for (key, value) in parameters {
                    multipartFormData.append((value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!), withName: key)
                }
            }, to:BaseUrl!,method:.post,headers:self.getHeaders())
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                    })
                    upload.responseJSON { response in
                        print("RESPONSE \(response)")
                        success(response)
                    }
                case .failure(let error):
                    print("FAILURE RESPONSE: \(error.localizedDescription)")
                    if error._code == NSURLErrorTimedOut{
                    }else if error._code == NSURLErrorNotConnectedToInternet{
                    }else{
                    }
                }
            }
        }
    }
    //MARK: http headers
    func getHeaders() -> HTTPHeaders {
        let headers: HTTPHeaders = [
            "Authorization": UserModel.shared.accessToken,
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        return headers
    }
    
    

    
}
