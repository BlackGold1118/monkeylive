//
//  RemoteConfig.swift
//  Livza
//
//  Created by Serge Vysotsky on 26.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import FirebaseRemoteConfig

enum RemoteConfigKey: String {
    case show_in_first_launch
    case should_use_one_month_subscription
    case countdown_seconds
    case enable_offer
}

extension RemoteConfig {
    func configValue(forKey key: RemoteConfigKey) -> RemoteConfigValue {
        configValue(forKey: key.rawValue)
    }
}
