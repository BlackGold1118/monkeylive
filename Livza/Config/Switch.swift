//
//  Switch.swift
//  Livza
//
//  Created by HTS-Product on 04/09/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import Foundation

//MARK: ENABLE LIVZA  **************
let IS_LIVZA = false

// //MARK: LIVZA
// let APP_NAME = "Livza"
// //MARK: Config color
// let PRIMARY_COLOR = UIColor().hexValue(hex: "#08AC90")
// //MARK: Dynamic links
// let DYNAMIC_LINK = "https://livza.page.link" // create it from firebase dynamic link section
// let IOS_BUNDLE_ID = "com.app.livza"
// let ANDROID_PACKAGE_NAME = "com.hitasoft.app.livza" // android app package name
// let APP_STORE_ID = "1354347653" // get it from itunes connect once create the app
// //API - configuration **************
///*
// let SITE_URL = "https://livzaapp.com" // site main url
// let BASE_URL = "\(SITE_URL):3005" // base url for all api
// let WEB_SOCKET_URL = "ws://randou.appkodes.com:8083/" // socket for live streaming
// let WEB_SOCKET_CHAT_URL = "wss://livzaapp.com:3006/" // socket for chat
//*/
//let SITE_URL = "https://prodev.hitasoft.in" // site main url
//let BASE_URL = "\(SITE_URL):3001" // base url for all api
//let WEB_SOCKET_URL = "wss://prodev.hitasoft.in:3009/" // socket for live streaming
//let WEB_SOCKET_CHAT_URL = "wss://prodev.hitasoft.in:8085/" // socket for chat
//let SOCKET_IO_URL = "\(SITE_URL):3009" // base url for all api
//
// let APP_RTC_URL = "http://turn.hitasoft.in:8080" // for webrtc
// //In-App purpose
// let APP_SPECIFIC_SHARED_SECRET = "43209512e4dd41d8a3a1c81b4c4d1814" // create this from in-app puchase section in itunesconnect



//MARK: RANDOU
let APP_NAME = "MonkeyLive"
//MARK: Config color
let PRIMARY_COLOR = UIColor().hexValue(hex: "#3f1afa")
//MARK: Dynamic links
let DYNAMIC_LINK = "https://monkeylive.page.link" // create it from firebase dynamic link section
let IOS_BUNDLE_ID = "com.sppalabs.livechat"
let ANDROID_PACKAGE_NAME = "com.android.android"
let APP_STORE_ID = "1529630343"
//API - configuration **************
let SITE_URL = "https://monkeylive.app" // site main url
let BASE_URL = "\(SITE_URL):3000"  //base url for all api
let WEB_SOCKET_URL = "wss://monkeylive.app:8081/" // socket for live streaming
let WEB_SOCKET_CHAT_URL = "wss://monkeylive.app:8087/" // Message Chat Port
let APP_RTC_URL = "http://monkeylive.app:8080" // for webrtc
let SOCKET_IO_URL = "\(SITE_URL):3009"   //Live Broadcast Port
let APP_SPECIFIC_SHARED_SECRET = "798df9d6936340dcbb27e3e2a3f54c7b"


//*************************** PLIST CONFIGURATION ***************************

/*
 
 //MARK:Livza info.Plist configuration
 
 //facebook
 FacebookAppID - 139620876732542
 FacebookDisplayName - Livza
 
 //URL Types
 facebook - fb139620876732542
 google - com.googleusercontent.apps.880412617942-au345adp4uufg51d2n4bp5j1r39da2i5
 //Build settings
 Product name - Livza
 //Bundle id
 com.app.livza
 
 ***********************************************************************************
 
 //MARK:Randou info.Plist configuration
 //facebook
 FacebookAppID - 2268089100124119
 FacebookDisplayName - Randou
 
 //URL Types
 facebook - fb2268089100124119
 google - com.googleusercontent.apps.456188595787-rtcceq5bg7ei43ehqs38dd72tt8nbehv
 
 //Build settings
 Product name - Randou
 //Bundle id
 com.hitasoft.randou
 
 */

//NOTE: Change Google .plist based on product
