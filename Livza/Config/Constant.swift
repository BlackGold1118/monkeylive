//
//  Constant.swift
//  Randoo
//
//  Created by HTS-Product on 31/01/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import Foundation

//MARK: Size configuration
let FULL_WIDTH = UIScreen.main.bounds.width
let FULL_HEIGHT = UIScreen.main.bounds.height

//MARK: Device Models
let IS_IPHONE_X = UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height >= 2436
let IS_IPHONE_PLUS = UIDevice().userInterfaceIdiom == .phone && (UIScreen.main.nativeBounds.height == 2208 || UIScreen.main.nativeBounds.height == 1920)
let IS_IPHONE_678 = UIDevice().userInterfaceIdiom == .phone && (UIScreen.main.nativeBounds.height == 1334)
let IS_IPHONE_5 = UIDevice().userInterfaceIdiom == .phone && (UIScreen.main.nativeBounds.height == 1136)


let TEXT_PRIMARY_COLOR = UIColor().hexValue(hex: "#252525")
let TEXT_SECONDARY_COLOR = UIColor().hexValue(hex: "#6e6e6e")
let BG_FACEBOOK_COLOR = UIColor().hexValue(hex: "#3b5998")
let LINE_COLOR = UIColor().hexValue(hex: "#d5d5d5")
let CHAT_RIGHT_COLOR = UIColor().hexValue(hex: "#ffffff")
let GALLERY_CIRCLE_BG = UIColor().hexValue(hex: "#f0f0f0")

//Other colors
let CIRCLE_BG_COLOR = UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)
let PINK_DARK_COLOR = UIColor().hexValue(hex: "#A11771")
let STICKERVIEW_BORDER = UIColor().hexValue(hex: "#2A2A2A")
let CHAT_SEND = UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.8)
let CHAT_RECEIVE = UIColor().hexValue(hex: "#FFFFFF")
let BLACK_COLOR = UIColor.black
let LIVE_COLOR = UIColor(red: 219.0/255.0, green: 32.0/255.0, blue: 70.0/255.0, alpha: 1.0)
let TEXT_COLOR = UIColor(red: 58.0/255.0, green: 58.0/255.0, blue: 58.0/255.0, alpha: 1.0)

//MARK: image config
let PLACE_HOLDER_IMG = UIImage.init(named: "user_placeholder")

//MARK: Config Font
let APP_FONT_REGULAR = "ArgentumSans-Regular"
let APP_FONT_LIGHT = "ArgentumSans-Light"

//MARK: Font sizes
let high = UIFont.init(name: APP_FONT_REGULAR, size: 28)
let averageReg = UIFont.init(name: APP_FONT_REGULAR, size: 19)
let mediumReg = UIFont.init(name: APP_FONT_REGULAR, size: 17)
let liteReg = UIFont.init(name: APP_FONT_REGULAR, size: 15)
let smallReg = UIFont.init(name: APP_FONT_REGULAR, size: 13)
let lowReg = UIFont.init(name: APP_FONT_REGULAR, size: 11)

let average = UIFont.init(name: APP_FONT_LIGHT, size: 19)
let medium = UIFont.init(name: APP_FONT_LIGHT, size: 17)
let lite = UIFont.init(name: APP_FONT_LIGHT, size: 15)
let small = UIFont.init(name: APP_FONT_LIGHT, size: 13)
let low = UIFont.init(name: APP_FONT_LIGHT, size: 11)

//MARK: user status
let OFFLINE = "0"
let ONLINE = "1"
let IN_SEARCH = "2"
let IN_CHAT =  "3"

let RISTRICTED_CHARACTERS = "'*=+[]\\|;:'\",<>/?%!@#$^&(){}[].~-_£€₹ "


let EMPTY_STRING = ""

let APPHUB_SDK_TOKEN = "app_8pPwyTpRCGyL89TsinMKDnJesjNahd"
let APPHUB_STORE_SHARED_SECRET = "798df9d6936340dcbb27e3e2a3f54c7b"

let GEMICONS: [String] = ["ic_gems_150", "ic_gems_350", "ic_gems_850", "ic_gems_1850", "ic_gems_3950", "ic_gems_8500"]
let GEMIDS: [String] = ["com.gems.150", "com.gems.350", "com.gems.850", "com.gems.1850", "com.gems.3950", "com.gems.8500"]

let MONEKYLIVONEMONTH = "com.monkeylivevip.1month"
let MONEKYLIVONEMONTHOFFER = "com.monkeylivevip.1monthoffer"

