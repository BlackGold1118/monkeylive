//
//  Config.swift
//  Randoo
//
//  Created by HTS-Product on 31/01/19.
//  Copyright © 2019 Hitasoft. All rights reserved.

import Foundation

//MARK: Default Language **************
let DEFAULT_LANGUAGE = "English" // default app language

//MARK: Encrypt Key **************
let ENCRYPT_KEY = "crypt@123" // message encryption key.

//MARK: PUSH NOTIFICATION **************
let DEVICE_MODE = "1" //0-Development, 1-Production
//Change entitlements APS Environment "development/production"
//AppDelegate change APNSToken type "sandbox/prod" under "didRegisterForRemoteNotificationsWithDeviceToken"

//MARK: In-App purchase Validation **************
//let IAP_VALIDATION_URL = "https://sandbox.itunes.apple.com/verifyReceipt" // sandbox
let IAP_VALIDATION_URL = "https://buy.itunes.apple.com/verifyReceipt" // live



//API - attachment URL **************
let PROFILE_IMAGE_URL = "\(SITE_URL)/public/img/accounts/"
let GIFT_IMAGE_URL = "\(SITE_URL)/public/img/gifts/"
let PRIME_IMAGE_URL = "\(SITE_URL)/public/img/slider/"
let GEMS_IMAGE_URL = "\(SITE_URL)/public/img/gems/"
let CHAT_IMAGE_URL = "\(SITE_URL)/public/img/chats/"

//status
let SUCCESS = "true"
let FAILURE = "false"

//RANDOU API's
let SIGNIN_API = "/accounts/signin"
let SIGNUP_API = "/accounts/signup"
let PROFILE_API = "/accounts/profile"
let PROFILE_PIC_API = "/accounts/uploadprofile"
let RTC_PARAMS_API = "/chats/rtcparams"
let IN_SEARCH_API = "/chats/insearch"
let FOLLOWERS_API = "/activities/followerslist"
let FOLLOWINGS_API = "/activities/followingslist"
let FOLLOW_API = "/activities/follow"
let PUSH_SIGNIN_API = "/devices/register"
let PUSH_SIGNOUT_API = "/devices/unregister"
let TERMS_API = "/helps/loginterms"
let APP_DEFAULT_API = "/activities/appdefaults"
let REPORT_USER_API = "/activities/reportuser"
let REPORT_ATTACHMENT_API = "/activities/uploadreport"
let GEMS_LIST_API = "/activities/gemstore"
let GEMS_PURCHASE_API = "/payments/purchasegems"
let PRIME_PURCHASE_API = "/payments/subscription"
let RECENT_VIDEOCHATS_API = "/chats/recentvideochats"
let DELETE_VIDEOCHATS_API = "/chats/clearvideochats"
let GIFT_TO_GEM_API = "/activities/gifttogems"
let HELP_API = "/helps/allterms"
let REWARD_VIDEO_API = "/accounts/rewardvideo"
let UPLOAD_CHAT_API = "/accounts/upmychat"
let ADMIN_MSG_API = "/activities/adminmessages"
let CHECK_ACTIVE_API = "/accounts/isActive"
let RENEWAL_SUBSCRIPTION = "/payments/autorenewalsubscription"
let INVITE_CREDITS = "/accounts/invitecredits"
let CHARGE_CALL_API = "/accounts/chargecalls"
let DELETE_ACCOUNT_API = "/accounts/"

//LIVZA API's
let LIVE_STREAM_API = "/livestreams"
let ALL_USER_API = "/accounts/showall"
let CREATE_STREAM_API = "/livestreams/createstream"
let START_STREAM_API = "/livestreams/startstream"
let STOP_STREAM_API = "/livestreams/stopstream"
let UPLOAD_THUMBNAIL_API = "/livestreams/uploadstream"
let DELETE_VIDEO_API = "/livestreams/deletestream"
let REPORT_STREAM_API = "/livestreams/reportstream"
let STREAM_INFO_API = "/livestreams/streamdetails"

//API - Social login details for reference **************
/*
 Facebook :
 Username - socialapps@hitasoft.com
 Password - ********
 Firebase :
 Username - hitasoftsocialapp@gmail.com
 Password - ********
 */
