//
//  Utility.swift
//  Randoo
//
//  Created by HTS-Product on 06/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit
import Lottie
import SystemConfiguration

var lottieView = AnimationView()

class Utility: NSObject {
    static let shared = Utility()
    static let language = Utility().appLanguage
    private static let reachability: Reachability? = {
        let reachability = Reachability()
        try? reachability?.startNotifier()
        return reachability
    }()

    func showLoader()  {
        lottieView.isHidden  = false
        lottieView.play{ (finished) in
            // Do Something
        }
    }
    func hideLoader()  {
        lottieView.stop()
//        lottieView.isHidden  = true
    }

    func addLoader(customView:UIView)  {
        lottieView = AnimationView(name: "findLoader")
        if IS_IPHONE_PLUS {
            lottieView.frame = CGRect.init(x: 0, y: FULL_HEIGHT/2-115, width: FULL_WIDTH, height: 230)
        }else{
            lottieView.frame = CGRect.init(x: 0, y: FULL_HEIGHT/2-105, width: FULL_WIDTH, height: 210)
        }
        lottieView.loopMode = .loop
        customView.addSubview(lottieView)
        lottieView.isHidden  = true
    }
    
    //MARK: Get Random like heart color
    func likeHexColorCode() -> String {
        let likeColorArray = ["#05ac90","#ac5b05","#ac1905","#8305ac","#ac0577","#0563ac","#7bac05"]
        let randomIndex = Int(arc4random_uniform(UInt32(likeColorArray.count)))
        return (likeColorArray[randomIndex] )
    }
    //MARK: Configure app language
    func configLanguage()  {
        if let path = Bundle.main.path(forResource:UserModel.shared.language, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                self.appLanguage = jsonResult as! NSDictionary
            } catch {
                // handle error
            }
        }
    }
    //MARK:set App language
    @UserDefault(.app_language)
    var appLanguage: NSDictionary
    
    @UserDefault(.language)
    var language: String

    //MARK:set PrimePackage
    @OptionalUserDefault(.first_prime_package)
    var primePackage: NSDictionary?
    
    //banner ad id
    @UserDefault(.ads_unit_id)
    var adUnitId: String
    
    //video ad id
    @UserDefault(.video_ads_unit_id)
    var adsVideoId: String
    
    //google ads
    @UserDefault(.ads_enable)
    private var isAdsEnabled: Bool
    
    func adsEnable(status: Bool){
        isAdsEnabled = status
    }
    
    func isAdsEnable() -> Bool {
        UserModel().isPremium ? false : isAdsEnabled
    }
    
    //MARK:NOTIFICATION REDIRECTION
    @UserDefault(.notification_info)
    var notificationInfo: NSDictionary
    
    func setMsg(scope:String,name:String,id:String,img:String){
        isNotificationRedirect = true
        let dict = NSMutableDictionary()
        dict.setValue(scope, forKey: "scope")
        dict.setValue(name, forKey: "user_name")
        dict.setValue(id, forKey: "user_id")
        dict.setValue(img, forKey: "user_image")
        notificationInfo = dict
    }
    func setCall(dict:NSDictionary){
        isNotificationRedirect = true
        notificationInfo = dict
    }
    func setFollow(scope:String,id:String){
        isNotificationRedirect = true
        let dict = NSMutableDictionary()
        dict.setValue(scope, forKey: "scope")
        dict.setValue(id, forKey: "user_id")
        notificationInfo = dict
    }
    
    @UserDefault(.notification_redirect)
    var isNotificationRedirect: Bool
    
    func getNotificationInfo() -> NSDictionary? {
        notificationInfo
    }
    
    func clearRedirectionInfo()  {
        isNotificationRedirect = false
    }
    //MARK: Convert string to dict
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
   
    //MARK: Network rechability
    func isConnectedToNetwork() -> Bool {
        /*var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability! , &flags) == false {
            return false
        }
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        return ret*/
        return Utility.reachability?.isReachable == true
    }
    
    //push notification register
    func pushsignIn(){
        if !UserModel.shared.userId.isEmpty && !UserModel.shared.VOIP.isEmpty {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.VOIP, forKey: "voip_token")
        requestDict.setValue(UserModel.shared.FCM, forKey: "fcm_token")
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue("0", forKey: "device_type")//android or ios
        requestDict.setValue(DEVICE_MODE, forKey: "device_mode")//push notification
        requestDict.setValue(UIDevice.current.identifierForVendor!.uuidString, forKey: "device_id")
        requestDict.setValue(UIDevice.current.model, forKey: "device_model")
        Obj.baseService(subURl: PUSH_SIGNIN_API, params: requestDict as? Parameters, onSuccess: {response in
                let dict = response.result.value as? NSDictionary
                let status = dict?.value(forKey: "status") as! String
                if status == "true"{
                }
            })
        }
    }
    //push notification register
    func pushsignOut(){
        if !UserModel.shared.userId.isEmpty && !UserModel.shared.VOIP.isEmpty{
            let Obj = BaseWebService()
            Obj.delete(subURl: "\(PUSH_SIGNOUT_API)/\(UIDevice.current.identifierForVendor!.uuidString)", onSuccess: {response in
                
            })
        }
    }
    
    //get app default details
    func getDefaultDetails()  {
        let Obj = BaseWebService()
        Obj.getDetails(subURl: "\(APP_DEFAULT_API)/ios", onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.setDefaults(dict: dict!)
                
                /*self.setAds(unitId:dict?.value(forKey: "google_ads_client") as! String)
                self.setVidoeAds(id: dict?.value(forKey: "video_ads_client") as! String)*/
                
                self.adUnitId = dict?.value(forKey: "google_ads_client") as? String ?? ""
                self.adsVideoId = dict?.value(forKey: "video_ads_client") as? String ?? ""
                let showAds = dict?.value(forKey: "show_ads") as! String
                if showAds == "1"{
                    self.adsEnable(status: true)
                }else{
                    self.adsEnable(status: false)
                }
            }
        })
    }
    
    //get formatted json from array
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    //set Default filter
    func setDefaultFilter()  {
        if !UserModel.shared.userId.isEmpty{
            //set default filter location
            guard let defaultDict:NSDictionary = UserModel.shared.getDefaults() else { return }
            //set location names
            let locationArray = NSMutableArray()
            locationArray.addObjects(from: defaultDict.value(forKey: "locations") as! [Any])
            let allLoc = Utility.language.value(forKey: "select_all") as! String
            locationArray.add(allLoc)
            UserDefaultKey.location.set(locationArray)
            //set partner ids
            let partnerArray = NSMutableArray()
            partnerArray.add(UserModel.shared.userId as Any)
            UserDefaultKey.partner_list.set(partnerArray)
            //set other filter values
            UserDefaultKey.min_age.set("18")
            UserDefaultKey.max_age.set("99")
            UserDefaultKey.filter_applied.set("0")
            UserDefaultKey.gender.set("both")
        }
    }
    
    func height(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: width, height:  CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    //convert timestamp with required format
    func timeStamp(time:Date,format:String) -> String {
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone.current //Set timezone that you want
        dateFormat.locale = NSLocale.current
        dateFormat.dateFormat = format
        return dateFormat.string(from: time)
    }
    
    func makeFormat(to:Date) -> String {
        let calendar = NSCalendar.current
        if calendar.isDateInToday(to) {
            return "Today, \(self.timeStamp(time: to, format: "hh:mm a"))"
        }else if calendar.isDateInYesterday(to){
            return "Yesterday, \(self.timeStamp(time: to, format: "hh:mm a"))"
        }else{
            return self.timeStamp(time: to, format: "dd/MM/yyyy hh:mm a")
        }
    }
  
    //get current utc time
    func getUTCTime() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let utcTimeZoneStr = formatter.string(from: date)
        return utcTimeZoneStr
    }
    //get random id
    func randomID()-> String  {
        let timestamp = NSDate().timeIntervalSince1970
        return "\(UserModel.shared.userId)\(String(format: "%.0f", timestamp.rounded()))"
    }
    func newID()-> String  {
        let timestamp = NSDate().timeIntervalSince1970
        return "\(UserModel.shared.userId)234324SFSD\(String(format: "%.0f", timestamp.rounded()))"
    }
    func goToLoginPage()  {
        appDelegate.moveLoginPage()
    }
    //MARK: Check string is empty
    func checkEmpty(str:String) -> Bool {
        if  (str == "") || (str == "NULL") || (str == "(null)") || (str == "<null>") || (str == "Json Error") || (str.isEmpty) ||  str.trimmingCharacters(in: .whitespaces).isEmpty || str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty  {
            return true
        }
        return false
    }
    //invite credits
    func inviteCredits()  {
        let obj = BaseWebService()
        obj.getDetails(subURl: "\(INVITE_CREDITS)/\(UserModel.shared.inviteCode)", onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.deleteInviteCode()
            }
        })
    }
    //check auto newal subacription inapp purchase
    func iapActiveStatus() {
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                let receiptString = receiptData.base64EncodedString(options: [])
                let dict = ["receipt-data" : receiptString, "password" : APP_SPECIFIC_SHARED_SECRET] as [String : Any]
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                    if let iapBaseURL = Foundation.URL(string:IAP_VALIDATION_URL) {
                        var request = URLRequest(url: iapBaseURL)
                        request.httpMethod = "POST"
                        request.httpBody = jsonData
                        let session = URLSession(configuration: URLSessionConfiguration.default)
                        let task = session.dataTask(with: request) { data, response, error in
                            if let receivedData = data,
                                let httpResponse = response as? HTTPURLResponse,
                                error == nil,
                                httpResponse.statusCode == 200 {
                                do {
                                    print("Receipt response \(dict)")
                                    print(response)
                                    if let jsonResponse = try JSONSerialization.jsonObject(with: receivedData, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject> {
                                        
                                        print("heng", jsonResponse)
                                        var dict = NSDictionary()
                                        dict = jsonResponse as NSDictionary
                                        guard let receiptArray = dict.value(forKey: "latest_receipt_info") as? NSArray else { return }
                                        if receiptArray.count != 0 {
                                            print(receiptArray)
                                            guard let receiptDict = receiptArray.lastObject as? NSDictionary else { return }
                                            print("last dict \(receiptDict)")
                                            let last_expiryDate = receiptDict["expires_date_ms"]
                                            let updateOBj = BaseWebService()
                                            let request = NSMutableDictionary()
                                            request.setValue(UserModel.shared.userId, forKey: "user_id")
                                            request.setValue(receiptDict["product_id"], forKey: "package_id")
                                            request.setValue(last_expiryDate, forKey: "renewal_time")
                                            request.setValue("ios", forKey: "platform")
                                            updateOBj.baseService(subURl: RENEWAL_SUBSCRIPTION, params:  request as? Parameters, onSuccess: {response in
                                                let dict = response.result.value as? NSDictionary
                                                let status = dict?.value(forKey: "status") as! String
                                                if status == "true"{
                                                    UserModel.shared.updateProfile()
                                                }
                                            })
                                        }
                                    } else { print("Failed to cast serialized JSON to Dictionary<String, AnyObject>") }
                                }
                                catch { print("Couldn't serialize JSON with error: " + error.localizedDescription) }
                            }
                        }
                        task.resume()
                    } else { print("Couldn't convert string into URL. Check for special characters.") }
                }
                catch { print("Couldn't create JSON with error: " + error.localizedDescription) }
            }
            catch { print("Couldn't read receipt data with error: " + error.localizedDescription) }
        }
    }

    
}

extension Int {
        var kmFormatted: String {
            let thousandNum = self/1000
            let millionNum = self/1000000
            if self >= 1000 && self < 1000000{
                if(thousandNum == thousandNum){
                    return("\(thousandNum)K")
                }
                return("\(thousandNum)K")
            }
            if self > 1000000{
                if(millionNum == millionNum){
                    return("\(millionNum)M")
                }
                return ("\(millionNum)M")
            }
            else{
                if(self == self){
                    return ("\(self)")
                }
                return ("\(self)")
            }
        }
    
    
    
}

extension Float {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}


//MESSAGE SECTION
extension Utility{
    func addToLocal(dict:NSDictionary) {
        var userid = dict.value(forKey: "user_id") as! String
        let receiverid = dict.value(forKey: "receiver_id") as! String
        if userid == UserModel.shared.userId{
            userid = receiverid
        }
        
        if (userid == UserModel.shared.userId) && (receiverid == UserModel.shared.userId){
        }else{
            //RECENT CHATS *********************
            if !Entities.sharedInstance.userExist(user_id: userid) {
                Entities.sharedInstance.addRecent(name: dict.value(forKey: "user_name") as! String,
                                          user_id: userid,
                                          time: dict.value(forKey: "chat_time") as! String,
                                          msg: dict.value(forKey: "message") as! String,
                                          user_image: dict.value(forKey: "user_image") as! String,
                                          msg_id: dict.value(forKey: "msg_id") as! String,
                                          msg_type: dict.value(forKey: "msg_type") as! String,chat_type:"user_chat")
            }else{
                Entities.sharedInstance.updateRecent(name: dict.value(forKey: "user_name") as! String,
                                                     user_id: userid,
                                                     time: dict.value(forKey: "chat_time") as! String,
                                                     msg: dict.value(forKey: "message") as! String,
                                                     user_image: dict.value(forKey: "user_image") as! String,
                                                     msg_id: dict.value(forKey: "msg_id") as! String,
                                                     msg_type: dict.value(forKey: "msg_type") as! String)
            }
            
            //CHECK AND ADD DATE STICKY IN MESSAEGS*********
            var lastMsg:Messages?
            lastMsg = Entities.sharedInstance.getLast(user_id: userid)
            var stickyAdded = false
            if lastMsg != nil {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                
                if let currentMsgTime = (dict.value(forKey: "chat_time") as? String).flatMap(formatter.date),
                    let lastMsgTime = (lastMsg?.time).flatMap(formatter.date) {
                    let currentDate = Utility.shared.timeStamp(time: currentMsgTime, format: "MMMM dd yyyy")
                    let lastDate = Utility.shared.timeStamp(time: lastMsgTime, format: "MMMM dd yyyy")
                    
                    if currentDate != lastDate {
                        self.addStickyDate(dict: dict)
                        stickyAdded = true
                    }
                }
            } else {
                self.addStickyDate(dict: dict)
                stickyAdded = true
            }
            
            var msgID = String()
            var msgTime = String()

            if stickyAdded{
                msgID = Utility.shared.newID()
                msgTime = Utility.shared.getUTCTime()
            }else{
                msgID = dict.value(forKey: "msg_id") as! String
                msgTime = dict.value(forKey: "chat_time") as! String

            }
            
            //MESSAGE SECTION ******************
            Entities.sharedInstance.addMsg(name: dict.value(forKey: "user_name") as! String,
                                        user_id: dict.value(forKey: "user_id") as! String,
                                        time: msgTime,
                                        msg: dict.value(forKey: "message") as! String,
                                        user_image: dict.value(forKey: "user_image") as! String,
                                        msg_id:msgID,
                                        msg_type: dict.value(forKey: "msg_type") as! String,
                                        status: "unread",
                                        receiver_id:receiverid)
        }

    }
    
    //add offline msg
    func addOfflineMsg(dict:NSDictionary)  {
        let msgArray = dict.value(forKey: "records") as! NSArray
        for msg in msgArray {
            self.addToLocal(dict: msg as! NSDictionary)
        }
    }
    
    //update  read status
    func updateReadStatus(dict:NSDictionary)  {
        let user_id = dict.value(forKey: "user_id") as! String
        _ = Entities.sharedInstance.getLastMsgID(user_id:user_id,type:"instant")
        HSChatSocket.sharedInstance.passDetails(type: "_receiveReadStatus", dict:dict)
    }
    
    //add offline read status
    func addOfflineReadStatus(dict:NSDictionary)  {
        let readChatsArray  = dict.value(forKey: "records") as! NSArray
        for chats in readChatsArray {
            self.updateReadStatus(dict: chats as! NSDictionary)
        }
    }
    //add sticky header
    func addStickyDate(dict:NSDictionary) {
        print("msg id date \(dict.value(forKey: "msg_id") as! String)")

        var userid = dict.value(forKey: "user_id") as! String
        let receiverid = dict.value(forKey: "receiver_id") as! String
        if userid == UserModel.shared.userId{
            userid = receiverid
        }
        Entities.sharedInstance.addMsg(name: dict.value(forKey: "user_name") as! String,
                                       user_id: dict.value(forKey: "user_id") as! String,
                                       time: dict.value(forKey: "chat_time") as! String,
                                       msg: dict.value(forKey: "message") as! String,
                                       user_image: dict.value(forKey: "user_image") as! String,
                                       msg_id: dict.value(forKey: "msg_id") as! String,
                                       msg_type: "sticky",
                                       status: "unread",
                                       receiver_id:receiverid)
    }
    
    //ADMIN MESSAGE SECTION *****************
    func addAdminInitialMsg()  {
        let user_id = APP_NAME
        let user_name = "\(user_id) Team"
        guard let defaultDict = UserModel.shared.getDefaults() else { return }
        let msg = defaultDict.value(forKey: "welcome_message") as! String

        let cryptLib = CryptLib()
        let encryptedText = cryptLib.encryptPlainTextRandomIV(withPlainText:msg, key: ENCRYPT_KEY)
        let timestamp = NSDate().timeIntervalSince1970

        Entities.sharedInstance.addRecent(name: user_name,
                                          user_id:user_id,
                                          time: Utility.shared.getUTCTime(),
                                          msg: encryptedText!,
                                          user_image: "",
                                          msg_id: Utility.shared.randomID(),
                                          msg_type:"text",chat_type:"admin_chat")
        //initial date
        Entities.sharedInstance.addMsg(name:user_name,
                                       user_id: user_id,
                                       time: Utility.shared.getUTCTime(),
                                       msg: encryptedText!,
                                       user_image: "",
                                       msg_id: Utility.shared.newID(),
                                       msg_type: "sticky",
                                       status: "unread",
                                       receiver_id:String(format: "%.0f", timestamp.rounded()))
        //new msg
        Entities.sharedInstance.addMsg(name:user_name,
                                       user_id: user_id,
                                       time: Utility.shared.getUTCTime(),
                                       msg: encryptedText!,
                                       user_image: "",
                                       msg_id: Utility.shared.randomID(),
                                       msg_type: "text",
                                       status: "unread",
                                       receiver_id:String(format: "%.0f", timestamp.rounded()))
        self.getAdminMsg()
    }
    
    //check blocked status
    func checkBlocked()  {
        let Obj = BaseWebService()
        Obj.getDetails(subURl: "\(CHECK_ACTIVE_API)/\(UserModel.shared.userId)", onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "false"{
                
                let alertVC = CPAlertVC(title:APP_NAME, message: Utility.language.value(forKey: "acc_blocked") as! String)
                                               alertVC.animationType = .bounceUp
                                               alertVC.addAction(CPAlertAction(title: "Ok", type: .normal, handler: {
                                                 UserModel.shared.logoutFromAll()
                                                 Utility.shared.goToLoginPage()
                                               }))
                alertVC.show(into: UIApplication.shared.keyWindow?.rootViewController)
               
            }
        })
    }
    //get all admin messages
    func getAdminMsg()  {
        let last_msg = Entities.sharedInstance.getLast(user_id: APP_NAME)
        if last_msg != nil {
            
        do{
            let jsonDecoder = JSONDecoder()
            let profile = try jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData)
            let Obj = BaseWebService()
            Obj.getDetails(subURl: "\(ADMIN_MSG_API)/\(UserModel.shared.userId)/\(profile.created_at!)/\(last_msg!.receiver_id!)", onSuccess: {response in
                let dict = response.result.value as? NSDictionary
                let status = dict?.value(forKey: "status") as! String
                if status == "true"{
                    let msgArray = dict!.value(forKey: "message_data") as! NSArray
                    for admin in msgArray {
                        self.addAdminMsgs(dict: admin as! NSDictionary)
                    }
                }
            })
        }catch{
        }
        }else{
            self.addAdminInitialMsg()
        }

    }
    
    func addAdminMsgs(dict:NSDictionary) {
        let userid = APP_NAME
        let user_name = "\(userid) Team"

        let receiverid = dict.value(forKey: "msg_at") as! NSNumber
        let msg = dict.value(forKey: "msg_data") as! String
        
        let cryptLib = CryptLib()
        let encryptedText = cryptLib.encryptPlainTextRandomIV(withPlainText:msg, key: ENCRYPT_KEY)

        Entities.sharedInstance.updateRecent(name: user_name,
                                                     user_id: userid,
                                                     time: dict.value(forKey: "created_at") as! String,
                                                     msg: encryptedText!,
                                                     user_image: "",
                                                     msg_id: dict.value(forKey: "msg_id") as! String,
                                                     msg_type: dict.value(forKey: "msg_type") as! String)
        
            //CHECK AND ADD DATE STICKY IN MESSAEGS*********
            var lastMsg:Messages?
            lastMsg = Entities.sharedInstance.getLast(user_id: userid)
            var stickyAdded = false
            if lastMsg != nil {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                
                if let currentMsgTime = (dict.value(forKey: "chat_time") as? String).flatMap(formatter.date),
                    let lastMsgTime = (lastMsg?.time).flatMap(formatter.date) {
                    let currentDate = Utility.shared.timeStamp(time: currentMsgTime, format: "MMMM dd yyyy")
                    let lastDate = Utility.shared.timeStamp(time: lastMsgTime, format: "MMMM dd yyyy")
                    
                    if currentDate != lastDate {
                        let time = dict.value(forKey: "msg_at") as! NSNumber
                        Entities.sharedInstance.addMsg(
                            name:user_name,
                            user_id: userid,
                            time: dict.value(forKey: "created_at") as! String,
                            msg: encryptedText!,
                            user_image: "",
                            msg_id: Utility.shared.randomID(),
                            msg_type: "sticky",
                            status: "unread",
                            receiver_id:time.stringValue
                        )
                        
                        stickyAdded = true
                    }
                }
            }
            
            var msgID = String()
            if stickyAdded{
                msgID = Utility.shared.newID()
            }else{
                msgID = dict.value(forKey: "msg_id") as! String
            }
            
            //MESSAGE SECTION ******************
            Entities.sharedInstance.addMsg(name: user_name,
                                           user_id: userid,
                                           time: dict.value(forKey: "created_at") as! String,
                                           msg: encryptedText!,
                                           user_image: "",
                                           msg_id:msgID,
                                           msg_type: dict.value(forKey: "msg_type") as! String,
                                           status: "unread",
                                           receiver_id:receiverid.stringValue)
        
        
    }
    
    

  
    func timeAgoSinceDate(_ date:Date, numericDates:Bool = false,type:NSString) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < date ? now : date
        let latest = (earliest == now) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) min ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) sec ago"
        } else {
            return "Just now"
        }
        
    }
    //MARK: Check string is empty
    func checkEmptyWithString(value:String) -> Bool {
        if  (value == "") || (value == "NULL") || (value == "(null)") || (value == "<null>") || (value == "Json Error") || (value == "0") || (value.isEmpty) ||  value.trimmingCharacters(in: .whitespaces).isEmpty || value.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty  || value == nil{
            return true
        }
        return false
    }
    //MARK: Alert view
    public func showAlertWithTitle(alertTitle:NSString, alertMsg:NSString,viewController:UIViewController){
        
        let alertVC = CPAlertVC(title: alertTitle as String, message: alertMsg as String)
        alertVC.animationType = .bounceUp
        alertVC.show(into: viewController)
    }
}
