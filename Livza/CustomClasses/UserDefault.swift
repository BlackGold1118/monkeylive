//
//  UserDefault.swift
//  Livza
//
//  Created by Serge Vysotsky on 06.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import Foundation

protocol UserDefaultWrapperStore {
    associatedtype Value
    var key: UserDefaultKey { get }
    init(_ key: UserDefaultKey, default value: Value)
    
    var wrappedValue: Value { get set }
    func removeObject()
}

extension UserDefaultWrapperStore {
    var defaults: UserDefaults {
        .standard
    }
    
    func removeObject() {
        defaults.removeObject(forKey: key.rawValue)
    }
}

@propertyWrapper
struct UserDefault<Value>: UserDefaultWrapperStore {
    let key: UserDefaultKey
    
    init(_ key: UserDefaultKey, default value: Value) {
        self.key = key
        defaults.register(defaults: [key.rawValue: value])
    }
    
    var wrappedValue: Value {
        set { defaults.set(newValue, forKey: key.rawValue) }
        get { defaults.value(forKey: key.rawValue) as! Value }
    }
}

@propertyWrapper
struct OptionalUserDefault<Value>: UserDefaultWrapperStore {
    let key: UserDefaultKey
    
    init(_ key: UserDefaultKey, default value: Value?) {
        self.key = key
        if let value = value {
            defaults.register(defaults: [key.rawValue: value])
        }
    }
    
    init(_ key: UserDefaultKey) {
        self.init(key, default: nil)
    }
    
    var wrappedValue: Optional<Value> {
        get { defaults.value(forKey: key.rawValue) as? Value }
        set {
            newValue.map {
                defaults.set($0, forKey: key.rawValue)
            } ?? removeObject()
        }
    }
}

enum UserDefaultKey: String {
    // UserModel
    case home_page_scroll
    case local_user_id
    case language_name
    case user_local_details
    case user_voip_id
    case user_fcm_id
    case user_partner_details
    case turn_server_config
    case web_rtc_web
    case user_accessToken
    case touch_to_start
    case profile_complete_status
    case firs_login
    case user_login_type
    case user_login_id
    case user_fb_details
    case signin_apple_id
    case signin_mail_id
    case basicinfo_enabled
    case profilepic_enabled
    case app_defaults_info
    case my_premium_status
    case already_subscribed
    case my_gem_count
    case video_call_enabled
    case previous_caller_id
    case watch_video_enddate
    case invite_code
    
    // Utility
    case app_language
    case language
    case first_prime_package
    case ads_unit_id
    case video_ads_unit_id
    case ads_enable
    case notification_info
    case notification_redirect
    case location
    case partner_list
    case min_age
    case max_age
    case filter_applied
    case gender
    
    case isFirstLaunch
    
    // login
    case authVerificationID
    
    // offer
    case offerCountdown
    case lastCountdownDate
}

extension UserDefaultKey {
    func removeObject(_ defaults: UserDefaults = .standard) {
        defaults.removeObject(forKey: rawValue)
    }
    
    func set<Value>(_ value: Value, to defaults: UserDefaults = .standard) {
        defaults.set(value, forKey: rawValue)
    }
}

protocol EmptyInitable { init() }
extension UserDefault where Value: EmptyInitable {
    init(_ key: UserDefaultKey) {
        self.init(key, default: Value())
    }
}

extension String: EmptyInitable {}
extension Data: EmptyInitable {}
extension Bool: EmptyInitable {}
extension NSDictionary: EmptyInitable {}
extension Int: EmptyInitable {}
