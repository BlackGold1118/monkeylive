//
//  Event.swift
//  Livza
//
//  Created by Serge Vysotsky on 12.07.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import Amplitude
import FirebaseAnalytics

enum Event: String {
    case Subscribe_1Month
    case Subscribe_1Month_40_offer = "Subscribe_1Month/40%offer"
    case Subscribe_3Pricing_1Month = "Subscribe_3Pricing/1Month"
    case Subscribe_3Pricing_3Months = "Subscribe_3Pricing/3Months"
    case Subscribe_3Pricing_1Year60_Offer = "Subscribe_3Pricing/1Year60%Offer"
    
    case FacebookSignIn
    case AppleSignin
    case PhoneNumberSignin
    
    case AccountDeleted
    case StartLiveChat      // (when you click start live chat ...)
    case MatchedOnLiveChat  // (when the user is matched with someone ...)
    case MatchedFemale      // (when the user is matched with female ...)
    case MatchedMale        // (when the user is matched with male ...)
    case StopLiveChat       // (when user exits live chat)
    case FollowFriend
    case UnfollowFriend
    case DirectMessageSent  // (when user sends a message in private chat ...)
    case VideoCall          // (when user made a video call with someone in private ...)
    
    case AgeFilterApplied
    case FemaleFilterApplied
    case MaleFilterApplied
    case LocationFilterApplied
    case NoUserFound
}

extension Event {
    // GemPurchase-Amount (Amount = how much gems ...)
    static func logGemPurchase(_ amount: Int) {
        logEvent("GemPurchase-\(amount)", parameters: nil)
    }
    
    // SendGift-(Gift Name)
    static func sendGift(_ name: String) {
        logEvent("SendGift-(\(name))", parameters: nil)
    }
    
    // UserReported-(Report Reason)
    static func userReported(_ reason: String) {
        logEvent("UserReported-(\(reason))", parameters: nil)
    }
}

extension Event {
    func log() { Self.log(self) }
}

extension Signin {
    var event: Event {
        switch self {
        case .apple:
            return .AppleSignin
        case .phonenumber:
            return .PhoneNumberSignin
        case .facebook:
            return .FacebookSignIn
        }
    }
}

extension SubscriptionPlan {
    var event: Event {
        switch self {
        case .threeMonths:
            return .Subscribe_3Pricing_3Months
        case .twelveMonths:
            return .Subscribe_3Pricing_1Year60_Offer
        case .month:
            return .Subscribe_3Pricing_1Month
        case .monthNew:
            return .Subscribe_1Month
        case .monthNewOffer:
            return .Subscribe_1Month_40_offer
        }
    }
}

extension Event {
    static func log(_ event: Event) {
        logEvent(event.rawValue, parameters: nil)
    }
    
    static func logEvent(_ name: String, parameters: [String : Any]?) {
        Analytics.logEvent(name, parameters: parameters)
        Amplitude.instance()?.logEvent(name, withEventProperties: parameters)
    }
}
