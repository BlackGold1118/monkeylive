//
//  UserModel.swift
//  Randoo
//
//  Created by HTS-Product on 04/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import Foundation
import FBSDKLoginKit

final class UserModel {
    static let shared = UserModel()
    static var isFirstSession = true
    
    init() {
        Self.isFirstSession = isFirstLaunch
    }
    
    //MARK: scrollEnabled
    
    @UserDefault(.home_page_scroll)
    var scrollEnabled: Bool
    
    //MARK: move to home page
    func goToHomePage()  {
        if #available(iOS 9.0, *) {
            appDelegate.setInitialViewController()
        }
    }
    
    //MARK: user id
    
    @UserDefault(.local_user_id)
    var userId: String
    
    //MARK: app language
    
    @UserDefault(.language_name, default: DEFAULT_LANGUAGE)
    var language: String
    
    // MARK: userData
    
    @UserDefault(.user_local_details)
    var userData: Data
    
    // MARK: VOIP token
    
    @UserDefault(.user_voip_id)
    var VOIP: String
    
    //MARK: FCM token
    
    @UserDefault(.user_fcm_id)
    var FCM: String
    
    //MARK: set & get partner model
    
    @UserDefault(.user_partner_details)
    var partnerModel: Data
    
    //MARK: TURN DETAILS
    
    @UserDefault(.turn_server_config) private var turnDetails: NSDictionary
    @UserDefault(.web_rtc_web) private var turnURL: String
    
    func setTurn(Details:NSDictionary,url:String){
        turnDetails = Details
        turnURL = url
    }
    
    //MARK: store & get user accesstoken
    
    @UserDefault(.user_accessToken)
    var accessToken: String

    //MARK: touch tool
    
    @UserDefault(.touch_to_start)
    var touchToolTip: Bool

    //MARK: store & get initial login process
    //*********
    
    @UserDefault(.profile_complete_status)
    var profileComplete: Bool
    
    //MARK: firs login
    
    @UserDefault(.firs_login)
    var isFirstLogin: Bool
    
    @UserDefault(.isFirstLaunch, default: true)
    var isFirstLaunch: Bool
    
    //MARK: first login details
    
    @UserDefault(.user_login_type) var userLoginType: String
    @UserDefault(.user_login_id) var userLoginId: String
    func setLogindetails(type:String,id:String){
        userLoginType = type
        userLoginId = id
    }

    // MARK: Login details
    
    @UserDefault(.user_fb_details)
    var fbDetails: NSDictionary
    
    @UserDefault(.signin_apple_id)
    var appleName: String
    
    @UserDefault(.signin_mail_id)
    var email: String
    
    func removeMail() {
        _email.removeObject()
    }
    
    //MARK: basic info
    
    @UserDefault(.basicinfo_enabled)
    var basicInfoEnabled: Bool
    
    //MARK: profile pic
    
    @UserDefault(.profilepic_enabled)
    var profilePicEnabled: Bool

    //MARK: set defaultDetails
    @OptionalUserDefault(.app_defaults_info)
    private var defaultDetails: Data?
    
    func setDefaults(dict: NSDictionary) {
        defaultDetails = try? NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: false)
    }
    
    func getDefaults() -> NSDictionary? {
        if let data = defaultDetails, let dict = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? NSDictionary {
            return dict
        }
        
        return nil
    }
    
    //MARK: premium
    
    #if DEBUG
    @UserDefault(.my_premium_status)
    private var __isPremium: Bool
    private var _isPremium: UserDefault<Bool> { ___isPremium }
    
    var isPremium: Bool {
        set { __isPremium = newValue }
        get { __isPremium } // replace with something for testing purposes
    }
    
    #else
    @UserDefault(.my_premium_status)
    var isPremium: Bool
    
    #endif

    @UserDefault(.already_subscribed)
    var alreadySubscribed: Bool

    //MARK: gem count
    @UserDefault(.my_gem_count)
    var gemCount: Int
    
    //basic info
    @UserDefault(.video_call_enabled)
    var alreadyInCall: Bool
    
    //basic info
    @UserDefault(.previous_caller_id)
    var previousCallerId: String
    
    @OptionalUserDefault(.watch_video_enddate)
    var lastWatchedTime: Date?
    
    //MARK: store,delete & get user invite code
    @UserDefault(.invite_code)
    var inviteCode: String
    
    func deleteInviteCode() {
        _inviteCode.removeObject()
    }
    
    //**********
    //profile api service
    func updateProfile()  {
        let Obj = BaseWebService()
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(UserModel.shared.userId, forKey: "user_id")
        requestDict.setValue(UserModel.shared.userId, forKey: "profile_id")
        Obj.baseService(subURl: PROFILE_API, params: requestDict as? Parameters, onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                UserModel.shared.userData = response.data!
                let premium_member = dict?.value(forKey: "premium_member") as! String
                UserModel.shared.isPremium = premium_member == "true"
                let available_gems = dict?.value(forKey: "available_gems") as! NSNumber
                UserModel.shared.gemCount = available_gems.intValue
            }
        })
    }
    
    //rtc turn details  service
    func getTurnServers()  {
        let Obj = BaseWebService()
        Obj.getDetails(subURl: "\(RTC_PARAMS_API)/\(UserModel.shared.userId)", onSuccess: {response in
            let dict = response.result.value as? NSDictionary
            let status = dict?.value(forKey: "status") as! String
            if status == "true"{
                let turnDict : NSDictionary = dict?.value(forKey: "params") as! NSDictionary
                self.setTurn(Details: turnDict,url:APP_RTC_URL)
            }
        })
    }
    
    //logout user from all
    
    func logoutFromAll() {
        Entities.sharedInstance.deleteAllRecords()
        Utility.shared.pushsignOut()
        let login = LoginManager()
        login.logOut()
        _userId.removeObject()
        _userData.removeObject()
        _partnerModel.removeObject()
        _turnDetails.removeObject()
        _FCM.removeObject()
        _profileComplete.removeObject()
        _userLoginType.removeObject()
        _fbDetails.removeObject()
        _basicInfoEnabled.removeObject()
        _profilePicEnabled.removeObject()
        _isPremium.removeObject()
        _gemCount.removeObject()
        _alreadySubscribed.removeObject()
        _isFirstLogin.removeObject()
        UserDefaultKey.first_prime_package.removeObject()
//        _alreadySubscribed.removeObject() // was commented
    }
    
}
