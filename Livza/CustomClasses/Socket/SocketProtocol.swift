//
//  SocketProtocol.swift
//  Livza
//
//  Created by Serge Vysotsky on 12.06.2020.
//  Copyright © 2020 Hitasoft. All rights reserved.
//

import Foundation

protocol SocketProtocol: class {
    var socket: SRWebSocket { get }
}

extension SocketProtocol {
    func sendRequestOrReconnect(builder buildRequest: @escaping (NSMutableDictionary) -> Void) {
        if Utility.shared.isConnectedToNetwork() {
            if isSocketConnected {
                let request = NSMutableDictionary()
                buildRequest(request)
                socket.send(convert(dict: request))
            } else {
                appDelegate.connectSockets()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    self?.sendRequestOrReconnect(builder: buildRequest)
                }
            }
        }
    }
    
    //convert dict to json string
    func convert(dict: NSMutableDictionary) -> String {
        let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: [])
        let jsonString = String(data: jsonData, encoding: .utf8)!
        
        print("SEND SOCKET \(jsonString)")
        return jsonString
    }
}
