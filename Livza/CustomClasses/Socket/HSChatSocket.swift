//
//  HSChatSocket.swift
//  Randoo
//
//  Created by HTS-Product on 31/05/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import Foundation
import UIKit

protocol socketDelegate {
    func gotSocketInfo(type:String,dict:NSDictionary?)
}

class HSChatSocket: SocketProtocol {
    static var chatSocket = SRWebSocket()
    static let sharedInstance = HSChatSocket()
    var delegate : socketDelegate?
    
    var socket: SRWebSocket { HSChatSocket.chatSocket }
    
    //open socket
    func connect()  {
        let socketURL = URL.init(string: "\(WEB_SOCKET_CHAT_URL)\(UserModel.shared.userId)")
        HSChatSocket.chatSocket = SRWebSocket.init(url: socketURL)
        HSChatSocket.chatSocket.open()
      
    }
    
    //update user live status
    func makeAlive()  {
        if UIApplication.shared.applicationState == .active{
            print("make live")
            sendRequestOrReconnect { req in
                req.setValue("_updateLive", forKey: "type")
                req.setValue(Utility.shared.getUTCTime(), forKey: "timestamp")
                req.setValue(UserModel.shared.userId, forKey: "user_id")
            }
        }
    }
    //send typing status
    func typing(status:String,receiver_id:String)  {
        sendRequestOrReconnect { req in
            req.setValue("_userTyping", forKey: "type")
            req.setValue(receiver_id, forKey: "receiver_id")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue(status, forKey: "typing_status")
        }
    }
    //check partner live status
    func checkOnline(receiver_id:String)  {
        sendRequestOrReconnect { req in
            req.setValue("_profileLive", forKey: "type")
            req.setValue(receiver_id, forKey: "receiver_id")
        }
    }
    //check partner live status
    func onlineList(ids:NSMutableArray)  {
        sendRequestOrReconnect { req in
            req.setValue("_onlineList", forKey: "type")
            req.setValue(ids, forKey: "users_list")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
        }
    }
    //block
    func block(receiver_id:String,status:Bool)  {
        sendRequestOrReconnect { req in
            req.setValue("_blockUser", forKey: "type")
            req.setValue(receiver_id, forKey: "receiver_id")
            req.setValue(status, forKey: "blocked")
        }
    }
    //update read status
    func updateRead(receiver_id:String)  {
        sendRequestOrReconnect { req in
            req.setValue("_updateRead", forKey: "type")
            req.setValue(receiver_id, forKey: "receiver_id")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue("\(UserModel.shared.userId)\(receiver_id)", forKey: "chat_id")
        }
    }
    //send sticker
    func notifyUser(receiver_id:String,type:String)  {
        sendRequestOrReconnect { req in
            req.setValue("_userNotify", forKey: "type")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue(receiver_id, forKey: "receiver_id")
            req.setValue(type, forKey: "msg_type")
        }
    }
    //send msg
    func sendMsg(receiverID:String,msgType:String,msg:String,name:String,img:String,attachment:Data?)  {
        sendRequestOrReconnect { req in
            let jsonDecoder = JSONDecoder()
            guard let profile = try? jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData) else { return }
            let time = Utility.shared.getUTCTime()
            req.setValue("_sendChat", forKey: "type")
            req.setValue(time, forKey: "chat_time")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue(profile.user_image, forKey: "user_image")
            req.setValue(profile.name, forKey: "user_name")
            req.setValue(receiverID, forKey: "receiver_id")
            req.setValue("user_chat", forKey: "chat_type")
            req.setValue("\(UserModel.shared.userId)\(receiverID)", forKey: "chat_id")
            req.setValue(msgType, forKey: "msg_type")
            let msgID = Utility.shared.randomID()
            req.setValue(msgID, forKey: "msg_id")
            let cryptLib = CryptLib()
            let encryptedText = cryptLib.encryptPlainTextRandomIV(withPlainText:msg, key: ENCRYPT_KEY)
            req.setValue(encryptedText, forKey: "message")
            
            let localCopy = NSMutableDictionary(dictionary: req)
            localCopy.removeObject(forKey: "user_image")
            localCopy.removeObject(forKey: "user_name")
            localCopy.setValue(img, forKey: "user_image")
            localCopy.setValue(name, forKey: "user_name")
            if msgType != "missed" {
                Utility.shared.addToLocal(dict: localCopy)
            }
            
            if msgType == "image", let attachment = attachment {
                Entities.sharedInstance.updateDownload(
                    msg_id: msgID,
                    imgData: attachment
                )
            }
        }
    }
    //initate call
    func manageCall(receiverID:String,room:String,type:String)  {
        sendRequestOrReconnect { req in
            let jsonDecoder = JSONDecoder()
            guard let profile = try? jsonDecoder.decode(ProfileModel.self, from: UserModel.shared.userData) else { return }
            req.setValue("_createCall", forKey: "type")
            req.setValue(Utility.shared.getUTCTime(), forKey: "timestamp")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue(profile.user_image, forKey: "user_image")
            req.setValue(profile.name, forKey: "user_name")
            req.setValue(receiverID, forKey: "receiver_id")
            req.setValue("video", forKey: "chat_type")
            req.setValue(type, forKey: "call_type")
            req.setValue(Utility.shared.getUTCTime(), forKey: "created_at")
            req.setValue("ios", forKey: "platform")
            req.setValue(room, forKey: "room_id")
        }
    }
    
    func passDetails(type:String,dict:NSDictionary?) {
        self.delegate?.gotSocketInfo(type: type, dict: dict)
    }
}
