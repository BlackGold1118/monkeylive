//
//  HSWebSocket.swift
//  Randoo
//
//  Created by HTS-Product on 25/02/19.
//  Copyright © 2019 Hitasoft. All rights reserved.
//

import UIKit

class HSWebSocket: SocketProtocol {
    static var webSocket = SRWebSocket()
    static let sharedInstance = HSWebSocket()
    
    var socket: SRWebSocket { HSWebSocket.webSocket }
    
    //open socket
    func connect()  {
        let socketURL = URL.init(string: "\(WEB_SOCKET_URL)\(UserModel.shared.userId)")
        HSWebSocket.webSocket = SRWebSocket.init(url: socketURL)
        HSWebSocket.webSocket.open()
        
    }
    
    //update user live status
    func update(status:String)  {
        sendRequestOrReconnect { req in
            req.setValue("_liveStatus", forKey: "type")
            req.setValue(status, forKey: "status")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
        }
    }
    
    //find user
    func findPartner()  {
        sendRequestOrReconnect { req in
            let locationArray = NSMutableArray()
            let partnerArray = NSMutableArray()
            var filterEnable = String()
            filterEnable = UserDefaults.standard.value(forKeyPath: "filter_applied") as? String ?? ""
            if filterEnable == "1" {
                req.setValue("1", forKey: "filter_applied")
                req.setValue(UserDefaults.standard.value(forKeyPath: "gender"), forKey: "gender")
                req.setValue(UserDefaults.standard.value(forKeyPath: "min_age"), forKey: "min_age")
                req.setValue(UserDefaults.standard.value(forKeyPath: "max_age"), forKey: "max_age")
                let allLoc = Utility.language.value(forKey: "select_all") as! String
                locationArray.addObjects(from: UserDefaults.standard.value(forKeyPath: "location") as! [Any])
                
                if locationArray.contains(allLoc){
                    locationArray.remove(Utility.language.value(forKey: "select_all") as Any)
                }
                locationArray.add("global")
                req.setValue(locationArray, forKey: "location")
            }else{
                req.setValue("0", forKey: "filter_applied")
            }
            
            if let partnerList = UserDefaults.standard.value(forKey: "partner_list") as? [Any] {
                partnerArray.addObjects(from: partnerList)
            }
            
            req.setValue(partnerArray, forKey: "partner_list")
            req.setValue("_findPartner", forKey: "type")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue("ios", forKey: "platform")
        }
    }
    //join room
    func joinRoom(partner:String)  {
        sendRequestOrReconnect { req in
            req.setValue("_joinRoom", forKey: "type")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue(partner, forKey: "partner_id")
            req.setValue("ios", forKey: "platform")
        }
    }
    //filter updated
    func filterUpdated(status:String)  {
        sendRequestOrReconnect { req in
            req.setValue("_filterUpdated", forKey: "type")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue(status, forKey: "filter_search_result")
        }
    }
    //busy request
    func busyRequest(partner:String)  {
        sendRequestOrReconnect { req in
            req.setValue("_committed", forKey: "type")
            req.setValue(partner, forKey: "partner_id")
        }
    }
    func breakupPartner(partner:String){
        sendRequestOrReconnect { req in
            req.setValue("_breakupPartner", forKey: "type")
            req.setValue(partner, forKey: "partner_id")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
        }
    }
    //for recent history
    func setRecent(partner:String){
        sendRequestOrReconnect { req in
            req.setValue("_onChat", forKey: "type")
            req.setValue(partner, forKey: "partner_id")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
        }
    }
    
    //send sticker
    func sendSticker(partner:String,s_name:String)  {
        sendRequestOrReconnect { req in
            req.setValue("_sendSticker", forKey: "type")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue(partner, forKey: "partner_id")
            req.setValue(s_name, forKey: "sticker_id")
        }
    }
    //send sticker
    func notifyUser(partner_id:String,type:String)  {
        sendRequestOrReconnect { req in
            req.setValue("_userNotify", forKey: "type")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue(partner_id, forKey: "partner_id")
            req.setValue(type, forKey: "msg_type")
        }
    }
    
    //send gift
    func sendGift(partner:String,giftDict:NSDictionary)  {
        sendRequestOrReconnect { req in
            Event.sendGift(giftDict.value(forKey: "gft_title") as? String ?? "")
            
            req.setValue("_sendGift", forKey: "type")
            req.setValue(UserModel.shared.userId, forKey: "user_id")
            req.setValue(partner, forKey: "partner_id")
            req.setValue(giftDict.value(forKey: "_id"), forKey: "gift_id")
            req.setValue(giftDict.value(forKey: "gft_title"), forKey: "gift_title")
            req.setValue(giftDict.value(forKey: "gft_icon"), forKey: "gift_icon")
            if UserModel.shared.isPremium{
                req.setValue(giftDict.value(forKey: "gft_gems_prime"), forKey: "gems_count")
            }else {
                req.setValue(giftDict.value(forKey: "gft_gems"), forKey: "gems_count")
            }
            req.setValue(giftDict.value(forKey: "gft_gems"), forKey: "gems_earnings")
        }
    }
}
